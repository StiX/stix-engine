
#include "PageAllocator.h"
#include "Debug.h"
#include "VirtualPageAllocator.h"


//-------------------------------------------------------------------------------------------
PageAllocator::PageAllocator(u32 reserveSize)
	: m_pAllocation(VirtualPageAllocator::Reserve(reserveSize))
	, m_reservedSize(VirtualPageAllocator::AlignToPageBoundary(reserveSize))
	, m_committedSize(0)
{
}

//-------------------------------------------------------------------------------------------
PageAllocator::~PageAllocator()
{
	ASSERT(m_committedSize == 0);
}

//-------------------------------------------------------------------------------------------
void* PageAllocator::Alloc(size_t size)
{
	ASSERT(m_committedSize == 0);
	ASSERT(size < m_reservedSize);

	m_committedSize = VirtualPageAllocator::AlignToPageBoundary(size);
	return VirtualPageAllocator::Commit(m_pAllocation, m_committedSize);
}

//-------------------------------------------------------------------------------------------
bool PageAllocator::Free(void* ptr)
{
	if (!ptr)
	{
		return false;
	}

	ASSERT(ptr == m_pAllocation);

	VirtualPageAllocator::Decommit(ptr, m_committedSize);
	m_committedSize = 0;
	return true;
}

//-------------------------------------------------------------------------------------------
void* PageAllocator::Realloc(void* ptr, size_t size)
{
	if (size <= m_reservedSize)
	{
		ASSERT(ptr == m_pAllocation);

		u8* commitAddress = (u8*)ptr + m_committedSize;
		VirtualPageAllocator::Commit(commitAddress, size - m_committedSize);
		m_committedSize = size;

		return ptr;
	}

	ASSERT(!"Not enought space reserved");
	return nullptr;
}