
/************************************************************************/
/* This allocator was designed for a single virtual pages allocation    */
/* It keep track of an allocation and it's reserver and committed size, */
/* so it can grow or shrink requested allocation.                       */
/* It's ideal to use it for allocation of dynamic arrays.               */
/************************************************************************/

#pragma once

#include "Types.h"
#include "ClassHelpers.h"


class PageAllocator
{
public:
			PageAllocator(u32 reserveSize);
			~PageAllocator();

	void*	Alloc(size_t size);
	bool	Free(void* ptr);
	void*	Realloc(void* ptr, size_t size);

protected:
private:
	void*	m_pAllocation;
	size_t	m_reservedSize;
	size_t	m_committedSize;

	MAKE_UNCOPYABLE(PageAllocator);
};