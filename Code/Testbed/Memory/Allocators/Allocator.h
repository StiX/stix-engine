
#pragma once

#include <cstdlib>


struct Allocator
{
	template <class T, class ... Types>
	static T*		New(Types&& ... args);

	template <class T, class ... Types>
	static T*		NewArray(u32 elementsCount, Types&& ... args);

	template <class T>
	static void		Delete(T* ptr);

	template <class T>
	static void		DeleteArray(u32 elementsCount, T* ptr);

private:
	static const u32 kControlBlockSize = 16;
};





//-------------------------------------------------------------------------------------------
template <class T, class ... Types>
__forceinline T* Allocator::New(Types&& ... args)
{
	void* pBuffer = malloc(sizeof(T));
	return new (pBuffer)T(std::forward<Types>(args)...);
}

//-------------------------------------------------------------------------------------------
template <class T, class ... Types>
__forceinline T* Allocator::NewArray(u32 elementsCount, Types&& ... args)
{
	u8* pBuffer = (u8*)malloc(sizeof(T) * elementsCount + kControlBlockSize);
	*((u32*)pBuffer) = elementsCount;
	return new (pBuffer + kControlBlockSize) T[elementsCount](std::forward<Types>(args)...);
}

//-------------------------------------------------------------------------------------------
template <class T>
__forceinline void Allocator::Delete(T* ptr)
{
	ptr->~T();
	free(ptr);
}

//-------------------------------------------------------------------------------------------
template <class T>
__forceinline void Allocator::DeleteArray(u32 elementsCount, T* ptr)
{
	u8* originalPtr = (u8*)ptr - kControlBlockSize;
	const u32 kElementsCount = *((u32*)originalPtr);
	for (u32 i = 0; i < kElementsCount; ++i)
	{
		ptr->~T();
		++ptr;
	}

	free(originalPtr);
}
