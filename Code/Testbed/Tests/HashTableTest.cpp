
#include "HashTableTest.h"
#include "HashTable.h"
#include "Debug.h"
#include <unordered_map>
#include "Timer.h"


//-------------------------------------------------------------------------------------------
void test_general() {
	HashTable<u32, const char*> map(64);

	map.Insert(1, "foo");
	map.Insert(2, "bar");

	auto it = map.GetFirstElement();
	u32 count = 0;
	while (it != map.GetEnd()) {
		++count;
		++it;

		ASSERT(count <= map.GetSize());
	}

	ASSERT(count == map.GetSize());
}

//-------------------------------------------------------------------------------------------
void TestReallocation()
{
	const u32 kElemsCount = 4096 * 16 * 16;
	HashTable<u32, u32> map(16);

	u32 counter = 1;
	for (; counter < kElemsCount; ++counter)
	{
		map.Insert(counter, 1000 + counter);
	}

	u32 value = 223;
	map.Insert(value, 1223);

	for (u32 i = 1; i < counter; ++i)
	{
		ASSERT(*map.Find(i) == (u32)(1000 + i));
	}
}

//-------------------------------------------------------------------------------------------
void PerfTestInsertion()
{
	const u32 kElemsCount = 4096 * 16 * 1;

	//***	HashTable Insertion test	***
	double start = timer::micro_seconds();
	HashTable<u32, u32> map(32);

	for (u32 i = 1; i < kElemsCount; ++i)
	{
		map.Insert(i, 1000 + i);
	}
	double delta = timer::micro_seconds() - start;

	printf("Insertion:\n");
	printf("  map: %f\n", delta);


	//***	STL map Insertion test	***
	start = timer::micro_seconds();

	typedef std::unordered_map<u32, u32> stlMap;
	std::unordered_map<u32, u32> stdMap;

	for (u32 i = 1; i < kElemsCount; ++i)
	{
		std::pair<u32, u32> pair(i, 1000 + i);
		stdMap.insert(pair);
	}
	delta = timer::micro_seconds() - start;

	printf("  std map: %f\n", delta);


	//***	HashTable search test	***
	printf("Search:\n");
	start = timer::micro_seconds();

	for (u32 i = 1; i < kElemsCount; ++i)
	{
		if (*map.Find(i) != 1000 + i)
		{
			printf("  value: %d was not found", i);
		}
	}
	delta = timer::micro_seconds() - start;
	printf("  map: %f\n", delta);


	//***	STL map search test	***
	start = timer::micro_seconds();

	for (u32 i = 1; i < kElemsCount; ++i)
	{
		if (stdMap.find(i)->second != 1000 + i)
		{
			printf("  value: %d was not found", i);
		}
	}
	delta = timer::micro_seconds() - start;
	printf("  map: %f\n", delta);
}

void TestHashTable()
{
	test_general();
#ifdef DEBUG
	TestReallocation();
#else
	PerfTestInsertion();
#endif
}