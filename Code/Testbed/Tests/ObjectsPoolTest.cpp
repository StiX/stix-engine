
#include "ObjectsPoolTest.h"
#include "ObjectsPool.h"
#include "HashTable.h"
#include "Timer.h"
#include "SpinLock.h"


struct foo
{
	u32 a[10];

	foo(int _a)
	{
		a[0] = _a;
	}
};


void Test()
{
	const u32 kElemsCount = 4096 * 16 * 1;

	HashTable<u32, foo*> map(32);
	ObjectsPool<foo> pool(32, 2);

	for (u32 i = 1; i < kElemsCount; ++i)
	{
		foo* ptr = pool.CreateObject(i);
		map.Insert(i, ptr);
	}

	for (u32 i = 1; i < kElemsCount; ++i)
	{
		foo* pFoo = map.Find(i);
		ASSERT(pFoo->a[0] == i);
		pool.DeleteObject(pFoo);
	}
}

void TestPerf()
{
	const u32 kElemsCount = 4096 * 16 * 1;
	const u32 kTries = 1000;

	double start = timer::micro_seconds();
	for (u32 i = 0; i < kTries; ++i)
	{
		HashTable<u32, foo*> map(32);
		ObjectsPool<foo> pool(32, 2);
		//SpinLock lock;

		for (u32 j = 1; j < kElemsCount; ++j)
		{
			//lock.Lock();
			foo* ptr = pool.CreateObject(j);
			map.Insert(j, ptr);
			//lock.Unlock();
		}
	}
	double delta = timer::micro_seconds() - start;

	printf("Pool & map time: %f\n", delta / kTries);
}

void TestObjectsPool()
{
#ifdef DEBUG
	Test();
#else
	TestPerf();
#endif
}