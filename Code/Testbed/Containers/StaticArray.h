
#pragma once

#include <new>
#include <utility>
#include "Types.h"
#include "Debug.h"
#include "ClassHelpers.h"


template <class T, class ALLOC>
class StaticArray
{
public:
	
				StaticArray();
				StaticArray(u32 capacity);
				~StaticArray();

	void		Allocate(u32 capacity);

	template<class ... Args>
	void		Emplace(u32 index, Args... args);

	T&			operator[](u32 index);
	const T&	operator[](u32 index) const;

protected:
private:
	T*			m_array;
	u32			m_capacity;

	MAKE_UNCOPYABLE(StaticArray);
};





//-------------------------------------------------------------------------------------------
template <class T, class ALLOC>
inline StaticArray<T, ALLOC>::StaticArray()
	: m_array(nullptr), m_capacity(0)
{
}

//-------------------------------------------------------------------------------------------
template <class T, class ALLOC>
inline StaticArray<T, ALLOC>::StaticArray(u32 capacity)
	: m_array(nullptr)
{
	Allocate(capacity);
}

//-------------------------------------------------------------------------------------------
template <class T, class ALLOC>
inline StaticArray<T, ALLOC>::~StaticArray()
{
	ALLOC::Free(m_array, m_capacity * sizeof(T));
}

//-------------------------------------------------------------------------------------------
template <class T, class ALLOC>
inline void StaticArray<T, ALLOC>::Allocate(u32 capacity)
{
	ASSERT(!m_array);

	m_array = (T*)ALLOC::Alloc(capacity * sizeof(T));
	m_capacity = capacity;
}

//-------------------------------------------------------------------------------------------
template <class T, class ALLOC>
template<class ... Args>
inline void StaticArray<T, ALLOC>::Emplace(u32 index, Args... args)
{
	ASSERT(index < m_capacity);

	new (&m_array[index]) T(std::forward<Args>(args)...);
}

//-------------------------------------------------------------------------------------------
template <class T, class ALLOC>
inline T& StaticArray<T, ALLOC>::operator[](u32 index)
{
	ASSERT(index < m_capacity);

	return m_array[index];
}

//-------------------------------------------------------------------------------------------
template <class T, class ALLOC>
inline const T& StaticArray<T, ALLOC>::operator[](u32 index) const
{
	ASSERT(index < m_capacity);

	return m_array[index];
}