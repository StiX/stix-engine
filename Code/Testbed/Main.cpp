
#ifdef __WIN__
#	include <vld.h>
#endif
#include "HashTableTest.h"
#include "ObjectsPoolTest.h"


int main()
{
	TestObjectsPool();
	TestHashTable();

	return 0;
}