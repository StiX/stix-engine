
#include "VertexDescription.h"
#include "Debug.h"


static_assert(VertexAttribute::MAX <= 16, "Max allowed number of vertex attributes in 16");

//-------------------------------------------------------------------------------------------
static u16 GetAttributeSize(VertexAttribute attribute)
{
	u16 size;

	switch (attribute.format)
	{
	case VertexAttribute::Byte:
	case VertexAttribute::UnsignedByte:
		size = 1;
		break;

	case VertexAttribute::Short:
	case VertexAttribute::UnsignedShort:
	case VertexAttribute::HalfFloat:
		size = 2;
		break;

	default:
		ASSERT(!"Value out of range");
	case VertexAttribute::Float:
	case VertexAttribute::Int:
	case VertexAttribute::UnsignedInt:
	case VertexAttribute::Int2_10_10_10_reversed:
	case VertexAttribute::Unsigned_2_10_10_10_reversed:
		size = 4;
		break;
	}

	return (u16)(size * attribute.componentsCount);
}

//-------------------------------------------------------------------------------------------
const char* VertexAttribute::GetName(Type vertexAttribute)
{
	static const char* names[] = { "position", "normal", "uv", "color", "jointsIndices", "jointsWeights" };

	static_assert(sizeof(names) / sizeof(names[0]) == Type::MAX, "Some of the names are not initialized");

	return names[vertexAttribute];
}

//-------------------------------------------------------------------------------------------
VertexDescription::VertexDescription()
	: attributesCount(0), vertexSize(0)
{
}

//-------------------------------------------------------------------------------------------
void VertexDescription::AddAttribute(VertexAttribute::Type type, VertexAttribute::Format format, u32 components_count, bool normalize) {
	VertexAttribute* attr = &attributes[attributesCount];
	attr->type = type;
	attr->format = format;
	attr->offset = vertexSize;
	attr->componentsCount = components_count;
	attr->normalize = normalize;

	++attributesCount;
	vertexSize += GetAttributeSize(*attr);
}

//-------------------------------------------------------------------------------------------
u32 VertexDescription::serialization_size() const {
	return sizeof(attributesCount) + sizeof(vertexSize) + sizeof(VertexAttribute) * attributesCount;
}