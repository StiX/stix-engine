
#pragma once

#include "math_types.h"
#include "StringHash.h"
#include "ClassHelpers.h"
#include "RendererTypes.h"

class StringHash;


namespace shader {

enum class constant_type {
	float3,
	float4,
	float4x4,
	float3x4,
	sampler2d
};

struct constants_buffer {
	u16	count;
	u16	data_size;
	u8	data[0];
};

struct constant {
	StringHash		name;
	constant_type	type;
	u8				data[0];
};

constants_buffer*	allocate_constants(u16 count);
void				free_constants(constants_buffer* ptr);



class constants_writer {
public:
	explicit		constants_writer(constants_buffer& con);

	void			append(const StringHash& name, const vec3f& vec);
	void			append(const StringHash& name, const vec4f& vec);
	void			append(const StringHash& name, const mat4& mat);
	void			append(const StringHash& name, const mat3x4& mat);
	void			append(const StringHash& name, TextureHandle sampler_2d);

private:
	constants_buffer* _constants;
	u32				_data_size;
	u32				_count;

	void			append(const StringHash& name, constant_type type, const void* ptr, u32 size);

	MAKE_UNCOPYABLE(constants_writer);
};



class constants_reader {
public:
	explicit			constants_reader(const constants_buffer& con);

	const constant*		read();

private:
	const constants_buffer*	_constants;
	u32					_current_offset;

	MAKE_UNCOPYABLE(	constants_reader);
};

}	// namespace shader