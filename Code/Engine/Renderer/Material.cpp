
#include "Material.h"


static_assert((u32)BlendFunc::BlendMax == 2, "BlendFunc should have only 2 fields");
static_assert((u32)DepthTestFunc::DepthMax == 8, "DepthTestFunc should have only 8 fields");
static_assert(sizeof(RendererState) == sizeof(u32), "Size of RendererState should be equal to size of u32");