
#pragma once

#include "Types.h"


enum class eShaderType : u8 {
	Vertex,
	Pixel,
};

enum class HandleType
{
	VertexBuffer,
	VertexBinding,
	Shader,
	ShaderProgram,
	Texture,
};


template<HandleType type>
struct RendererHandle
{
	u32			m_handle;

				RendererHandle() : m_handle(0) {}
				RendererHandle(u32 handle) : m_handle(handle) {}
				
	bool		operator!=(RendererHandle other) { return m_handle != other.m_handle; }
	bool		operator==(RendererHandle other) { return m_handle == other.m_handle; }
				operator bool() { return m_handle != 0; }
	
	HandleType	GetType() const { return type; }
	u32			GetIntType() const { return (u32)type; }
};


typedef RendererHandle<HandleType::Shader>			ShaderHandle;
typedef RendererHandle<HandleType::Texture>			TextureHandle;
typedef RendererHandle<HandleType::VertexBuffer>	VertexBufferHandle;
typedef RendererHandle<HandleType::VertexBinding>	VertexBindingHandle;
typedef RendererHandle<HandleType::ShaderProgram>	ShaderProgramHandle;