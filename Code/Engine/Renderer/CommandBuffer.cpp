
#include "CommandBuffer.h"
#include <algorithm>
#include "Debug.h"
#include "Renderer.h"


namespace {
//-------------------------------------------------------------------------------------------
inline bool sort_function(Pair<u64, void*>& a, Pair<u64, void*>& b) {
	return a.key < b.key;
}

}	//namespace


//-------------------------------------------------------------------------------------------
Command_buffer::Command_buffer()
	: _allocator(32 * 4096, 4096), _can_write(false)
{}

//-------------------------------------------------------------------------------------------
void* Command_buffer::allocate(size_t size, size_t alignment) {
	ASSERT(_can_write);

	return _allocator.Allocate(size, alignment);
}

//-------------------------------------------------------------------------------------------
void Command_buffer::add_command(u64 key, void* data) {
	_commands.EmplaceBack(key, data);
}

//-------------------------------------------------------------------------------------------
void Command_buffer::execute() {
	ASSERT(!_can_write);

	std::sort(_commands.Begin(), _commands.End(), sort_function);

	const u32 count = _commands.GetSize();
	for (u32 i = 0; i < count; ++i) {
		renderer::dispatch_function* data = (renderer::dispatch_function*)_commands[i].value;
		renderer::dispatch_function* function = data - 1;
		(*function)(data);
	}

	_allocator.Reset();
	_commands.Reset();
}

//-------------------------------------------------------------------------------------------
void Command_buffer::can_write(bool can_write) {
	_can_write = can_write;
}