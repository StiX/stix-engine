
#pragma once

#include "Types.h"
#include "mat4.h"
#include "ClassHelpers.h"
#include "VertexBuffer.h"
#include "RendererTypes.h"
#include "RendererState.h"
#include "ShaderParamInfo.h"
#include "shader_constants.h"

struct Material;
struct texture_params;
struct ImageDescription;
struct VertexDescription;


struct DrawContext
{
	VertexBindingHandle	vertexBinding;
	VertexBufferHandle	vertexBuffer;
	VertexBufferHandle	indexBuffer;
	TextureHandle		diffuse;
	TextureHandle		normal;
	TextureHandle		metallic;
	TextureHandle		roughness;
	RendererState		rendererState;
	ShaderProgramHandle	shader;
	bool				bDoubleSided;

	union
	{
		u32				indicesCount;	//TODO: replace by draw_count
		u32				verticesCount;
	};
	shader::constants_buffer constants;
};

class RendererGL3;
typedef RendererGL3 Renderer;


class RendererGL3
{
public:
						RendererGL3();
						~RendererGL3();

	void				Init(const vec2i& screen_size);
	void				Shutdown();

	void				Draw(const DrawContext& context);

	VertexBufferHandle	CreateVertexBuffer(VertexBufferType type, const StringHash* name) const;
	void				DeleteVertexBuffer(VertexBufferHandle buffer) const;
	void				UploadVertexBufferData(VertexBufferHandle buffer, VertexBufferType bufferType, const void* pData, size_t size, VertexBufferUsage usageHint);

	VertexBindingHandle	CreateVertexBinding(const VertexDescription& vertexDescription, VertexBufferHandle vertexDataBuffer, VertexBufferHandle indexBuffer, const StringHash* name);
	void				DeleteVertexBinding(VertexBindingHandle buffer) const;

	ShaderHandle		CreateShader(eShaderType type, const char* src, const StringHash* name) const;
	void				DeleteShader(ShaderHandle shader) const;

	ShaderProgramHandle	CreateShaderProgram(const StringHash& name, ShaderHandle vertexShader, ShaderHandle pixelShader);
	void				DeleteShaderProgram(ShaderProgramHandle shader);

	u32					GetUniformsCount(ShaderProgramHandle shader) const;
	ShaderParamInfo		GetUniformDescription(ShaderProgramHandle shader, u32 uniformIndex) const;

	u32					GetAttributesCount(ShaderProgramHandle shader) const;
	ShaderParamInfo		GetAttributeDescription(ShaderProgramHandle shader, u32 attributeIndex) const;

	TextureHandle		CreateTexture(const ImageDescription& imageDescription, const StringHash* name, const texture_params& params);
	void				DeleteTexture(TextureHandle texture) const;

	void				Begin();
	void				End();

protected:
private:
	MAKE_UNCOPYABLE(	RendererGL3);
};