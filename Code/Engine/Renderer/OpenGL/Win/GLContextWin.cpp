
#include "GLContextWin.h"
#include <windows.h>
#include "Types.h"
#include "OpenGL.h"


//-------------------------------------------------------------------------------------------
GLContext::GLContext()
	: m_pContext(nullptr)
{
}

//-------------------------------------------------------------------------------------------
GLContext::~GLContext() {
	if (m_pContext) {
		Destroy();
	}
}

//-------------------------------------------------------------------------------------------
bool GLContext::InitOpenGL(HWND hwnd) {
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;// | PFD_DEPTH_DONTCARE;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 24;
	pfd.cStencilBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;

	HDC hdc = GetDC(hwnd);
	if (!hdc) {				// Did We Get A Device Context?
		MessageBox(NULL, "Can't Create A GL Device Context!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	int pixelFormat = ChoosePixelFormat(hdc, &pfd);
	if (!pixelFormat) {
		MessageBox(NULL, "Can't Find A Suitable PixelFormat!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	if (!SetPixelFormat(hdc, pixelFormat, &pfd)) {
		MessageBox(NULL, "Can't Set The PixelFormat!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	m_pContext = wglCreateContext(hdc);
	if (!m_pContext) {
		MessageBox(NULL, "Can't Create A GL Rendering Context!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	if (!wglMakeCurrent(hdc, m_pContext)) {
		MessageBox(NULL, "Can't Activate The GL Rendering Context!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	ogl_LoadFunctions();
	wgl_LoadFunctions(hdc);

	return true;
}

//-------------------------------------------------------------------------------------------
bool GLContext::InitOpenGL33(HWND hwnd, int samplesCount) {
	HDC hdc = GetDC(hwnd);
	if (!hdc) {          // Did We Get A Device Context?
		MessageBox(NULL, "Can't Create A GL Device Context!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	int attribList[] = {
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_COLOR_BITS_ARB, 32,
		WGL_DEPTH_BITS_ARB, 24,
		WGL_STENCIL_BITS_ARB, 8,
		WGL_AUX_BUFFERS_ARB, 0,
		0, 0,	//WGL_SAMPLE_BUFFERS_ARB, 1,
		0, 0,	//WGL_SAMPLES_ARB, 8,
		0,		//End
	};

	u32 i = 0;
	while (attribList[i] != 0) {
		i += 2;
	}

	if (samplesCount) {
		attribList[i] = WGL_SAMPLE_BUFFERS_ARB;
		attribList[++i] = 1;
		attribList[++i] = WGL_SAMPLES_ARB;
		attribList[++i] = samplesCount;
	}

	const u32 PIXEL_FORMATS_COUNT = 1;
	u32 numFormats;
	s32 pixelFormats[PIXEL_FORMATS_COUNT];

	if (!wglChoosePixelFormatARB(hdc, attribList, NULL, PIXEL_FORMATS_COUNT, pixelFormats, &numFormats)) {
		MessageBox(NULL, "Can't Find A Suitable PixelFormat!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	int pixelFormat = pixelFormats[0];

	PIXELFORMATDESCRIPTOR pfd;
	if (!SetPixelFormat(hdc, pixelFormat, &pfd)) {
		MessageBox(NULL, "Can't Set The PixelFormat!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	int attribs[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 3,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
#ifdef RENDERER_DEBUG
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
#endif
		0
	};

	m_pContext = wglCreateContextAttribsARB(hdc, 0, attribs);
	if (!m_pContext) {
		MessageBox(NULL, "Can't Create A GL Rendering Context!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	if (!wglMakeCurrent(hdc, m_pContext)) {
		MessageBox(NULL, "Can't Activate The GL Rendering Context!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	wglSwapIntervalEXT(1);	// enable vsync

	return true;
}

//-------------------------------------------------------------------------------------------
void GLContext::Destroy() {
	if (m_pContext) {
		if (!wglMakeCurrent(nullptr, nullptr)) {
			MessageBox(nullptr, "Release Of DC And RC Failed!", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(m_pContext)) {
			MessageBox(nullptr, "Release Rendering Context Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}
	}

	m_pContext = nullptr;
}