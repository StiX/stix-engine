
#pragma once

struct HGLRC__;
typedef HGLRC__* HGLRC;

struct HWND__;
typedef HWND__* HWND;


class GLContext {
public:
			GLContext();
			~GLContext();

	bool	InitOpenGL(HWND hwnd);
	bool	InitOpenGL33(HWND hwnd, int samplesCount);

	void	Destroy();

protected:
private:
	HGLRC	m_pContext;
};