
#pragma once


#ifdef __WIN__
#	include "gl_core_3_3.h"
#	include "wgl.h"
#elif defined __OSX__
#	define GLFW_INCLUDE_GLCOREARB
#	define GLFW_INCLUDE_GLEXT
#	include "glfw3.h"
#elif defined __iOS__
#   include <OpenGLES/ES3/gl.h>
#   include <OpenGLES/ES3/glext.h>
#elif defined __ANDROID__
#	include <GLES3/gl3.h>
#	include <GLES3/gl3ext.h>
#else
#	error "platform is not supported"
#endif
