
#include "RendererGL3.h"
#include "Log.h"
#include "mat4.h"
#include "Array.h"
#include "Image.h"
#include "OpenGL.h"
#include "Material.h"
#include "ShaderProgram.h"
#include "GPULeaksDetector.h"
#include "RendererDispatch.h"
#include "VertexDescription.h"


namespace {

GPULeaksDetector		_gpu_leaks_detector;
RendererState			_renderer_state;
ShaderProgramHandle		_current_shader = 0;
VertexBufferHandle		_buffer_bindings[(u32)VertexBufferType::Count];
VertexBindingHandle		_current_vertex_binding;
Array<ShaderProgram>	_shader_programs;	//TODO: replace by free list

int						_texture_units_count = 0;
const int				_max_texture_units = 32;
TextureHandle			_texture_bindings[_max_texture_units];

void					SetBlendState(RendererState rendererState);
void					SetDepthState(RendererState rendererState);

void					UseShader(ShaderProgramHandle shader);
void					bind_vertex_buffer(VertexBufferHandle buffer, VertexBufferType type);

ShaderProgram*			GetShader(ShaderProgramHandle handle);
void					bind_texture(TextureHandle texture, u16 type, u16 unit);



#ifdef RENDERER_DEBUG

//-------------------------------------------------------------------------------------------
u32 gl_debug_level_to_int(GLenum level) {
	switch (level) {
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		return 0;
	case GL_DEBUG_SEVERITY_LOW:
		return 1;
	case GL_DEBUG_SEVERITY_MEDIUM:
		return 2;
	case GL_DEBUG_SEVERITY_HIGH:
		return 3;
	default:
		return (u32)-1;
	}
}

//	GL_DEBUG_SEVERITY_HIGH			All OpenGL Errors, shader compilation / linking errors, or highly - dangerous undefined behavior
//	GL_DEBUG_SEVERITY_MEDIUM		Major performance warnings, shader compilation / linking warnings, or the use of deprecated functionality
//	GL_DEBUG_SEVERITY_LOW			Redundant state change performance warning, or unimportant undefined behavior
//	GL_DEBUG_SEVERITY_NOTIFICATION
u32 _debug_level = gl_debug_level_to_int(GL_DEBUG_SEVERITY_LOW);

//-------------------------------------------------------------------------------------------
const char* source_to_str(GLenum source) {
	switch (source) {
	case GL_DEBUG_SOURCE_API:
		return "OpenGL";
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		return "Shader Compiler";
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		return "Window System";
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		return "3rd Party";
	case GL_DEBUG_SOURCE_APPLICATION:
		return "Engine";
	default:
	case GL_DEBUG_SOURCE_OTHER:
		return "Other";
	}
}

//-------------------------------------------------------------------------------------------
void gl_debug_output(GLenum source, GLenum type, GLuint /*id*/, GLenum severity, GLsizei /*length*/, const GLchar* message, const void* /*user_data*/) {
	if (gl_debug_level_to_int(severity) < _debug_level) {
		return;
	}
	
	const char* system = source_to_str(source);

	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		log_error("Renderer", "%s. %s", system, message);
		break;

	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
	case GL_DEBUG_TYPE_PERFORMANCE:
	case GL_DEBUG_TYPE_PORTABILITY:
		log_warning("Renderer", "%s. %s", system, message);
		break;

	case GL_DEBUG_TYPE_OTHER:
		if (severity == GL_DEBUG_SEVERITY_NOTIFICATION) {
			log_message("Renderer", "%s. %s", system, message);
		} else {
			log_warning("Renderer", "%s. %s", system, message);
		}
		break;

	case GL_DEBUG_TYPE_MARKER:
	case GL_DEBUG_TYPE_PUSH_GROUP:
	case GL_DEBUG_TYPE_POP_GROUP:
		log_message("Renderer", "%s. %s", system, message);
		break;
	}
}
#else
#	define GL_VERTEX_ARRAY	0x8074
#	define GL_BUFFER		0x82E0
#	define GL_SHADER		0x82E1
#	define GL_PROGRAM		0x82E2
#endif	//RENDERER_DEBUG

//-------------------------------------------------------------------------------------------
inline u32 GetOpenGLVBOType(VertexBufferType type)
{
	switch (type)
	{
	default:
		ASSERT(!"Unknown type %d", (u32)type);
	case VertexBufferType::VertexData:
		return GL_ARRAY_BUFFER;

	case VertexBufferType::IndexData:
		return GL_ELEMENT_ARRAY_BUFFER;

	case VertexBufferType::ReadTarget:
		return GL_COPY_READ_BUFFER;

	case VertexBufferType::WriteTarget:
		return GL_COPY_WRITE_BUFFER;

	case VertexBufferType::TextureReadTarget:
		return GL_PIXEL_UNPACK_BUFFER;

	case VertexBufferType::TextureWriteTarget:
		return GL_PIXEL_PACK_BUFFER;

	case VertexBufferType::TransformFeedback:
		return GL_TRANSFORM_FEEDBACK_BUFFER;

	case VertexBufferType::UniformBuffer:
		return GL_UNIFORM_BUFFER;
	}
}

//-------------------------------------------------------------------------------------------
inline u32 GetOpenGLVBOHint(VertexBufferUsage usageHint)
{
	switch (usageHint)
	{
	default:
		ASSERT(!"Unknown usage hint %d", (u32)usageHint);
	case VertexBufferUsage::StreamDraw:
		return GL_STREAM_DRAW;

	case VertexBufferUsage::StreamRead:
		return GL_STREAM_READ;

	case VertexBufferUsage::StreamCopy:
		return GL_STREAM_COPY;

	case VertexBufferUsage::StaticDraw:
		return GL_STATIC_DRAW;

	case VertexBufferUsage::StaticRead:
		return GL_STATIC_READ;

	case VertexBufferUsage::StaticCopy:
		return GL_STATIC_COPY;

	case VertexBufferUsage::DynamicDraw:
		return GL_DYNAMIC_DRAW;

	case VertexBufferUsage::DynamicRead:
		return GL_DYNAMIC_READ;

	case VertexBufferUsage::DynamicCopy:
		return GL_DYNAMIC_COPY;
	}
}

//-------------------------------------------------------------------------------------------
inline u32 GetOGLVertexFormat(VertexAttribute::Format format)
{
	switch (format)
	{
	case VertexAttribute::Byte:
		return GL_BYTE;

	case VertexAttribute::UnsignedByte:
		return GL_UNSIGNED_BYTE;

	case VertexAttribute::Short:
		return GL_SHORT;
		
	case VertexAttribute::UnsignedShort:
		return GL_UNSIGNED_SHORT;

	case VertexAttribute::Int:
		return GL_INT;

	case VertexAttribute::UnsignedInt:
		return GL_UNSIGNED_INT;

	case VertexAttribute::HalfFloat:
		return GL_HALF_FLOAT;

	default:
		ASSERT(!"Format is out of range %d", format);
	case VertexAttribute::Float:
		return GL_FLOAT;

	case VertexAttribute::Int2_10_10_10_reversed:
		return GL_INT_2_10_10_10_REV;

	case VertexAttribute::Unsigned_2_10_10_10_reversed:
		return GL_UNSIGNED_INT_2_10_10_10_REV;
	}
}

//-------------------------------------------------------------------------------------------
inline u32 GetOGLDepthTestFunc(DepthTestFunc func)
{
	switch (func)
	{
	default:
	case DepthTestFunc::Less:
		return GL_LESS;

	case DepthTestFunc::Equal:
		return GL_EQUAL;

	case DepthTestFunc::LessEqual:
		return GL_LEQUAL;

	case DepthTestFunc::Greater:
		return GL_GREATER;

	case DepthTestFunc::NotEqual:
		return GL_NOTEQUAL;

	case DepthTestFunc::GreaterEqual:
		return GL_GEQUAL;

	case DepthTestFunc::Always:
		return GL_ALWAYS;

	case DepthTestFunc::Never:
		return GL_NEVER;
	}
}

//-------------------------------------------------------------------------------------------
inline u32 GetGLTextureFormat(ImageDescription::Format format)
{
	switch (format)
	{
#ifndef __PLATFORM_MOBILE__
	case ImageDescription::Format::DXT1:
		return GL_COMPRESSED_RGB_S3TC_DXT1_EXT;

	case ImageDescription::Format::DXT3:
		return GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;

	case ImageDescription::Format::DXT5:
		return GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
#elif defined __iOS__
	case ImageDescription::Format::PVRTC_2BPP_RGB:
		return GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;

	case ImageDescription::Format::PVRTC_2BPP_RGBA:
		return GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;

	case ImageDescription::Format::PVRTC_4BPP_RGB:
		return GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG;

	case ImageDescription::Format::PVRTC_4BPP_RGBA:
		return GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
#elif defined __ANDROID__
	case ImageDescription::Format::ETC2_RGB:
		return GL_COMPRESSED_RGB8_ETC2;

	case ImageDescription::Format::ETC2_RGB_A1:
		return GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2;

	case ImageDescription::Format::ETC2_RGBA:
		return GL_COMPRESSED_RGBA8_ETC2_EAC;
#endif
            
	default:
		return GL_RGBA;
	}
}

//-------------------------------------------------------------------------------------------
inline u32 shader_const_type_to_gl(shader::constant_type type) {
	switch (type) {
	case shader::constant_type::float3:
		return GL_FLOAT_VEC3;

	case shader::constant_type::float4:
		return GL_FLOAT_VEC4;
	
	default:
		ASSERT(!"Shader constant type %u mapping to GL is not implemented", type);
	case shader::constant_type::float4x4:
		return GL_FLOAT_MAT4;

	case shader::constant_type::float3x4:
		return GL_FLOAT_MAT3x4;

	case shader::constant_type::sampler2d:
		return GL_SAMPLER_2D;
	}
}

//-------------------------------------------------------------------------------------------
inline void set_debug_name(GLenum identifier, GLenum handle, const StringHash* name) {
	(void)identifier;
	(void)handle;
	(void)name;

#ifdef RENDERER_DEBUG
	if (name) {
		glObjectLabel(identifier, handle, -1, name->c_str());
	}
#endif
}

struct scoped_vertex_binding {
	scoped_vertex_binding(VertexBindingHandle vao) {
		ASSERT(_current_vertex_binding.m_handle == 0);
		glBindVertexArray(vao.m_handle);
		_current_vertex_binding = vao;
	}

	~scoped_vertex_binding() {
		//It's mandatory to unbind vertex binding after modifications are done, since other objects can modify previously bound vao, so vao caching should never happen
		glBindVertexArray(0);
		_current_vertex_binding = 0;
	}
};

//-------------------------------------------------------------------------------------------
GLint texture_filtering_to_gl(texture_params::filtering filtering) {
	switch (filtering) {
	default:
		XERROR("Undefined texture filtering type `%d`", filtering);
	case texture_params::NEAREST:
		return GL_NEAREST;
	case texture_params::LINEAR:
		return GL_LINEAR;
	case texture_params::NEAREST_MIPMAP:
		return GL_NEAREST_MIPMAP_NEAREST;
	case texture_params::LINEAR_MIPMAP:
		return GL_LINEAR_MIPMAP_NEAREST;
	case texture_params::NEAREST_2_MIPMAPS:
		return GL_NEAREST_MIPMAP_LINEAR;
	case texture_params::LINEAR_2_MIPMAPS:
		return GL_LINEAR_MIPMAP_LINEAR;
	}
}

}	// nameless namespace



//-------------------------------------------------------------------------------------------
RendererGL3::RendererGL3() {
	for (u32 i = 0; i < (u32)VertexBufferType::Count; ++i)
	{
		_buffer_bindings[i] = 0;
	}
}

//-------------------------------------------------------------------------------------------
RendererGL3::~RendererGL3() {
}

//-------------------------------------------------------------------------------------------
void RendererGL3::Init(const vec2i& screen_size)
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	//glFrontFace(GL_CW);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	_renderer_state.blendFunc = BlendFunc::AlphaBlend;

	glDisable(GL_BLEND);
	_renderer_state.blendEnabled = false;

	glEnable(GL_DEPTH_TEST);
	_renderer_state.depthTestEnabled = true;

	glDepthFunc(GetOGLDepthTestFunc(_renderer_state.depthFunc));
	glDepthMask(_renderer_state.depthWriteEnabled);

	glViewport(0, 0, screen_size.x, screen_size.y);

	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &_texture_units_count);
	if (_texture_units_count > _max_texture_units) {
		log_warning("Renderer", "Cache for texture units is too small - %u, requested %u", _texture_units_count, _max_texture_units);

		_texture_units_count = _max_texture_units;
	}

	memset(_texture_bindings, 0, sizeof(_texture_bindings));

#ifdef RENDERER_DEBUG
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(&gl_debug_output, nullptr);
#endif
}

//-------------------------------------------------------------------------------------------
void RendererGL3::Shutdown() {
	ASSERT(_shader_programs.GetSize() == 0);

	_gpu_leaks_detector.Shutdown();
}

//-------------------------------------------------------------------------------------------
void RendererGL3::Draw(const DrawContext& context)
{
	scoped_vertex_binding vao_binding(context.vertexBinding);

	SetBlendState(context.rendererState);
	SetDepthState(context.rendererState);

	const ShaderProgram* pShader = GetShader(context.shader);
	UseShader(pShader->GetHandle());
	
	u32 current_texture_unit = 0;
	const TextureHandle* texture = &context.diffuse;
	const StringHash maps[] = { "albedo_map", "normal_map", "metallic_map", "roughness_map" };
	for (u32 i = 0; i < 4; ++i, ++texture) {
		if (texture->m_handle) {
			if (const ShaderParamInfo* uniform_info = pShader->GetUniformInfo(maps[i])) {
				bind_texture(*texture, GL_TEXTURE_2D, (u16)current_texture_unit);
				glUniform1i(uniform_info->location, current_texture_unit);
				++current_texture_unit;
			}
		}
	}

	shader::constants_reader reader(context.constants);

	for (u32 i = 0; i < context.constants.count; ++i) {
		const shader::constant* c = reader.read();
		if (const ShaderParamInfo* uniform_info = pShader->GetUniformInfo(c->name)) {
			ASSERT(shader_const_type_to_gl(c->type) == uniform_info->type);

			switch (c->type) {
			case shader::constant_type::float3:
				glUniform3fv(uniform_info->location, 1, (float*)c->data);
				break;

			case shader::constant_type::float4:
				glUniform4fv(uniform_info->location, 1, (float*)c->data);
				break;

			case shader::constant_type::float4x4:
				glUniformMatrix4fv(uniform_info->location, 1, GL_FALSE, (float*)c->data);
				break;

			case shader::constant_type::float3x4:
				glUniformMatrix3x4fv(uniform_info->location, 1, GL_FALSE, (float*)c->data);
				break;

			//case shader::constant_type::sampler2d:
			//	bind_texture(get_internal_id(*(u32*)c->data), GL_TEXTURE_2D, current_texture_unit);
			//	glUniform1i(uniform_info->location, current_texture_unit);
			//	++current_texture_unit;
			//	break;

			default:
				XERROR("Case %s is not handled", c->type);
			}
		} else {
			log_warning("Renderer", "Constant %s is missing for shader %s", c->name.c_str(), pShader->name().c_str());
		}
	}

	if (context.indexBuffer.m_handle)
	{
		glDrawElements(GL_TRIANGLES, context.indicesCount, GL_UNSIGNED_SHORT, 0);
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, context.verticesCount);
	}
}

//-------------------------------------------------------------------------------------------
VertexBufferHandle RendererGL3::CreateVertexBuffer(VertexBufferType type, const StringHash* name) const
{
	VertexBufferHandle buffer;
	glGenBuffers(1, &buffer.m_handle);
	bind_vertex_buffer(buffer, type);
	set_debug_name(GL_BUFFER, buffer.m_handle, name);

	_gpu_leaks_detector.ReportAllocation(buffer, 0);

	return buffer;
}

//-------------------------------------------------------------------------------------------
void RendererGL3::DeleteVertexBuffer(VertexBufferHandle buffer) const
{
	glDeleteBuffers(1, &buffer.m_handle);
	for (u32 i = 0; i < (u32)VertexBufferType::Count; ++i) {
		if (_buffer_bindings[i] == buffer) {
			bind_vertex_buffer(0, (VertexBufferType)i);
			break;
		}
	}

	_gpu_leaks_detector.ReportDeletion(buffer);
}

//-------------------------------------------------------------------------------------------
void RendererGL3::UploadVertexBufferData(VertexBufferHandle buffer, VertexBufferType bufferType, const void* pData, size_t size, VertexBufferUsage usageHint)
{
	bind_vertex_buffer(buffer, bufferType);
	const u32 gl_type = GetOpenGLVBOType(bufferType);
	glBufferData(gl_type, size, pData, GetOpenGLVBOHint(usageHint));
}

//-------------------------------------------------------------------------------------------
VertexBindingHandle RendererGL3::CreateVertexBinding(const VertexDescription& vertexDescription, VertexBufferHandle vertexDataBuffer, VertexBufferHandle indexBuffer, const StringHash* name)
{
	VertexBindingHandle vertexBinding;
	glGenVertexArrays(1, &vertexBinding.m_handle);
	scoped_vertex_binding vao_binding(vertexBinding);
	set_debug_name(GL_VERTEX_ARRAY, vertexBinding.m_handle, name);

	_gpu_leaks_detector.ReportAllocation(vertexBinding, 0);

	_buffer_bindings[(u32)VertexBufferType::IndexData] = 0;		// Index buffer is part of a VAO state; it's set to 0, on a newly created VAO
	bind_vertex_buffer(indexBuffer, VertexBufferType::IndexData);

	// vertex buffer is not a part of VAO state, but it is a part of glVertexAttribPointer and should be binded
	bind_vertex_buffer(vertexDataBuffer, VertexBufferType::VertexData);

	for (u16 i = 0; i < vertexDescription.attributesCount; ++i)
	{
		const VertexAttribute kAttribute = vertexDescription.attributes[i];
		glEnableVertexAttribArray(kAttribute.type);
		const intptr_t offset = kAttribute.offset;	// fix for a compiler warning
		glVertexAttribPointer(kAttribute.type, kAttribute.componentsCount, GetOGLVertexFormat(kAttribute.format), kAttribute.normalize, vertexDescription.vertexSize, (void*)offset);
	}

	return vertexBinding;
}

//-------------------------------------------------------------------------------------------
void RendererGL3::DeleteVertexBinding(VertexBindingHandle buffer) const
{
	glDeleteVertexArrays(1, &buffer.m_handle);
	_gpu_leaks_detector.ReportDeletion(buffer);
}

//-------------------------------------------------------------------------------------------
ShaderHandle RendererGL3::CreateShader(eShaderType type, const char* src, const StringHash* name) const
{
	GLenum glShaderType;

	switch (type)
	{
	default:
	case eShaderType::Vertex:
		glShaderType = GL_VERTEX_SHADER;
		break;

	case eShaderType::Pixel:
		glShaderType = GL_FRAGMENT_SHADER;
		break;
	}

	ShaderHandle shader = glCreateShader(glShaderType);
	set_debug_name(GL_SHADER, shader.m_handle, name);

#ifdef __PLATFORM_DESKTOP__
	const char header[] = "#version 330 core\n"
		"#define highp\n"
		"#define mediump\n"
		"#define lowp\n";
#elif defined __PLATFORM_MOBILE__
	const char header[] = "#version 300 es\n"
		"precision highp float;\n";
#endif

	const char* sources[] = { header, src };

	glShaderSource(shader.m_handle, sizeof(sources) / sizeof(char*), sources, nullptr);
	glCompileShader(shader.m_handle);

#ifdef DEBUG
	int logLength;
	glGetShaderiv(shader.m_handle, GL_INFO_LOG_LENGTH, &logLength);

	if (logLength > 0)
	{
		const u32 kBufferSize = 2048;
		char logBuffer[kBufferSize];

		glGetShaderInfoLog(shader.m_handle, kBufferSize, &logLength, logBuffer);

		int compilationStatus = 0;
		glGetShaderiv(shader.m_handle, GL_COMPILE_STATUS, &compilationStatus);

		if (logLength > 0)
		{
			if (compilationStatus == GL_TRUE)
			{
				log_warning("Renderer", "Shader `%s` successfully compiled with message! %s", name->c_str(), logBuffer);
			}
			else
			{
				log_error("Renderer", "Shader `%s` compilation log: %s", name->c_str(), logBuffer);
			}
		}
	}
	else
	{
		log_message("Renderer", "Shader `%s` compiled successfully!", name->c_str());
	}
#endif

	_gpu_leaks_detector.ReportAllocation(shader, 0);

	return shader;
}

//-------------------------------------------------------------------------------------------
void RendererGL3::DeleteShader(ShaderHandle shader) const
{
	glDeleteShader(shader.m_handle);

	_gpu_leaks_detector.ReportDeletion(shader);
}

//-------------------------------------------------------------------------------------------
ShaderProgramHandle RendererGL3::CreateShaderProgram(const StringHash& name, ShaderHandle vertexShader, ShaderHandle pixelShader)
{
	const u32 shader = glCreateProgram();
	set_debug_name(GL_PROGRAM, shader, &name);

	for (u32 i = 0; i < VertexAttribute::MAX; ++i)
	{
		glBindAttribLocation(shader, i, VertexAttribute::GetName((VertexAttribute::Type)i));
	}

	glAttachShader(shader, vertexShader.m_handle);
	glAttachShader(shader, pixelShader.m_handle);

	glLinkProgram(shader);

#ifdef DEBUG
	int logLength;
	glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &logLength);

	if (logLength > 0)
	{
		const u32 kBufferSize = 2048;
		char logBuffer[kBufferSize];

		glGetProgramInfoLog(shader, kBufferSize, &logLength, logBuffer);

		int linkStatus = 0;
		glGetProgramiv(shader, GL_LINK_STATUS, &linkStatus);

		if (logLength > 0)
		{
			if (linkStatus == GL_TRUE)
			{
				log_warning("Renderer", "Shader program `%s` successfully linked with message: %s", name.c_str(), logBuffer);
			}
			else
			{
				log_error("Renderer", "Shader `%s` linking log: %s", name.c_str(), logBuffer);
			}
		}
	}
	else
	{
		log_message("Renderer", "Shader program `%s` linked successfully!", name.c_str());
	}
#endif

	glDetachShader(shader, vertexShader.m_handle);
	glDetachShader(shader, pixelShader.m_handle);

	_gpu_leaks_detector.ReportAllocation(ShaderProgramHandle(shader), 0);

	_shader_programs.EmplaceBack(name, shader);

	return shader;
}

//-------------------------------------------------------------------------------------------
void RendererGL3::DeleteShaderProgram(ShaderProgramHandle shader)
{
	for (u32 i = 0; i < _shader_programs.GetSize(); ++i) {	// TODO: fix this
		if (_shader_programs[i].GetHandle() == shader) {
			_shader_programs.RemoveFast(i);
			break;
		}
	}

	glDeleteProgram(shader.m_handle);

	_gpu_leaks_detector.ReportDeletion(shader);
}

//-------------------------------------------------------------------------------------------
u32 RendererGL3::GetUniformsCount(ShaderProgramHandle shader) const
{
	int uniformsCount = 0;
	glGetProgramiv(shader.m_handle, GL_ACTIVE_UNIFORMS, &uniformsCount);

	return uniformsCount;
}

//-------------------------------------------------------------------------------------------
ShaderParamInfo RendererGL3::GetUniformDescription(ShaderProgramHandle shader, u32 uniformIndex) const
{
	int size;
	int length;
	char uniformName[64];
	GLenum type = GL_FLOAT;

	glGetActiveUniform(shader.m_handle, uniformIndex, sizeof(uniformName) - 1, &length, &size, &type, uniformName);

	ASSERT(type < (u16)-1);

	return ShaderParamInfo(uniformName, (u16)type, (u16)glGetUniformLocation(shader.m_handle, uniformName));
}

//-------------------------------------------------------------------------------------------
u32 RendererGL3::GetAttributesCount(ShaderProgramHandle shader) const
{
	int attributesCount = 0;
	glGetProgramiv(shader.m_handle, GL_ACTIVE_ATTRIBUTES, &attributesCount);

	return attributesCount;
}

//-------------------------------------------------------------------------------------------
ShaderParamInfo RendererGL3::GetAttributeDescription(ShaderProgramHandle shader, u32 attributeIndex) const
{
	int size;
	int length;
	char attribName[64];
	GLenum type = GL_FLOAT;

	glGetActiveAttrib(shader.m_handle, attributeIndex, sizeof(attribName) - 1, &length, &size, &type, attribName);

	ASSERT(type < (u16)-1);

	return ShaderParamInfo(attribName, (u16)type, (u16)glGetAttribLocation(shader.m_handle, attribName));
}

//-------------------------------------------------------------------------------------------
TextureHandle RendererGL3::CreateTexture(const ImageDescription& imageDescription, const StringHash* name, const texture_params& params)
{
	TextureHandle texture;
	glGenTextures(1, &texture.m_handle);
	bind_texture(texture, GL_TEXTURE_2D, 0);		// TODO: fix const unit
	set_debug_name(GL_TEXTURE, texture.m_handle, name);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, texture_filtering_to_gl(params.minification));
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, texture_filtering_to_gl(params.magnification));

	const GLenum kFormat = GetGLTextureFormat(imageDescription.m_format);

	u32 dataSize = 0;

	if (imageDescription.m_isCompressed)
	{
		for (u32 i = 0; i < imageDescription.m_mipMapsCount; ++i)
		{
			glCompressedTexImage2D(GL_TEXTURE_2D, i, kFormat, imageDescription.m_width[i], imageDescription.m_height[i], 0, imageDescription.m_dataSize[i], imageDescription.m_pData[i]);
			dataSize += imageDescription.m_dataSize[i];
		}
	}
	else
	{
		GLenum type = GL_UNSIGNED_BYTE;

		for (u32 i = 0; i < imageDescription.m_mipMapsCount; ++i)
		{
			glTexImage2D(GL_TEXTURE_2D, i, kFormat, imageDescription.m_width[i], imageDescription.m_height[i], 0, kFormat, type, imageDescription.m_pData[i]);
			dataSize += imageDescription.m_dataSize[i];
		}
	}

	_gpu_leaks_detector.ReportAllocation(texture, dataSize);

	return texture;
}

//-------------------------------------------------------------------------------------------
void RendererGL3::DeleteTexture(TextureHandle texture) const
{
	glDeleteTextures(1, &texture.m_handle);

	_gpu_leaks_detector.ReportDeletion(texture);
}

//-------------------------------------------------------------------------------------------
void RendererGL3::Begin()
{
	// force-enable depth write
	RendererState tempRendererState = _renderer_state;
	tempRendererState.depthWriteEnabled = true;
	SetDepthState(tempRendererState);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

//-------------------------------------------------------------------------------------------
void RendererGL3::End()
{
}



namespace {
//-------------------------------------------------------------------------------------------
void SetBlendState(RendererState state) {
	if (state.blendEnabled != _renderer_state.blendEnabled) {
		if (state.blendEnabled) {
			glEnable(GL_BLEND);

			// change blend mode only in case when blend is enabled and blend mode is different from previous
			if (state.blendFunc != _renderer_state.blendFunc) {
				switch (state.blendFunc) {
				default:
				case BlendFunc::AlphaBlend:
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					break;

				case BlendFunc::AdditiveBlend:
					glBlendFunc(GL_SRC_ALPHA, GL_ONE);
					break;
				}

				_renderer_state.blendFunc = state.blendFunc;
			}
		} else {
			glDisable(GL_BLEND);
		}

		_renderer_state.blendEnabled = state.blendEnabled;
	}
}

//-------------------------------------------------------------------------------------------
void SetDepthState(RendererState state) {
	if (state.depthTestEnabled != _renderer_state.depthTestEnabled) {
		if (state.depthTestEnabled) {
			glEnable(GL_DEPTH_TEST);

			if (state.depthFunc != _renderer_state.depthFunc) {
				glDepthFunc(GetOGLDepthTestFunc(state.depthFunc));
				_renderer_state.depthFunc = state.depthFunc;
			}
		} else {
			glDisable(GL_DEPTH_TEST);
		}

		_renderer_state.depthTestEnabled = state.depthTestEnabled;
	}

	if (state.depthWriteEnabled != _renderer_state.depthWriteEnabled) {
		glDepthMask(state.depthWriteEnabled);

		_renderer_state.depthWriteEnabled = state.depthWriteEnabled;
	}
}

//-------------------------------------------------------------------------------------------
inline void UseShader(ShaderProgramHandle shader)
{
	if (shader != _current_shader)
	{
		glUseProgram(shader.m_handle);
		_current_shader = shader;
	}
}

//-------------------------------------------------------------------------------------------
inline void bind_vertex_buffer(VertexBufferHandle buffer, VertexBufferType type) {
	if (_buffer_bindings[(u32)type] != buffer) {
		const u32 gl_type = GetOpenGLVBOType(type);
		glBindBuffer(gl_type, buffer.m_handle);

		_buffer_bindings[(u32)type] = buffer;
	}
}

//-------------------------------------------------------------------------------------------
inline ShaderProgram* GetShader(ShaderProgramHandle handle) {
	for (u32 i = 0; i < _shader_programs.GetSize(); ++i) {
		if (_shader_programs[i].GetHandle() == handle) {
			return &_shader_programs[i];
		}
	}

	ASSERT(!"Cannot find specified shader id %d", handle);
	return nullptr;
}

//-------------------------------------------------------------------------------------------
inline void bind_texture(TextureHandle texture, u16 type, u16 unit) {
	if (_texture_bindings[unit] != texture) {
		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(type, texture.m_handle);
		_texture_bindings[unit] = texture;
	}
}

}	// nameless namespace