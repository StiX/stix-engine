
#pragma once

#include "RendererGL3.h"
#include "ResourceMesh.h"

struct Material;
struct allocation_info;
class  ThreadsSyncPoint;
struct ImageDescription;
namespace shader {
struct constants_buffer;
}


namespace renderer {

typedef void		(*dispatch_function)(const void*);


void				init(const vec2i& screen_size);
void				shutdown();
void				update();

void				activate(bool active);

ThreadsSyncPoint&	frame_end_sync_point();


void				render(const Material& material, const shader::constants_buffer& constants, const MeshResourceHandle& mesh);
void				render(const Material& material, const shader::constants_buffer& constants, VertexBufferHandle vb, VertexBufferHandle ib, VertexBindingHandle vd, u32 count);

ShaderHandle		create_shader(const StringHash& name, eShaderType shader_type, const char* shader_code);
void				delete_shader(ShaderHandle shader);

ShaderProgramHandle	create_shader_program(const StringHash& name, ShaderHandle vertex_shader, ShaderHandle pixel_shader);
void				delete_shader_program(ShaderProgramHandle program);

TextureHandle		create_texture(const StringHash& name, const ImageDescription& image, const allocation_info& allocation, const texture_params& params);
void				delete_texture(TextureHandle handle);

VertexBufferHandle	create_vertex_buffer(const StringHash& name, VertexBufferType type, const void* data, u32 size, VertexBufferUsage usage_hint, const allocation_info* allocation);
void				delete_vertex_buffer(VertexBufferHandle handle);

VertexBindingHandle	create_vertex_binding(const StringHash& name, const VertexDescription& vertex_description, VertexBufferHandle vertex_buffer, VertexBufferHandle index_buffer);
void				delete_vertex_binding(VertexBindingHandle handle);

void*				add_command(dispatch_function function, size_t data_size, u64 key = (u64)-1);

}