
#pragma once

#include "Types.h"

#ifdef DEBUG
#	include "HashTable.h"
#	include "ObjectsPool.h"
#	include "RendererTypes.h"
#endif


class GPULeaksDetector
{
public:
							GPULeaksDetector();
							~GPULeaksDetector();

	void					Shutdown();

	template<class T> void	ReportAllocation(T handle, u32 size);
	template<class T> void	ReportDeletion(T handle);

protected:
private:
#ifdef DEBUG
	static const u32 kBackTraceDepth = 64;

	struct BackTrace
	{
		u32					allocationSize;
		u32					framesCount;
		void*				callstack[kBackTraceDepth];
	};

	typedef HashTable<u32, BackTrace*> Map;
	
	Map						m_map;
	u32						m_resourcesAllocatedSize;
	ObjectsPool<BackTrace>	m_pool;

	template<class T> u32	GenerateKey(T handle) { return handle.m_handle | (handle.GetIntType() << 16); }

	void					AddObject(u32 key, u32 size);
	void					RemoveObject(u32 key);
#endif
};





//-------------------------------------------------------------------------------------------
template<class T>
void GPULeaksDetector::ReportAllocation(T handle, u32 size)
{
	(void)size;
	(void)handle;

#ifdef DEBUG
	const u32 kKey = GenerateKey(handle);
	AddObject(kKey, size);
#endif
}

//-------------------------------------------------------------------------------------------
template<class T>
void GPULeaksDetector::ReportDeletion(T handle)
{
	(void)handle;

#ifdef DEBUG
	const u32 kKey = GenerateKey(handle);
	RemoveObject(kKey);
#endif
}