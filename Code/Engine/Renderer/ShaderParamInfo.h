
#pragma once

#include "StringHash.h"


struct ShaderParamInfo
{
	StringHash	name;
	u16			location;
	u16			type;

	ShaderParamInfo(const StringHash& name, u16 type, u16 location)
		: name(name), location(location), type(type) {}
};