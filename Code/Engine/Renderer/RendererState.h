
#pragma once

#include "Types.h"


enum class BlendFunc : u32
{
	AlphaBlend,
	AdditiveBlend,

	BlendMax
};

enum class DepthTestFunc : u32
{
	Never,
	Less,
	Equal,
	LessEqual,
	Greater,
	NotEqual,
	GreaterEqual,
	Always,

	DepthMax
};


struct RendererState {
	// blend state
	u32				blendEnabled : 1;
	BlendFunc		blendFunc : 1;

	// depth state
	u32				depthTestEnabled : 1;
	u32				depthWriteEnabled : 1;
	DepthTestFunc	depthFunc : 3;

					
					RendererState()
						: blendEnabled(false)
						, blendFunc(BlendFunc::AlphaBlend)
						, depthTestEnabled(true)
						, depthWriteEnabled(true)
						, depthFunc(DepthTestFunc::Less) {}
};