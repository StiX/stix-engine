
#pragma once

#include "Vector4D.h"
#include "resource_font.h"

struct Material;


namespace renderer {
	void render_text_2d(const char* text, font_resource_handle font, Material &material, const mat4& view_proj, const vec3f& position, const vec4f& color = vec4f(1.0f));
}