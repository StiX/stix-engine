
#pragma once

#include "Image.h"
#include "memory.h"
#include "StringHash.h"
#include "VertexBuffer.h"
#include "RendererTypes.h"
#include "VertexDescription.h"


struct CreateShaderCmd {
#ifdef DEBUG
	StringHash		name;
#endif
	ShaderHandle	handle;
	eShaderType		shaderType;
	char			code[0];
};

struct CreateShaderProgramCmd {
	StringHash			name;
	ShaderProgramHandle	shaderHandle;
	ShaderHandle		vertexShader;
	ShaderHandle		pixelShader;
};

struct CreateTextureCmd {
#ifdef RENDERER_DEBUG
	StringHash						name;
#endif
	allocation_info					allocation;
	TextureHandle					handle;
	ImageDescription				image;
	texture_params					params;
};

struct CreateVertexBufferCmd {
#ifdef RENDERER_DEBUG
	StringHash						name;
#endif
	const void*						pData;
	allocation_info					allocation;
	u32								size;
	VertexBufferHandle				handle;
	VertexBufferType				type;
	VertexBufferUsage				usageHint;
};

struct CreateVertexBindingCmd {
#ifdef RENDERER_DEBUG
	StringHash			name;
#endif
	VertexBindingHandle	handle;
	VertexBufferHandle	vertexDataBuffer;
	VertexBufferHandle	indexBuffer;
	VertexDescription	vertexDescription;
};


void CreateShader(const void* pData);
void DeleteShader(const void* handle_ptr);

void CreateShaderProgram(const void* pData);
void DeleteShaderProgram(const void* handle_ptr);

void CreateTexture(const void* pData);		//TODO: unify create\delete resource functions
void DeleteTexture(const void* handle_ptr);

void CreateVertexBuffer(const void* pData);
void DeleteVertexBuffer(const void* handle_ptr);

void CreateVertexBinding(const void* pData);
void DeleteVertexBinding(const void* handle_ptr);

void Draw(const void* pData);