
#pragma once

#include "Types.h"


struct VertexAttribute
{
	enum Type
	{
		Position,
		Normal,
		UV,
		Color,
		JointsIndices,
		JointsWeights,

		MAX,
	};

	enum Format
	{
		Byte,
		UnsignedByte,
		Short,
		UnsignedShort,
		Int,
		UnsignedInt,
		HalfFloat,
		Float,
		Int2_10_10_10_reversed,
		Unsigned_2_10_10_10_reversed,
	};

	Type	type : 8;	//TODO: consider making it a string hash?!
	Format	format : 8;
	u32		offset : 8;
	u32		componentsCount : 7;
	u32		normalize : 1;

	static const char* GetName(Type vertexAttribute);
};



struct VertexDescription
{
	u16				attributesCount;
	u16				vertexSize;
	VertexAttribute	attributes[VertexAttribute::MAX];

					VertexDescription();

	void			AddAttribute(VertexAttribute::Type type, VertexAttribute::Format format, u32 components_count, bool normalize = false);

	u32				serialization_size() const;

	template <class T>
	void			serialize(T& stream);

	template <class T>
	void			serialize(T& stream) const;
};





//-------------------------------------------------------------------------------------------
template <class T>
void VertexDescription::serialize(T& stream) {
	stream & attributesCount;
	stream & vertexSize;

	for (u32 i = 0; i < attributesCount; ++i) {
		stream & attributes[i];
	}
}

//-------------------------------------------------------------------------------------------
template <class T>
void VertexDescription::serialize(T& stream) const {
	const_cast<VertexDescription*>(this)->serialize(stream);
}