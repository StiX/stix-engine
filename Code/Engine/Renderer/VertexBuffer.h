
#pragma once

#include "Types.h"

// TODO: move to either RendererGL3.h or to RendererTypes.h


enum class VertexBufferType : u8
{
	VertexData,
	IndexData,
	ReadTarget,
	WriteTarget,
	TextureReadTarget,
	TextureWriteTarget,
	TransformFeedback,
	UniformBuffer,
	Count
};

enum class VertexBufferUsage : u8
{
	StreamDraw,			// The data will be uploaded once by app and will by used a few times to draw
	StreamRead,			// The data will be read once from GL and queried few times
	StreamCopy,			// The data will be read once from GL and will by used a few times to draw
	StaticDraw,			// The data will be uploaded once by app and will by used many times to draw
	StaticRead,			// The data will be read once from GL and queried many times
	StaticCopy,			// The data will be read once from GL and will by used many times to draw
	DynamicDraw,		// The data will be reuploaded many times by app and will be used many times to draw
	DynamicRead,		// The data will be read many times from GL and queried many times
	DynamicCopy,		// The data will be read many times from GL and will by used many times to draw
};