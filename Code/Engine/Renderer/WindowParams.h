
#pragma once

#include "Vector2D.h"


struct WindowParams
{
	vec2i	m_size;
	bool	m_bFullScreen;
	
			WindowParams()
				: m_size(vec2i(1024, 768)), m_bFullScreen(false)
			{
			}
	
	explicit WindowParams(const vec2i& size, bool bFullScreen = false)
				: m_size(size), m_bFullScreen(bFullScreen)
			{
			}
};