
#include "shader_constants.h"
#include "mat4.h"
#include "mat3x4.h"
#include "WriteStream.h"
#include "GenericAllocator.h"


namespace shader {

//-------------------------------------------------------------------------------------------
u32 get_size_for_type(constant_type type) {
	switch (type) {
	case constant_type::float3:
		return sizeof(vec3f);

	case constant_type::float4:
		return sizeof(vec4f);

	default:
		ASSERT(!"Size for type %u is not implemented", type);
	case constant_type::float4x4:
		return sizeof(mat4);

	case constant_type::float3x4:
		return sizeof(mat3x4);

	case constant_type::sampler2d:
		return sizeof(TextureHandle);
	}
}

//-------------------------------------------------------------------------------------------
constants_buffer* allocate_constants(u16 count) {
	const size_t size = sizeof(constants_buffer) + count * (sizeof(StringHash) + sizeof(constant_type) + sizeof(mat4));
	constants_buffer* constants_buf = (constants_buffer*)GenericAllocator::Alloc(size);
	constants_buf->count = count;
	constants_buf->data_size = (u16)size;

	return constants_buf;
}

//-------------------------------------------------------------------------------------------
void free_constants(constants_buffer* ptr) {
	GenericAllocator::Free(ptr);
}



//-------------------------------------------------------------------------------------------
constants_writer::constants_writer(constants_buffer& con)
	: _constants(&con), _data_size(con.data_size), _count(con.count)
{
	(void)_count;
	_constants->count = 0;
	_constants->data_size = 0;
}

//-------------------------------------------------------------------------------------------
void constants_writer::append(const StringHash& name, const vec3f& vec) {
	append(name, constant_type::float3, &vec, sizeof(vec));
}

//-------------------------------------------------------------------------------------------
void constants_writer::append(const StringHash& name, const vec4f& vec) {
	append(name, constant_type::float4, &vec, sizeof(vec));
}

//-------------------------------------------------------------------------------------------
void constants_writer::append(const StringHash& name, const mat4& mat) {
	append(name, constant_type::float4x4, &mat, sizeof(mat));
}

//-------------------------------------------------------------------------------------------
void constants_writer::append(const StringHash& name, const mat3x4& mat) {
	append(name, constant_type::float3x4, &mat, sizeof(mat));
}

//-------------------------------------------------------------------------------------------
void constants_writer::append(const StringHash& name, TextureHandle sampler_2d) {
	append(name, constant_type::sampler2d, &sampler_2d, sizeof(sampler_2d));
}

//-------------------------------------------------------------------------------------------
void constants_writer::append(const StringHash& name, constant_type type, const void* ptr, u32 size) {
	ASSERT(_constants->count < _count);
	++_constants->count;

	WriteStream stream(&_constants->data[_constants->data_size], _data_size);		// we don't really care about providing the correct size here
	stream.Write(name);			_constants->data_size += sizeof(name);
	stream.Write(type);			_constants->data_size += sizeof(type);
	stream.Write(ptr, size);	_constants->data_size += (u16)size;
}



//-------------------------------------------------------------------------------------------
constants_reader::constants_reader(const constants_buffer& con)
	: _constants(&con), _current_offset(0)
{
}

//-------------------------------------------------------------------------------------------
const constant* constants_reader::read() {
	constant* c = (constant*)&_constants->data[_current_offset];
	_current_offset += sizeof(c->name) + sizeof(c->type) + get_size_for_type(c->type);
	ASSERT(_current_offset <= _constants->data_size);

	return c;
}

}	// namespace shader