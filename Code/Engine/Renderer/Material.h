
#pragma once

#include "RendererState.h"
#include "RendererTypes.h"
#include "ResourceTexture.h"


struct Material
{
	RendererState			m_rendererState;
	TextureResourceHandle	diffuse;
	TextureResourceHandle	normal;
	TextureResourceHandle	metallic;
	TextureResourceHandle	roughness;
	ShaderProgramHandle		m_shader;
	bool					m_bDoubleSided;
};