
#include "GPULeaksDetector.h"
#include "Log.h"
#include "StackBackTrace.h"


//-------------------------------------------------------------------------------------------
GPULeaksDetector::GPULeaksDetector()
#ifdef DEBUG
	: m_resourcesAllocatedSize(0), m_pool(16000, 4)
{
}
#else
{
}
#endif

//-------------------------------------------------------------------------------------------
GPULeaksDetector::~GPULeaksDetector()
{
	Shutdown();
}

//-------------------------------------------------------------------------------------------
void GPULeaksDetector::Shutdown() {
#ifdef DEBUG
	if (m_map.GetSize()) {
		log_warning("Renderer", "Detected %u memory leaks!", m_map.GetSize());

		Map::Iterator it = m_map.GetFirstElement();

		while (it) {
			BackTrace* pBackTrace = it.GetValue();
			m_map.Remove(it);
			++it;

			LogMemoryLeaks(pBackTrace->framesCount, pBackTrace->callstack);

			m_pool.DeleteObject(pBackTrace);
		}
	}
#endif
}

#ifdef DEBUG
//-------------------------------------------------------------------------------------------
void GPULeaksDetector::AddObject(u32 key, u32 size)
{
	BackTrace* pBackTrace = m_pool.CreateObject();
	pBackTrace->allocationSize = size;
	pBackTrace->framesCount = GetStackBackTrace(3, kBackTraceDepth, pBackTrace->callstack);

	m_map.Insert(key, pBackTrace);

	m_resourcesAllocatedSize += size;
}

//-------------------------------------------------------------------------------------------
void GPULeaksDetector::RemoveObject(u32 key)
{
	Map::Iterator it = m_map.FindIterator(key);

	if (it != m_map.GetEnd())
	{
		BackTrace* pBackTrace = it.GetValue();
		m_resourcesAllocatedSize -= pBackTrace->allocationSize;
		m_pool.DeleteObject(pBackTrace);

		m_map.Remove(it);
	}
}
#endif