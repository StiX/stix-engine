
#pragma once

#include "Types.h"
#include "StringHash.h"
#include "ClassHelpers.h"
#include "RendererTypes.h"

struct ShaderParamInfo;


class ShaderProgram
{
public:
							ShaderProgram(const StringHash& name, ShaderProgramHandle shaderProgram);
							~ShaderProgram();

	ShaderProgramHandle		GetHandle() const;
	const StringHash&		name() const;

	const ShaderParamInfo*	GetUniformInfo(const StringHash& uniform) const;
	const ShaderParamInfo*	GetAttributeInfo(const StringHash& attribute) const;

protected:
private:
	ShaderParamInfo*		m_uniformLocations;
	ShaderParamInfo*		m_attributeLocations;
	ShaderProgramHandle		m_handle;
	StringHash				_name;
	u32						_uniforms_count;
	u32						_attributes_count;

	void					Create(ShaderProgramHandle program);

	void					ProcessUniforms();
	void					ProcessAttributes();

	MAKE_UNCOPYABLE(		ShaderProgram);
};





//-------------------------------------------------------------------------------------------
inline ShaderProgram::ShaderProgram(const StringHash& name, ShaderProgramHandle shaderProgram)
	: m_uniformLocations(nullptr), m_attributeLocations(nullptr)
	, _name(name), _uniforms_count(0), _attributes_count(0)
{
	Create(shaderProgram);
}

//-------------------------------------------------------------------------------------------
inline ShaderProgramHandle ShaderProgram::GetHandle() const
{
	return m_handle;
}