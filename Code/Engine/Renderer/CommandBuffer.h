
#pragma once

#include "Pair.h"
#include "Array.h"
#include "ClassHelpers.h"
#include "LinearAllocator.h"


class Command_buffer {
public:
						Command_buffer();

	void*				allocate(size_t size, size_t alignment);
	void				add_command(u64 key, void* data);
	void				execute();

	void				can_write(bool can_write);

protected:
private:
	typedef Pair<u64, void*> Pair_t;

	Array<Pair_t>		_commands;
	LinearAllocator		_allocator;
	bool				_can_write;

	MAKE_UNCOPYABLE(	Command_buffer);
};