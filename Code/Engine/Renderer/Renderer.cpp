
#include "Renderer.h"
#include "Thread.h"
#include "Material.h"
#include "CommandBuffer.h"
#include "RendererDispatch.h"
#include "ThreadsSyncPoint.h"
#include "SystemEnvironment.h"


namespace {
Renderer			_renderer;
ThreadsSyncPoint	_frame_end_sync_point(2);
bool				_active = true;
u32					_frame_id = 0;
u64					_current_sort_key = 0;

Command_buffer		_command_buffers[2];
u32					_write_buffer_index = 0;

//-------------------------------------------------------------------------------------------
Command_buffer* swap_command_buffers() {
	_current_sort_key = 0;

	_command_buffers[_write_buffer_index].can_write(false);
	_command_buffers[!_write_buffer_index].can_write(true);

	_write_buffer_index = !_write_buffer_index;
	return &_command_buffers[!_write_buffer_index];
}

//-------------------------------------------------------------------------------------------
inline u32 allocate_handle() {
	static u32 handle_id = 0;
	return ++handle_id;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline void add_debug_name(T* cmd, const StringHash& name) {
	(void)cmd;
	(void)name;
#ifdef RENDERER_DEBUG
	cmd->name = name;
#endif
}

//-------------------------------------------------------------------------------------------
template <>
inline void add_debug_name(CreateShaderCmd* cmd, const StringHash& name) {
	(void)cmd;
	(void)name;
#ifdef DEBUG
	cmd->name = name;
#endif
}

//-------------------------------------------------------------------------------------------
void delete_resource(u32 handle, renderer::dispatch_function function) {
	u32* cmd = (u32*)renderer::add_command(function, sizeof(handle));
	*cmd = handle;
}

}	// namespace


namespace renderer {
//-------------------------------------------------------------------------------------------
void init(const vec2i& screen_size) {
	g_sysEnv.pRenderer = &_renderer;

	_renderer.Init(screen_size);
	swap_command_buffers();
}

//-------------------------------------------------------------------------------------------
void shutdown() {
	Command_buffer* command_buffer = swap_command_buffers();
	++_frame_id;

	// run one last renderer frame to finish executing whatever update thread generated
	_renderer.Begin();
	command_buffer->execute();
	_renderer.End();

	_renderer.Shutdown();
}

//-------------------------------------------------------------------------------------------
void update() {
	//TODO: this is unsafe, protect it
	Command_buffer* command_buffer = swap_command_buffers();

	_frame_end_sync_point.WaitForCondition();

	++_frame_id;

	if (_active) {
		_renderer.Begin();
		command_buffer->execute();
		_renderer.End();
	}
}

//-------------------------------------------------------------------------------------------
void activate(bool active) {
	_active = active;
}

//-------------------------------------------------------------------------------------------
ThreadsSyncPoint& frame_end_sync_point() {
	return _frame_end_sync_point;
}

//-------------------------------------------------------------------------------------------
void render(const Material& material, const shader::constants_buffer& constants, const MeshResourceHandle& mesh) {
	render(material, constants, mesh->GetVertexDataBuffer(), mesh->GetIndexBuffer(), mesh->GetVertexBinding(), mesh->GetIndicesCount());
}

//-------------------------------------------------------------------------------------------
void render(const Material& material, const shader::constants_buffer& constants, VertexBufferHandle vb, VertexBufferHandle ib, VertexBindingHandle vd, u32 count) {
	DrawContext* context = (DrawContext*)add_command(&::Draw, sizeof(DrawContext) + sizeof(constants) + constants.data_size);
	context->vertexBinding = vd;
	context->indexBuffer = ib;
	context->vertexBuffer = vb;
	context->indicesCount = count;
	context->diffuse = material.diffuse->GetHandle();
	context->normal = material.normal ? material.normal->GetHandle() : TextureHandle(0);
	context->metallic = material.metallic ? material.metallic->GetHandle() : TextureHandle(0);
	context->roughness = material.roughness ? material.roughness->GetHandle() : TextureHandle(0);
	context->shader = material.m_shader;
	context->bDoubleSided = material.m_bDoubleSided;
	context->rendererState = material.m_rendererState;
	context->constants = constants;

	memcpy(context->constants.data, constants.data, constants.data_size);
}

//-------------------------------------------------------------------------------------------
ShaderHandle create_shader(const StringHash& name, eShaderType shader_type, const char* shader_code) {
	const u32 handle = allocate_handle();

	const u32 code_length = (u32)strlen(shader_code) + 1;
	CreateShaderCmd* cmd = (CreateShaderCmd*)add_command(&::CreateShader, sizeof(CreateShaderCmd) + code_length);
	add_debug_name(cmd, name);
	cmd->handle = handle;
	cmd->shaderType = shader_type;
	memcpy(cmd->code, shader_code, code_length);

	return handle;
}

//-------------------------------------------------------------------------------------------
void delete_shader(ShaderHandle shader) {
	delete_resource(shader.m_handle, &::DeleteShader);
}

//-------------------------------------------------------------------------------------------
ShaderProgramHandle create_shader_program(const StringHash& name, ShaderHandle vertex_shader, ShaderHandle pixel_shader) {
	const u32 handle = allocate_handle();

	CreateShaderProgramCmd* cmd = (CreateShaderProgramCmd*)add_command(&::CreateShaderProgram, sizeof(CreateShaderProgramCmd));
	cmd->name = name;
	cmd->shaderHandle = handle;
	cmd->vertexShader = vertex_shader;
	cmd->pixelShader = pixel_shader;

	return handle;
}

//-------------------------------------------------------------------------------------------
void delete_shader_program(ShaderProgramHandle program) {
	delete_resource(program.m_handle, &::DeleteShaderProgram);
}

//-------------------------------------------------------------------------------------------
TextureHandle create_texture(const StringHash& name, const ImageDescription& image, const allocation_info& allocation, const texture_params& params) {
	const u32 handle = allocate_handle();

	CreateTextureCmd* cmd = (CreateTextureCmd*)add_command(&::CreateTexture, sizeof(CreateTextureCmd));
	add_debug_name(cmd, name);
	cmd->allocation = allocation;
	cmd->handle = handle;
	cmd->image = image;
	cmd->params = params;

	return handle;
}

//-------------------------------------------------------------------------------------------
void delete_texture(TextureHandle handle) {
	delete_resource(handle.m_handle, &::DeleteTexture);
}

//-------------------------------------------------------------------------------------------
VertexBufferHandle create_vertex_buffer(const StringHash& name, VertexBufferType type, const void* data, u32 size, VertexBufferUsage usage_hint, const allocation_info* allocation) {
	const u32 handle = allocate_handle();

	CreateVertexBufferCmd* cmd = (CreateVertexBufferCmd*)add_command(&::CreateVertexBuffer, sizeof(CreateVertexBufferCmd));
	add_debug_name(cmd, name);
	cmd->pData = data;
	cmd->handle = handle;
	cmd->type = type;
	cmd->size = size;
	cmd->usageHint = usage_hint;
	if (allocation) {
		cmd->allocation = *allocation;
	} else {
		cmd->allocation.deallocator = nullptr;
	}

	return handle;
}

//-------------------------------------------------------------------------------------------
void delete_vertex_buffer(VertexBufferHandle handle) {
	delete_resource(handle.m_handle, &::DeleteVertexBuffer);
}

//-------------------------------------------------------------------------------------------
VertexBindingHandle create_vertex_binding(const StringHash& name, const VertexDescription& vertex_description, VertexBufferHandle vertex_buffer, VertexBufferHandle index_buffer) {
	//TODO: cache and reuse vertex bindings
	const u32 handle = allocate_handle();

	const u32 vertex_attributes_size = vertex_description.attributesCount * sizeof(*vertex_description.attributes);
	CreateVertexBindingCmd* cmd = (CreateVertexBindingCmd*)add_command(&::CreateVertexBinding, sizeof(CreateVertexBindingCmd) + vertex_attributes_size);
	add_debug_name(cmd, name);
	cmd->handle = handle;
	cmd->vertexDataBuffer = vertex_buffer;
	cmd->indexBuffer = index_buffer;
	memcpy(&cmd->vertexDescription, &vertex_description, sizeof(vertex_description) + vertex_attributes_size);

	return handle;
}

//-------------------------------------------------------------------------------------------
void delete_vertex_binding(VertexBindingHandle handle) {
	delete_resource(handle.m_handle, &::DeleteVertexBinding);
}

//-------------------------------------------------------------------------------------------
void* add_command(dispatch_function function, size_t data_size, u64 key) {
	Command_buffer* command_buffer = &_command_buffers[_write_buffer_index];
	renderer::dispatch_function* data = (renderer::dispatch_function*)command_buffer->allocate(sizeof(renderer::dispatch_function) + data_size, alignof(renderer::dispatch_function));
	*data = function;
	++data;
	
	if (key == (u64)-1) {
		key = ++_current_sort_key;
	}
	command_buffer->add_command(key, data);
	return data;
}

}	//namespace renderer