
#include "ShaderProgram.h"
#include "Debug.h"
#include "Renderer.h"
#include "ShaderParamInfo.h"
#include "GenericAllocator.h"
#include "SystemEnvironment.h"


//-------------------------------------------------------------------------------------------
ShaderProgram::~ShaderProgram() {
	GenericAllocator::Free(m_attributeLocations);
	GenericAllocator::Free(m_uniformLocations);
}

//-------------------------------------------------------------------------------------------
const StringHash& ShaderProgram::name() const {
	return _name;
}

//-------------------------------------------------------------------------------------------
const ShaderParamInfo* ShaderProgram::GetUniformInfo(const StringHash& uniform) const {
	for (u32 i = 0; i < _uniforms_count; ++i) {
		if (m_uniformLocations[i].name == uniform) {
			return &m_uniformLocations[i];
		}
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------
const ShaderParamInfo* ShaderProgram::GetAttributeInfo(const StringHash& attribute) const {
	for (u32 i = 0; i < _attributes_count; ++i) {
		if (m_attributeLocations[i].name == attribute) {
			return &m_attributeLocations[i];
		}
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------
void ShaderProgram::Create(ShaderProgramHandle program)
{
	m_handle = program;

	ASSERT(!m_uniformLocations);
	ASSERT(!m_attributeLocations);

	ProcessUniforms();
	ProcessAttributes();
}

//-------------------------------------------------------------------------------------------
void ShaderProgram::ProcessUniforms()
{
	_uniforms_count = g_sysEnv.pRenderer->GetUniformsCount(m_handle);
	if (!_uniforms_count)
	{
		return;
	}

	m_uniformLocations = (ShaderParamInfo*)GenericAllocator::Alloc(_uniforms_count * sizeof(*m_uniformLocations));
	for (u32 i = 0; i < _uniforms_count; ++i)
	{
		m_uniformLocations[i] = g_sysEnv.pRenderer->GetUniformDescription(m_handle, i);
	}
}

//-------------------------------------------------------------------------------------------
void ShaderProgram::ProcessAttributes()
{
	_attributes_count = g_sysEnv.pRenderer->GetAttributesCount(m_handle);

	m_attributeLocations = (ShaderParamInfo*)GenericAllocator::Alloc(_attributes_count * sizeof(*m_attributeLocations));
	for (u32 i = 0; i < _attributes_count; ++i)
	{
		m_attributeLocations[i] = g_sysEnv.pRenderer->GetAttributeDescription(m_handle, i);
	}
}