
#include "RendererDispatch.h"
#include "HashTable.h"
#include "ShaderProgram.h"
#include "SystemEnvironment.h"


typedef HashTable<u32, u32> LUT;
static LUT resourcesLUT;


namespace {
//-------------------------------------------------------------------------------------------
u32 lut_pop_handle(const void* id_ptr) {
	u32* pHandle = (u32*)id_ptr;
	LUT::Iterator it = resourcesLUT.FindIterator(*pHandle);
	ASSERT(it != resourcesLUT.GetEnd());

	const u32 id = *it.GetValue();
	resourcesLUT.Remove(it);

	return id;
}

//-------------------------------------------------------------------------------------------
template<HandleType type>
inline void lut_patch_handle(RendererHandle<type>& handle) {
	u32* h = resourcesLUT.Find(handle.m_handle);
	ASSERT(h);
	handle.m_handle = *h;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline const StringHash* get_debug_name(const T* cmd) {
	(void)cmd;
	const StringHash* name = nullptr;
#ifdef RENDERER_DEBUG
	name = &cmd->name;
#endif
	return name;
}
    
//-------------------------------------------------------------------------------------------
template <>
inline const StringHash* get_debug_name(const CreateShaderCmd* cmd) {
    (void)cmd;
    const StringHash* name = nullptr;
#ifdef DEBUG
    name = &cmd->name;
#endif
    return name;
}

}	// namespace


//-------------------------------------------------------------------------------------------
void CreateShader(const void* pData) {
	CreateShaderCmd* pCmd = (CreateShaderCmd*)pData;

	ShaderHandle rendererHandle = g_sysEnv.pRenderer->CreateShader(pCmd->shaderType, pCmd->code, get_debug_name(pCmd));
	resourcesLUT.Insert(pCmd->handle.m_handle, rendererHandle.m_handle);
}

//-------------------------------------------------------------------------------------------
void DeleteShader(const void* handle_ptr) {
	g_sysEnv.pRenderer->DeleteShader(lut_pop_handle(handle_ptr));
}

//-------------------------------------------------------------------------------------------
void CreateShaderProgram(const void* pData) {
	CreateShaderProgramCmd* pCmd = (CreateShaderProgramCmd*)pData;

	lut_patch_handle(pCmd->vertexShader);
	lut_patch_handle(pCmd->pixelShader);

	const ShaderProgramHandle handle = g_sysEnv.pRenderer->CreateShaderProgram(pCmd->name, pCmd->vertexShader, pCmd->pixelShader);
	resourcesLUT.Insert(pCmd->shaderHandle.m_handle, handle.m_handle);
}

//-------------------------------------------------------------------------------------------
void DeleteShaderProgram(const void* handle_ptr) {
	g_sysEnv.pRenderer->DeleteShaderProgram(lut_pop_handle(handle_ptr));
}

//-------------------------------------------------------------------------------------------
void CreateTexture(const void* pData) {
	CreateTextureCmd* pCmd = (CreateTextureCmd*)pData;
	const TextureHandle handle = g_sysEnv.pRenderer->CreateTexture(pCmd->image, get_debug_name(pCmd), pCmd->params);
	resourcesLUT.Insert(pCmd->handle.m_handle, handle.m_handle);

	if (pCmd->allocation.deallocator) {
		pCmd->allocation.deallocator(pCmd->allocation.ptr, pCmd->allocation.size);
	}
}

//-------------------------------------------------------------------------------------------
void DeleteTexture(const void* handle_ptr) {
	g_sysEnv.pRenderer->DeleteTexture(lut_pop_handle(handle_ptr));
}

//-------------------------------------------------------------------------------------------
void CreateVertexBuffer(const void* pData) {
	CreateVertexBufferCmd* pCmd = (CreateVertexBufferCmd*)pData;
	const VertexBufferHandle handle = g_sysEnv.pRenderer->CreateVertexBuffer(pCmd->type, get_debug_name(pCmd));
	resourcesLUT.Insert(pCmd->handle.m_handle, handle.m_handle);

	g_sysEnv.pRenderer->UploadVertexBufferData(handle, pCmd->type, pCmd->pData, pCmd->size, pCmd->usageHint);

	if (pCmd->allocation.deallocator) {
		pCmd->allocation.deallocator(pCmd->allocation.ptr, pCmd->allocation.size);
	}
}

//-------------------------------------------------------------------------------------------
void DeleteVertexBuffer(const void* handle_ptr) {
	g_sysEnv.pRenderer->DeleteVertexBuffer(lut_pop_handle(handle_ptr));
}

//-------------------------------------------------------------------------------------------
void CreateVertexBinding(const void* pData) {
	CreateVertexBindingCmd* pCmd = (CreateVertexBindingCmd*)pData;

	lut_patch_handle(pCmd->vertexDataBuffer);
	if (pCmd->indexBuffer) {
		lut_patch_handle(pCmd->indexBuffer);
	}

	const VertexBindingHandle handle = g_sysEnv.pRenderer->CreateVertexBinding(pCmd->vertexDescription, pCmd->vertexDataBuffer
																			   , pCmd->indexBuffer ? pCmd->indexBuffer : VertexBufferHandle(), get_debug_name(pCmd));
	resourcesLUT.Insert(pCmd->handle.m_handle, handle.m_handle);
}

//-------------------------------------------------------------------------------------------
void DeleteVertexBinding(const void* handle_ptr) {
	g_sysEnv.pRenderer->DeleteVertexBinding(lut_pop_handle(handle_ptr));
}

//-------------------------------------------------------------------------------------------
void Draw(const void* pData) {
	DrawContext* pContext = (DrawContext*)pData;

	lut_patch_handle(pContext->vertexBinding);
	lut_patch_handle(pContext->vertexBuffer);
	lut_patch_handle(pContext->indexBuffer);

	TextureHandle* texture = &pContext->diffuse;
	for (u32 i = 0; i < 4; ++i, ++texture) {
		if (texture->m_handle) {
			lut_patch_handle(*texture);
		}
	}

	lut_patch_handle(pContext->shader);

	g_sysEnv.pRenderer->Draw(*pContext);
}