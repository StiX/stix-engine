
#include "renderer_util.h"
#include "memory.h"
#include "Vector3D.h"
#include "Renderer.h"
#include "WriteStream.h"
#include "VertexDescription.h"


namespace {
//-------------------------------------------------------------------------------------------
void free_mem(void* ptr, size_t) {
	free(ptr);
}
}

//-------------------------------------------------------------------------------------------
void renderer::render_text_2d(const char* text, font_resource_handle font, Material &material, const mat4& view_proj, const vec3f& position, const vec4f& color) {
	struct vertex {
		float x, y, z;
		float u, v;
	};

	const u32 len = (u32)strlen(text);
	u32 vertex_data_size = sizeof(vertex) * 4 * len;
	u32 index_data_size = sizeof(u16) * 6 * len;
	const u32 data_size = vertex_data_size + index_data_size;
	void* vertex_data = malloc(data_size);
	WriteStream stream(vertex_data, data_size);

	vec3f pos = position;
	float line_height = position.y;

	u32 chars_count = 0;

	for (u32 i = 0; i < len; ++i) {
		/*
		 1*----*4
		  |\   |
		  | \  |
		  |  \ |
		  |   \|
		 2*----*3
		*/

		if (text[i] == ' ') {
			pos.x += font->get_glyph(text[i])->x_advance;
			continue;
		} else if (text[i] == '\n') {
			line_height -= font->line_height();
			pos.y = line_height;
			pos.x = position.x;
			continue;
		}

		const resource_font::glyph* glyph = font->get_glyph(text[i]);
		
		pos.x += glyph->x_offset;
		pos.y -= glyph->y_offset;
		stream.Write(pos);
		stream.Write(glyph->u_bl);
		stream.Write(glyph->v_tr);

		pos.y -= glyph->height;
		stream.Write(pos);
		stream.Write(glyph->u_bl);
		stream.Write(glyph->v_bl);

		pos.x += glyph->width;
		stream.Write(pos);
		stream.Write(glyph->u_tr);
		stream.Write(glyph->v_bl);

		pos.y += glyph->height;
		stream.Write(pos);
		stream.Write(glyph->u_tr);
		stream.Write(glyph->v_tr);

		pos.x -= glyph->width + glyph->x_offset;
		pos.x += glyph->x_advance;
		pos.y = line_height;

		++chars_count;
	}

	vertex_data_size = chars_count * sizeof(vertex) * 4;
	index_data_size = chars_count * sizeof(u16) * 6;

	void* index_data = stream.Get(0);
	u16 index = 0;
	for (u32 i = 0; i < chars_count; ++i) {
		stream.Write(index);
		stream.Write(++index);
		stream.Write(++index);
		
		stream.Write(++index);
		stream.Write(index -= 3);
		stream.Write(index += 2);

		index += 2;
	}

	VertexDescription vd;
	vd.AddAttribute(VertexAttribute::Position, VertexAttribute::Float, 3);
	vd.AddAttribute(VertexAttribute::UV, VertexAttribute::Float, 2);

	const allocation_info allocation = { &free_mem, vertex_data, data_size };
	VertexBufferHandle vb = renderer::create_vertex_buffer(text, VertexBufferType::VertexData, vertex_data, vertex_data_size, VertexBufferUsage::DynamicDraw, nullptr);
	VertexBufferHandle ib = renderer::create_vertex_buffer(text, VertexBufferType::IndexData, index_data, index_data_size, VertexBufferUsage::DynamicDraw, &allocation);
	VertexBindingHandle vertexBinding = renderer::create_vertex_binding(text, vd, vb, ib);

	shader::constants_buffer* constants = shader::allocate_constants(2);
	shader::constants_writer writer(*constants);
	writer.append("mvp", view_proj);
	writer.append("color", color);

	renderer::render(material, *constants, vb, ib, vertexBinding, 6 * chars_count);
	shader::free_constants(constants);

	renderer::delete_vertex_buffer(vb);
	renderer::delete_vertex_buffer(ib);
	renderer::delete_vertex_binding(vertexBinding);
}