
#pragma once

#include <stddef.h>
#include "Types.h"

class ThreadEvent;

void	UpdateThreadSpawn(ThreadEvent* on_init_done_event);
void	UpdateThreadRequestTermination();
void	UpdateThreadWaitToTerminate();
bool	IsUpdateThread(size_t threadId);