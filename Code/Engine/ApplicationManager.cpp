
#include "ApplicationManager.h"
#include "Timer.h"
#include "input.h"
#include "System.h"
#include "Renderer.h"
#include "FSWatchDog.h"
#include "FileSystem.h"
#include "ResourceManager.h"
#include "SystemEnvironment.h"


System			g_system;
FSWatchDog		g_FSWatchDog;
ResourceManager	g_resourceManager;


//-------------------------------------------------------------------------------------------
ApplicationManager::ApplicationManager()
	: m_pApplication(g_pApplication), m_frameId(0)
{
}

//-------------------------------------------------------------------------------------------
ApplicationManager::~ApplicationManager()
{
	ASSERT(!g_sysEnv.pApplication, "You have to call ShutDown before destroying the ApplicationManager");
}

//-------------------------------------------------------------------------------------------
void ApplicationManager::Init()
{
	InitSubsystems();

	m_timeStamp = timer::seconds();
	m_startupTime = m_timeStamp;

	m_pApplication->Init();
}

//-------------------------------------------------------------------------------------------
void ApplicationManager::Run()
{
	++m_frameId;

	Update();
	Render();
}

//-------------------------------------------------------------------------------------------
void ApplicationManager::ShutDown()
{
	m_pApplication->Shutdown();

	ShutdownSubsystems();

	// Clean pointers to all subsystems, except renderer
	Renderer* pRenderer = g_sysEnv.pRenderer;
	memset(&g_sysEnv, 0, sizeof(g_sysEnv));
	g_sysEnv.pRenderer = pRenderer;
}

//-------------------------------------------------------------------------------------------
void ApplicationManager::Update()
{
	const float kDeltaTime = CalculateFrameDeltaTime();

	UpdateSubsystems(kDeltaTime);
	
	m_pApplication->Update(kDeltaTime);
}

//-------------------------------------------------------------------------------------------
void ApplicationManager::Render()
{
	m_pApplication->Render();
}

//-------------------------------------------------------------------------------------------
float ApplicationManager::CalculateFrameDeltaTime()
{
	double time = timer::seconds();
	float deltaTime = (float)(time - m_timeStamp);
	const float kMaximumDeltaTime = 1.0f / 5.0f;
	deltaTime = deltaTime < kMaximumDeltaTime ? deltaTime : kMaximumDeltaTime;

	m_timeStamp = time;
	m_applicationTime = (float)(time - m_startupTime);

	m_fps = 1.0f / deltaTime;

	return deltaTime;
}

//-------------------------------------------------------------------------------------------
void ApplicationManager::InitSubsystems()
{
	timer::init();
	VirtualPageAllocator::Init();

	g_sysEnv.pSystem		= &g_system;
	g_sysEnv.pAppManager	= this;
	g_sysEnv.pFSWatchDog	= &g_FSWatchDog;
	g_sysEnv.pResManager	= &g_resourceManager;
	g_sysEnv.pApplication	= m_pApplication;

	g_FSWatchDog.Init(2.0f);
	g_resourceManager.Init(&g_FSWatchDog);
}

//-------------------------------------------------------------------------------------------
void ApplicationManager::ShutdownSubsystems()
{
	g_resourceManager.ShutDown();
	g_FSWatchDog.Shutdown();
}

//-------------------------------------------------------------------------------------------
void ApplicationManager::UpdateSubsystems(float deltaTime)
{
	input::update();
	g_FSWatchDog.Update(deltaTime);
}