
#pragma once

#include <type_traits>


template <class T>
class ResourceHandle
{
public:
					ResourceHandle();
					ResourceHandle(const T* pResource);
					ResourceHandle(const ResourceHandle& other);
					~ResourceHandle();

	ResourceHandle&	operator=(const ResourceHandle& other);

	const T&		operator*() const;
	const T*		operator->() const;

					operator bool() const;

protected:
private:
	const T*		m_pResourceHandle;
};





//-------------------------------------------------------------------------------------------
template <class T>
inline ResourceHandle<T>::ResourceHandle()
	: m_pResourceHandle(nullptr)
{
}

//-------------------------------------------------------------------------------------------
template <class T>
inline ResourceHandle<T>::ResourceHandle(const T* pResource)
	: m_pResourceHandle(pResource)
{
	if (m_pResourceHandle)
	{
		m_pResourceHandle->Grab();
	}
}

//-------------------------------------------------------------------------------------------
template <class T>
inline ResourceHandle<T>::ResourceHandle(const ResourceHandle& other)
	: m_pResourceHandle(other.m_pResourceHandle)
{
	if (m_pResourceHandle)
	{
		m_pResourceHandle->Grab();
	}
}

//-------------------------------------------------------------------------------------------
template <class T>
inline ResourceHandle<T>::~ResourceHandle()
{
	static_assert(std::is_base_of<IResource, T>::value, "Templated type is not a child of IResource");

	if (m_pResourceHandle)
	{
		m_pResourceHandle->Release();
	}
}

//-------------------------------------------------------------------------------------------
template <class T>
inline ResourceHandle<T>& ResourceHandle<T>::operator=(const ResourceHandle& other)
{
	if (m_pResourceHandle)
	{
		m_pResourceHandle->Release();
	}
	
	m_pResourceHandle = other.m_pResourceHandle;
	
	if (m_pResourceHandle)
	{
		m_pResourceHandle->Grab();
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline const T& ResourceHandle<T>::operator*() const
{
	return *m_pResourceHandle;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline const T* ResourceHandle<T>::operator->() const
{
	return m_pResourceHandle;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline ResourceHandle<T>::operator bool() const {
	return m_pResourceHandle != nullptr;
}