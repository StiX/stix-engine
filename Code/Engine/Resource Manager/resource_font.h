
#pragma once

#include "IResource.h"
#include "ResourceTexture.h"


class resource_font : public IResource {
public:
	struct glyph {
		float				u_bl;
		float				v_bl;
		float				u_tr;
		float				v_tr;
		u8					width;
		u8					height;
		s8					x_offset;
		s8					y_offset;
		s8					x_advance;
	};

							resource_font(const StringHash& name, const StringHash& type);
							~resource_font();

	bool					Load(const allocation_info& allocation, void* user_data = nullptr);
	void					texture(const ResourceTexture* texture);
	TextureResourceHandle	texture() const;
	const char*				texture_filename() const;

	const glyph*			get_glyph(char c) const;
	u8						line_height() const;

protected:
private:
	const ResourceTexture*	_texture;
	mutable const char*		_texture_filename;	//TODO: fix it in the future, since now it points to the data that will be freed on a next frame
	glyph					_chars[95];	//95 printable commonly used chars
	u8						_line_height;
};


extern template class ResourceHandle<resource_font>;
typedef ResourceHandle<resource_font> font_resource_handle;