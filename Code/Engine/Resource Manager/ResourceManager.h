
#pragma once

#include "Renderer.h"
#include "IResource.h"
#include "HashTable.h"
#include "StringHash.h"
#include "ZipArchive.h"
#include "ResourceMesh.h"
#include "resource_font.h"
#include "PoolAllocator.h"
#include "ResourceTexture.h"

class FSWatchDog;


class ResourceManager
{
public:
							ResourceManager(u32 initialResourcesCapacity = 1024);
							~ResourceManager();

	void					Init(FSWatchDog* pFSWatchDog);
	void					ShutDown();

	void					ReloadFile(const char* filePath);

	MeshResourceHandle		LoadMesh(const char* filePath);
	TextureResourceHandle	LoadTexture(const char* filePath);
	font_resource_handle	load_font(const char* filePath);

	void					RegisterResource(IResource* resource);
	MeshResourceHandle		GetMesh(const StringHash& res_name);

protected:
private:
	typedef HashTable<StringHash, IResource*> ResourcesHashTable;
	typedef HashTable<StringHash, ResourcesHashTable*> res_type_to_table_map;

	FSWatchDog*				m_pFSWatchDog;
	const char*				m_resourceDir;
	index_t					m_observerHandle;
	PoolAllocator			m_resourcesPool;
	ZipArchive				m_resourcesArchive;
	res_type_to_table_map	_type_to_table_map;
	ResourcesHashTable		_textures_table;
	ResourcesHashTable		_meshes_table;
	ResourcesHashTable		_fonts_table;

	template<class T>
	T*						LoadResource(const char* filePath, void* user_data = nullptr);
	void					unload_resource(IResource* resource);

	void					populate_type_map();
	ResourcesHashTable*		resource_table(const StringHash& type);
};





//-------------------------------------------------------------------------------------------
inline ResourceManager::ResourceManager(u32 initialResourcesCapacity)
	: m_pFSWatchDog(nullptr), m_resourceDir("game data/resources/"), m_observerHandle(kInvalidIndex)
	, m_resourcesPool(4096 * 2, 0), _textures_table(initialResourcesCapacity), _meshes_table(initialResourcesCapacity)
{
}