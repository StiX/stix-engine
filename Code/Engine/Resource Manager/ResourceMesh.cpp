
#include "ResourceMesh.h"
#include "File.h"
#include "memory.h"
#include "Renderer.h"
#include "ReadStream.h"
#include "ResourceHandle.h"
#include "VertexDescription.h"


//-------------------------------------------------------------------------------------------
ResourceMesh::~ResourceMesh() {
	renderer::delete_vertex_buffer(m_indexBuffer);
	renderer::delete_vertex_buffer(m_vertexDataBuffer);

	renderer::delete_vertex_binding(m_vertexBinding);
}

//-------------------------------------------------------------------------------------------
bool ResourceMesh::Load(const allocation_info& allocation, void* /*user_data*/) {
	ReadStream readStream(allocation.ptr, (u32)allocation.size);
	const u32 kVerticesCount = readStream.Read<u32>();
	m_indicesCount = readStream.Read<u32>();

	VertexDescription vd;
	vd.serialize(readStream);

	const u32 kVertexDataSize = kVerticesCount * vd.vertexSize;
	const allocation_info* vb_alloc = m_indicesCount ? nullptr : &allocation;
	m_vertexDataBuffer = renderer::create_vertex_buffer(GetName(), VertexBufferType::VertexData, readStream.Read(kVertexDataSize),
														kVertexDataSize, VertexBufferUsage::StaticDraw, vb_alloc);

	if (m_indicesCount) {
		const u32 kIndexDataSize = m_indicesCount * sizeof(u16);
		m_indexBuffer = renderer::create_vertex_buffer(GetName(), VertexBufferType::IndexData, readStream.Read(kIndexDataSize), kIndexDataSize,
													   VertexBufferUsage::StaticDraw, &allocation);
	}

	m_vertexBinding = renderer::create_vertex_binding(GetName(), vd, m_vertexDataBuffer, m_indexBuffer);

	return true;
}


// explicit template instantiation
template class ResourceHandle<ResourceMesh>;