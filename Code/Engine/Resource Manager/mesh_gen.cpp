
#include "mesh_gen.h"
#include "memory.h"
#include "math_lib.h"
#include "WriteStream.h"
#include "ResourceManager.h"
#include "SystemEnvironment.h"
#include "VertexDescription.h"


namespace {
//-------------------------------------------------------------------------------------------
void free_mem(void* ptr, size_t) {
	free(ptr);
}
}

//-------------------------------------------------------------------------------------------
MeshResourceHandle generate_sphere_mesh(u16 segments) {
	const StringHash res_name("Sphere");
	if (MeshResourceHandle mesh = g_sysEnv.pResManager->GetMesh(res_name)) {
		return mesh;
	}

	VertexDescription vd;
	vd.AddAttribute(VertexAttribute::Position, VertexAttribute::Float, 3);
	vd.AddAttribute(VertexAttribute::Normal, VertexAttribute::Float, 3);
	vd.AddAttribute(VertexAttribute::UV, VertexAttribute::Float, 2);

	const u32 y_segments = (u32)math::round((float)segments / 2.0f);
	const u32 vertices_count = (segments + 1) * (y_segments - 1) + segments * 2;
	const u32 indices_count = segments * 2 * 3 + (segments + 1) * (y_segments - 2) * 6;
	const u32 vertex_size = 32;
	const size_t stream_size = sizeof(u32) * 2 + vd.serialization_size() + vertex_size * vertices_count + sizeof(u16) * indices_count;
	void* data = malloc(stream_size);

	WriteStream stream(data, (u32)stream_size);
	stream.Write(vertices_count);
	stream.Write(indices_count);
	
	vd.serialize(stream);

	const float radius = 1.0f;
	const float delta = 1.0f / (float)segments;
	const float delta_y = 1.0f / (float)y_segments;

	//North pole
	for (u32 x = 0; x < segments; ++x) {
		stream.Write(0.0f);	stream.Write(radius);	stream.Write(0.0f);
		stream.Write(0.0f);	stream.Write(1.0f);		stream.Write(0.0f);
		
		stream.Write(delta * x + delta / 2.0f);
		stream.Write(0.0f);
	}

	for (u32 y = 1; y < y_segments; ++y) {
		for (u32 x = 0; x <= segments; ++x) {
			const float dx = delta * x;
			const float dy = delta_y * y;
			const float nx = math::cos(2 * math::pi * dx) * math::sin(math::pi * dy);
			const float ny = math::cos(math::pi * dy);
			const float nz = math::sin(2 * math::pi * dx) * math::sin(math::pi * dy);

			stream.Write(nx * radius);	stream.Write(ny * radius);	stream.Write(nz * radius);
			stream.Write(nx);			stream.Write(ny);			stream.Write(nz);
			stream.Write(dx);			stream.Write(dy);
		}
	}

	//South pole
	for (u32 x = 0; x < segments; ++x) {
		stream.Write(0.0f);	stream.Write(-radius);	stream.Write(0.0f);
		stream.Write(0.0f);	stream.Write(-1.0f);	stream.Write(0.0f);
		
		stream.Write(delta * x + delta / 2.0f);
		stream.Write(1.0f);
	}


	// most north latitude
	for (u16 x = 0; x < segments; ++x) {
		u16 index = x;
		stream.Write(index);
		
		index = x + segments + 1;
		stream.Write(index);

		index = x + segments;
		stream.Write(index);
	}

	for (u16 y = 1; y < y_segments - 1; ++y) {
		const u16 upper_row = y * (segments + 1) - 1;
		const u16 lower_row = upper_row + segments + 1;

		for (u16 x = 0; x <= segments; ++x) {
			/*	v1    v2
				  *---*
				  |\  |
				  | \ |
				  |  \|
				  *---*
				v3    v4  */

			const u16 v1 = upper_row + x;
			const u16 v2 = v1 + 1;
			const u16 v3 = lower_row + x;
			const u16 v4 = v3 + 1;

			stream.Write(v1);
			stream.Write(v4);
			stream.Write(v3);

			stream.Write(v1);
			stream.Write(v2);
			stream.Write(v4);
		}
	}

	// south most latitude
	for (u16 x = 0; x < segments; ++x) {
		const u16 pole = (u16)vertices_count - segments + x;
		stream.Write(pole);

		u16 index = pole - segments - 1;
		stream.Write(index);

		index = pole - segments;
		stream.Write(index);
	}

	allocation_info allocation = {&free_mem, data, stream_size};
	ResourceMesh* resource_mesh = new ResourceMesh(res_name, "smf");
	resource_mesh->Load(allocation);
	
	g_sysEnv.pResManager->RegisterResource(resource_mesh);

	return resource_mesh;
}