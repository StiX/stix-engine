
#pragma once

#include "Types.h"
#include "ResourceMesh.h"


MeshResourceHandle generate_sphere_mesh(u16 segments);