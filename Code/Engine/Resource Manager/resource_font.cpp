
#include "resource_font.h"
#include "memory.h"
#include "ReadStream.h"


namespace {
//-------------------------------------------------------------------------------------------
u32 make_fourcc(char a, char b, char c, char d) {
	return ((u32)(a) | (u32)((b) << 8) | (u32)((c) << 16) | (u32)((d) << 24));
}
}


//-------------------------------------------------------------------------------------------
resource_font::resource_font(const StringHash& name, const StringHash& type)
	: IResource(name, type), _texture_filename(nullptr), _line_height(0)
{
}

//-------------------------------------------------------------------------------------------
resource_font::~resource_font() {

}

//-------------------------------------------------------------------------------------------
bool resource_font::Load(const allocation_info& allocation, void* /*user_data*/) {
	// http://www.angelcode.com/products/bmfont/doc/file_format.html#bin
	const u32 bmf_header = make_fourcc('B', 'M', 'F', 3);

	ReadStream stream(allocation.ptr, (u32)allocation.size);
	if (stream.Read<u32>() != bmf_header) {
		return false;
	}

	u8 block_id = stream.Read<u8>();
	ASSERT(block_id == 1);

	u32 block_size = stream.Read<u32>();
	stream.Read(block_size);

	block_id = stream.Read<u8>();
	ASSERT(block_id == 2);

	block_size = stream.Read<u32>();
	_line_height = (u8)stream.Read<u16>();
	stream.Read<u16>();
	float scale_w = (float)stream.Read<u16>();
	float scale_h = (float)stream.Read<u16>();
	u16 pages = stream.Read<u16>(); (void)pages;
	ASSERT(pages == 1);

	stream.Read(5);

	block_id = stream.Read<u8>();
	ASSERT(block_id == 3);

	block_size = stream.Read<u32>();
	_texture_filename = (char*)stream.Read(block_size);

	
	block_id = stream.Read<u8>();
	ASSERT(block_id == 4);

	block_size = stream.Read<u32>();
	const u32 chars_count = block_size / 20;
	for (u32 i = 0; i < chars_count; ++i) {
		const char c = (char)stream.Read<u32>();
		ASSERT(c >= 32);
		ASSERT(c <= 126);
		glyph* g = &_chars[c - 32];

		const float x = (float)stream.Read<u16>();
		const float y = (float)stream.Read<u16>();
		const u16 w = stream.Read<u16>();
		const u16 h = stream.Read<u16>();

		g->u_bl = x / scale_w;
		g->v_bl = 1.0f - ((y + h) / scale_h);
		g->u_tr = (x + w) / scale_w;
		g->v_tr = 1.0f - (y / scale_h);

		g->width = (u8)w;
		g->height = (u8)h;

		g->x_offset = (s8)stream.Read<s16>();
		g->y_offset = (s8)stream.Read<s16>();
		g->x_advance = (s8)stream.Read<s16>();

		stream.Read(2);
	}

	return true;
}

//-------------------------------------------------------------------------------------------
void resource_font::texture(const ResourceTexture* texture) {
	_texture = texture;
}

//-------------------------------------------------------------------------------------------
TextureResourceHandle resource_font::texture() const {
	return _texture;
}

//-------------------------------------------------------------------------------------------
const char* resource_font::texture_filename() const {
	const char* filename = _texture_filename;
	_texture_filename = nullptr;
	return filename;
}

//-------------------------------------------------------------------------------------------
const resource_font::glyph* resource_font::get_glyph(char c) const {
	ASSERT(c >= 32);
	ASSERT(c <= 126);
	return &_chars[c - 32];
}

//-------------------------------------------------------------------------------------------
u8 resource_font::line_height() const {
	return _line_height;
}


// explicit template instantiation
template class ResourceHandle<resource_font>;