
#pragma once

#include "IResource.h"
#include "RendererTypes.h"
#include "ResourceHandle.h"


class ResourceMesh : public IResource
{
public:
						ResourceMesh(const StringHash& name, const StringHash& type);
	virtual				~ResourceMesh();

	virtual bool		Load(const allocation_info& allocation, void* user_data = nullptr) override;

	u32					GetIndicesCount() const;
	VertexBufferHandle	GetIndexBuffer() const;
	VertexBufferHandle	GetVertexDataBuffer() const;

	VertexBindingHandle	GetVertexBinding() const;

protected:
private:
	VertexBufferHandle	m_indexBuffer;
	VertexBufferHandle	m_vertexDataBuffer;
	VertexBindingHandle	m_vertexBinding;
	u32					m_indicesCount;
};

extern template class ResourceHandle<ResourceMesh>;
typedef ResourceHandle<ResourceMesh> MeshResourceHandle;





//-------------------------------------------------------------------------------------------
inline ResourceMesh::ResourceMesh(const StringHash& name, const StringHash& type)
	: IResource(name, type), m_indicesCount(0)
{
}

//-------------------------------------------------------------------------------------------
inline u32 ResourceMesh::GetIndicesCount() const
{
	return m_indicesCount;
}

//-------------------------------------------------------------------------------------------
inline VertexBufferHandle ResourceMesh::GetIndexBuffer() const
{
	return m_indexBuffer;
}

//-------------------------------------------------------------------------------------------
inline VertexBufferHandle ResourceMesh::GetVertexDataBuffer() const
{
	return m_vertexDataBuffer;
}

//-------------------------------------------------------------------------------------------
inline VertexBindingHandle ResourceMesh::GetVertexBinding() const
{
	return m_vertexBinding;
}