
#include "ResourceManager.h"
#include <cstring>
#include "Str.h"
#include "Log.h"
#include "System.h"
#include "memory.h"
#include "FileSystem.h"
#include "FSWatchDog.h"
#include "SystemUtil.h"
#include "ResourceMesh.h"
#include "SystemEnvironment.h"


//-------------------------------------------------------------------------------------------
static void CheckPathSeparators(const char* filePath)
{
	(void)filePath;

#ifdef DEBUG
	while (*filePath)
	{
		ASSERT(*filePath != '\\');

		++filePath;
	}
#endif
}

//-------------------------------------------------------------------------------------------
static const char* ExtractFileName(const char* filePath, size_t& length)
{
	const size_t kStrLen = strlen(filePath);

	length = kStrLen;

	for (ptrdiff_t i = kStrLen - 1; i >= 0; --i)
	{
		if (filePath[i] == '.')
		{
			length = i;
		}
		else if (filePath[i] == '/' || filePath[i] == '\\')
		{
			length -= i + 1;
			return &filePath[i + 1];
		}
	}

	return filePath;
}

//-------------------------------------------------------------------------------------------
inline void extract_resource_name(const char* file_path, StringHash& out_name, StringHash& out_type) {
	size_t name_length;
	const char* resourceName = ExtractFileName(file_path, name_length);
	out_name = StringHash(resourceName, name_length);
	out_type = StringHash(resourceName + name_length + 1);
}

//-------------------------------------------------------------------------------------------
static File OpenResourceFile(const char* filePath)
{
	const char platformSuffix[] =
#ifdef __PLATFORM_DESKTOP__ 
		".pc";
#elif defined (__iOS__)
		".ios";
#elif defined (__ANDROID__)
		".android";
#else
#	error "Platform suffix is not defined"
#endif

	CheckPathSeparators(filePath);

	const s16 kStrLen = (s16)strlen(filePath);

	for (s16 i = kStrLen - 1; i >= 0; --i)
	{
		if (filePath[i] == '.')
		{
			String str;
			str.Reserve(kStrLen + sizeof(platformSuffix));
			str.CopyString(filePath, i);

			str += platformSuffix;
			str += &filePath[i];

			if (FileExists(str.c_str()))
			{
				return OpenFile(str.c_str(), F_ACCESS_READ, F_SEQUENTIAL_ACCESS_HINT);
			}

			break;
		}
		else if (filePath[i] == '\\' || filePath[i] == '/')
		{
			break;
		}
	}

	return OpenFile(filePath, F_ACCESS_READ, F_SEQUENTIAL_ACCESS_HINT);
}

//-------------------------------------------------------------------------------------------
static bool LoadResource(IResource* pResource, const char* filePath, const ZipArchive& resourcesArchive, void* user_data)
{
#ifdef __ANDROID__
	log_disable();
#endif
	File file = OpenResourceFile(filePath);
#ifdef __ANDROID__
	log_enable();
#endif

	ZipFile zipFile;
	void* pMapping = nullptr;
	u32 fileSize = 0;
	
	if (file.IsOpened())
	{
		pMapping = file.MapFile(F_MAPPING_READ);
		fileSize = (u32)file.GetSize();
	}
	else
	{
		const char* rootFolder =
#ifdef __ANDROID__
			"assets/";
#else
			"";
#endif
		String archPath(rootFolder);
		archPath += filePath;
		zipFile = resourcesArchive.FindFile(archPath.c_str());
		if (zipFile.fileIndex != -1)
		{
			pMapping = resourcesArchive.MapFile(zipFile);
			fileSize = zipFile.size;
		}
		else
		{
			log_warning("Resource Manager", "Cannot locate file %s", filePath);
			return false;
		}
	}
	
	allocation_info allocation = {&UnmapFile, pMapping, fileSize};

	// force load file into memory to avoid i\o stalls in renderer thread
	for (u32 i = 0; i < fileSize; i += g_sysEnv.pSystem->GetVirtualPageSize()) {
		volatile u8* dummy = (u8*)pMapping + i;
		*dummy;
	}

	log_set_error_context(&pResource->GetName());
	const bool result = pResource->Load(allocation, user_data);
	log_set_error_context(nullptr);
	if (!result) {
		log_error("Resource Manager", "Cannot load resource %s", filePath);
	}

	return result;
}

//-------------------------------------------------------------------------------------------
void FSWatchDogCallback(const char* filePath, FSWatchDog::eEventType eventType, void* pUserData)
{
	if (eventType == FSWatchDog::eFileModified)
	{
		((ResourceManager*)pUserData)->ReloadFile(filePath);
	}
}


//-------------------------------------------------------------------------------------------
ResourceManager::~ResourceManager()
{
#ifdef DEBUG
	res_type_to_table_map::Iterator it = _type_to_table_map.GetFirstElement();
	while (it) {
		ResourcesHashTable* table = it.GetValue();
		ASSERT(table->GetSize() == 0, "You have to call Shutdown before destroying the ResourceManager");

		++it;
	}
#endif
}

//-------------------------------------------------------------------------------------------
void ResourceManager::Init(FSWatchDog* pFSWatchDog)
{
	m_pFSWatchDog = pFSWatchDog;

	String path;
	path.Reserve(MAX_PATH);

	path = GetWorkDir();
	path += m_resourceDir;

	if (m_pFSWatchDog)
	{
		m_observerHandle = m_pFSWatchDog->AddObserver(path.c_str(), true, &FSWatchDogCallback, this);
	}

#ifdef __ANDROID__
	m_resourcesArchive.OpenArchive(GetExecutableFilePath());
#endif

	populate_type_map();
}

//-------------------------------------------------------------------------------------------
void ResourceManager::ShutDown()
{
	res_type_to_table_map::Iterator it = _type_to_table_map.GetFirstElement();
	while (it) {
		ResourcesHashTable* table = it.GetValue();
		ResourcesHashTable::Iterator res = table->GetFirstElement();
		while (res) {
			IResource* resource = res.GetValue();
			ASSERT(resource->GetReferenceCount() == 1, "Resource `%s` wasn't released", resource->GetName().c_str());

			table->Remove(res);
			resource->Release();
			m_resourcesPool.Delete(resource);

			++res;
		}

		++it;
	}

	if (m_pFSWatchDog)
	{
		m_pFSWatchDog->RemoveObserver(m_observerHandle);
	}
}

//-------------------------------------------------------------------------------------------
void ResourceManager::ReloadFile(const char* filePath)
{
	StringHash resource_name;
	StringHash resource_type;
	extract_resource_name(filePath, resource_name, resource_type);

	ResourcesHashTable* table = resource_table(resource_type);
	if (IResource* pResource = table->Find(resource_name))
	{
		log_message("Resource Manager", "Reloading modified file %s", filePath);
		
		String path;
		path.Reserve(MAX_PATH);
		path = m_resourceDir;
		path += filePath;

#if defined(__WIN__) && defined(DEBUG)
		path.FixPathSeparators();
#endif

		// this will reload platform specific resource if there is a substitution for the file to be reloaded
		::LoadResource(pResource, path.c_str(), m_resourcesArchive, nullptr);
	}
}

//-------------------------------------------------------------------------------------------
MeshResourceHandle ResourceManager::LoadMesh(const char* filePath)
{
	return LoadResource<ResourceMesh>(filePath);
}

//-------------------------------------------------------------------------------------------
TextureResourceHandle ResourceManager::LoadTexture(const char* filePath)
{
	return LoadResource<ResourceTexture>(filePath);
}

//-------------------------------------------------------------------------------------------
font_resource_handle ResourceManager::load_font(const char* filePath) {
	resource_font* font = LoadResource<resource_font>(filePath);
	if (font) {
		String texture_filepath = String(filePath).StripFileName() + font->texture_filename();
		texture_params params(texture_params::NEAREST, texture_params::NEAREST);
		ResourceTexture* texture = LoadResource<ResourceTexture>(texture_filepath.c_str(), &params);
		if (!texture) {
			unload_resource(font);
			return nullptr;
		}

		font->texture(texture);
	}

	return font;
}

//-------------------------------------------------------------------------------------------
void ResourceManager::RegisterResource(IResource* resource) {
	ResourcesHashTable* table = resource_table(resource->type());
	ASSERT(!table->Find(resource->GetName()));

	table->Insert(resource->GetName(), resource);
	resource->Grab();
}

//-------------------------------------------------------------------------------------------
MeshResourceHandle ResourceManager::GetMesh(const StringHash& res_name) {
	if (IResource* pResource = _meshes_table.Find(res_name)) {
		ASSERT(dynamic_cast<ResourceMesh*>(pResource));
		return static_cast<ResourceMesh*>(pResource);
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------
template<class T>
T* ResourceManager::LoadResource(const char* filePath, void* user_data) {
	StringHash resource_name;
	StringHash resource_type;
	extract_resource_name(filePath, resource_name, resource_type);

	ResourcesHashTable* table = resource_table(resource_type);
	if (IResource* pResource = table->Find(resource_name)) {
		ASSERT(dynamic_cast<T*>(pResource));
		return static_cast<T*>(pResource);
	}

	T* pResource = m_resourcesPool.Create<T>(resource_name, resource_type);

	const bool result = ::LoadResource(pResource, filePath, m_resourcesArchive, user_data);
	if (!result) {
		log_error("Resource Manager", "Failed to load `%s` resource", filePath);
		// TODO: think about policy\placeholders for resources that failed to load. Do I need to track resource that failed to load? Should I return nullptr?
		m_resourcesPool.Delete(pResource);

		return nullptr;
	}

	table->Insert(resource_name, pResource);
	pResource->Grab();

	return pResource;
}

//-------------------------------------------------------------------------------------------
void ResourceManager::unload_resource(IResource* resource) {
	ResourcesHashTable* table = resource_table(resource->type());
	table->Remove(resource->GetName());
	resource->Release();
	m_resourcesPool.Delete(resource);
}

//-------------------------------------------------------------------------------------------
void ResourceManager::populate_type_map() {
	// textures table: pvr, dds
	_type_to_table_map.Insert("pvr", &_textures_table);
	_type_to_table_map.Insert("dds", &_textures_table);

	_type_to_table_map.Insert("smf", &_meshes_table);

	_type_to_table_map.Insert("fnt", &_fonts_table);
}

//-------------------------------------------------------------------------------------------
ResourceManager::ResourcesHashTable* ResourceManager::resource_table(const StringHash& type) {
	ResourcesHashTable* table = _type_to_table_map.Find(type);
	ASSERT(table, "Cannot find table for resource `%s`", type.c_str());

	return table;
}