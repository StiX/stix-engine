
#pragma once

#include "Image.h"
#include "IResource.h"
#include "RendererTypes.h"
#include "ResourceHandle.h"


class ResourceTexture : public IResource
{
public:
						ResourceTexture(const StringHash& name, const StringHash& type);
	virtual				~ResourceTexture();

	virtual bool		Load(const allocation_info& allocation, void* user_data) override;

	TextureHandle		GetHandle() const;

protected:
private:
	TextureHandle		m_texture;
	ImageDescription	m_image;
	texture_params		_params;
};

extern template class ResourceHandle<ResourceTexture>;
typedef ResourceHandle<ResourceTexture> TextureResourceHandle;




//-------------------------------------------------------------------------------------------
inline ResourceTexture::ResourceTexture(const StringHash& name, const StringHash& type)
	: IResource(name, type), m_texture(0)
{
}

//-------------------------------------------------------------------------------------------
inline TextureHandle ResourceTexture::GetHandle() const
{
	return m_texture;
}