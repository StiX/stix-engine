
#include "ResourceTexture.h"
#include "File.h"
#include "Debug.h"
#include "memory.h"
#include "Renderer.h"
#include "ReadStream.h"


//-------------------------------------------------------------------------------------------
ResourceTexture::~ResourceTexture() {
	renderer::delete_texture(m_texture);
}

//-------------------------------------------------------------------------------------------
bool ResourceTexture::Load(const allocation_info& allocation, void* user_data) {
	ReadStream readStream(allocation.ptr, (u32)allocation.size);
	const bool result = LoadTexture(readStream, m_image);

	if (result) {
		if (user_data) {
			_params = *(texture_params*)user_data;
		}
		ASSERT(m_texture.m_handle == 0, "You cannot reassing texture ID to existing texture");

		m_texture = renderer::create_texture(GetName(), m_image, allocation, _params);
	}

	return result;
}


// explicit template instantiation
template class ResourceHandle<ResourceTexture>;