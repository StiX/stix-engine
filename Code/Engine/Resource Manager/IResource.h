
#pragma once

#include "Debug.h"
#include "StringHash.h"
#include "ClassHelpers.h"

struct allocation_info;


class IResource
{
public:
						IResource(const StringHash& name, const StringHash& type);
	virtual				~IResource() = 0;

	virtual bool		Load(const allocation_info& allocation, void* user_data) = 0;

	const StringHash&	GetName() const;
	const StringHash&	type() const;

	void				Grab() const;
	void				Release() const;
	u32					GetReferenceCount() const;
	
protected:
private:
	StringHash			m_name;
	StringHash			_type;
	mutable u32			m_referenceCount;	// Dirty hack that allows to pass IResources by const reference\pointer

	MAKE_UNCOPYABLE(	IResource);
};





//-------------------------------------------------------------------------------------------
inline IResource::IResource(const StringHash& name, const StringHash& type)
	: m_name(name), _type(type), m_referenceCount(0)
{
}

//-------------------------------------------------------------------------------------------
inline IResource::~IResource()
{
	ASSERT(m_referenceCount == 0);
}

//-------------------------------------------------------------------------------------------
inline const StringHash& IResource::GetName() const
{
	return m_name;
}

//-------------------------------------------------------------------------------------------
inline const StringHash& IResource::type() const {
	return _type;
}

//-------------------------------------------------------------------------------------------
inline void IResource::Grab() const
{
	++m_referenceCount;
}

//-------------------------------------------------------------------------------------------
inline void IResource::Release() const
{
	ASSERT(m_referenceCount);
	--m_referenceCount;
}

//-------------------------------------------------------------------------------------------
inline u32 IResource::GetReferenceCount() const
{
	return m_referenceCount;
}