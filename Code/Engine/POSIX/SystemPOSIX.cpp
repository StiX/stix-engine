
#include "System.h"
#include <unistd.h>


//-------------------------------------------------------------------------------------------
System::System()
{
	m_virtualPageSize = (u32)getpagesize();
	m_coresCount = (u32)sysconf(_SC_NPROCESSORS_ONLN);
	
#ifdef __i386__
	m_procArch = x86;
#elif defined __x86_64__
	m_procArch = AMD64;
#elif defined __ARM_ARCH_7__
	m_procArch = ARMv7;
#elif defined __ARM_ARCH_8__
	m_procArch = ARMv8;
#else
	m_procArch = ArchUnknown;
#endif
}