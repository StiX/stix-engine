
#include "camera.h"
#include "Debug.h"
#include "math_lib.h"


namespace {

const vec3f& side_vec(const mat3x4& view)		{ return *(vec3f*)&view.m_1_1; }
const vec3f& up_vec(const mat3x4& view)			{ return *(vec3f*)&view.m_2_1; }
const vec3f& direction_vec(const mat3x4& view)	{ return *(vec3f*)&view.m_3_1; }

}

//-------------------------------------------------------------------------------------------
camera::camera()
	: _pos(0)
	, _yaw(0)
	, _pitch(0)
{
	rotate_by(_yaw, _pitch);
}

//-------------------------------------------------------------------------------------------
void camera::set_3d_proj(float fovy, float aspect, float z_near, float z_far) {
	ASSERT(z_near >= 0.00001f);
	ASSERT(z_far > z_near);

	const float thf = math::tan(math::deg_to_rad(fovy * 0.5f));
	const float a = 1.0f / (aspect * thf);
	const float b = 1.0f / thf;
	const float c = z_far / (z_near - z_far);
	const float d = -(z_far * z_near) / (z_far - z_near);

	_projection.m_1_1 = a;	_projection.m_1_2 = 0;	_projection.m_1_3 = 0;	_projection.m_1_4 = 0;
	_projection.m_2_1 = 0;	_projection.m_2_2 = b;	_projection.m_2_3 = 0;	_projection.m_2_4 = 0;
	_projection.m_3_1 = 0;	_projection.m_3_2 = 0;	_projection.m_3_3 = c;	_projection.m_3_4 = d;
	_projection.m_4_1 = 0;	_projection.m_4_2 = 0;	_projection.m_4_3 = -1;	_projection.m_4_4 = 0;
}

//-------------------------------------------------------------------------------------------
void camera::move_by(const vec3f& delta) {
	const vec3f& side	= side_vec(_view);
	const vec3f& up		= up_vec(_view);
	const vec3f& dir	= direction_vec(_view);

	_pos += side * delta.x;
	_pos += up * delta.y;
	_pos += dir * delta.z;

	const vec3f pos = vec3f(-side.Dot(_pos), -up.Dot(_pos), -dir.Dot(_pos));
	_view.translate_to(pos);
}

//-------------------------------------------------------------------------------------------
void camera::rotate_by(float yaw, float pitch) {
	_yaw += yaw;
	_pitch = math::clamp(_pitch + pitch, -90.0f, 90.0f);

	float cos_yaw;
	float sin_yaw;
	math::sin_cos(math::deg_to_rad(_yaw), sin_yaw, cos_yaw);

	float cos_pitch;
	float sin_pitch;
	math::sin_cos(math::deg_to_rad(_pitch), sin_pitch, cos_pitch);

	const vec3f _side		= vec3f(cos_yaw, 0, -sin_yaw);
	const vec3f _up			= vec3f(sin_yaw * sin_pitch, cos_pitch, cos_yaw * sin_pitch);
	const vec3f _direction	= vec3f(sin_yaw * cos_pitch, -sin_pitch, cos_pitch * cos_yaw);
	const vec3f pos			= vec3f(_side.Dot(_pos), _up.Dot(_pos), _direction.Dot(_pos));

	_view = {
		_side.x,		_side.y,		_side.z,		-pos.x,
		_up.x,			_up.y,			_up.z,			-pos.y,
		_direction.x,	_direction.y,	_direction.z,	-pos.z
	};
}