
#pragma once

#include "mat4.h"
#include "mat3x4.h"


class camera {
public:
					camera();

	void			set_3d_proj(float fovy, float aspect, float z_near, float z_far);

	void			move_by(const vec3f& delta);
	void			rotate_by(float yaw, float pitch);
	vec3f			position() const { return _pos; }

	const mat4&		projection() const	{ return _projection; }
	const mat3x4&	view() const		{ return _view;}

protected:
private:
	mat4			_projection;
	mat3x4			_view;
	vec3f			_pos;
	float			_yaw;
	float			_pitch;
};