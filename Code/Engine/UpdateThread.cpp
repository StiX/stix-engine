
#include "UpdateThread.h"
#include "Thread.h"
#include "Renderer.h"
#include "ThreadEvent.h"
#include "ThreadsSyncPoint.h"
#include "ApplicationManager.h"


static Thread updateThread;


//-------------------------------------------------------------------------------------------
static u32 UpdateThreadEntry(void* data) {
	ApplicationManager appManager;
	appManager.Init();

	ThreadEvent* event = (ThreadEvent*)data;
	event->Signal();

	while (!updateThread.IsTerminationRequested()) {
		appManager.Run();

		renderer::frame_end_sync_point().WaitForCondition();
	}

	appManager.ShutDown();

	return 0;
}

//-------------------------------------------------------------------------------------------
void UpdateThreadSpawn(ThreadEvent* on_init_done_event) {
	updateThread.Spawn(&UpdateThreadEntry, on_init_done_event, "Update");
}

//-------------------------------------------------------------------------------------------
void UpdateThreadRequestTermination() {
	updateThread.RequestTermination();
}

//-------------------------------------------------------------------------------------------
void UpdateThreadWaitToTerminate() {
	updateThread.WaitToTerminate();
}

//-------------------------------------------------------------------------------------------
bool IsUpdateThread(size_t threadId) {
	return updateThread.GetId() == threadId;
}