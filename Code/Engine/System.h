
#pragma once

#include "Types.h"


enum eProcessorArch
{
	ArchUnknown,
	x86,
	AMD64,
	ARMv7,
	ARMv8
};


class System
{
public:
					System();

	eProcessorArch	GetArchitecture() const;
	u32				GetCoresCount() const;
	u32				GetVirtualPageSize() const;

protected:
private:
	eProcessorArch	m_procArch;		// on 32 bit application it will be equal to x86 for AMD64 processors
	u32				m_virtualPageSize;
	u32				m_coresCount;
};





//-------------------------------------------------------------------------------------------
inline eProcessorArch System::GetArchitecture() const
{
	return m_procArch;
}

//-------------------------------------------------------------------------------------------
inline u32 System::GetCoresCount() const
{
	return m_coresCount;
}

//-------------------------------------------------------------------------------------------
inline u32 System::GetVirtualPageSize() const
{
	return m_virtualPageSize;
}