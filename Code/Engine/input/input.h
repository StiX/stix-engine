
#pragma once

#include "Vector2D.h"


namespace input {

enum key_code {
	backspace,
	tab,
	enter,
	caps_lock,
	escape,
	space,
	page_up,
	page_down,
	end,
	home,
	left,
	up,
	right,
	down,
	print_screen,
	insert,
	del,
	_0,
	_1,
	_2,
	_3,
	_4,
	_5,
	_6,
	_7,
	_8,
	_9,
	a,
	b,
	c,
	d,
	e,
	f,
	g,
	h,
	i,
	j,
	k,
	l,
	m,
	n,
	o,
	p,
	q,
	r,
	s,
	t,
	u,
	v,
	w,
	x,
	y,
	z,
	numpad_0,
	numpad_1,
	numpad_2,
	numpad_3,
	numpad_4,
	numpad_5,
	numpad_6,
	numpad_7,
	numpad_8,
	numpad_9,
	multiply,
	add,
    subtract,
	decimal,
	divide,
	f1,
	f2,
	f3,
	f4,
	f5,
	f6,
	f7,
	f8,
	f9,
	f10,
	f11,
	f12,
	f13,
	f14,
	f15,
	f16,
	f17,
	f18,
	f19,
	f20,
	f21,
	f22,
	f23,
	f24,
	num_lock,
	lshift,
	rshift,
	lcontrol,
	rcontrol,
	lmouse,
	rmouse,
	mmouse,
	key_max,
};

enum touch_state {
	touch_none,
	touch_down,
	touch_released,
	touch_moved,
};

struct touch {
	touch_state	state;
	vec2i		position;
	vec2i		prev_position;
};


void			update();
bool			key_down(key_code key);
const touch&	get_touch();


namespace internal {

void			on_touch(const touch& t);
void			on_key(key_code key, bool down);

}

}	// ~namespace input
