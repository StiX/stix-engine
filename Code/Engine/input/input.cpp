
#include "input.h"
#include "Types.h"


namespace input {

bool	_touch_updated = false;
touch	_touch = {touch_none, vec2i(0, 0), vec2i(0, 0)};
bool	_key_states[key_max] = {false};
vec2i	_old_touch_pos;

namespace internal {

//-------------------------------------------------------------------------------------------
void on_touch(const touch& t) {
	_touch = t;
	_touch_updated = true;
}

//-------------------------------------------------------------------------------------------
void on_key(key_code key, bool down) {
	_key_states[key] = down;
}

} // ~namespace internal


//-------------------------------------------------------------------------------------------
void update() {
	// Useful information regarding handling of game input:
	// https://web.archive.org/web/20100305074754/http://www.dominikgrabiec.com:80/2009/01/21/game-engine-input/

	if (_touch.state != touch_down) {
		_touch.prev_position = _old_touch_pos;
		_old_touch_pos = _touch.position;

		if (_touch.state == touch_released) {
			_touch.state = touch_none;
		} else if (_touch.state == touch_moved && !_touch_updated) {
			// started to drag, but stopped at one position holding touch\mouse down and no new move events raised, so we have to manually update prev position
			_touch.prev_position = _touch.position;
		}
	}
    
    _old_touch_pos = _touch.position;

	_touch_updated = false;
}

//-------------------------------------------------------------------------------------------
bool key_down(key_code key) {
	return _key_states[key];
}

//-------------------------------------------------------------------------------------------
const touch& get_touch() {
	return _touch;
}

} // ~namespace input
