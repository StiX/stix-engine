
#pragma once

#include <cstring>
#include "Renderer.h"

class System;
class Window;
class FSWatchDog;
struct IApplication;
class ResourceManager;
class ApplicationManager;


struct SystemEnvironment
{
	Renderer*			pRenderer;
	FSWatchDog*			pFSWatchDog;
	IApplication*		pApplication;
	const System*		pSystem;
	const Window*		window;
	ResourceManager*	pResManager;
	ApplicationManager*	pAppManager;

						SystemEnvironment();
};

extern SystemEnvironment g_sysEnv;





//-------------------------------------------------------------------------------------------
inline SystemEnvironment::SystemEnvironment()
{
	memset(this, 0, sizeof(*this));
}