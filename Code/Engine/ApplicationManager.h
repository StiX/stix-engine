
#pragma once

#include "Types.h"
#include "Application.h"


class ApplicationManager
{
public:
				ApplicationManager();
				~ApplicationManager();

	void		Init();
	void		Run();
	void		ShutDown();

protected:
private:
	IApplication* m_pApplication;
	double		m_timeStamp;
	double		m_startupTime;
	float		m_applicationTime;
	float		m_fps;
	u32			m_frameId;

	void		Update();
	void		Render();

	float		CalculateFrameDeltaTime();

	void		InitSubsystems();
	void		ShutdownSubsystems();
	void		UpdateSubsystems(float deltaTime);

	MAKE_UNCOPYABLE(ApplicationManager);
};