
#pragma once

#include <EGL/egl.h>
#include "ClassHelpers.h"
#include "WindowParams.h"

struct ANativeWindow;


class Window
{
public:
					Window();
					~Window();

	bool			Create(ANativeWindow* pNativeWindow, const WindowParams* pParams = nullptr);
	void			Destroy();

	bool			IsCreated() const { return m_context != EGL_NO_CONTEXT; }

	void			Draw();	// just presents back buffer

	const WindowParams&	parameters() const { return m_params; }

protected:
private:
	EGLDisplay		m_display;
	EGLSurface		m_surface;
	EGLContext		m_context;
	WindowParams	m_params;

	MAKE_UNCOPYABLE(Window);
};