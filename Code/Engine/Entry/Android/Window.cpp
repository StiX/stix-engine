
#include "Log.h"
#include "Window.h"
#include "WindowParams.h"
#include <android/native_window.h>


//-------------------------------------------------------------------------------------------
Window::Window()
	: m_display(EGL_NO_DISPLAY), m_surface(EGL_NO_SURFACE), m_context(EGL_NO_CONTEXT)
{
}

//-------------------------------------------------------------------------------------------
Window::~Window()
{
	if (m_display != EGL_NO_DISPLAY)
	{
		Destroy();
	}
}

//-------------------------------------------------------------------------------------------
bool Window::Create(ANativeWindow* pNativeWindow, const WindowParams* pParams)
{
	if (pParams)
	{
		m_params = *pParams;
	}

	EGLint attributes[] =
	{
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_DEPTH_SIZE, 24,
		EGL_STENCIL_SIZE, 8,
		EGL_NONE, 0,	//EGL_SAMPLE_BUFFERS, 0,
		EGL_NONE, 0,	//EGL_SAMPLES, 0,
		EGL_NONE,
	};

	int i = 0;
	while (attributes[i] != EGL_NONE)
	{
		i += 2;
	}

	const int kSamplesCount = 4;
	if (kSamplesCount)
	{
		attributes[i] = EGL_SAMPLE_BUFFERS;
		attributes[++i] = 1;
		attributes[++i] = EGL_SAMPLES;
		attributes[++i] = kSamplesCount;
	}

	m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

	EGLint majorVer, minorVer;
	if (!eglInitialize(m_display, &majorVer, &minorVer))
	{
		log_error("Window", "Failed to initialize EGL!");
		return false;
	}

	EGLConfig config;
	EGLint configsCount;
	if (!eglChooseConfig(m_display, attributes, &config, 1, &configsCount))
	{
		//TODO: print attributes
		log_error("Window", "Failed to find matching EGL framebuffer config!");
		return false;
	}

	// Get native visual ID to reconfigure default framebuffer
	EGLint visualID;
	if (!eglGetConfigAttrib(m_display, config, EGL_NATIVE_VISUAL_ID, &visualID))
	{
		log_error("Window", "Failed to get EGL frame buffer information!");
		return false;
	}

	ANativeWindow_setBuffersGeometry(pNativeWindow, 0, 0, visualID);

	m_params.m_size.x = ANativeWindow_getWidth(pNativeWindow);
	m_params.m_size.y = ANativeWindow_getHeight(pNativeWindow);

	m_surface = eglCreateWindowSurface(m_display, config, pNativeWindow, nullptr);
	if (EGL_NO_SURFACE == m_surface)
	{
		log_error("Window", "Failed to create EGL surface!");
		return false;
	}

	EGLint contextAttribs[] =
	{
		EGL_CONTEXT_CLIENT_VERSION, 3,
		EGL_NONE,
	};

	m_context = eglCreateContext(m_display, config, nullptr, contextAttribs);
	if (EGL_NO_CONTEXT == m_context)
	{
		log_error("Window", "Failed to create EGL context!");
		return false;
	}

	if (eglMakeCurrent(m_display, m_surface, m_surface, m_context) == EGL_FALSE)
	{
		log_error("Window", "Failed to set EGL context!");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------
void Window::Destroy()
{
	if (m_display != EGL_NO_DISPLAY)
	{
		eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		if (m_context != EGL_NO_CONTEXT)
		{
			eglDestroyContext(m_display, m_context);
			m_context = EGL_NO_CONTEXT;
		}

		if (m_surface != EGL_NO_SURFACE)
		{
			eglDestroySurface(m_display, m_surface);
			m_surface = EGL_NO_SURFACE;
		}

		eglTerminate(m_display);
		m_display = EGL_NO_DISPLAY;
	}
}

//-------------------------------------------------------------------------------------------
void Window::Draw()
{
	eglSwapBuffers(m_display, m_surface);
}