
#include "Window.h"
#include "input.h"
#include "Renderer.h"
#include "SystemUtil.h"
#include "FileSystem.h"
#include "SystemUtil.h"
#include "ThreadEvent.h"
#include "UpdateThread.h"
#include "ThreadsSyncPoint.h"
#include "SystemEnvironment.h"
#include "android_native_app_glue.h"


namespace {

Window _window;


//-------------------------------------------------------------------------------------------
void SetExecutablePath(ANativeActivity* pActivity) {
	JNIEnv* pJniEnv = 0;

	pActivity->vm->AttachCurrentThread(&pJniEnv, NULL);

	jclass clazz = pJniEnv->GetObjectClass(pActivity->clazz);
	jmethodID methodID = pJniEnv->GetMethodID(clazz, "getPackageCodePath", "()Ljava/lang/String;");
	jobject result = pJniEnv->CallObjectMethod(pActivity->clazz, methodID);

	jboolean isCopy;
	const char* executablePath = pJniEnv->GetStringUTFChars((jstring)result, &isCopy);
	SetExecutableFilePath(executablePath);

	pActivity->vm->DetachCurrentThread();
}

//-------------------------------------------------------------------------------------------
void ProccessEvents(android_app* pApp, int cmd) {
	switch (cmd) {
	case APP_CMD_INIT_WINDOW:
		_window.Create(pApp->window);
		break;

	case APP_CMD_TERM_WINDOW:
		_window.Destroy();
		break;

	case APP_CMD_SAVE_STATE:
		// called on app switch or screen lock
		break;

	case APP_CMD_PAUSE:
		break;

	case APP_CMD_GAINED_FOCUS:
		break;

	case APP_CMD_LOST_FOCUS:
		break;

	default:
		break;
	}
}

//-------------------------------------------------------------------------------------------
int handle_input(struct android_app* app, AInputEvent* event) {
	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) {
		if (AInputEvent_getSource(event) & AINPUT_SOURCE_CLASS_POINTER) {
			input::touch touch;
			const float pos_x = AMotionEvent_getX(event, 0);
			const float pos_y = _window.parameters().m_size.y - AMotionEvent_getY(event, 0);

			const int action = AKeyEvent_getAction(event) & AMOTION_EVENT_ACTION_MASK;
			switch (action) {
			case AMOTION_EVENT_ACTION_DOWN:
				touch.state = input::touch_down;
				touch.position = vec2i(pos_x, pos_y);
				touch.prev_position = touch.position;

				input::internal::on_touch(touch);
				input::internal::on_key(input::lmouse, true);
				return 1;

			case AMOTION_EVENT_ACTION_UP:
				touch.state = input::touch_released;
				touch.position = vec2i(pos_x, pos_y);

				input::internal::on_touch(touch);
				input::internal::on_key(input::lmouse, false);
				return 1;

			case AMOTION_EVENT_ACTION_MOVE:
				touch.state = input::touch_moved;
				touch.position = vec2i(pos_x, pos_y);

				input::internal::on_touch(touch);
				return 1;
			}
		}
	}

	return 0;
}

//-------------------------------------------------------------------------------------------
void PollEvents(android_app* pApp) {
	int ident;
	int events;
	android_poll_source* outSource;

	while ((ident = ALooper_pollAll(0, nullptr, &events, (void**)&outSource)) >= 0) {
		if (outSource != nullptr) {
			outSource->process(pApp, outSource);
		}
	}
}

}	//~namespace

//-------------------------------------------------------------------------------------------
void android_main(struct android_app* pApp)
{
	ANativeActivity* pActivity = pApp->activity;

	SetExecutablePath(pActivity);
	SetWorkDir(GetExecutableDirPath());

	pApp->onAppCmd = ProccessEvents;
	pApp->onInputEvent = handle_input;

	while (!_window.IsCreated())
	{
		PollEvents(pApp);
		SystemSleep(16);
	}

	g_sysEnv.window = &_window;

	RendererInit(_window.parameters().m_size);

	ThreadEvent update_thread_initialized;
	UpdateThreadSpawn(&update_thread_initialized);
	update_thread_initialized.Wait();

	while (true)
	{
		PollEvents(pApp);

		RendererUpdate();
		_window.Draw();
	}

	UpdateThreadRequestTermination();
	GetFrameEndSyncPoint().ReleaseWaiters();
	UpdateThreadWaitToTerminate();

	RendererShutdown();
}