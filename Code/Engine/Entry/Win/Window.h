
#pragma once

#include <windows.h>
#include "WindowParams.h"
#include "ClassHelpers.h"



class Window
{
public:
					Window();
					~Window();

	bool			Create(const WindowParams* pParams = nullptr, const char* title = "StiX Engine");
	void			Destroy();
	void			Show() const;

	HWND			GetHandle() const;

	const WindowParams&	parameters() const { return m_params; }

protected:
private:
	HDC				m_hDC;              // GDI device context
	HWND			m_hWnd;             // Window handle
	HINSTANCE		m_hInstance;        // Application instance
	WindowParams	m_params;

	bool			GoFullscreen(DWORD& dwExStyle, DWORD& dwStyle);
	HWND			CreateWnd(DWORD dwExStyle, DWORD dwStyle, const char* title);

	MAKE_UNCOPYABLE(Window);
};