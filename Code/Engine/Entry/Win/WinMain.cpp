
//#include <vld.h>
#include <windows.h>
#include "Log.h"
#include "Window.h"
#include "Renderer.h"
#include "GLContext.h"
#include "FileSystem.h"
#include "SystemUtil.h"
#include "ThreadEvent.h"
#include "UpdateThread.h"
#include "ThreadsSyncPoint.h"
#include "SystemEnvironment.h"

#pragma warning(push)
#pragma warning (disable: 4073)
#pragma init_seg(lib)
#pragma warning(pop)


struct MemoryLeaskChecker
{
	~MemoryLeaskChecker()
	{
#ifdef DEBUG
		_CrtMemState memState;
		_CrtMemCheckpoint(&memState);

		if (memState.lCounts[_NORMAL_BLOCK])
		{
			_CrtDbgReport(_CRT_WARN, nullptr, 0, nullptr, "%d memory leak(s) detected! (Blocks without file name were allocated using new)\n", memState.lCounts[_NORMAL_BLOCK]);
			
			_CrtDumpMemoryLeaks();
		}
#endif
	}
};


static MemoryLeaskChecker memoryLeaksChecker;


//-------------------------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, LPSTR /*lpCmdLine*/, int /*nCmdShow*/)
{
	String exePath = GetExecutableDirPath();	// Bin* dir
	exePath.StripDir();							// Engine root dir
	SetWorkDir(exePath.c_str());

	log_init("Engine Log.html");

	Window win;
	if (!win.Create())
	{
		return EXIT_FAILURE;
	}

	g_sysEnv.window = &win;

	GLContext glContext;
	// init OpenGL to get function pointers and destroy window
	glContext.InitOpenGL(win.GetHandle());
	glContext.Destroy();	
	win.Destroy();

	// ... to recreate it once again with newly retrieved functions
	win.Create();
	glContext.InitOpenGL33(win.GetHandle(), 8);	// 8 samples count

	win.Show();

	renderer::init(win.parameters().m_size);

	ThreadEvent update_thread_initialized;
	UpdateThreadSpawn(&update_thread_initialized);
	update_thread_initialized.Wait();

	MSG msg;
	ZeroMemory(&msg, sizeof(msg));

	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			renderer::update();

			const HWND kWndHandle = win.GetHandle();
			const HDC kDcHandle = GetDC(kWndHandle);
			SwapBuffers(kDcHandle);
		}
	}

	UpdateThreadRequestTermination();
	renderer::frame_end_sync_point().ReleaseWaiters();
	UpdateThreadWaitToTerminate();

	renderer::shutdown();

	glContext.Destroy();
	win.Destroy();

	log_shutdown();

	return EXIT_SUCCESS;
}