
#include "Window.h"
#include "input.h"
#include "Renderer.h"


namespace {

input::key_code convert_key_code(WPARAM system_key_code);

//-------------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	// Get pointer to Window instance
	Window* pWindow = (Window*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

	switch (message) {
	case WM_CREATE:
	{
		LPCREATESTRUCT lpcs = (LPCREATESTRUCT)lParam;

		// Last param in CreateWindowEx func
		pWindow = (Window*)(lpcs->lpCreateParams);

		// Put pointer into Windows user data to extract them later through GetWindowLong
		SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)pWindow);

		break;
	}

	case WM_ACTIVATE: {               // Watch For Window Activate Message
		if (!HIWORD(wParam)) {        // Check Minimization State
			renderer::activate(true);
		}

		input::touch touch = input::get_touch();
		if (touch.state == input::touch_moved) {
			touch.state = input::touch_released;
			touch.position = touch.prev_position;
			input::internal::on_touch(touch);
		}

		return 0;
	}

	case WM_SYSCOMMAND:
		switch (wParam) {
		case SC_SCREENSAVE:      // Screensaver Trying To Start?
		case SC_MONITORPOWER:    // Monitor Trying To Enter power save?
			return 0;            // Prevent From Happening

		case SC_MINIMIZE:
			renderer::activate(false);
			break;
		}

		break;

	case WM_LBUTTONDOWN:
	{
		POINT pos;
		GetCursorPos(&pos);
		ScreenToClient(hWnd, &pos);
		pos.y = pWindow->parameters().m_size.y - pos.y;

		input::touch touch;
		touch.state = input::touch_down;
		touch.position = vec2i(pos.x, pos.y);
		touch.prev_position = touch.position;

		input::internal::on_touch(touch);
		input::internal::on_key(input::lmouse, true);

		return 0;
	}

	case WM_RBUTTONDOWN:
		input::internal::on_key(input::rmouse, true);
		return 0;

	case WM_MBUTTONDOWN:
		input::internal::on_key(input::mmouse, true);
		return 0;

	case WM_LBUTTONUP:
	{
		POINT pos;
		GetCursorPos(&pos);
		ScreenToClient(hWnd, &pos);
		pos.y = pWindow->parameters().m_size.y - pos.y;

		input::touch touch;
		touch.state = input::touch_released;
		touch.position = vec2i(pos.x, pos.y);

		input::internal::on_touch(touch);
		input::internal::on_key(input::lmouse, false);

		return 0;
	}

	case WM_RBUTTONUP:
		input::internal::on_key(input::rmouse, false);
		return 0;

	case WM_MBUTTONUP:
		input::internal::on_key(input::mmouse, false);
		return 0;

	case WM_MOUSEMOVE:
	{
		POINT pos;
		GetCursorPos(&pos);
		ScreenToClient(hWnd, &pos);
		pos.y = pWindow->parameters().m_size.y - pos.y;

		input::touch touch;
		touch.state = input::touch_moved;
		touch.position = vec2i(pos.x, pos.y);

		input::internal::on_touch(touch);

		return 0;
	}

	case WM_KEYDOWN: {
		const input::key_code key = convert_key_code(wParam);
		if (key != input::key_max) {
			input::internal::on_key(key, true);
		}

		return 0;
	}

	case WM_KEYUP: {
		const input::key_code key = convert_key_code(wParam);
		if (key != input::key_max) {
			input::internal::on_key(key, false);
		}

		return 0;
	}

	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;

	case WM_SIZE:
		// LoWord=Width, HiWord=Height
		//pView->ResizeViewWindow(LOWORD(lParam), HIWORD(lParam));
		return 0;
	}

	// Pass All unhandled Messages To DefWindowProc, Windows will handle them
	return DefWindowProc(hWnd, message, wParam, lParam);
}

//-------------------------------------------------------------------------------------------
input::key_code convert_key_code(WPARAM system_key_code) {
	if (system_key_code >= VK_PRIOR && system_key_code <= VK_DOWN) {
		return (input::key_code)(system_key_code - VK_PRIOR + input::page_down);
	} else if (system_key_code >= 0x30 && system_key_code <= 0x39) {
		return (input::key_code)(system_key_code - 0x30 + input::_0);
	} else if (system_key_code >= 0x41 && system_key_code <= 0x5A) {
		return (input::key_code)(system_key_code - 0x41 + input::a);
	} else if (system_key_code >= VK_NUMPAD0 && system_key_code <= VK_F24) {
		return (input::key_code)(system_key_code - VK_NUMPAD0 + input::numpad_0);
	} else if (system_key_code >= VK_LSHIFT && system_key_code <= VK_RCONTROL) {
		return (input::key_code)(system_key_code - VK_LSHIFT + input::lshift);
	}

	switch (system_key_code) {
	case VK_LBUTTON:
		return input::lmouse;

	case VK_RBUTTON:
		return input::rmouse;

	case VK_MBUTTON:
		return input::mmouse;

	case VK_BACK:
		return input::backspace;

	case VK_TAB:
		return input::tab;

	case VK_RETURN:
		return input::enter;

	case VK_CAPITAL:
		return input::caps_lock;

	case VK_ESCAPE:
		return input::escape;

	case VK_SNAPSHOT:
		return input::print_screen;

	case VK_INSERT:
		return input::insert;

	case VK_DELETE:
		return input::del;

	case VK_NUMLOCK:
		return input::num_lock;

	default:
		return input::key_max;
	}
}

}	// ~anonymous namespace

//-------------------------------------------------------------------------------------------
Window::Window()
	: m_hDC(nullptr), m_hWnd(nullptr)
{
	WNDCLASS wc;

	// Get application handle
	m_hInstance = GetModuleHandle(nullptr);
	// Redraw On Move, And Own DC For Window
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;			 // WndProc Handles Messages
	wc.cbClsExtra = 0;
	wc.cbWndExtra = sizeof(LONG_PTR);
	wc.hInstance = m_hInstance;					 // Set The Instance
	wc.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = nullptr;
	wc.lpszMenuName = nullptr;
	wc.lpszClassName = "StiX Engine";

	if (!RegisterClass(&wc))
	{
		MessageBox(nullptr, "Failed To Register The Window Class.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
	}
}

//-------------------------------------------------------------------------------------------
Window::~Window()
{
	if (m_hWnd)
	{
		Destroy();
	}
	
	if (!UnregisterClass("StiX Engine", m_hInstance))
	{
		MessageBox(nullptr, "Could Not Unregister Class.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
	}
}

//-------------------------------------------------------------------------------------------
bool Window::Create(const WindowParams* pParams, const char* title)
{
	if (pParams)
	{
		m_params = *pParams;
	}

	DWORD dwExStyle;						//Extended window style
	DWORD dwStyle;							//window style

	if (m_params.m_bFullScreen)
	{
		if (!GoFullscreen(dwExStyle, dwStyle))
		{
			MessageBox(nullptr, "Program Will Now Close!", "ERROR", MB_OK | MB_ICONSTOP);
			return false;
		}
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle = WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_SIZEBOX | WS_BORDER;
	}

	m_hWnd = CreateWnd(dwExStyle, dwStyle, title);
	if (!m_hWnd)
	{
		MessageBox(nullptr, "Window Creation Error!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		Destroy();
		return false;
	}

	m_hDC = GetDC(m_hWnd);

	return true;
}

//-------------------------------------------------------------------------------------------
void Window::Destroy()
{
	if (m_params.m_bFullScreen)
	{
		ChangeDisplaySettings(nullptr, 0);
		ShowCursor(true);
	}

	if (m_hDC && !ReleaseDC(m_hWnd, m_hDC))
	{
		MessageBox(nullptr, "Release Device Context Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
	}

	if (m_hWnd && !DestroyWindow(m_hWnd))
	{
		MessageBox(nullptr, "Could Not Release hWnd.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
	}

	m_hDC = nullptr;
	m_hWnd = nullptr;
}

//-------------------------------------------------------------------------------------------
void Window::Show() const
{
	SetForegroundWindow(m_hWnd);

	ShowWindow(m_hWnd, SW_SHOW);
	SetFocus(m_hWnd);						// Sets Keyboard Focus To The Window
}

//-------------------------------------------------------------------------------------------
HWND Window::GetHandle() const
{
	return m_hWnd;
}

//-------------------------------------------------------------------------------------------
HWND Window::CreateWnd(DWORD dwExStyle, DWORD dwStyle, const char* title) {
	RECT windowRect;
	windowRect.left = 0;
	windowRect.right = m_params.m_size.x;
	windowRect.top = 0;
	windowRect.bottom = m_params.m_size.y;

	// Adjust Window To True Requested Size
	AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);

	const int screenW = GetSystemMetrics(SM_CXSCREEN);
	const int screenH = GetSystemMetrics(SM_CYSCREEN);

	return CreateWindowEx(dwExStyle,						// Extended Style For The Window
			"StiX Engine",									// Class Name
			title,
			WS_CLIPSIBLINGS | WS_CLIPCHILDREN | dwStyle,	// Required Window Style
			(screenW - m_params.m_size.x) / 2,
			(screenH - m_params.m_size.y) / 2,				// Window Position
			windowRect.right - windowRect.left,				// Calculate Adjusted Window Width
			windowRect.bottom - windowRect.top,				// Calculate Adjusted Window Height
			nullptr,										// No Parent Window
			nullptr,										// No Menu
			m_hInstance,
			this);											// Pointer to window creation data
}

//-------------------------------------------------------------------------------------------
bool Window::GoFullscreen(DWORD& dwExStyle, DWORD& dwStyle)
{
	DEVMODE dmScreenSettings;                // Device Mode
	memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));

	dmScreenSettings.dmSize = sizeof(dmScreenSettings);
	dmScreenSettings.dmPelsWidth = m_params.m_size.x;
	dmScreenSettings.dmPelsHeight = m_params.m_size.y;
	dmScreenSettings.dmBitsPerPel = 32;
	dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

	// Try To Set Selected Mode And Get Results.
	// NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
	if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
	{
		if (MessageBox(nullptr, "The Requested Fullscreen Mode Is Not "
			"Supported By\nYour Video Card. Use Windowed Mode Instead?",
			"StiX Engine", MB_YESNO | MB_ICONEXCLAMATION) == IDYES)
		{
			m_params.m_bFullScreen = false;
		}
		else
		{
			return false;
		}
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW;        // Window Extended Style
		dwStyle = WS_POPUP;               // Window Style
		ShowCursor(false);
	}

	return true;
}