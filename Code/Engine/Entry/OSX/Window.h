
#pragma once

#include "WindowParams.h"
#include "ClassHelpers.h"

struct GLFWwindow;


class Window
{
public:
					Window();
					~Window();
	
	bool			Create(const WindowParams* pParams = nullptr, const char* title = "StiX Engine");
	void			Destroy();
	
	void			Draw();	// just presents back buffer
	bool			IsRunning() const;
	
	const WindowParams& parameters() const { return m_winParams; }
	
protected:
private:
	GLFWwindow*		m_window;
	WindowParams	m_winParams;
	
	MAKE_UNCOPYABLE(Window);
};
