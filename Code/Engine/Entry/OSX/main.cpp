
#include <stdlib.h>
#include "Log.h"
#include "Str.h"
#include "glfw3.h"
#include "Window.h"
#include "Renderer.h"
#include "FileSystem.h"
#include "SystemUtil.h"
#include "ThreadEvent.h"
#include "UpdateThread.h"
#include "ThreadsSyncPoint.h"


int main(int /*argc*/, const char** /*argv*/)
{
	String exePath(GetExecutableDirPath());		// Bin* dir
	exePath.StripDir();							// Engine root dir
	
	SetWorkDir(exePath.c_str());

	log_init("Engine Log.html");
	
	Window wnd;
	if (!wnd.Create())
	{
		return EXIT_FAILURE;
	}
	
	renderer::init(wnd.parameters().m_size);
    
    ThreadEvent update_thread_initialized;
    UpdateThreadSpawn(&update_thread_initialized);
    update_thread_initialized.Wait();
	
	while (wnd.IsRunning())
	{
		renderer::update();

		wnd.Draw();
		glfwPollEvents();
	}
	
	UpdateThreadRequestTermination();
	renderer::frame_end_sync_point().ReleaseWaiters();
	UpdateThreadWaitToTerminate();
	
	renderer::shutdown();

	log_shutdown();

    return EXIT_SUCCESS;
}

