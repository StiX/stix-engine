
#include "Window.h"
#include "input.h"
#include "OpenGL.h"


namespace {
    
input::key_code convert_key_code(int key);
    
//-------------------------------------------------------------------------------------------
void on_key_callback(GLFWwindow*, int system_key, int, int action, int) {
    if (action == GLFW_REPEAT) {
        return;
    }
    
    const input::key_code key = convert_key_code(system_key);
    input::internal::on_key(key, action == GLFW_PRESS);
}
    
//-------------------------------------------------------------------------------------------
int convert_mouse_y(GLFWwindow* window, double pos_y) {
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    
    return height - (int)pos_y;
}
    
//-------------------------------------------------------------------------------------------
void on_mouse_move_callback(GLFWwindow* window, double pos_x, double pos_y) {
    input::touch touch;
    touch.state = input::touch_moved;
    touch.position = vec2i((int)pos_x, convert_mouse_y(window, pos_y));
    
    input::internal::on_touch(touch);
}
    
//-------------------------------------------------------------------------------------------
void on_mouse_button_callback(GLFWwindow* window, int button, int action, int) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        
        input::touch touch;
        touch.position = vec2i((int)xpos, convert_mouse_y(window, ypos));
        if (action == GLFW_PRESS) {
            touch.state = input::touch_down;
            touch.prev_position = touch.position;
        } else {
            touch.state = input::touch_released;
        }
        
        input::internal::on_touch(touch);
        input::internal::on_key(input::lmouse, action == GLFW_PRESS);
    } else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        input::internal::on_key(input::rmouse, action == GLFW_PRESS);
    } else if (button == GLFW_MOUSE_BUTTON_MIDDLE) {
        input::internal::on_key(input::mmouse, action == GLFW_PRESS);
    }
}
    
//-------------------------------------------------------------------------------------------
input::key_code convert_key_code(int key) {
    if (key >= GLFW_KEY_0 && key <= GLFW_KEY_9) {
        return input::key_code(key - GLFW_KEY_0 + input::_0);
    } else if (key >= GLFW_KEY_A && key <= GLFW_KEY_Z) {
        return input::key_code(key - GLFW_KEY_A + input::a);
    } else if (key >= GLFW_KEY_F1 && key <= GLFW_KEY_F24) {
        return input::key_code(key - GLFW_KEY_F1 + input::f1);
    } else if (key >= GLFW_KEY_KP_0 && key <= GLFW_KEY_KP_9) {
        return input::key_code(key - GLFW_KEY_KP_0 + input::numpad_0);
    }
    
    switch (key) {
    case GLFW_KEY_KP_DECIMAL:
        return input::decimal;
            
    case GLFW_KEY_KP_DIVIDE:
        return input::divide;
            
    case GLFW_KEY_BACKSPACE:
        return input::backspace;
            
    case GLFW_KEY_TAB:
        return input::tab;
            
    case GLFW_KEY_ENTER:
        return input::enter;
            
    case GLFW_KEY_LEFT_SHIFT:
        return input::lshift;
            
    case GLFW_KEY_LEFT_CONTROL:
        return input::lcontrol;
            
    case GLFW_KEY_CAPS_LOCK:
        return input::caps_lock;
            
    case GLFW_KEY_ESCAPE:
        return input::escape;
            
    case GLFW_KEY_SPACE:
        return input::space;
            
    case GLFW_KEY_PAGE_UP:
        return input::page_up;
            
    case GLFW_KEY_PAGE_DOWN:
        return input::page_down;
            
    case GLFW_KEY_END:
        return input::end;
            
    case GLFW_KEY_HOME:
        return input::home;
            
    case GLFW_KEY_LEFT:
        return input::left;
            
    case GLFW_KEY_RIGHT:
        return input::right;
            
    case GLFW_KEY_UP:
        return input::up;
            
    case GLFW_KEY_DOWN:
        return input::down;
            
    case GLFW_KEY_PRINT_SCREEN:
        return input::print_screen;
            
    case GLFW_KEY_INSERT:
        return input::insert;
            
    case GLFW_KEY_DELETE:
        return input::del;
            
    case GLFW_KEY_KP_MULTIPLY:
        return input::multiply;
            
    case GLFW_KEY_KP_ADD:
        return input::add;
            
    case GLFW_KEY_KP_SUBTRACT:
        return input::subtract;
            
    case GLFW_KEY_NUM_LOCK:
        return input::num_lock;
            
    case GLFW_KEY_RIGHT_SHIFT:
        return input::rshift;
            
    case GLFW_KEY_RIGHT_CONTROL:
        return input::rcontrol;
            
    default:
        return input::key_max;
    }
}
    
}   //~ nameless namespace

//-------------------------------------------------------------------------------------------
Window::Window()
	: m_window(nullptr)
{
}

//-------------------------------------------------------------------------------------------
Window::~Window()
{
	if (m_window)
	{
		Destroy();
	}
	
	glfwTerminate();
}

//-------------------------------------------------------------------------------------------
bool Window::Create(const WindowParams* pParams, const char* title)
{
	if (pParams)
	{
		m_winParams = *pParams;
	}
	
	if (!glfwInit())
	{
		return false;
	}
	
	const int kSamplesCount = 8;
	glfwWindowHint(GLFW_SAMPLES, kSamplesCount);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	m_window = glfwCreateWindow(m_winParams.m_size.x, m_winParams.m_size.y, title, NULL, NULL);
	if (!m_window)
	{
		m_window = nullptr;
		glfwTerminate();
		return false;
	}
	
	glfwMakeContextCurrent(m_window);
	
	const bool kVsync = true;
	if (kVsync)
	{
		glfwSwapInterval(1);
	}
    
    glfwSetKeyCallback(m_window, on_key_callback);
    glfwSetCursorPosCallback(m_window, on_mouse_move_callback);
    glfwSetMouseButtonCallback(m_window, on_mouse_button_callback);
	
	return true;
}

//-------------------------------------------------------------------------------------------
void Window::Destroy()
{
	if (m_window)
	{
		glfwDestroyWindow(m_window);
		m_window = nullptr;
	}
}

//-------------------------------------------------------------------------------------------
void Window::Draw()
{
	glfwSwapBuffers(m_window);
}

//-------------------------------------------------------------------------------------------
bool Window::IsRunning() const
{
	return !glfwWindowShouldClose(m_window);
}
