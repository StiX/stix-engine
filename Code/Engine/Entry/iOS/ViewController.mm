//
//  ViewController.m
//  StiX Engine
//
//  Created by StiX on 8/18/15.
//  Copyright © 2015 StiX. All rights reserved.
//

#import "ViewController.h"
#import "Log.h"
#import "input.h"
#import "Renderer.h"
#import "FileSystem.h"
#import "SystemUtil.h"
#import "ThreadEvent.h"
#import "UpdateThread.h"
#import "ThreadsSyncPoint.h"


@interface ViewController()
{
}

@property (strong, nonatomic) EAGLContext* context;

@end


@implementation ViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

	self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];

	if (!self.context) {
		NSLog(@"Failed to create ES context");
	}
	
	[EAGLContext setCurrentContext:self.context];
	
	GLKView *view = (GLKView *)self.view;
	view.context = self.context;
	view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    self.preferredFramesPerSecond = 60;
	
	SetWorkDir(GetExecutableDirPath());
	
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* logFilePath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], @"Log.html"];
	
	log_init([logFilePath UTF8String]);

	const float scale = view.contentScaleFactor;
	const vec2i screen_size = vec2i(view.bounds.size.width * scale, view.bounds.size.height * scale);
	renderer::init(screen_size);
    
    ThreadEvent update_thread_initialized;
    UpdateThreadSpawn(&update_thread_initialized);
    update_thread_initialized.Wait();
}

- (void)dealloc
{
	UpdateThreadRequestTermination();
	renderer::frame_end_sync_point().ReleaseWaiters();
	UpdateThreadWaitToTerminate();
	
	renderer::shutdown();
	
	if ([EAGLContext currentContext] == self.context) {
		[EAGLContext setCurrentContext:nil];
	}
	
	self.context = nil;
//	[self.context release];

	log_shutdown();
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];

	// Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
	return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch0 = [[touches allObjects] objectAtIndex:0];
    CGPoint pos = [touch0 locationInView:self.view];
    
    GLKView *view = (GLKView *)self.view;
	pos.x *= view.contentScaleFactor;
    pos.y = view.drawableHeight - pos.y * view.contentScaleFactor;
    
    input::touch touch;
    touch.state = input::touch_down;
    touch.position = vec2i(pos.x, pos.y);
    touch.prev_position = touch.position;
    
    input::internal::on_touch(touch);
    input::internal::on_key(input::lmouse, true);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch0 = [[touches allObjects] objectAtIndex:0];
    CGPoint pos = [touch0 locationInView:self.view];
    
    GLKView *view = (GLKView *)self.view;
	pos.x *= view.contentScaleFactor;
    pos.y = view.drawableHeight - pos.y * view.contentScaleFactor;
    
    input::touch touch;
    touch.state = input::touch_released;
    touch.position = vec2i(pos.x, pos.y);
    
    input::internal::on_touch(touch);
    input::internal::on_key(input::lmouse, false);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch0 = [[touches allObjects] objectAtIndex:0];
    CGPoint pos = [touch0 locationInView:self.view];
    CGPoint prev_pos = [touch0 previousLocationInView:self.view];
    
    GLKView *view = (GLKView *)self.view;
	pos.x *= view.contentScaleFactor;
    pos.y = view.drawableHeight - pos.y * view.contentScaleFactor;
    
    input::touch touch;
    touch.state = input::touch_moved;
    touch.position = vec2i(pos.x, pos.y);
    
    prev_pos.y = view.drawableHeight - prev_pos.y * view.contentScaleFactor;
    touch.prev_position = vec2i(prev_pos.x, prev_pos.y);
    
    input::internal::on_touch(touch);
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
	renderer::update();
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{

}

@end
