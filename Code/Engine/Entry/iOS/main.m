//
//  main.m
//  StiX Engine
//
//  Created by StiX on 8/18/15.
//  Copyright © 2015 StiX. All rights reserved.
//

#import <UIKit/UIKit.h>


int main(int argc, char * argv[]) {
	@autoreleasepool {
		return UIApplicationMain(argc, argv, nil, @"AppDelegate");
	}
}
