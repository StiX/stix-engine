//
//  AppDelegate.h
//  StiX Engine
//
//  Created by StiX on 8/18/15.
//  Copyright © 2015 StiX. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

