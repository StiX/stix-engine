
#include "System.h"
#include <windows.h>


//-------------------------------------------------------------------------------------------
System::System()
{
	SYSTEM_INFO systemInfo;
	GetSystemInfo(&systemInfo);

	m_virtualPageSize = systemInfo.dwPageSize;
	m_coresCount = systemInfo.dwNumberOfProcessors;
	
	switch (systemInfo.wProcessorArchitecture)
	{
	case PROCESSOR_ARCHITECTURE_AMD64:
		m_procArch = AMD64;
		break;

	case PROCESSOR_ARCHITECTURE_ARM:
		m_procArch = ARMv7;
		break;

	case PROCESSOR_ARCHITECTURE_INTEL:
		m_procArch = x86;
		break;

	case PROCESSOR_ARCHITECTURE_UNKNOWN:
	default:
		m_procArch = ArchUnknown;
		break;
	}
}