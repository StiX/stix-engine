
#include "ImageLoader.h"
#include "Log.h"
#include "Image.h"
#include "ReadStream.h"


struct DDSPixelFormat
{
	u32 size;
	u32 flags;
	u32 fourCC;
	u32 RGBBitCount;
	u32 RBitMask;
	u32 GBitMask;
	u32 BBitMask;
	u32 ABitMask;
};

struct DDSHeader
{
	u32 size;
	u32 flags;
	u32 height;
	u32 width;
	u32 pitchOrLinearSize;
	u32 depth;
	u32 mipMapCount;
	u32 reserved1[11];
	DDSPixelFormat pixelFormat;
	u32 caps;
	u32 caps2;
	u32 caps3;
	u32 caps4;
	u32 reserved2;
};


//-------------------------------------------------------------------------------------------
bool LoadDDS(ReadStream& memoryStream, ImageDescription& imageDescription)
{
	const u32 kDDSPFFlagCompressed = 0x4;

#	define MAKEFOURCC(a, b, c, d) ((u32)(a) | (u32)((b) << 8) | (u32)((c) << 16) | (u32)((d) << 24))


	const DDSHeader* pHeader = memoryStream.ReadPtr<DDSHeader>();

	if (pHeader->caps2)
	{
		log_error("Texture Loader", "Cube\\Volume textures are not supported");
		return false;
	}

	imageDescription.m_mipMapsCount = pHeader->mipMapCount ? (u8)pHeader->mipMapCount : 1;
	imageDescription.m_isCompressed = (pHeader->pixelFormat.flags & kDDSPFFlagCompressed) != 0;

	if (imageDescription.m_isCompressed)
	{
		switch (pHeader->pixelFormat.fourCC)
		{
		case MAKEFOURCC('D', 'X', 'T', '1'):
			imageDescription.m_bytesPerElement = 8;
			imageDescription.m_format = ImageDescription::Format::DXT1;
			break;

		case MAKEFOURCC('D', 'X', 'T', '2'):
		case MAKEFOURCC('D', 'X', 'T', '3'):
			imageDescription.m_bytesPerElement = 16;
			imageDescription.m_format = ImageDescription::Format::DXT3;
			break;

		case MAKEFOURCC('D', 'X', 'T', '4'):
		case MAKEFOURCC('D', 'X', 'T', '5'):
			imageDescription.m_bytesPerElement = 16;
			imageDescription.m_format = ImageDescription::Format::DXT5;
			break;

		case MAKEFOURCC('D', 'X', '1', '0'):
			log_error("Texture Loader", "DX10 DDS textures are not supported yet");
			return false;

		default:
			log_error("Texture Loader", "Impossible four cc value in DDS pixel format: %.*s", 4, pHeader->pixelFormat.fourCC);
			return false;
		}
	}
	else
	{
		log_error("Texture Loader", "This loader does not support non-compressed textures");
		return false;
	}


	u16 width = (u16)pHeader->width;
	u16 height = (u16)pHeader->height;

	for (u32 mipLevel = 0; mipLevel < imageDescription.m_mipMapsCount; ++mipLevel)
	{
		imageDescription.m_width[mipLevel] = width;
		imageDescription.m_height[mipLevel] = height;

		const u32 kRowBlocks = (imageDescription.m_isCompressed) ? (width + 3) / 4 : width;
		const u32 kColumnBlocks = (imageDescription.m_isCompressed) ? (height + 3) / 4 : height;
		
		const u32 kDataSize = kRowBlocks * kColumnBlocks * imageDescription.m_bytesPerElement;
		imageDescription.m_dataSize[mipLevel] = kDataSize;
		imageDescription.m_pData[mipLevel] = memoryStream.Read(kDataSize);

		width = (width > 1) ? width >> 1 : 1;
		height = (height > 1) ? height >> 1 : 1;
	}

	return true;
}