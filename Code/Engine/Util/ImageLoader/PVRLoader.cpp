
#include "ImageLoader.h"
#include "Log.h"
#include "Image.h"
#include "math_lib.h"
#include "ReadStream.h"


struct PVRHeader
{
	u32 flags;
	u32 pixelFormatLow;
	u32 pixelFormatHi;
	u32 colorSpace;
	u32 channelType;
	u32 height;
	u32 width;
	u32 depth;
	u32 surfacesCount;
	u32 facesCount;
	u32 mipMapsCount;
	u32 metaDataSize;
};

enum PVRPixelFormat
{
	PVRTC_2BPP_RGB,
	PVRTC_2BPP_RGBA,
	PVRTC_4BPP_RGB,
	PVRTC_4BPP_RGBA,
	PVRTCII_2BPP,
	PVRTCII_4BPP,
	ETC1,
	DXT1,
	DXT2,
	DXT3,
	DXT4,
	DXT5,
	// some formats are skipped
	ETC2_RGB = 22,
	ETC2_RGBA,
	ETC2_RGB_A1
	// other formats are omitted
};

struct PVRMetaData
{
	u32 fourCC;
	u32 key;
	u32 dataSize;
	u8	data[0];
};

typedef void (*DataReaderFunc)(ReadStream& memoryStream, ImageDescription& imageDescription);


//-------------------------------------------------------------------------------------------
inline u16 Max(u16 a, u16 b)
{
	return a > b ? a : b;
}

//-------------------------------------------------------------------------------------------
void PvrtcDataReader(ReadStream& memoryStream, ImageDescription& imageDescription)
{
	u16 width = imageDescription.m_width[0];
	u16 height = imageDescription.m_height[0];
    
    ASSERT(math::is_power_of_2(width));
    ASSERT(math::is_power_of_2(height));

#ifdef __iOS__
    ASSERT(width == height, "iOS doesn't support not square PVR textures");
#endif

	const u16 kWidthMinBlockSize = imageDescription.m_bytesPerElement == 4 ? 8 : 16;

	for (u32 mipLevel = 0; mipLevel < imageDescription.m_mipMapsCount; ++mipLevel)
	{
		const u32 kDataSize = (Max(width, kWidthMinBlockSize) * Max(height, 8) * imageDescription.m_bytesPerElement + 7) / 8;

		imageDescription.m_width[mipLevel] = width;
		imageDescription.m_height[mipLevel] = height;
		imageDescription.m_dataSize[mipLevel] = kDataSize;
		imageDescription.m_pData[mipLevel] = memoryStream.Read(kDataSize);

		width = Max(width >> 1, 1);
		height = Max(height >> 1, 1);
	}
}

//-------------------------------------------------------------------------------------------
void BcDataReader(ReadStream& memoryStream, ImageDescription& imageDescription)
{
	u16 width = imageDescription.m_width[0];
	u16 height = imageDescription.m_height[0];

	for (u32 mipLevel = 0; mipLevel < imageDescription.m_mipMapsCount; ++mipLevel)
	{
		const u32 kRowBlocks = (width + 3) / 4;
		const u32 kColumnBlocks = (height + 3) / 4;
		const u32 kDataSize = kRowBlocks * kColumnBlocks * imageDescription.m_bytesPerElement;

		imageDescription.m_width[mipLevel] = width;
		imageDescription.m_height[mipLevel] = height;
		imageDescription.m_dataSize[mipLevel] = kDataSize;
		imageDescription.m_pData[mipLevel] = memoryStream.Read(kDataSize);

		width = Max(width >> 1, 1);
		height = Max(height >> 1, 1);
	}
}

//-------------------------------------------------------------------------------------------
bool LoadPVR(ReadStream& memoryStream, ImageDescription& imageDescription)
{
	const PVRHeader* pHeader = memoryStream.ReadPtr<PVRHeader>();

	if (pHeader->surfacesCount > 1)
	{
		log_error("Texture Loader", "Texture arrays are not supported");
		return false;
	}
	else if (pHeader->facesCount > 1)
	{
		log_error("Texture Loader", "Cubemap textures are not supported");
		return false;
	}

	memoryStream.Read(pHeader->metaDataSize);	// Skip metadata

	imageDescription.m_mipMapsCount = (u8)pHeader->mipMapsCount;
	imageDescription.m_isCompressed = true;// pHeader->pixelFormatLow >= PVRTC_2BPP_RGB && pHeader->pixelFormatLow <= DXT5;
	imageDescription.m_width[0] = (u16)pHeader->width;
	imageDescription.m_height[0] = (u16)pHeader->height;
	DataReaderFunc pDataReader = nullptr;

	switch (pHeader->pixelFormatLow)
	{
	case PVRTC_2BPP_RGB:
		imageDescription.m_bytesPerElement = 2;
		imageDescription.m_format = ImageDescription::Format::PVRTC_2BPP_RGB;
		pDataReader = &PvrtcDataReader;
		break;

	case PVRTC_2BPP_RGBA:
		imageDescription.m_bytesPerElement = 2;
		imageDescription.m_format = ImageDescription::Format::PVRTC_2BPP_RGBA;
		pDataReader = &PvrtcDataReader;
		break;

	case PVRTC_4BPP_RGB:
		imageDescription.m_bytesPerElement = 4;
		imageDescription.m_format = ImageDescription::Format::PVRTC_4BPP_RGB;
		pDataReader = &PvrtcDataReader;
		break;

	case PVRTC_4BPP_RGBA:
		imageDescription.m_bytesPerElement = 4;
		imageDescription.m_format = ImageDescription::Format::PVRTC_4BPP_RGBA;
		pDataReader = &PvrtcDataReader;
		break;

	case PVRTCII_2BPP:
		imageDescription.m_bytesPerElement = 2;
		imageDescription.m_format = ImageDescription::Format::PVRTCII_2BPP;
		pDataReader = &PvrtcDataReader;
		break;

	case PVRTCII_4BPP:
		imageDescription.m_bytesPerElement = 4;
		imageDescription.m_format = ImageDescription::Format::PVRTCII_4BPP;
		pDataReader = &PvrtcDataReader;
		break;

	case ETC2_RGB:
		imageDescription.m_bytesPerElement = 8;
		imageDescription.m_format = ImageDescription::Format::ETC2_RGB;		
		pDataReader = &BcDataReader;
		break;

	case ETC2_RGBA:
		imageDescription.m_bytesPerElement = 16;
		imageDescription.m_format = ImageDescription::Format::ETC2_RGBA;
		pDataReader = &BcDataReader;
		break;

	case ETC2_RGB_A1:
		imageDescription.m_bytesPerElement = 8;
		imageDescription.m_format = ImageDescription::Format::ETC2_RGB_A1;
		pDataReader = &BcDataReader;
		break;

	case DXT1:
		imageDescription.m_bytesPerElement = 8;
		imageDescription.m_format = ImageDescription::Format::DXT1;
		pDataReader = &BcDataReader;
		break;

	case DXT2:
	case DXT3:
		imageDescription.m_bytesPerElement = 16;
		imageDescription.m_format = ImageDescription::Format::DXT3;
		pDataReader = &BcDataReader;
		break;

	case DXT4:
	case DXT5:
		imageDescription.m_bytesPerElement = 16;
		imageDescription.m_format = ImageDescription::Format::DXT5;
		pDataReader = &BcDataReader;
		break;

	default:
		log_error("Texture Loader", "Unsupported pixel format %d", pHeader->pixelFormatLow);
		return false;
	}

	pDataReader(memoryStream, imageDescription);

	return true;
}