
#include "Image.h"
#include "Log.h"
#include "ReadStream.h"
#include "ImageLoader.h"


//-------------------------------------------------------------------------------------------
bool LoadTexture(ReadStream& stream, ImageDescription& result)
{
#	define MAKEFOURCC(a, b, c, d) ((u32)(a) | (u32)((b) << 8) | (u32)((c) << 16) | (u32)((d) << 24))


	const u32 kFourCC = stream.Read<u32>();
	switch (kFourCC)
	{
	case MAKEFOURCC('D', 'D', 'S', ' '):
		return LoadDDS(stream, result);

	case MAKEFOURCC('P', 'V', 'R', 3):
		return LoadPVR(stream, result);

	default:
		log_error("Texture Loader", "Unsupported texture four cc: %.*s", 4, (char*)&kFourCC);
		break;
	}

#	undef MAKEFOURCC

	return false;
}