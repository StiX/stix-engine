
#pragma once

#include "Types.h"

class ReadStream;


struct ImageDescription
{
	enum class Format : u8
	{
				DXT1,
				DXT3,
				DXT5,
				PVRTC_2BPP_RGB,
				PVRTC_2BPP_RGBA,
				PVRTC_4BPP_RGB,
				PVRTC_4BPP_RGBA,
				PVRTCII_2BPP,
				PVRTCII_4BPP,
				ETC2_RGB,
				ETC2_RGBA,
				ETC2_RGB_A1
	};

	enum
	{
				kMaxMipMapLevels = 14		// 14 mip map levels for up to 8K textures
	};


	const void*	m_pData[kMaxMipMapLevels];
	u32			m_dataSize[kMaxMipMapLevels];
	u16			m_width[kMaxMipMapLevels];
	u16			m_height[kMaxMipMapLevels];
	u8			m_mipMapsCount;
	u8			m_bytesPerElement;
	Format		m_format;
	bool		m_isCompressed;
};

struct texture_params {
	enum filtering {
				NEAREST,
				LINEAR,
				NEAREST_MIPMAP,
				LINEAR_MIPMAP,
				NEAREST_2_MIPMAPS,
				LINEAR_2_MIPMAPS
	};

	filtering	minification;
	filtering	magnification;

				texture_params() : minification(LINEAR_2_MIPMAPS), magnification(LINEAR) {}
				texture_params(filtering min, filtering mag) : minification(min), magnification(mag) {}
};



bool LoadTexture(ReadStream& stream, ImageDescription& result);