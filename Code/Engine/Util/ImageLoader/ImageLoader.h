
/************************************************************************/
/* Image Loaders require a memory stream to skip file's FourCC          */
/************************************************************************/

#pragma once

class ReadStream;
struct ImageDescription;


bool LoadDDS(ReadStream& memoryStream, ImageDescription& imageDescription);
bool LoadPVR(ReadStream& memoryStream, ImageDescription& imageDescription);