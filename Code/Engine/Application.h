
#pragma once

#include "ClassHelpers.h"

#ifdef __ANDROID__
#	include "android_native_app_glue.h"
#endif


struct IApplication
{
	virtual			~IApplication() {}

	virtual void	Init() = 0;
	virtual void	Shutdown() = 0;

	virtual void	Update(float deltaTime) = 0;
	virtual void	Render() const = 0;

protected:
					IApplication() {
#ifdef __ANDROID__
						app_dummy();
#endif
					}

private:
	MAKE_UNCOPYABLE(IApplication);
};


extern IApplication* g_pApplication;

#define DECLARE_APPLICATION(_app)	\
	static _app applicationInst;	\
	IApplication* g_pApplication = &applicationInst;