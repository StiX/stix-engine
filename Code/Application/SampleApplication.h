
#pragma once

#include "Application.h"


class SampleApplication : public IApplication
{
public:
					SampleApplication();
	virtual			~SampleApplication();

	virtual void	Init();
	virtual void	Shutdown();

	virtual void	Update(float deltaTime);
	virtual void	Render() const;

protected:
private:
};