
#pragma once

#include <utility>


template <class K, class V>
struct Pair
{
	K	key;
	V	value;

		inline Pair(const K& key, const V& value)
			: key(key), value(value)
		{
		}

		template <class OtherK, class OtherV>
		inline Pair(OtherK&& key, OtherV&& value)
			: key(std::forward<OtherK>(key)), value(std::forward<OtherV>(value))
		{
		}
};