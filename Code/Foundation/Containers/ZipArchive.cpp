
#include "ZipArchive.h"
#include <cstdlib>
#include "math_lib.h"


//-------------------------------------------------------------------------------------------
ZipArchive::ZipArchive()
{
	memset(&m_zipArchive, 0, sizeof(m_zipArchive));
}

//-------------------------------------------------------------------------------------------
ZipArchive::ZipArchive(const char* archivePath)
{
	memset(&m_zipArchive, 0, sizeof(m_zipArchive));
	OpenArchive(archivePath);
}

//-------------------------------------------------------------------------------------------
ZipArchive::~ZipArchive()
{
	mz_zip_reader_end(&m_zipArchive);
}

//-------------------------------------------------------------------------------------------
void ZipArchive::OpenArchive(const char* archivePath)
{
	ASSERT(m_zipArchive.m_archive_size == 0);

	const mz_bool kStatus = mz_zip_reader_init_file(&m_zipArchive, archivePath, 0);
	if (kStatus)
	{
		m_archivePath = archivePath;
		m_archive = ::OpenFile(archivePath, F_ACCESS_READ, F_SEQUENTIAL_ACCESS_HINT);
	}
}

//-------------------------------------------------------------------------------------------
u32 ZipArchive::GetFilesCount() const
{
	return mz_zip_reader_get_num_files(&m_zipArchive);
}

//-------------------------------------------------------------------------------------------
ZipFile ZipArchive::FindFile(const char* filePath) const
{
	ZipFile file;
	memset(&file, 0, sizeof(file));
	
	file.fileIndex = mz_zip_reader_locate_file(&m_zipArchive, filePath, "", 0);
	if (file.fileIndex == -1)
	{
		return file;
	}

	const u32 kLocalHeaderSize = 30;
	char localHeader[kLocalHeaderSize];

	mz_zip_archive_file_stat fileStat;
	mz_zip_reader_file_stat(&m_zipArchive, file.fileIndex, &fileStat);

	m_archive.Seek((ptrdiff_t)fileStat.m_local_header_ofs, FILE_ORIGIN_BEGIN);
	m_archive.Read(localHeader, kLocalHeaderSize);

	const u32 kLocalHeaderSig = 0x04034b50;
	if (*(u32*)localHeader != kLocalHeaderSig)
	{
		return file;
	}

	const u32 kFilenameLengthOffset = 26;
	const u32 kFilenameExtraLengthOffset = 28;
	const u32 kOffset = kLocalHeaderSize + *(u16*)math::ptr_offset(localHeader, kFilenameLengthOffset) + *(u16*)math::ptr_offset(localHeader, kFilenameExtraLengthOffset);

	file.offset = (u32)fileStat.m_local_header_ofs + kOffset;
	file.size = (u32)fileStat.m_comp_size;
	file.pArchive = this;

	return file;
}

//-------------------------------------------------------------------------------------------
void* ZipArchive::MapFile(const ZipFile& file) const
{
	ASSERT(file.pArchive == this);

	return m_archive.MapFilePart(F_MAPPING_READ, file.offset, file.size);
}