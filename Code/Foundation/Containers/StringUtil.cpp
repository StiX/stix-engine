
#include "StringUtil.h"
#include "Debug.h"


//-------------------------------------------------------------------------------------------
String StringUtil::ExtractFileName(const char* str) {
	const size_t kStrLen = strlen(str);

	for (ptrdiff_t i = kStrLen - 2; i >= 0; --i) {
		if (str[i] == '/' || str[i] == '\\') {
			return String(&str[i + 1], kStrLen - i - 1);
		}
	}

	return String(str, kStrLen);
}

//-------------------------------------------------------------------------------------------
bool StringUtil::MatchWildcardPattern(const char* str, const char* pattern) {
	while (*str) {
		if (*pattern == 0) {
			return false;
		}

		if (*pattern == '?') {
			// simply skip
		} else if (*pattern == '*') {
			++pattern;
			ASSERT(*pattern != '*' && *pattern != '?');

			do {
				++str;
			} while (*str && *str != *pattern);

			continue;
		} else if (*pattern != *str) {
			return false;
		}

		++str;
		++pattern;
	}

	return *pattern == 0;
}