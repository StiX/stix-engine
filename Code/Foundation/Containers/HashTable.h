
#pragma once

#include <cstdlib>
#include <utility>
#include <type_traits>
#include "Hash.h"
#include "Debug.h"
#include "math_lib.h"
#include "VirtualPageAllocator.h"


template<class KEY, class VALUE, class HASH = Hasher<KEY>, class ALLOC = VirtualPageAllocator>
class HashTable
{
public:
	class Iterator;
	typedef	typename std::conditional<std::is_pointer<VALUE>::value, VALUE, VALUE*>::type ValuePointer;

	typedef KEY		KeyType;
	typedef VALUE	ValueType;

					HashTable();
	explicit		HashTable(u32 initialCapacity);
					HashTable(HashTable&& other);
					HashTable(const HashTable& other);
					~HashTable();

	HashTable&		operator=(HashTable&& other);
	HashTable&		operator=(const HashTable& other);

	void			Insert(const KEY& object, VALUE value);
	void			Remove(const KEY& object);
	ValuePointer	Find(const KEY& object);						// Pointers CANNOT be cached!
	const ValuePointer Find(const KEY& object) const;
	Iterator		FindIterator(const KEY& object) const;

	Iterator		GetFirstElement() const;
	Iterator		GetEnd() const;

	void			Remove(const Iterator& iterator);

	u32				GetCapacity() const;
	u32				GetSize() const;

protected:
private:
	enum
	{
		MIN_SIZE =	256	// The max PoT number of elements that can fit in one 4K page (both x86 and x64)
	};

	template <bool T>
	struct ValueIsPointer
	{
		enum { value = T };
	};

	struct KeyValue
	{
		typename HASH::KeyType m_key;
		u32			m_nextCollided;
		VALUE		m_value;
	};

	KeyValue*		m_array;
	u32				m_capacity;
	u32				m_storedCount;
	u32				m_mask;

	u32				GetIndex(size_t key) const;
	u32				WrapAroundValue(u32 value) const;			// wrap around value, if it exceeds the capacity of array, the resulting value never will be a zero
	void			IncreaseStoredCount();						// returns true, if array was reallocated

	void			Realloc();									// capacity is always doubled
	void			Allocate(size_t size);
	void			AllocateAndCopyFrom(const HashTable& other);

	void			ReallocationInsert(typename HASH::KeyType key, VALUE value);

	ValuePointer	GetValuePointer(ValueIsPointer<true>, u32 index) const;
	ValuePointer	GetValuePointer(ValueIsPointer<false>, u32 index) const;

	u32				FindFirsNonFreeIndex(u32 initialIndex = 0) const;
};





template<class KEY, class VALUE, class HASH, class ALLOC>
class HashTable<KEY, VALUE, HASH, ALLOC>::Iterator
{
public:
	inline ValuePointer			GetValue()								{ return m_pHashTable->GetValuePointer(ValueIsPointer<std::is_pointer<VALUE>::value>(), m_index); }

	inline const ValuePointer	GetValue() const						{ return m_pHashTable->GetValuePointer(ValueIsPointer<std::is_pointer<VALUE>::value>(), m_index); }

	inline const Iterator&		operator++()							{ m_index = m_pHashTable->FindFirsNonFreeIndex(m_index); return *this; }

	inline bool					operator!=(const Iterator& other) const	{ return (m_index != other.m_index) || (m_pHashTable != other.m_pHashTable); }

	inline						operator bool() const					{ return m_index != kInvalidIndex; }

protected:
private:
	friend class HashTable<KEY, VALUE, HASH, ALLOC>;

	const HashTable*			m_pHashTable;
	u32							m_index;

	inline						Iterator(u32 index, const HashTable* pHashTable) : m_pHashTable(pHashTable), m_index(index) {}
};





//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline HashTable<KEY, VALUE, HASH, ALLOC>::HashTable()
	: m_capacity(MIN_SIZE), m_storedCount(0), m_mask(m_capacity - 1)
{
	Allocate(m_capacity);
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline HashTable<KEY, VALUE, HASH, ALLOC>::HashTable(u32 initialCapacity)
	: m_capacity(initialCapacity <= MIN_SIZE ? MIN_SIZE : math::nearest_pow2(initialCapacity)), m_storedCount(0), m_mask(m_capacity - 1)
{
	Allocate(m_capacity);
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline HashTable<KEY, VALUE, HASH, ALLOC>::HashTable(HashTable&& other)
	: m_capacity(other.m_capacity), m_storedCount(other.m_storedCount), m_mask(other.m_mask)
{
	m_array = other.m_array;
	other.m_array = nullptr;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline HashTable<KEY, VALUE, HASH, ALLOC>::HashTable(const HashTable& other)
	: m_capacity(other.m_capacity), m_storedCount(other.m_storedCount), m_mask(other.m_mask)
{
	AllocateAndCopyFrom(other);
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline HashTable<KEY, VALUE, HASH, ALLOC>::~HashTable()
{
	if (m_array)
	{
		ALLOC::Free(m_array, sizeof(KeyValue) * m_capacity);
	}

	//static_assert(sizeof(KeyValue) <= 16, "MIN_SIZE * 16 = 4K (size of one virtual page)");
	static_assert(std::is_integral<typename HASH::KeyType>::value, "HASH::KeyType should be of integral type");
	//static_assert(std::is_trivially_copyable<VALUE>::value, "VALUE type should be a POD type");
	static_assert(sizeof(VALUE) <= sizeof(size_t), "size of VALUE type should be less or equal to sizeof(size_t)");
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline HashTable<KEY, VALUE, HASH, ALLOC>& HashTable<KEY, VALUE, HASH, ALLOC>::operator=(HashTable&& other)
{
	std::swap(m_array, other.m_array);
	// other object will be destroyed on exit from this function, so there are no need to swap other fields
	m_capacity = other.m_capacity;
	m_storedCount = other.m_storedCount;
	m_mask = other.m_mask;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline HashTable<KEY, VALUE, HASH, ALLOC>& HashTable<KEY, VALUE, HASH, ALLOC>::operator=(const HashTable& other)
{
	ALLOC::Free(m_array, sizeof(KeyValue) * m_capacity);
	m_capacity = other.m_capacity;
	m_storedCount = other.m_storedCount;
	m_mask = other.m_mask;

	AllocateAndCopyFrom(other);
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline void HashTable<KEY, VALUE, HASH, ALLOC>::Insert(const KEY& object, VALUE value)
{
	const typename HASH::KeyType kKey = HASH::GetKey(object);
	ASSERT(kKey != 0, "This hash table cannot handle 0s as a key");

	const size_t kHash = HASH::Hash(kKey);
	u32 index = GetIndex(kHash);

	switch (m_array[index].m_key)
	{
	default:
		if (m_array[index].m_key == kKey)
		{
			m_array[index].m_value = value;
			return;
		}
		else
		{
			// find if this key already exists in the table
			while (m_array[index].m_nextCollided)
			{
				index = m_array[index].m_nextCollided;

				if (m_array[index].m_key == kKey)
				{
					m_array[index].m_value = value;
					return;
				}
				else if (m_array[index].m_key == 0)
				{
					m_array[index].m_key = kKey;
					m_array[index].m_value = value;

					IncreaseStoredCount();
					return;
				}
			}

			// the key was not found, now it's time to do linear probing
			const u32 kPrevIndex = index;

			do
			{
				index = WrapAroundValue(++index);
			} while (m_array[index].m_key);	// since array can never be 100% full, this loop will always hit the break condition

			m_array[kPrevIndex].m_nextCollided = index;
		}

	case 0:
		m_array[index].m_key = kKey;
		m_array[index].m_value = value;

		IncreaseStoredCount();
	}
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline void HashTable<KEY, VALUE, HASH, ALLOC>::Remove(const KEY& object)
{
	const typename HASH::KeyType kKey = HASH::GetKey(object);
	if (kKey == 0)
	{
		return;
	}

	const size_t kHash = HASH::Hash(kKey);
	u32 index = GetIndex(kHash);

	if (m_array[index].m_key == kKey)
	{
		m_array[index].m_key = 0;
		// do not override m_nextCollided, since this element is the beginning of linked list
		--m_storedCount;
	}
	else
	{
		while (m_array[index].m_nextCollided)
		{
			const u32 kPrevIndex = index;
			index = m_array[index].m_nextCollided;

			if (m_array[index].m_key == kKey)
			{
				m_array[index].m_key = 0;
				m_array[kPrevIndex].m_nextCollided = m_array[index].m_nextCollided;
				--m_storedCount;

				break;
			}
		}
	}
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline typename HashTable<KEY, VALUE, HASH, ALLOC>::ValuePointer HashTable<KEY, VALUE, HASH, ALLOC>::Find(const KEY& object)
{
	return const_cast<ValuePointer>(const_cast<const HashTable<KEY, VALUE, HASH, ALLOC>*>(this)->Find(object));
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline const typename HashTable<KEY, VALUE, HASH, ALLOC>::ValuePointer HashTable<KEY, VALUE, HASH, ALLOC>::Find(const KEY& object) const
{
	Iterator it = FindIterator(object);

	if (it.m_index != kInvalidIndex)
	{
		return GetValuePointer(ValueIsPointer<std::is_pointer<VALUE>::value>(), it.m_index);
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline typename HashTable<KEY, VALUE, HASH, ALLOC>::Iterator HashTable<KEY, VALUE, HASH, ALLOC>::FindIterator(const KEY& object) const
{
	const typename HASH::KeyType kKey = HASH::GetKey(object);
	if (kKey != 0)
	{
		const size_t kHash = HASH::Hash(kKey);
		u32 index = GetIndex(kHash);

		if (m_array[index].m_key == kKey)
		{
			return Iterator(index, this);
		}
		else
		{
			while (m_array[index].m_nextCollided)
			{
				index = m_array[index].m_nextCollided;

				if (m_array[index].m_key == kKey)
				{
					return Iterator(index, this);
				}
			}
		}
	}

	return GetEnd();
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline u32 HashTable<KEY, VALUE, HASH, ALLOC>::FindFirsNonFreeIndex(u32 initialIndex) const
{
	ASSERT(initialIndex < m_capacity);

	for (++initialIndex; initialIndex < m_capacity; ++initialIndex)
	{
		if (m_array[initialIndex].m_key)
		{
			return initialIndex;
		}
	}
	
	return kInvalidIndex;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline typename HashTable<KEY, VALUE, HASH, ALLOC>::Iterator HashTable<KEY, VALUE, HASH, ALLOC>::GetFirstElement() const
{
	const u32 kIndex = FindFirsNonFreeIndex();
	if (kIndex)
	{
		return Iterator(kIndex, this);
	}

	return GetEnd();
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline typename HashTable<KEY, VALUE, HASH, ALLOC>::Iterator HashTable<KEY, VALUE, HASH, ALLOC>::GetEnd() const
{
	return Iterator(kInvalidIndex, this);
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline void HashTable<KEY, VALUE, HASH, ALLOC>::Remove(const Iterator& iterator)
{
	ASSERT(iterator.m_pHashTable == this);
	ASSERT(iterator.m_index < m_capacity);
	ASSERT(m_array[iterator.m_index].m_key);

	m_array[iterator.m_index].m_key = 0;
	--m_storedCount;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline u32 HashTable<KEY, VALUE, HASH, ALLOC>::GetCapacity() const
{
	return m_capacity;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline u32 HashTable<KEY, VALUE, HASH, ALLOC>::GetSize() const
{
	return m_storedCount;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline u32 HashTable<KEY, VALUE, HASH, ALLOC>::GetIndex(size_t key) const
{
	return (u32)(key & m_mask);
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline u32 HashTable<KEY, VALUE, HASH, ALLOC>::WrapAroundValue(u32 value) const
{
	return value & m_mask;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline void HashTable<KEY, VALUE, HASH, ALLOC>::IncreaseStoredCount()
{
	++m_storedCount;

	if (4 * m_storedCount > 3 * m_capacity)
	{
		Realloc();
	}
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline void HashTable<KEY, VALUE, HASH, ALLOC>::Realloc()
{
	const u32 kOldCapacity = m_capacity;

	m_capacity *= 2;
	m_mask = m_capacity - 1;

	KeyValue* pOldArray = m_array;
	Allocate(m_capacity);

	for (u32 i = 0; i < kOldCapacity; ++i)
	{
		if (pOldArray[i].m_key != 0)
		{
			ReallocationInsert(pOldArray[i].m_key, pOldArray[i].m_value);
		}
	}

	ALLOC::Free(pOldArray, sizeof(KeyValue) * kOldCapacity);
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline void HashTable<KEY, VALUE, HASH, ALLOC>::Allocate(size_t size)
{
	m_array = (KeyValue*)ALLOC::AllocAndZero(size * sizeof(KeyValue));
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline void HashTable<KEY, VALUE, HASH, ALLOC>::AllocateAndCopyFrom(const HashTable& other)
{
	m_array = (KeyValue*)ALLOC::Alloc(other.m_capacity * sizeof(KeyValue));
	memcpy(m_array, other.m_capacity, m_capacity * sizeof(KeyValue));
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline void HashTable<KEY, VALUE, HASH, ALLOC>::ReallocationInsert(typename HASH::KeyType key, VALUE value)
{
	const size_t kHash = HASH::Hash(key);
	u32 index = GetIndex(kHash);

	if (m_array[index].m_key != 0)		// hash collision
	{
		while (m_array[index].m_nextCollided)
		{
			index = m_array[index].m_nextCollided;
		}

		// do the linear probing to find a free cell
		const u32 kPrevIndex = index;

		do
		{
			index = WrapAroundValue(++index);
		} while (m_array[index].m_key);	// since array can never be 100% full, this loop will always hit the break condition

		m_array[kPrevIndex].m_nextCollided = index;
	}

	m_array[index].m_key = key;
	m_array[index].m_value = value;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline typename HashTable<KEY, VALUE, HASH, ALLOC>::ValuePointer HashTable<KEY, VALUE, HASH, ALLOC>::GetValuePointer(ValueIsPointer<true>, u32 index) const
{
	ASSERT(index < m_capacity);
	return m_array[index].m_value;
}

//-------------------------------------------------------------------------------------------
template<class KEY, class VALUE, class HASH, class ALLOC>
inline typename HashTable<KEY, VALUE, HASH, ALLOC>::ValuePointer HashTable<KEY, VALUE, HASH, ALLOC>::GetValuePointer(ValueIsPointer<false>, u32 index) const
{
	ASSERT(index < m_capacity);
	return &m_array[index].m_value;
}