
/*
NOTE:
Insert and InsertArray can accept index equal to size, because of previous value on index position is shifted to index + 1 position
and this allows to insert elements into the end of array.
*/

#pragma once

#include <utility>
#include <algorithm>
#include "Types.h"
#include "GenericAllocator.h"
#include "Debug.h"


template<class T, class Alloc = GenericAllocator>
class Array
{
public:
	typedef T*		Iterator;

					Array();												// Default constructor
	explicit		Array(u32 initialCapacity);							// Constructor with initial array capacity
					Array(Array<T>&& rhs);									// Move constructor
					Array(const Array<T>& rhs);								// Copy constructor
					~Array();

	Array<T>&		operator=(Array<T>&& rhs);
	Array<T>&		operator=(const Array<T>& rhs);
	bool			operator==(const Array<T>& rhs) const;
	bool			operator!=(const Array<T>& rhs) const;
	T&				operator[](index_t index);
	const T&		operator[](index_t index) const;

	void			Append(T&& elm);
	void			Append(const T& elm);									// Insert element at the end
	void			Append(Array<T>&& rhs);
	void			Append(const Array<T>& rhs);							// Insert Array at the end
	void			Insert(index_t index, T&& elm);
	void			Insert(index_t index, const T& elm);					// Insert element; element at index position will be shifted to index + 1 position
	void			InsertFast(index_t index, T&& elm);
	void			InsertFast(index_t index, const T& elm);				// Insert element at index position; previous element will be appended; destroys order;

	template<class ... Types>
	void			EmplaceBack(Types&& ... args);							// Construct the element at the end of array. This is a preferred way of adding elements
	template<class ... Types>
	void			Emplace(index_t index, Types&& ... args);
	template<class ... Types>
	void			EmplaceFast(index_t index, Types&& ... args);

	void			Remove(index_t index);									// Remove element at index
	void			RemoveLast();											// Remove element at the end
	void			RemoveFast(index_t index);								// Remove element and place last into index position; breaks sorting
	void			RemoveRange(index_t index, u32 count);					// Remove count elements starting from index
	void			RemoveRangeFast(index_t index, u32 count);				// Uses RemoveFast; breaks sorting

	T&				First() const;											// Return reference to first element
	T&				Last() const;											// Return reference to last element
	Iterator		Begin() const;											// Return reference to first element
	Iterator		End() const;											// Return reference to past the end element

	u32				GetSize() const;										// Return count of elements
	u32				GetCapacity() const;									// Return count of elements allocated for
	u32				GetAllocatedSize() const;								// Return size of allocated data

	bool			IsEmpty() const;
	void			Reserve(u32 count);										// Resize array to reserve count elements more

	void			Clear();												// Clear array; calls destructors
	void			Reset();												// Set elements count to 0; does not calls destructors

	void			Sort();
	Iterator		FindIteratorLinear(const T& elm) const;
	index_t			FindIndexLinear(const T& elm) const;
	index_t			FindIndexBinary(const T& elm) const;					// Binary search

private:
	T*				m_array;
	u32				m_size;
	u32				m_capacity;

	static const u32 kMinIncreaseCount = 16;
	static const u32 kMaxIncreaseCount = 4096;

	void			Copy(const Array<T>& rhs);								// Copy data from one array to another
	void			Allocate(u32 count);									// Allocates array for count elements
	void			DestroyElement(T& elm);
	void			MoveElementsToLeft(u32 index);

	void			Increase();
	void			ResizeTo(u32 count);									// Resize array to count. If count is <= Size, than nothing happens
	void			GrowToFitElement();
	void			GrowToFitMore(u32 count);

	void			SetValue(index_t index, T&& value);
	void			SetValue(index_t index, const T& value);
};





//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline Array<T, Alloc>::Array()
	: m_array(nullptr), m_size(0), m_capacity(0)
{
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline Array<T, Alloc>::Array(u32 initialCapacity)
	: m_array(nullptr), m_size(0), m_capacity(initialCapacity)
{
	if (initialCapacity > 0)
	{
		Allocate(initialCapacity);
	}
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline Array<T, Alloc>::Array(Array<T>&& rhs)
	: m_array(rhs.m_array), m_size(rhs.m_size), m_capacity(rhs.m_capacity)
{
	rhs.m_array = nullptr;
	rhs.m_size = 0;
	rhs.m_capacity = 0;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline Array<T, Alloc>::Array(const Array<T>& rhs)
	: m_array(nullptr), m_size(0), m_capacity(0)
{
	Copy(rhs);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline Array<T, Alloc>::~Array()
{
	Clear();
	Alloc::Free(m_array);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline Array<T>& Array<T, Alloc>::operator=(Array<T>&& rhs)
{
	std::swap(m_array, rhs.m_array);
	std::swap(m_size, rhs.m_size);
	std::swap(m_capacity, rhs.m_capacity);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline Array<T>& Array<T, Alloc>::operator=(const Array<T>& rhs)
{
	if (this != &rhs)
	{
		Copy(rhs);
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline bool Array<T, Alloc>::operator==(const Array<T>& rhs) const
{
	if (rhs.m_size != m_size)
	{
		return false;
	}
	else
	{
		return memcmp(m_array, rhs.m_array, m_size * sizeof(T));
	}
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline bool Array<T, Alloc>::operator!=(const Array<T>& rhs) const
{
	return !(*this == rhs);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline T& Array<T, Alloc>::operator[](index_t index)
{
	ASSERT(index < m_size);

	return m_array[index];
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline const T& Array<T, Alloc>::operator[](index_t index) const
{
	ASSERT(index < m_size);

	return m_array[index];
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Append(T&& elm)
{
	GrowToFitElement();

	SetValue(m_size, std::forward<T>(elm));

	++m_size;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Append(const T& elm)
{
	GrowToFitElement();

	SetValue(m_size, elm);

	++m_size;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Append(Array<T>&& rhs)
{
	GrowToFitMore(rhs.m_size);

	memcpy(&m_array[m_size], rhs.m_array, sizeof(T) * rhs.m_size);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Append(const Array<T>& rhs)
{
	GrowToFitMore(rhs.m_size);

	for (u32 i = 0; i < rhs.m_size; ++i)
	{
		SetValue(m_size, rhs.m_array[i]);
		++m_size;
	}
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Insert(index_t index, T&& elm)
{
	ASSERT(index < m_size);

	GrowToFitElement();
	MoveElementsToLeft(index);

	++m_size;

	SetValue(index, std::forward<T>(elm));
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Insert(index_t index, const T& elm)
{
	ASSERT(index < m_size);

	GrowToFitElement();
	MoveElementsToLeft(index);

	++m_size;

	SetValue(index, elm);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::InsertFast(index_t index, T&& elm)
{
	ASSERT(index < m_size);

	GrowToFitElement();

	memcpy(&m_array[m_size], &m_array[index], sizeof(T));
	++m_size;

	SetValue(index, std::forward<T>(elm));
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::InsertFast(index_t index, const T& elm)
{
	ASSERT(index < m_size);

	GrowToFitElement();

	memcpy(&m_array[m_size], &m_array[index], sizeof(T));
	++m_size;

	SetValue(index, elm);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
template<class ... Types>
inline void Array<T, Alloc>::EmplaceBack(Types&& ... args)
{
	GrowToFitElement();

	new (&m_array[m_size]) T(std::forward<Types>(args)...);

	++m_size;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
template<class ... Types>
inline void Array<T, Alloc>::Emplace(index_t index, Types&& ... args)
{
	ASSERT(index < m_size);

	GrowToFitElement();
	MoveElementsToLeft(index);
	++m_size;

	new (&m_array[index]) T(std::forward<Types>(args)...);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
template<class ... Types>
inline void Array<T, Alloc>::EmplaceFast(index_t index, Types&& ... args)
{
	ASSERT(index < m_size);

	GrowToFitElement();

	memcpy(&m_array[m_size], &m_array[index], sizeof(T));
	++m_size;

	new (&m_array[index]) T(std::forward<Types>(args)...);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Remove(index_t index)
{
	ASSERT(index < m_size);

	DestroyElement(m_array[index]);

	memcpy(&m_array[index], &m_array[index + 1], sizeof(T) * (m_size - index - 1));

	--m_size;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::RemoveLast()
{
	ASSERT(m_size > 0);

	DestroyElement(m_array[--m_size]);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::RemoveFast(index_t index)
{
	ASSERT(index < m_size);

	DestroyElement(m_array[index]);

	if (index != m_size - 1)
	{
		memcpy(&m_array[index], &m_array[m_size - 1], sizeof(T));
	}

	--m_size;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::RemoveRange(index_t index, u32 count)
{
	ASSERT(index + count <= m_size);

	const u32 kLastIndex = index + count;

	for (u32 i = index; i < kLastIndex; ++i)
	{
		DestroyElement(m_array[i]);
	}

	memcpy(&m_array[index], &m_array[kLastIndex], sizeof(T) * (m_size - kLastIndex));

	m_size -= count;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::RemoveRangeFast(index_t index, u32 count)
{
	ASSERT(index + count <= m_size);

	const u32 kLastIndex = index + count;

	for (u32 i = index; i < kLastIndex; ++i)
	{
		DestroyElement(m_array[i]);
	}

	const u32 kElementsAtLeftCount = m_size - kLastIndex;
	const u32 kCopyCount = kElementsAtLeftCount > count ? count : kElementsAtLeftCount;

	memcpy(&m_array[index], &m_array[m_size - kCopyCount], sizeof(T) * kCopyCount);

	m_size -= count;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline T& Array<T, Alloc>::First() const
{
	return m_array[0];
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline T& Array<T, Alloc>::Last() const
{
	return m_array[m_size - 1];
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline typename Array<T, Alloc>::Iterator Array<T, Alloc>::Begin() const
{
	return &First();
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline typename Array<T, Alloc>::Iterator Array<T, Alloc>::End() const
{
	return &m_array[m_size];
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline u32 Array<T, Alloc>::GetSize() const
{
	return m_size;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline u32 Array<T, Alloc>::GetCapacity() const
{
	return m_capacity;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline u32 Array<T, Alloc>::GetAllocatedSize() const
{
	return m_capacity * sizeof(T);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline bool Array<T, Alloc>::IsEmpty() const
{
	return (m_size == 0);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Reserve(u32 count)
{
	ResizeTo(m_capacity + count);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Clear()
{
	for (u32 i = 0; i < m_size; ++i)
	{
		DestroyElement(m_array[i]);
	}

	m_size = 0;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Reset()
{
	m_size = 0;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Sort()
{
	std::sort(Begin(), End());
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline typename Array<T, Alloc>::Iterator Array<T, Alloc>::FindIteratorLinear(const T& elm) const
{
	for (u32 i = 0; i < m_size; ++i)
	{
		if (m_array[i] == elm)
		{
			return &m_array[i];
		}
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline index_t Array<T, Alloc>::FindIndexLinear(const T& elm) const
{
	for (u32 i = 0; i < m_size; ++i)
	{
		if (m_array[i] == elm)
		{
			return i;
		}
	}

	return kInvalidIndex;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline index_t Array<T, Alloc>::FindIndexBinary(const T& elm) const
{
	u32 first = 0;
	u32 last = m_size - 1;
	u32 count = m_size;
	u32 median = count / 2;

	while (count != 0)
	{
		if (elm < m_array[median])
		{
			last = median;
		}
		else if (elm > m_array[median])
		{
			first = median;
		}
		else
		{
			return median;
		}

		count = last - first;
		median = count / 2 + first;
	}

	return kInvalidIndex;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Copy(const Array<T>& rhs)
{
	Clear();

	if (rhs.m_size != 0)
	{
		if (rhs.m_size > m_capacity)
		{
			ResizeTo(rhs.m_size);
		}

		for (u32 i = 0; i < rhs.m_size; ++i)
		{
			SetValue(i, rhs.m_array[i]);
		}

		m_size = rhs.m_size;
	}
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Allocate(u32 count)
{
	ASSERT(count > 0);

	m_array = (T*)Alloc::Alloc(count * sizeof(T));
	m_capacity = count;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::DestroyElement(T& elm)
{
	(void)&elm;	// fix for a msvc w4 level unreferenced parameter warning
	elm.~T();
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::MoveElementsToLeft(u32 initialIndex)
{
	for (u32 i = m_size; i > initialIndex; --i)
	{
		memcpy(&m_array[i], &m_array[i - 1], sizeof(T));
	}
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::Increase()
{
	u32 growElementsCount = m_capacity / 2 + kMinIncreaseCount;

	if (growElementsCount > kMaxIncreaseCount)
	{
		growElementsCount = kMaxIncreaseCount;
	}

	const u32 newCapacity = m_size + growElementsCount;

	ResizeTo(newCapacity);
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::ResizeTo(u32 count)
{
	m_array = (T*)Alloc::Realloc(m_array, count * sizeof(T));
	m_capacity = count;
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::GrowToFitElement()
{
	if (m_size == m_capacity)
	{
		Increase();
	}
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
inline void Array<T, Alloc>::GrowToFitMore(u32 count)
{
	const u32 SIZE_SUM = m_size + count;
	if (SIZE_SUM >= m_capacity)
	{
		u32 newCapacity = m_size + m_capacity / 2 + kMinIncreaseCount;

		if (newCapacity < SIZE_SUM)
		{
			newCapacity = SIZE_SUM;
		}

		ResizeTo(newCapacity);
	}
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
__forceinline void Array<T, Alloc>::SetValue(index_t index, T&& value)
{
	new (&m_array[index]) T(std::forward<T>(value));
}

//-------------------------------------------------------------------------------------------
template<class T, class Alloc>
__forceinline void Array<T, Alloc>::SetValue(index_t index, const T& value)
{
	new (&m_array[index]) T(value);
}