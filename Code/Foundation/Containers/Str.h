
#pragma once

#include <cstdlib>
#include <utility>
#include "Types.h"
#include "Debug.h"
#include "ConstCharHelper.h"
#include "GenericAllocator.h"


template <class Alloc = GenericAllocator>
class BaseString
{
public:
					BaseString();
					BaseString(BaseString&& str);
					BaseString(const BaseString& str);
					BaseString(ConstCharHelper str);
					BaseString(const char* str, size_t length);
					template <u32 N>
					BaseString(const char(&str)[N]);
					~BaseString();

	void			CopyString(const char* str, u16 strLen);	// will replace anything, that existed before

	BaseString&		operator=(BaseString&& str);
	BaseString&		operator=(const BaseString& str);
	BaseString&		operator=(const char* str);

	BaseString		operator+(BaseString&& str) const;
	BaseString		operator+(const BaseString& str) const;
	BaseString		operator+(const char* str) const;

	BaseString&		operator+=(BaseString&& str);
	BaseString&		operator+=(const BaseString& str);
	BaseString&		operator+=(const char* str);

	bool			operator==(const char* str) const;
	bool			operator==(const BaseString& str) const;
	friend bool		operator==(const char* cStr, const BaseString& str);
	bool			operator!=(const char* str) const;
	bool			operator!=(const BaseString& str) const;

	char&			operator[](index_t index);
	const char&		operator[](index_t index) const;

	const char*		c_str() const;
	u16				GetLength() const;

	void			Clear();
	bool			IsEmpty() const;
	void			Reserve(size_t size);

	// Non safe methods
	char*			GetBuffer();
	void			SetStringLength(u32 length);
	void			AcquireStringOwnership(char* str);

	ConstCharHelper	GetFileExtension() const;	// returns ConstCharHelper so you can easily compare resulting string to another c string
	BaseString&		StripFileExtension();
	BaseString		ExtractFileName() const;	// returns *this, if cannot extract file name
	BaseString		ExtractDirName() const;		// returns *this, if cannot extract dir name
	BaseString&		StripFileName();			// leaves only dir path
	BaseString&		StripDir();

	void			FixPathSeparators();

protected:
private:
	enum
	{
					OnStack = 0,
					OnHeap = 1,
					LocalBufferSize = 19,
	};

	struct CharArray
	{
		char		m_charArray[LocalBufferSize];

					CharArray()
					{
						m_charArray[0] = 0;
					}
	};
	
	template <bool V>
	struct StoreOnHeap
	{
		enum { value = V };
	};
	

	char*			m_pBuffer;
	char*			m_pHeapBuffer;
	u16				m_length;
	u16				m_heapBufferSize;
	char			m_flags;
	CharArray		m_localBuffer;

	void			Copy(const char* str, size_t length);
	
	void			SelectHeapStorage(size_t strBuflength, StoreOnHeap<true>);
	void			SelectHeapStorage(size_t strBuflength, StoreOnHeap<false>);

	void			AllocateBuffer(u16 size);
	void			ReallocBuffer(u16 size);
	void			RecreateBuffer(u16 newSize);

	bool			IsOnHeap() const;
	bool			IsOnStack() const;
	void			SetOnHeap();
	void			SetOnStack();
	
	BaseString		Concat(BaseString& str) const;
	BaseString&		StripFileDirInternal(u32 offsetFromEnd);
};


//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc> operator+(const char* str1, const BaseString<Alloc>& str2)
{
	return str2 + str1;
}





//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>::BaseString()
	: m_pBuffer(m_localBuffer.m_charArray), m_pHeapBuffer(nullptr), m_length(0), m_heapBufferSize(0), m_flags(0)
{
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>::BaseString(BaseString<Alloc>&& str)
	: m_pHeapBuffer(str.m_pHeapBuffer), m_length(str.m_length), m_heapBufferSize(str.m_heapBufferSize), m_flags(str.m_flags), m_localBuffer(str.m_localBuffer)
{
	m_pBuffer = IsOnHeap() ? m_pHeapBuffer : m_localBuffer.m_charArray;
	str.m_pHeapBuffer = 0;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>::BaseString(const BaseString<Alloc>& str)
	: m_pHeapBuffer(nullptr), m_length(str.m_length), m_heapBufferSize(0), m_flags(str.m_flags)
{
	if (IsOnHeap())
	{
		AllocateBuffer(str.m_heapBufferSize);
		memcpy(m_pHeapBuffer, str.m_pHeapBuffer, str.m_length + 1);
		m_pBuffer = m_pHeapBuffer;
	}
	else
	{
		m_localBuffer = str.m_localBuffer;
		m_pBuffer = m_localBuffer.m_charArray;
	}
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>::BaseString(ConstCharHelper str)
	: m_pHeapBuffer(nullptr), m_heapBufferSize(0)
{
	Copy(str.m_str, strlen(str.m_str));
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>::BaseString(const char* str, size_t length)
	: m_pHeapBuffer(nullptr), m_heapBufferSize(0)
{
	Copy(str, length);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
template <u32 N>
inline BaseString<Alloc>::BaseString(const char(&str)[N])
	: m_pHeapBuffer(nullptr), m_length(N - 1), m_heapBufferSize(0)
{
	XERROR("This function will be called for both char buffer[64] and `string`, although the fist one will take more space");

	SelectHeapStorage(N, StoreOnHeap<(N > LocalBufferSize)>());

	for (u32 i = 0; i < N; ++i)
	{
		m_pBuffer[i] = str[i];
	}
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>::~BaseString()
{
	if (m_pHeapBuffer)
	{
		Alloc::Free(m_pHeapBuffer);
	}
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::CopyString(const char* str, u16 strLen)
{
	if (strLen > m_length)
	{
		if (strLen > LocalBufferSize)
		{
			RecreateBuffer(strLen);
			SetOnHeap();
		}
	}

	m_length = (u16)strLen;

	memcpy(m_pBuffer, str, m_length);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::operator=(BaseString<Alloc>&& str)
{
	m_length = str.m_length;
	m_heapBufferSize = str.m_heapBufferSize;
	m_flags = str.m_flags;

	if (IsOnHeap())
	{
		std::swap(m_pHeapBuffer, str.m_pHeapBuffer);
		m_pBuffer = m_pHeapBuffer;
	}
	else
	{
		m_localBuffer = str.m_localBuffer;
		m_pBuffer = m_localBuffer.m_charArray;
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::operator=(const BaseString<Alloc>& str)
{
	if (this == &str)
	{
		return *this;
	}

	m_length = str.m_length;
	m_flags = str.m_flags;

	if (IsOnStack())
	{
		m_localBuffer = str.m_localBuffer;
		m_pBuffer = m_localBuffer.m_charArray;
	}
	else
	{
		if (m_heapBufferSize <= m_length)
		{
			RecreateBuffer(m_length + 1);
		}

		memcpy(m_pHeapBuffer, str.m_pHeapBuffer, m_length + 1);
		m_pHeapBuffer = m_pHeapBuffer;
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::operator=(const char* str)
{
	m_length = (u16)strlen(str);

	if (m_length < LocalBufferSize)
	{
		SetOnStack();
	}
	else
	{
		if (m_heapBufferSize <= m_length)
		{
			RecreateBuffer(m_length + 1);
		}

		SetOnHeap();
	}

	memcpy(m_pBuffer, str, m_length + 1);

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc> BaseString<Alloc>::operator+(BaseString<Alloc>&& str) const
{
	if (m_length == 0)
	{
		return BaseString(std::move(str));
	}
	else
	{
		return Concat(str);
	}
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc> BaseString<Alloc>::operator+(const BaseString<Alloc>& str) const
{
	if (m_length == 0)
	{
		return BaseString(str);
	}
	else
	{
		return Concat(str);
	}
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc> BaseString<Alloc>::operator+(const char* str) const
{
	if (m_length == 0)
	{
		return BaseString(str);
	}
	else
	{
		BaseString result;

		const size_t kStrLen = strlen(str);

		result.m_length = (u16)(m_length + kStrLen);
		if (m_length >= LocalBufferSize)
		{
			result.AllocateBuffer(result.m_length + 1);
			result.SetOnHeap();
		}
		else
		{
			result.SetOnStack();
		}

		memcpy(result.m_pBuffer, m_pBuffer, m_length);
		memcpy(&result.m_pBuffer[m_length], str, kStrLen + 1);

		return result;
	}
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::operator+=(BaseString<Alloc>&& str)
{
	if (m_length == 0)
	{
		m_pHeapBuffer = str.m_pHeapBuffer;
		m_length = str.m_length;
		m_heapBufferSize = str.m_heapBufferSize;
		m_flags = str.m_flags;
		m_localBuffer = str.m_localBuffer;
		m_pBuffer = IsOnHeap() ? m_pHeapBuffer : m_localBuffer.m_charArray;
		
		str.m_pHeapBuffer = 0;
	}
	else
	{
		*this += str;
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::operator+=(const BaseString<Alloc>& str)
{
	const u16 kNewLength = m_length + str.m_length;
	if (kNewLength >= LocalBufferSize)
	{
		// Place resulting string on heap
		if (IsOnHeap())
		{
			if (m_heapBufferSize <= kNewLength)
			{
				ReallocBuffer(kNewLength + 1);
				m_pBuffer = m_pHeapBuffer;
			}
		}
		else
		{
			if (m_heapBufferSize <= kNewLength)
			{
				RecreateBuffer(kNewLength + 1);
			}

			SetOnHeap();

			memcpy(m_pHeapBuffer, m_localBuffer.m_charArray, m_length);
		}
	}

	memcpy(&m_pBuffer[m_length], str.m_pBuffer, str.m_length + 1);
	m_length = kNewLength;

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::operator+=(const char* str)
{
	const u16 kStrLength = (u16)strlen(str);

	const u16 kNewLength = m_length + kStrLength;
	if (kNewLength >= LocalBufferSize)
	{
		if (IsOnHeap())
		{
			if (m_heapBufferSize <= kNewLength)
			{
				ReallocBuffer(kNewLength + 1);
				m_pBuffer = m_pHeapBuffer;
			}
		}
		else
		{
			if (m_heapBufferSize <= kNewLength)
			{
				RecreateBuffer(kNewLength + 1);
			}

			SetOnHeap();

			memcpy(m_pHeapBuffer, m_localBuffer.m_charArray, m_length);
		}
	}

	memcpy(&m_pBuffer[m_length], str, kStrLength + 1);

	m_length = kNewLength;

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline bool BaseString<Alloc>::operator==(const char* str) const
{
	return !strcmp(m_pBuffer, str);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline bool BaseString<Alloc>::operator==(const BaseString<Alloc>& str) const
{
	if (m_length != str.m_length)
	{
		return false;
	}

	return !strcmp(m_pBuffer, str.m_pBuffer);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline bool operator==(const char* cStr, const BaseString<Alloc>& str)
{
	return str == cStr;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline bool BaseString<Alloc>::operator!=(const char* str) const
{
	return !(*this == str);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline bool BaseString<Alloc>::operator!=(const BaseString<Alloc>& str) const
{
	return !(*this == str);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline char& BaseString<Alloc>::operator[](index_t index)
{
	ASSERT(index < m_length);

	return m_pBuffer[index];
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline const char& BaseString<Alloc>::operator[](index_t index) const
{
	ASSERT(index < m_length);

	return m_pBuffer[index];
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline const char* BaseString<Alloc>::c_str() const
{
	return m_pBuffer;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline u16 BaseString<Alloc>::GetLength() const
{
	return m_length;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::Clear()
{
	m_pBuffer[0] = 0;
	m_length = 0;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline bool BaseString<Alloc>::IsEmpty() const
{
	return m_length == 0;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::Reserve(size_t size)
{
	const u16 kNewSize = m_heapBufferSize + (u16)size;

	if (IsOnStack())
	{
		m_pHeapBuffer = (char*)Alloc::Alloc((size_t)kNewSize);
		memcpy(m_pHeapBuffer, m_localBuffer.m_charArray, m_length + 1);
	}
	else
	{
		m_pHeapBuffer = (char*)Alloc::Realloc(m_pHeapBuffer, (size_t)kNewSize);
	}

	SetOnHeap();
	m_heapBufferSize = kNewSize;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline char* BaseString<Alloc>::GetBuffer()
{
	ASSERT(m_pBuffer);

	return m_pBuffer;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::SetStringLength(u32 length)
{
	m_length = (u16)length;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::AcquireStringOwnership(char* str)
{
	m_length = (u16)strlen(str);

	if (m_length < LocalBufferSize)
	{
		SetOnStack();
		memcpy(m_pBuffer, str, m_length + 1);
	}
	else
	{
		if (IsOnHeap())
		{
			Alloc::Free(m_pHeapBuffer);
		}

		m_pHeapBuffer = str;
		m_heapBufferSize = m_length + 1;
		SetOnHeap();
	}
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline ConstCharHelper BaseString<Alloc>::GetFileExtension() const
{
	for (int i = m_length - 2; i >= 0; --i)
	{
		if (m_pBuffer[i] == '.')
		{
			return ConstCharHelper(&m_pBuffer[i + 1]);
		}
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::StripFileExtension()
{
	for (u16 i = m_length - 2; i > 0; --i)
	{
		if (m_pBuffer[i] == '.')
		{
			m_pBuffer[i] = 0;
			m_length = i;
			break;
		}
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc> BaseString<Alloc>::ExtractFileName() const
{
	for (int i = m_length - 2; i >= 0; --i)
	{
		if (m_pBuffer[i] == '/' || m_pBuffer[i] == '\\')
		{
			return BaseString(&m_pBuffer[i + 1], m_length - i - 1);
		}
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc> BaseString<Alloc>::ExtractDirName() const
{
	char* lastSlashPtr = nullptr;
	int lastSlashIndex = m_length - 1;

	for (; lastSlashIndex >= 0; --lastSlashIndex)
	{
		if (m_pBuffer[lastSlashIndex] == '/' || m_pBuffer[lastSlashIndex] == '\\')
		{
			lastSlashPtr = &m_pBuffer[lastSlashIndex];
			--lastSlashIndex;
			break;
		}
	}

	if (!lastSlashPtr)
	{
		return *this;
	}

	for (int i = lastSlashIndex; i >= 0; --i)
	{
		if (m_pBuffer[i] == '/' || m_pBuffer[i] == '\\')
		{
			const int kIndex = i + 1;
			return BaseString(&m_pBuffer[kIndex], lastSlashIndex - kIndex);
		}
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::StripFileName()
{
	return StripFileDirInternal(1);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::StripDir()
{
	return StripFileDirInternal(2);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::Copy(const char* str, size_t length)
{
	m_length = (u16)length;

	if (length < LocalBufferSize)
	{
		SetOnStack();
	}
	else
	{
		AllocateBuffer(m_length + 1);
		SetOnHeap();
	}

	memcpy(m_pBuffer, str, length + 1);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::SelectHeapStorage(size_t strBuflength, StoreOnHeap<true>)
{
	AllocateBuffer((u16)strBuflength);
	SetOnHeap();
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::SelectHeapStorage(size_t strBuflength, StoreOnHeap<false>)
{
	(void)strBuflength;
	SetOnStack();
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::AllocateBuffer(u16 size)
{
	m_pHeapBuffer = (char*)Alloc::Alloc((size_t)size);
	m_heapBufferSize = size;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::ReallocBuffer(u16 size)
{
	const size_t kNewSize = size_t(size * 1.5f);
	ASSERT(kNewSize < 65536);
	m_pHeapBuffer = (char*)Alloc::Realloc(m_pHeapBuffer, kNewSize);
	m_heapBufferSize = (u16)kNewSize;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::RecreateBuffer(u16 newSize)
{
	Alloc::Free(m_pHeapBuffer);
	AllocateBuffer(newSize);
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline bool BaseString<Alloc>::IsOnHeap() const
{
	return m_flags == OnHeap;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline bool BaseString<Alloc>::IsOnStack() const
{
	return m_flags == OnStack;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::SetOnHeap()
{
	m_flags = OnHeap;
	m_pBuffer = m_pHeapBuffer;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::SetOnStack()
{
	m_flags = OnStack;
	m_pBuffer = m_localBuffer.m_charArray;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc> BaseString<Alloc>::Concat(BaseString<Alloc>& str) const
{
	BaseString result;

	result.m_length = m_length + str.m_length;
	if (m_length >= LocalBufferSize)
	{
		result.AllocateBuffer(result.m_length + 1);
		result.SetOnHeap();
	}
	else
	{
		result.SetOnStack();
	}

	memcpy(result.m_pBuffer, m_pBuffer, m_length);
	memcpy(&result.m_pBuffer[m_length], str.m_pBuffer, str.m_length + 1);

	return result;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline BaseString<Alloc>& BaseString<Alloc>::StripFileDirInternal(u32 offsetFromEnd)
{
	for (int i = m_length - offsetFromEnd; i >= 0; --i)
	{
		if (m_pBuffer[i] == '/' || m_pBuffer[i] == '\\')
		{
			m_length = (u16)(i + 1);
			m_pBuffer[m_length] = '\0';

			return *this;
		}
	}

	return *this;
}

//-------------------------------------------------------------------------------------------
template <class Alloc>
inline void BaseString<Alloc>::FixPathSeparators()
{
	for (u32 i = 0; i < m_length; ++i)
	{
		if (m_pBuffer[i] == '\\')
		{
			m_pBuffer[i] = '/';
		}
	}
}


typedef BaseString<GenericAllocator> String;