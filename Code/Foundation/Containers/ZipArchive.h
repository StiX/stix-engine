
#pragma once

#include "Str.h"
#include "miniz.h"
#include "Types.h"
#include "FileSystem.h"

class ZipArchive;


struct ZipFile
{
	const ZipArchive*	pArchive;
	u32					offset;
	u32					size;
	int					fileIndex;
};


class ZipArchive
{
public:
					ZipArchive();
	explicit		ZipArchive(const char* archivePath);
					~ZipArchive();

	void			OpenArchive(const char* archivePath);

	u32				GetFilesCount() const;

	ZipFile			FindFile(const char* filePath) const;
	
	void*			MapFile(const ZipFile& file) const;

protected:
private:
	String			m_archivePath;
	mz_zip_archive	m_zipArchive;
	File			m_archive;
};