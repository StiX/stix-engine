
#pragma once

#include <cstring>


struct ConstCharHelper
{
	const char*		m_str;

					ConstCharHelper(const char* str);

	bool			operator==(const char* str);
};





//-------------------------------------------------------------------------------------------
inline ConstCharHelper::ConstCharHelper(const char* str)
	: m_str(str)
{
}

//-------------------------------------------------------------------------------------------
inline bool ConstCharHelper::operator==(const char* str)
{
	return !strcmp(m_str, str);
}

//-------------------------------------------------------------------------------------------
inline bool operator==(const char* str1, ConstCharHelper str2)
{
	return str2 == str1;
}