
#include "StringHash.h"
#include "Debug.h"

#ifdef DEBUG_STRING_HASH
#	include "HashTable.h"


	struct string_hash_debug_map {
		typedef HashTable<u32, const char*> map;
		map	_m;

					~string_hash_debug_map() {
						map::Iterator it = _m.GetFirstElement();
						while (it != _m.GetEnd()) {
							delete[] it.GetValue();
							++it;
						}
					}
	};

	//-------------------------------------------------------------------------------------------
	string_hash_debug_map* debug_map() {
		static string_hash_debug_map m;
		return &m;
	}
#endif

//-------------------------------------------------------------------------------------------
const char* StringHash::c_str() const {
#ifdef DEBUG_STRING_HASH
	return _str;
#else
	return stringify("#[0x%X]", _hash);
#endif
}

//-------------------------------------------------------------------------------------------
void StringHash::add_debug_string(const char* str, size_t length) {
	(void)str;
	(void)length;

#ifdef DEBUG_STRING_HASH
	const char* existing_string = debug_map()->_m.Find(_hash);
	if (existing_string) {
		ASSERT(!strncmp(str, existing_string, length), "Hash collision!");
		_str = existing_string;
	} else {
		char* new_str = new char[length + 1];
		memcpy(new_str, str, length);
		new_str[length] = 0;			// Do not remove this line. It's necessary to have it like that, since hashing length of a string can be lower, than actual string length

		_str = new_str;

		debug_map()->_m.Insert(_hash, _str);
	}
#endif
}