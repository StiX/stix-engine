
#pragma once

#include "Str.h"


struct StringUtil
{
	static String	ExtractFileName(const char* str);
	static bool		MatchWildcardPattern(const char* str, const char* pattern);		// matches * or ? only
};