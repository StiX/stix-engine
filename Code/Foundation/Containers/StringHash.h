
#pragma once

#include <cstring>
#include "Log.h"
#include "Types.h"
#include "farmhash.h"


class StringHash
{
public:
				StringHash();
                StringHash(const char* str);
				StringHash(const char* str, size_t length);

	bool		operator==(const StringHash& other) const;
	bool		operator!=(const StringHash& other) const;

	u32			hash() const;

	const char*	c_str() const;

protected:
private:
#ifdef DEBUG_STRING_HASH
	const char*	_str;
#endif
	u32			_hash;		//TODO: move to 64 bit hash?!

	void		add_debug_string(const char* str, size_t length);
};





//-------------------------------------------------------------------------------------------
inline StringHash::StringHash()
	: StringHash(0, 0)
{
}

//-------------------------------------------------------------------------------------------
inline StringHash::StringHash(const char* str)
    : StringHash(str, strlen(str))
{
}

//-------------------------------------------------------------------------------------------
inline StringHash::StringHash(const char* str, size_t length)
	: _hash(farmhash::Hash32(str, length))
{
	add_debug_string(str, length);
}

//-------------------------------------------------------------------------------------------
inline bool StringHash::operator==(const StringHash& other) const
{
	return _hash == other._hash;
}

//-------------------------------------------------------------------------------------------
inline bool StringHash::operator!=(const StringHash& other) const
{
	return _hash != other._hash;
}

//-------------------------------------------------------------------------------------------
inline u32 StringHash::hash() const
{
	return _hash;
}