
#pragma once

#include <cstddef>


typedef void(*Deallocator)(void*, size_t);

struct allocation_info {
	Deallocator	deallocator;
	void*		ptr;
	size_t		size;
};