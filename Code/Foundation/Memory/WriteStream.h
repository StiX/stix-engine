
#pragma once

#include "Types.h"
#include "Debug.h"
#include "ClassHelpers.h"


class WriteStream
{
public:
					WriteStream(void* ptr, u32 size);

	void*			Get(u32 bytes);

	template <class T>
	void			Write(const T& value);
	void			Write(const void* ptr, u32 size);

	template <class T>
	WriteStream&	operator&(const T& value);

protected:
private:
	u8*				m_ptr;
	u32				m_offset;
	u32				m_size;

	MAKE_UNCOPYABLE(WriteStream);
};





//-------------------------------------------------------------------------------------------
inline WriteStream::WriteStream(void* ptr, u32 size)
	: m_ptr((u8*)ptr), m_offset(0), m_size(size)
{
	(void)m_size;
	ASSERT(ptr);
	ASSERT(size);
}

//-------------------------------------------------------------------------------------------
inline void* WriteStream::Get(u32 bytes)
{
	void* ptr = &m_ptr[m_offset];

	m_offset += bytes;
	ASSERT(m_offset <= m_size);

	return ptr;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline void WriteStream::Write(const T& value)
{
	T* ptr = (T*)&m_ptr[m_offset];

	m_offset += sizeof(T);
	ASSERT(m_offset <= m_size);

	*ptr = value;
}

//-------------------------------------------------------------------------------------------
inline void WriteStream::Write(const void* ptr, u32 size) {
	ASSERT(m_offset + size <= m_size);

	memcpy(&m_ptr[m_offset], ptr, size);
	m_offset += size;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline WriteStream& WriteStream::operator&(const T& value) {
	Write(value);
	return *this;
}