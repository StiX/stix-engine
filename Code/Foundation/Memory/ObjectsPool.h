
#pragma once

#include <new>
#include <utility>
#include <type_traits>
#include "Types.h"
#include "Debug.h"
#include "VirtualPageAllocator.h"


template<class T>
class ObjectsPool
{
public:
						ObjectsPool(u32 initialCapacity, u32 dataReservationMultiplier);
						~ObjectsPool();

	u32					GetCapacity() const;
	u32					GetStoredObjectsCount() const;

	template<class ... Types>
	T*					CreateObject(Types&& ... args);
	void				DeleteObject(T* pObject);

protected:
private:
	union Slot
	{
		u8				m_objectBuf[sizeof(T)];
		Slot*			m_pNextFree;
	};

	struct AllocationBlock 
	{
		AllocationBlock* m_pNextBlock;
		size_t			m_blockReservedSize;
		Slot			m_objects[0];
	};

	Slot*				m_pFirstFreeSlot;
	Slot*				m_pLastFreeSlot;
	AllocationBlock*	m_pFirstBlock;
	AllocationBlock*	m_pCurrentBlock;
	size_t				m_currentBlockCommitedSize;
	u32					m_capacity;
	u32					m_storedObjectsCount;

	void				ExtendPool();
	void				AllocateBlock(size_t reserveSize, size_t commitSize);
	void				Commit(void* pAddress, size_t commitSize, u32 firstObjectIndex);

	void				LinkSlots(u32 firstIndex, u32 lastIndex);
};





//-------------------------------------------------------------------------------------------
template<class T>
inline ObjectsPool<T>::ObjectsPool(u32 initialCapacity, u32 dataReservationMultiplier)
	: m_capacity(0), m_storedObjectsCount(0)
{
	ASSERT(initialCapacity != 0);
	ASSERT(dataReservationMultiplier != 0);

	const size_t kDataSize = VirtualPageAllocator::AlignToPageBoundary(initialCapacity * sizeof(T) + sizeof(AllocationBlock));
	const size_t kReservedSize = kDataSize * dataReservationMultiplier;
	AllocateBlock(kReservedSize, kDataSize);
	m_pFirstBlock = m_pCurrentBlock;
}

//-------------------------------------------------------------------------------------------
template<class T>
inline ObjectsPool<T>::~ObjectsPool()
{
	ASSERT(m_storedObjectsCount == 0);

	do
	{
		AllocationBlock* pNextBlock = m_pFirstBlock->m_pNextBlock;
		VirtualPageAllocator::Free(m_pFirstBlock, m_pFirstBlock->m_blockReservedSize);
		m_pFirstBlock = pNextBlock;
	} while (m_pFirstBlock);
}

//-------------------------------------------------------------------------------------------
template<class T>
inline u32 ObjectsPool<T>::GetCapacity() const
{
	return m_capacity;
}

//-------------------------------------------------------------------------------------------
template<class T>
inline u32 ObjectsPool<T>::GetStoredObjectsCount() const
{
	return m_storedObjectsCount;
}

//-------------------------------------------------------------------------------------------
template<class T>
template<class ... Types>
inline T* ObjectsPool<T>::CreateObject(Types&& ... args)
{
	if (!m_pFirstFreeSlot)
	{
		ExtendPool();
	}

	Slot* pMem = m_pFirstFreeSlot;
	m_pFirstFreeSlot = pMem->m_pNextFree;
	
	++m_storedObjectsCount;

	return new (pMem) T(std::forward<Types>(args)...);
}

//-------------------------------------------------------------------------------------------
template<class T>
inline void ObjectsPool<T>::DeleteObject(T* pObject)
{
#ifdef DEBUG
	AllocationBlock* pBlock = m_pFirstBlock;
	bool bFound = false;

	while (pBlock)
	{
		const size_t kBlockAddress = (size_t)pBlock->m_objects;
		if ((kBlockAddress <= (size_t)pObject) && (kBlockAddress + pBlock->m_blockReservedSize - sizeof(AllocationBlock) > (size_t)pObject))
		{
			bFound = true;
			break;
		}

		pBlock = pBlock->m_pNextBlock;
	}

	ASSERT(bFound, "Object not found!");
#endif

	pObject->~T();
	
	--m_storedObjectsCount;

	// Update the free list of free objects in the pool
	m_pLastFreeSlot->m_pNextFree = (Slot*)pObject;
	m_pLastFreeSlot = (Slot*)pObject;
	m_pLastFreeSlot->m_pNextFree = nullptr;
}

//-------------------------------------------------------------------------------------------
template<class T>
inline void ObjectsPool<T>::ExtendPool()
{
	if (m_currentBlockCommitedSize < m_pCurrentBlock->m_blockReservedSize)
	{
		const u32 kObjectsCount = u32((m_currentBlockCommitedSize - sizeof(AllocationBlock)) / sizeof(T));
		m_capacity -= kObjectsCount;
		Commit((u8*)m_pCurrentBlock + m_currentBlockCommitedSize, VirtualPageAllocator::GetVirtualPageSize(), kObjectsCount);
	}
	else
	{
		AllocationBlock* pOldBlock = m_pCurrentBlock;
		AllocateBlock(m_pCurrentBlock->m_blockReservedSize, VirtualPageAllocator::GetVirtualPageSize());
		pOldBlock->m_pNextBlock = m_pCurrentBlock;
	}
}

//-------------------------------------------------------------------------------------------
template<class T>
inline void ObjectsPool<T>::AllocateBlock(size_t reserveSize, size_t commitSize)
{
	m_pCurrentBlock = (AllocationBlock*)VirtualPageAllocator::Reserve(reserveSize);
	m_currentBlockCommitedSize = 0;

	Commit(m_pCurrentBlock, commitSize, 0);

	m_pCurrentBlock->m_blockReservedSize = reserveSize;
	m_pCurrentBlock->m_pNextBlock = nullptr;
}

//-------------------------------------------------------------------------------------------
template<class T>
inline void ObjectsPool<T>::Commit(void* pAddress, size_t commitSize, u32 firstObjectIndex)
{
	VirtualPageAllocator::Commit(pAddress, commitSize);
	m_currentBlockCommitedSize += commitSize;

	const u32 kObjectsCount = u32((m_currentBlockCommitedSize - sizeof(AllocationBlock)) / sizeof(T));
	const u32 kLastIndex = kObjectsCount - 1;
	m_capacity += kObjectsCount;

	m_pFirstFreeSlot = &m_pCurrentBlock->m_objects[firstObjectIndex];
	m_pLastFreeSlot = &m_pCurrentBlock->m_objects[kLastIndex];

	LinkSlots(firstObjectIndex, kLastIndex);
}

//-------------------------------------------------------------------------------------------
template<class T>
inline void ObjectsPool<T>::LinkSlots(u32 firstIndex, u32 lastIndex)
{
	while (firstIndex < lastIndex)
	{
		m_pCurrentBlock->m_objects[firstIndex].m_pNextFree = &m_pCurrentBlock->m_objects[firstIndex + 1];
		++firstIndex;
	}

	m_pCurrentBlock->m_objects[lastIndex].m_pNextFree = nullptr;
}