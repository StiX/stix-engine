
#pragma once

#include <utility>
#include <type_traits>
#include "Types.h"
#include "Debug.h"
#include "VirtualPageAllocator.h"


template<class T, class ALLOC = VirtualPageAllocator>
class StaticObjectsPool
{
public:
	explicit			StaticObjectsPool(u32 initialCapacity);		//TODO: refactor. init with a memblock of certain size and remove ALLOC template param
						~StaticObjectsPool();

	u32					GetCapacity() const;

	template<class ... Types>
	T*					CreateObject(Types&& ... args);
	void				DeleteObject(T* pObject);

protected:
private:
	template <bool V>
	struct PODType
	{
		enum { value = V };
	};

	union Slot
	{
		u8				m_objectBuf[sizeof(T)];
		u32				m_nextFree;
	};

	const u32			m_capacity;
	u32					m_firstFreeSlotIndex;
	u32					m_lastFreeSlotIndex;
	Slot*				m_pObjects;

	void				DestroyObjects(PODType<true>);		// Pool destructor for POD objects (NO destructor at all)
	void				DestroyObjects(PODType<false>);		// Pool destructor for non-POD objects

	void				CheckMemoryLeaks() const;
};





//-------------------------------------------------------------------------------------------
template<class T, class ALLOC>
inline StaticObjectsPool<T, ALLOC>::StaticObjectsPool(u32 initialCapacity)
	: m_capacity(initialCapacity), m_firstFreeSlotIndex(0)
{
	ASSERT(m_capacity);

	m_pObjects = (Slot*)ALLOC::Alloc(m_capacity * sizeof(Slot));

	for (u32 i = 0; i < m_capacity - 1; ++i)
	{
		m_pObjects[i].m_nextFree = i + 1;
	}

	m_pObjects[m_capacity - 1].m_nextFree = kInvalidIndex;
	m_lastFreeSlotIndex = m_capacity - 1;
}


//-------------------------------------------------------------------------------------------
template<class T, class ALLOC>
inline StaticObjectsPool<T, ALLOC>::~StaticObjectsPool()
{
	CheckMemoryLeaks();	//TODO: check leaks via flag?!

	DestroyObjects(PODType<std::is_trivially_destructible<T>::value>());

	ALLOC::Free(m_pObjects, m_capacity * sizeof(T));
}

//-------------------------------------------------------------------------------------------
template<class T, class ALLOC>
inline u32 StaticObjectsPool<T, ALLOC>::GetCapacity() const
{
	return m_capacity;
}

//-------------------------------------------------------------------------------------------
template<class T, class ALLOC>
template<class ... Types>
inline T* StaticObjectsPool<T, ALLOC>::CreateObject(Types&& ... args)
{
	ASSERT(m_firstFreeSlotIndex != kInvalidIndex);

	Slot* pMem = &m_pObjects[m_firstFreeSlotIndex];
	m_firstFreeSlotIndex = pMem->m_nextFree;

	return new (pMem) T(std::forward<Types>(args)...);
}

//-------------------------------------------------------------------------------------------
template<class T, class ALLOC>
inline void StaticObjectsPool<T, ALLOC>::DeleteObject(T* pObject)
{
	const u32 kObjectIndexInPool = ((intptr_t)pObject - (intptr_t)m_pObjects) / sizeof(T);
	ASSERT(kObjectIndexInPool < m_capacity);
	ASSERT(kObjectIndexInPool != m_firstFreeSlotIndex);
	ASSERT(kObjectIndexInPool != m_lastFreeSlotIndex);

	pObject->~T();

	// Update the free list of free objects in the pool
	if (m_firstFreeSlotIndex == kInvalidIndex)
	{
		m_firstFreeSlotIndex = kObjectIndexInPool;
		m_lastFreeSlotIndex = kObjectIndexInPool;
	}

	m_pObjects[m_lastFreeSlotIndex].m_nextFree = kObjectIndexInPool;
	m_pObjects[kObjectIndexInPool].m_nextFree = kInvalidIndex;
	m_lastFreeSlotIndex = kObjectIndexInPool;
}

//-------------------------------------------------------------------------------------------
template<class T, class ALLOC>
inline void StaticObjectsPool<T, ALLOC>::DestroyObjects(PODType<true>)
{
	// Do nothing for POD objects
}

//-------------------------------------------------------------------------------------------
template<class T, class ALLOC>
inline void StaticObjectsPool<T, ALLOC>::DestroyObjects(PODType<false>)
{
	for (u32 i = 0; i < m_capacity; ++i)
	{
		while (i < m_firstFreeSlotIndex)
		{
			m_pObjects[i].~T();
			++i;
		}

		m_firstFreeSlotIndex = m_pObjects[m_firstFreeSlotIndex].m_nextFree;
	}
}

//-------------------------------------------------------------------------------------------
template<class T, class ALLOC>
inline void StaticObjectsPool<T, ALLOC>::CheckMemoryLeaks() const
{
#ifdef DEBUG
	u32 freeIndex = m_firstFreeSlotIndex;
	u32 freeObjectsCount = 0;

	while (freeIndex != kInvalidIndex)
	{
		freeIndex = m_pObjects[freeIndex].m_nextFree;
		++freeObjectsCount;
	}

	ASSERT(freeObjectsCount == m_capacity);
#endif
}