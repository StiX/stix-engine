
#pragma once

#include "Debug.h"
#include "ClassHelpers.h"


class ReadStream
{
public:
				ReadStream(const void* ptr, u32 size);
				~ReadStream();

	template <class T>
	const T&	Read();
	template <class T>
	const T*	ReadPtr();

	template <class T>
	ReadStream&	operator&(T& receiver);

	const void*	Read(u32 bytes);

	u32			GetSize() const;

protected:
private:
	const u8*	m_ptr;
	const u8*	_end_ptr;
	u32			m_size;

	MAKE_UNCOPYABLE(ReadStream);
};





//-------------------------------------------------------------------------------------------
inline ReadStream::ReadStream(const void* ptr, u32 size)
	: m_ptr((u8*)ptr), _end_ptr(m_ptr + size), m_size(size)
{
	ASSERT(ptr);
}

//-------------------------------------------------------------------------------------------
inline ReadStream::~ReadStream()
{
}

//-------------------------------------------------------------------------------------------
template <class T>
inline const T& ReadStream::Read()
{
	const T* ptr = (T*)m_ptr;
	
	m_ptr += sizeof(T);
	ASSERT(m_ptr <= _end_ptr);

	return *ptr;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline const T* ReadStream::ReadPtr()
{
	return (T*)Read(sizeof(T));
}

//-------------------------------------------------------------------------------------------
template <class T>
inline ReadStream& ReadStream::operator&(T& receiver) {
	receiver = Read<T>();
	return *this;
}

//-------------------------------------------------------------------------------------------
inline const void* ReadStream::Read(u32 bytes)
{
	const void* ptr = m_ptr;

	m_ptr += bytes;
	ASSERT(m_ptr <= _end_ptr);

	return ptr;
}

//-------------------------------------------------------------------------------------------
inline u32 ReadStream::GetSize() const {
	return m_size;
}