
#pragma once

#include <stddef.h>
#include "Types.h"
#include "ClassHelpers.h"


class LinearAllocator
{
public:
			LinearAllocator();
	explicit LinearAllocator(size_t reservedSize, size_t initialCommit = 0);
			~LinearAllocator();

	void	Init(size_t reservedSize, size_t initialCommit = 0);

	template <class T>
	T*		Allocate();														// Allocates a block of memory for T type, guaranteed to be aligned to native alignment of T
	void*	Allocate(size_t size, size_t alignment = alignof(void*));		// Allocates a block of memory with specified alignment

	void	Reset();

protected:
private:
	u8*		m_pBlockStart;
	size_t	m_currentOffset;
	size_t	m_reservedSize;
	size_t	m_commitedSize;

	MAKE_UNCOPYABLE(LinearAllocator);
};





//-------------------------------------------------------------------------------------------
template <class T>
inline T* LinearAllocator::Allocate()
{
	return (T*)Allocate(sizeof(T), alignof(T));
}