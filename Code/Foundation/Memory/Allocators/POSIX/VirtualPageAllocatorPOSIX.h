
#pragma once

#include "Types.h"
#include <sys/mman.h>
#include "Debug.h"
#include "Error.h"


class VirtualPageAllocator
{
public:
	static void		Init();		// This function is called automatically

	static void*	Alloc(size_t size, void* baseAddress = nullptr);
	static void*	AllocAndZero(size_t size);
	static void*	Reserve(size_t size, void* baseAddress = nullptr);
	static void*	Commit(void* baseAddress, size_t size);
	
	static bool		Free(void* address, size_t size);
	static bool		Decommit(void* address, size_t size);

	static size_t	GetVirtualPageSize();

	static size_t	AlignToPageBoundary(size_t size);
	
protected:
private:
	static size_t	m_virtualPageSize;
	static void*	RoundDownAddress(void* address);
};





//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::Alloc(size_t size, void* baseAddress)
{
	return mmap(baseAddress, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
}

//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::AllocAndZero(size_t size)
{
	return Alloc(size);
}

//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::Reserve(size_t size, void* baseAddress)
{
	return mmap(baseAddress, size, PROT_NONE, MAP_NORESERVE | MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
}

//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::Commit(void* baseAddress, size_t size)
{
	// http://forums.pcsx2.net/Thread-blog-VirtualAlloc-on-Linux
	
	void* alignedAddress = RoundDownAddress(baseAddress);
	size += (size_t)baseAddress - (size_t)alignedAddress;
	if (!mprotect(alignedAddress, size, PROT_READ | PROT_WRITE))
	{
		return alignedAddress;
	}
    
    XERROR("Cannot commit page. Error %s", Error::GetLastErrorMessage());
	return nullptr;
}

//-------------------------------------------------------------------------------------------
inline bool VirtualPageAllocator::Free(void* address, size_t size)
{
	if (address)
	{
		void* alignedAddress = RoundDownAddress(address);
		size += (size_t)address - (size_t)alignedAddress;
		return munmap(address, size) == 0;
	}
	
	return false;
}

//-------------------------------------------------------------------------------------------
inline bool VirtualPageAllocator::Decommit(void* address, size_t size)
{
	void* alignedAddress = RoundDownAddress(address);
	size += (size_t)address - (size_t)alignedAddress;
	madvise(address, size, MADV_DONTNEED);
	return mprotect(address, size, PROT_NONE) == 0;
}

//-------------------------------------------------------------------------------------------
inline size_t VirtualPageAllocator::GetVirtualPageSize()
{
	return m_virtualPageSize;
}

//-------------------------------------------------------------------------------------------
inline size_t VirtualPageAllocator::AlignToPageBoundary(size_t size)
{
	return (size + GetVirtualPageSize() - 1) & ~(GetVirtualPageSize() - 1);
}

//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::RoundDownAddress(void* address)
{
	return (void*)((size_t)address & -m_virtualPageSize);
}
