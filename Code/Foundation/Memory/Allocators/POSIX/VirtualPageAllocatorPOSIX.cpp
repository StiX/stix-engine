
#include "VirtualPageAllocatorPOSIX.h"
#include <unistd.h>
#include "Debug.h"


size_t VirtualPageAllocator::m_virtualPageSize = 4096;


//-------------------------------------------------------------------------------------------
void VirtualPageAllocator::Init()
{
	m_virtualPageSize = (size_t)sysconf(_SC_PAGE_SIZE);
}