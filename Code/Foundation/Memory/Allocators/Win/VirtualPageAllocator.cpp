
#include "VirtualPageAllocator.h"
#include "Debug.h"


size_t VirtualPageAllocator::m_virtualPageSize = 4096;
size_t VirtualPageAllocator::m_allocationGranularity = 65536;

//-------------------------------------------------------------------------------------------
void VirtualPageAllocator::Init()
{
	SYSTEM_INFO systemInfo;
	GetSystemInfo(&systemInfo);

	m_virtualPageSize = systemInfo.dwPageSize;
	//TODO: win allocation granularity is 65k, so we have to keep track of 61k that's left behind 
	m_allocationGranularity = systemInfo.dwAllocationGranularity;
}