
#pragma once

#include "Types.h"
#include <windows.h>


class VirtualPageAllocator
{
public:
	static void		Init();		// This function is called automatically

	static void*	Alloc(size_t size, void* baseAddress = nullptr);
	static void*	AllocAndZero(size_t size);
	static void*	Reserve(size_t size, void* baseAddress = nullptr);
	static void*	Commit(void* baseAddress, size_t size);

	static bool		Free(void* address, size_t size);
	static bool		Decommit(void* address, size_t size);

	static size_t	GetVirtualPageSize();

	static size_t	AlignToPageBoundary(size_t size);

protected:
private:
	static size_t	m_virtualPageSize;
	static size_t	m_allocationGranularity;

	static size_t	AlignAllocation(size_t size);
};





//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::Alloc(size_t size, void* baseAddress)
{
	size = AlignAllocation(size);
	return VirtualAlloc(baseAddress, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
}

//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::AllocAndZero(size_t size)
{
	return Alloc(size);
}

//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::Reserve(size_t size, void* baseAddress)
{
	size = AlignAllocation(size);
	return VirtualAlloc(baseAddress, size, MEM_RESERVE, PAGE_READWRITE);
}

//-------------------------------------------------------------------------------------------
inline void* VirtualPageAllocator::Commit(void* baseAddress, size_t size)
{
	return VirtualAlloc(baseAddress, size, MEM_COMMIT, PAGE_READWRITE);
}

//-------------------------------------------------------------------------------------------
inline bool VirtualPageAllocator::Free(void* address, size_t /*size*/)
{
	if (address)
	{
		return VirtualFree(address, 0, MEM_RELEASE) != 0;
	}

	return false;
}

//-------------------------------------------------------------------------------------------
inline bool VirtualPageAllocator::Decommit(void* address, size_t size)
{
	return VirtualFree(address, size, MEM_DECOMMIT) != 0;
}

//-------------------------------------------------------------------------------------------
inline size_t VirtualPageAllocator::GetVirtualPageSize()
{
	return m_virtualPageSize;
}

//-------------------------------------------------------------------------------------------
inline size_t VirtualPageAllocator::AlignToPageBoundary(size_t size)
{
	return (size + m_virtualPageSize - 1) & ~(m_virtualPageSize - 1);
}

//-------------------------------------------------------------------------------------------
inline size_t VirtualPageAllocator::AlignAllocation(size_t size)
{
	return (size + m_allocationGranularity - 1) & ~(m_allocationGranularity - 1);
}