
#include "LinearAllocator.h"
#include "Debug.h"
#include "math_lib.h"
#include "VirtualPageAllocator.h"


//-------------------------------------------------------------------------------------------
LinearAllocator::LinearAllocator()
	: m_pBlockStart(nullptr), m_currentOffset(0), m_reservedSize(0), m_commitedSize(0)
{
}

//-------------------------------------------------------------------------------------------
LinearAllocator::LinearAllocator(size_t reservedSize, size_t initialCommit)
	: m_pBlockStart(nullptr), m_currentOffset(0)
{
	Init(reservedSize, initialCommit);
}

//-------------------------------------------------------------------------------------------
LinearAllocator::~LinearAllocator()
{
	VirtualPageAllocator::Free(m_pBlockStart, m_reservedSize);
}

//-------------------------------------------------------------------------------------------
void LinearAllocator::Init(size_t reservedSize, size_t initialCommit)
{
	ASSERT(!m_pBlockStart);

	m_reservedSize = VirtualPageAllocator::AlignToPageBoundary(reservedSize);
	m_pBlockStart = (u8*)VirtualPageAllocator::Reserve(reservedSize);

	if (initialCommit)
	{
		m_commitedSize = VirtualPageAllocator::AlignToPageBoundary(initialCommit);
		VirtualPageAllocator::Commit(m_pBlockStart, initialCommit);
	}
}

//-------------------------------------------------------------------------------------------
void* LinearAllocator::Allocate(size_t size, size_t alignment)
{
	m_currentOffset = math::align(m_currentOffset, alignment);
	void* ptr = &m_pBlockStart[m_currentOffset];

	m_currentOffset += size;
	if (m_currentOffset > m_commitedSize)
	{
		ASSERT(m_currentOffset < m_reservedSize);

		VirtualPageAllocator::Commit(ptr, size);	// TODO: fix, when size is bigger than page
		m_commitedSize = VirtualPageAllocator::AlignToPageBoundary(m_currentOffset);
	}

	return ptr;
}

//-------------------------------------------------------------------------------------------
void LinearAllocator::Reset()
{
	m_currentOffset = 0;
}