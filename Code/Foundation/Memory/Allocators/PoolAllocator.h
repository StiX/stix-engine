
#pragma once

#include <new>
#include <utility>
#include "Types.h"


class PoolAllocator
{
public:
			PoolAllocator(u32 initialCommitSize, u32 reservedSize);
			~PoolAllocator();

	template <class T>
	T*		Allocate();
	template <class T, class ... Types>
	T*		Create(Types&& ... args);
	template <class T>
	void	Delete(T* ptr);

	void*	Alloc(size_t size);
	void	Free(void* ptr);

protected:
private:
};





//-------------------------------------------------------------------------------------------
template <class T>
inline T* PoolAllocator::Allocate()
{
	return (T*)Alloc(sizeof(T));
}

//-------------------------------------------------------------------------------------------
template <class T, class ... Types>
inline T* PoolAllocator::Create(Types&& ... args)
{
	void* pData = Alloc(sizeof(T));

	return new (pData) T(std::forward<Types>(args)...);
}

//-------------------------------------------------------------------------------------------
template <class T>
inline void PoolAllocator::Delete(T* ptr)
{
	ptr->~T();

	Free(ptr);
}