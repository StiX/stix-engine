
#pragma once

#ifdef __WIN__
#	include "Win\VirtualPageAllocator.h"
#else
#	include "POSIX/VirtualPageAllocatorPOSIX.h"
#endif