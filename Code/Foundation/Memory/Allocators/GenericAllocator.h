
#pragma once

#include <cstdlib>


struct GenericAllocator
{
	static void*	Alloc(size_t size);
	static void*	AllocAndZero(size_t size);
	static void		Free(void* ptr);
	static void*	Realloc(void* ptr, size_t size);
};





//-------------------------------------------------------------------------------------------
__forceinline void* GenericAllocator::Alloc(size_t size)
{
	return malloc(size);
}

//-------------------------------------------------------------------------------------------
__forceinline void* GenericAllocator::AllocAndZero(size_t size)
{
	void* ptr = Alloc(size);
	memset(ptr, 0, size);

	return ptr;
}

//-------------------------------------------------------------------------------------------
__forceinline void GenericAllocator::Free(void* ptr)
{
	free(ptr);
}

//-------------------------------------------------------------------------------------------
__forceinline void* GenericAllocator::Realloc(void* ptr, size_t size)
{
	return realloc(ptr, size);
}