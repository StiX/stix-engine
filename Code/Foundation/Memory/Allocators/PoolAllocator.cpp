
#include "PoolAllocator.h"
#include <cstdlib>


//-------------------------------------------------------------------------------------------
PoolAllocator::PoolAllocator(u32 initialCommitSize, u32 reservedSize)
{
	(void)initialCommitSize;
	(void)reservedSize;
}

//-------------------------------------------------------------------------------------------
PoolAllocator::~PoolAllocator()
{
}

//-------------------------------------------------------------------------------------------
void* PoolAllocator::Alloc(size_t size)
{
	return malloc(size);
}

//-------------------------------------------------------------------------------------------
void PoolAllocator::Free(void* ptr)
{
	::free(ptr);
}