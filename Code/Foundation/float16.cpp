
#include "float16.h"


union FP32
{
	u32		uint;
	float	flt;

	struct
	{
		u32	mantissa : 23;
		u32	exponent : 8;
		u32	sign : 1;
	};
};



//-------------------------------------------------------------------------------------------
float16::float16(float value)
{
	static_assert(sizeof(FP16) == 2, "Size of FP16 is not equal to 2 bytes. Try to remove nameless struct from the union");

	FP32 flt32;
	flt32.flt = value;

	const u32 kSign = flt32.uint & 0x80000000;
	const u32 kBits = flt32.uint & 0x7fffffff;	// Exponent and mantissa bits
	const u32 kExponent = flt32.uint & 0x7f800000;

	u32 result;

	if (kExponent < 0x38800000)					// 0x38800000 is min positive normal value, everything below (denormals) is clamped to 0
	{
		result = 0;
	}
	else if (kBits > 0x477fe100)				// 0x477fe100 is 65505.0f, clamp to INF
	{
		result = 0x7c00;
	}
	else
	{
		result = kBits >> 13;
		result -= 0x1c000;						// Adjust exponent
	}

	result |= kSign >> 16;
	m_value.uint = (u16)result;
}

//-------------------------------------------------------------------------------------------
float16::operator float() const
{
	const u32 kSign = m_value.uint & 0x8000;
	const u32 kBits = m_value.uint & 0x7fff;	// Exponent and mantissa bits
	const u32 kExponent = m_value.uint & 0x7c00;

	FP32 result;

	switch (kExponent)
	{
	case 0:										// Denormal value, Reset-to-zero
		result.uint = 0;
		break;

	case 0x7c00:								// INF/NAN value, handle NAN as Inf for performance reasons
		result.uint = 0xFF << 23;				// Set resulting exponent to 0xFF (INF)
		break;

	default:
		result.uint = kBits << 13;
		result.uint += 0x70 << 23;				// Adjust exponent
		break;
	}

	result.uint |= kSign << 16;

	return result.flt;
}