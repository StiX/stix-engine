
#include "JsonParser.h"
#include <new>


struct JsonNodeStack
{
	JsonNode*	m_stack[JSON_STACK_DEPTH];
	int			m_stackTop;

				JsonNodeStack();

	void		Push(JsonNode* pNode);
	JsonNode*	Pop();
};




//-------------------------------------------------------------------------------------------
inline JsonNodeStack::JsonNodeStack()
	: m_stackTop(-1)
{
}

//-------------------------------------------------------------------------------------------
inline void JsonNodeStack::Push(JsonNode* pNode)
{
	assert(m_stackTop < JSON_STACK_DEPTH - 1);
	m_stack[++m_stackTop] = pNode;
}

//-------------------------------------------------------------------------------------------
inline JsonNode* JsonNodeStack::Pop()
{
	assert(m_stackTop > -1);
	return m_stack[m_stackTop--];
}




//-------------------------------------------------------------------------------------------
inline bool IsDigit(char c)
{
	return c >= '0' && c <= '9';
}

//-------------------------------------------------------------------------------------------
double ToDigit(char* str, char** end)
{
#define TO_DIGIT(x) ((x) - '0')


	bool negative = false;
	if (*str == '-')
	{
		negative = true;
		++str;
	}

	double value = 0.0;
	while (IsDigit(*str))
	{
		value = value * 10.0 + TO_DIGIT(*str);
		++str;
	}

	if (*str == '.')
	{
		++str;

		double fraction = 1.0;
		while (IsDigit(*str))
		{
			fraction *= 0.1;
			value += fraction * TO_DIGIT(*str);
			++str;
		}
	}

	if (*str == 'e' || *str == 'E')
	{
		++str;

		double mult = 10.0;
		if (*str == '-')
		{
			mult = 0.1;
			++str;
		}
		else if (*str == '+')
		{
			++str;
		}

		unsigned power = 0;
		while (IsDigit(*str))
		{
			power = power * 10 + TO_DIGIT(*str);
			++str;
		}

		double val = 1.0;
		while (power)
		{
			if (power & 1)
			{
				val *= mult;
			}

			power = power >> 1;
			mult *= mult;
		}

		value *= val;
	}

	*end = str;

	return negative ? -value : value;

#undef TO_DIGIT
}

//-------------------------------------------------------------------------------------------
JsonResult JsonParser::ParseStringInplace(char* jsonString, size_t strLen)
{
#define SET_PARENT(__val, __type)	\
	parentNodeType = __type;		\
	pParentNode = __val

#define CREATE_NEW_NODE(__val, __is_key)																	\
	if (m_pCurrentBlock->m_currentOffset + sizeof(JsonNode) >= m_pCurrentBlock->m_blockSize)				\
	{																										\
		AllocateBlock(jsonString, lastBlockStrPos, strLen);													\
		lastBlockStrPos = jsonString;																		\
	}																										\
																											\
	pNode = new (&m_pCurrentBlock->m_data[m_pCurrentBlock->m_currentOffset]) JsonNode(__val, pParentNode);	\
	pPrevNode->m_pNeighbourNode = pNode;																	\
	pPrevNode = pNode;																						\
	m_pCurrentBlock->m_currentOffset += sizeof(JsonNode);													\
																											\
	if ((parentNodeType == eJsonArray) || __is_key)															\
	{																										\
		++pParentNode->m_childsCount;																		\
		pPrevLinkNode->m_pNextNode = pNode;																	\
		pPrevLinkNode = pNode;																				\
	}


	assert(jsonString);
	const char* lastBlockStrPos = jsonString;

	const size_t kAllocSize = strLen / 16 * sizeof(JsonNode);
	m_pFirstBlock = CreateAllocationBlock(kAllocSize);
	m_pCurrentBlock = m_pFirstBlock;

	JsonNode* pNode = 0;
	JsonNode dummyNode;
	JsonNode* pParentNode = &dummyNode;

	JsonValueType parentNodeType = eJsonObject;

	JsonNodeStack parentStack;
	JsonNodeStack nextNodeStack;

	JsonNode* pPrevNode = &dummyNode;
	JsonNode* pPrevLinkNode = &dummyNode;

	size_t expectsKeyToken = true;
	static const size_t kFlaggedMSB = (size_t)(size_t(1) << (CHAR_BIT * sizeof(size_t) - 1));

	while (*jsonString)
	{
		switch (*jsonString)
		{
		case '{':
			CREATE_NEW_NODE(JsonValue(eJsonObject), false);
			parentStack.Push(pParentNode);
			SET_PARENT(pNode, eJsonObject);
			expectsKeyToken = kFlaggedMSB;

			nextNodeStack.Push(pPrevLinkNode);
			pPrevLinkNode = &dummyNode;	// special placeholder
			break;

		case '[':
			CREATE_NEW_NODE(JsonValue(eJsonArray), false);
			parentStack.Push(pParentNode);
			SET_PARENT(pNode, eJsonArray);

			nextNodeStack.Push(pPrevLinkNode);
			pPrevLinkNode = &dummyNode;
			break;

		case ']':
		case '}':
		{
			if (parentStack.m_stackTop < 0)
			{
				return eJsonNonmatchingClosingBrace;
			}

			pPrevLinkNode = nextNodeStack.Pop();

			JsonNode* pParent = parentStack.Pop();
			SET_PARENT(pParent, pParent->GetValue().GetType());
			break;
		}

		case ':':
			expectsKeyToken = 0;
			break;

		case ',':
			expectsKeyToken = parentNodeType == eJsonArray ? 0 : kFlaggedMSB;
			break;

		case '\t':
		case '\r':
		case '\n':
		case ' ':
			break;

		case '\"':
		{
			++jsonString;
			char* pBeginPtr = jsonString;

			while (*jsonString)
			{
				if (*jsonString == '\\')	// do not care about escape sequences
				{
					++jsonString;
				}
				else if (*jsonString == '\"')
				{
					*jsonString = '\0';
					break;
				}

				++jsonString;
			}

			CREATE_NEW_NODE(JsonValue(eJsonString, pBeginPtr), expectsKeyToken);
			pNode->m_childsCount |= expectsKeyToken;
			break;
		}

		case '-':
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			CREATE_NEW_NODE(JsonValue(ToDigit(jsonString, &jsonString)), false);
			--jsonString;
			break;

		case 'n':
			for (const char* keyWord = "ull"; *keyWord; ++keyWord)
			{
				if (*(++jsonString) != *keyWord)
				{
					return eJsonBrokenValueKeyword;
				}
			}

			CREATE_NEW_NODE(JsonValue(eJsonNull), false);
			break;

		case 'f':
			for (const char* keyWord = "alse"; *keyWord; ++keyWord)
			{
				if (*(++jsonString) != *keyWord)
				{
					return eJsonBrokenValueKeyword;
				}
			}

			CREATE_NEW_NODE(JsonValue(eJsonBool), false);
			break;

		case 't':
			for (const char* keyWord = "rue"; *keyWord; ++keyWord)
			{
				if (*(++jsonString) != *keyWord)
				{
					return eJsonBrokenValueKeyword;
				}
			}

			CREATE_NEW_NODE(JsonValue(eJsonBool, (void*)true), false);
			break;

		default:
			return eJsonUnexpectedChar;
		}

		++jsonString;
	}

	pNode = (JsonNode*)m_pFirstBlock->m_data;
	pNode->m_pParentNode = 0;	// fix the ptr to dummy node

	return eJsonSuccess;

#undef SET_PARENT
#undef CREATE_NEW_NODE
}

//-------------------------------------------------------------------------------------------
void JsonParser::AllocateBlock(const char* currentJsonStrPos, const char* lastBlockStrPos, size_t& charsLeft)
{
	const size_t kProcessedCharsCount = size_t(currentJsonStrPos) - size_t(lastBlockStrPos);
	charsLeft -= kProcessedCharsCount;

	const size_t kCharsPerNode = kProcessedCharsCount * sizeof(JsonNode) / m_pCurrentBlock->m_blockSize;
	const size_t kMemSize = charsLeft / kCharsPerNode * sizeof(JsonNode);

	AllocationBlock* pBlock = CreateAllocationBlock(kMemSize);
	m_pCurrentBlock->m_pNext = pBlock;
	m_pCurrentBlock = pBlock;
}

//-------------------------------------------------------------------------------------------
inline JsonParser::AllocationBlock* JsonParser::CreateAllocationBlock(size_t memSize)  const
{
	AllocationBlock* pBlock = (AllocationBlock*)m_allocator.m_alloc(memSize + sizeof(AllocationBlock));
	pBlock->m_pNext = 0;
	pBlock->m_currentOffset = 0;
	pBlock->m_blockSize = memSize;

	return pBlock;
}

//-------------------------------------------------------------------------------------------
void JsonParser::FreeAllocationBlock(JsonParser::AllocationBlock* pBlock) const
{
	if (pBlock->m_pNext)
	{
		FreeAllocationBlock(pBlock->m_pNext);
	}

	m_allocator.m_free(pBlock);
}