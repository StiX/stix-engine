
#pragma once

class StringHash;


enum log_type {
	message,
	warning,
	error,
};

#ifdef DEBUG
	void		log_init(const char* log_path);
	void		log_shutdown();

	void		log_message(const char* system, const char* str, ...);
	void		log_warning(const char* system, const char* str, ...);
	void		log_error(const char* system, const char* str, ...);

	void		log_set_error_context(const StringHash* context);

	void		log_enable();
	void		log_disable();
	void		log_set_level(log_type level);
#else
	inline void	log_init(const char*) {}
	inline void	log_shutdown() {}

	inline void	log_message(const char*, const char*, ...) {}
	inline void	log_warning(const char*, const char*, ...) {}
	inline void	log_error(const char*, const char*, ...) {}

	inline void	log_set_error_context(const StringHash*) {}

	inline void	log_enable() {}
	inline void	log_disable() {}
	inline void	log_set_level(log_type) {}
#endif

const char*		stringify(const char* format, ...);