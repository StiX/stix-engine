
#pragma once

#include <limits.h>


#if (__STDC_VERSION__ >= 199901L) || (__cplusplus > 199711L)	// if has C99 or C++11 support
#	include <stdint.h>
	typedef int8_t				s8;
	typedef uint8_t				u8;
	typedef int16_t				s16;
	typedef uint16_t			u16;
	typedef int32_t				s32;
	typedef uint32_t			u32;
	typedef int64_t				s64;
	typedef uint64_t			u64;
#elif defined _MSC_VER
	typedef				__int8	s8;
	typedef unsigned	__int8	u8;
	typedef				__int16	s16;
	typedef unsigned	__int16	u16;
	typedef				__int32 s32;
	typedef unsigned	__int32 u32;
	typedef				__int64 s64;
	typedef unsigned	__int64 u64;
#endif


typedef	u32				index_t;

static const index_t	kInvalidIndex = (index_t)-1;