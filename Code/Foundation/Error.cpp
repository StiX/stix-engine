
#include "Error.h"

#ifdef __WIN__
#	include <windows.h>
#else
#	include <string.h>
#	include <errno.h>
#endif


//-------------------------------------------------------------------------------------------
const char* Error::GetLastErrorMessage()
{
#ifdef __WIN__
	const DWORD bufSize = 256;
	static char errorBuf[bufSize];
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), errorBuf, bufSize, NULL);

	return errorBuf;
#else
	return strerror(errno);
#endif
}