
#include "Timer.h"
#include <windows.h>

namespace timer {

double to_s = 0.0;
double to_ms = 0.0;
double to_us = 0.0;
double to_ns = 0.0;

//-------------------------------------------------------------------------------------------
void init() {
	s64 freq;
	QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	to_s = 1.0 / (double)freq;
	to_ms = 1000.0 / (double)freq;
	to_us = 1000000.0 / (double)freq;
	to_ns = 1000000000.0 / (double)freq;
}

//-------------------------------------------------------------------------------------------
double seconds() {
	s64 counter;
	QueryPerformanceCounter((LARGE_INTEGER *)&counter);
	return ((double)counter) * to_s;
}

//-------------------------------------------------------------------------------------------
double milli_seconds() {
	s64 counter;
	QueryPerformanceCounter((LARGE_INTEGER *)&counter);
	return ((double)counter) * to_ms;
}

//-------------------------------------------------------------------------------------------
double micro_seconds() {
	s64 counter;
	QueryPerformanceCounter((LARGE_INTEGER *)&counter);
	return ((double)counter) * to_us;
}

//-------------------------------------------------------------------------------------------
double nano_seconds() {
	s64 counter;
	QueryPerformanceCounter((LARGE_INTEGER *)&counter);
	return ((double)counter) * to_us;
}

//-------------------------------------------------------------------------------------------
u64 cycles_count() {
#ifdef __ARCH_INTEL__
	return __rdtsc();
#else
#	error "Cannot get cycles count on your platform"
#endif
}

}	//~namespace timer