
#include "Timer.h"
#include <mach/mach_time.h>


namespace timer {

double to_s = 0.0;
double to_ms = 0.0;
double to_us = 0.0;
double to_ns = 0.0;

//-------------------------------------------------------------------------------------------
void init() {
	mach_timebase_info_data_t timeInfo;
	mach_timebase_info(&timeInfo);

    to_s = (double)timeInfo.numer / ((double)timeInfo.denom * 1E+9);
	to_ms = (double)timeInfo.numer / ((double)timeInfo.denom * 1E+6);
	to_us = (double)timeInfo.numer / ((double)timeInfo.denom * 1E+3);
	to_ns = (double)timeInfo.numer / (double)timeInfo.denom;
}

//-------------------------------------------------------------------------------------------
double seconds() {
	return (double)mach_absolute_time() * to_s;
}

//-------------------------------------------------------------------------------------------
double milli_seconds() {
	return (double)mach_absolute_time() * to_ms;
}
    
//-------------------------------------------------------------------------------------------
double micro_seconds() {
	return (double)mach_absolute_time() * to_us;
}

//-------------------------------------------------------------------------------------------
double nano_seconds() {
	return (double)mach_absolute_time() * to_ns;
}
    
//-------------------------------------------------------------------------------------------
u64 cycles_count()
{
#ifdef __ARCH_INTEL__
	u32 lo, hi;
	__asm__ __volatile__("rdtsc" : "=a" (lo), "=d" (hi));
	return ((u64)hi << 32) | lo;
#elif defined __ARCH_ARM__
	return 0;
#else
#	error "Cannot get cycles count on your platform"
#endif
}
    
}   //~namespace timer
