
#pragma once

#include "Types.h"


namespace timer {
	void	init();

	double	seconds();
	double	milli_seconds();
	double	micro_seconds();
	double	nano_seconds();
	u64		cycles_count();
}