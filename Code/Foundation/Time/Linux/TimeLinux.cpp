
#include "Timer.h"
#include <time.h>


namespace timer {

//-------------------------------------------------------------------------------------------
void init() {}

//-------------------------------------------------------------------------------------------
double	seconds() {
	timespec time;
	clock_gettime(CLOCK_MONOTONIC, &time);

	return (double)time.tv_sec + (double)time.tv_nsec * 1E-9;
}

//-------------------------------------------------------------------------------------------
double	milli_seconds() {
	timespec time;
	clock_gettime(CLOCK_MONOTONIC, &time);

	return (double)time.tv_sec * 1E+3 + (double)time.tv_nsec * 1E-6;
}

//-------------------------------------------------------------------------------------------
double micro_seconds() {
	timespec time;
	clock_gettime(CLOCK_MONOTONIC, &time);

	return (double)time.tv_sec * 1E+6 + (double)time.tv_nsec * 1E-3;
}

//-------------------------------------------------------------------------------------------
double	nano_seconds() {
	timespec time;
	clock_gettime(CLOCK_MONOTONIC, &time);

	return (double)time.tv_sec * 1E+9 + (double)time.tv_nsec;
}

//-------------------------------------------------------------------------------------------
u64 cycles_count() {
#ifndef __ARCH_ARM__
#	error "Not implemented"
#endif

	return 0;
}

}	//~namespace timer