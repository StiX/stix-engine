
#pragma once

#include "Types.h"


class FSWatchDog
{
public:
	enum eEventType
	{
		eFileAdded,
		eFileRemoved,
		eFileModified,
		eFileRenamedOldName,
		eFileRenamedNewName,
	};
	
	typedef void	(*Callback)(const char* filePath, eEventType eventType, void* pUserData);
	
					FSWatchDog() {}
					~FSWatchDog() {}
	
	void			Init(float) {}
	void			Shutdown() {}
	
	index_t			AddObserver(const char*, bool, Callback, void*) { return kInvalidIndex; }
	void			RemoveObserver(index_t) {}
	
	void			Update(float) {}
};