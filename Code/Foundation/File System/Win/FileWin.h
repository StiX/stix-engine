
#pragma once

#include <Windows.h>
#include "FileSystemFlagsWin.h"
#include "ClassHelpers.h"
#include "Types.h"
#include "Debug.h"


enum eFileOrigin
{
	FILE_ORIGIN_BEGIN	= FILE_BEGIN,
	FILE_ORIGIN_END		= FILE_END,
	FILE_ORIGIN_CURRENT = FILE_CURRENT,
};


void UnmapFile(void* mappedAddress, size_t size);


class File
{
public:
					File();
					File(HANDLE handle, u32 flags);
					File(File&& rhs);
					~File();

	bool			IsOpened() const;

	File&			operator=(File&& other);

	template		<class T>
	T				Read() const;
	u32				Read(void* pBuf, size_t bytesCount) const;		// Returns count of actual bytes read
	
	template		<class T>
	u32				Write(const T& data) const;
	u32				Write(const void* pData, size_t bytesCount) const;
	
	void			Seek(ptrdiff_t distanceToMove, eFileOrigin origin) const;
	size_t			GetCurrentPos() const;
	bool			EoF() const;
	size_t			GetSize() const;

	void*			MapFile(eFileMappingAccessRights accessRights) const;
	void*			MapFilePart(eFileMappingAccessRights accessRights, size_t offset, size_t length) const;

protected:
private:
	HANDLE			m_handle;
	mutable HANDLE	m_mappingHandle;
#ifdef DEBUG
	u32				m_flags;
#endif

	//TODO: consider to allow copying
	MAKE_UNCOPYABLE(File);
};





//-------------------------------------------------------------------------------------------
inline File::File()
	: m_handle(0), m_mappingHandle(0)
#ifdef DEBUG
	, m_flags(0)
#endif
{
}

//-------------------------------------------------------------------------------------------
inline File::File(HANDLE handle, u32 flags)
	: m_handle(handle), m_mappingHandle(0)
#ifdef DEBUG
	, m_flags(flags)
#endif
{
	(void)flags;
}

//-------------------------------------------------------------------------------------------
inline File::File(File&& rhs)
	: m_handle(rhs.m_handle), m_mappingHandle(rhs.m_mappingHandle)
#ifdef DEBUG
	, m_flags(rhs.m_flags)
#endif
{
	rhs.m_handle = nullptr;
	rhs.m_mappingHandle = nullptr;
}

//-------------------------------------------------------------------------------------------
inline bool File::IsOpened() const
{
	return m_handle != 0;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline T File::Read() const
{
	T obj;
	Read(&obj, sizeof(obj));

	return obj;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline u32 File::Write(const T& data) const
{
	return Write(&data, sizeof(T));
}

//-------------------------------------------------------------------------------------------
inline void File::Seek(ptrdiff_t distanceToMove, eFileOrigin origin) const
{
	ASSERT(IsOpened());

#ifdef __ARCH_64BIT__
	LARGE_INTEGER li;
	li.QuadPart = distanceToMove;
	::SetFilePointer(m_handle, li.LowPart, &li.HighPart, origin);
#else
	::SetFilePointer(m_handle, distanceToMove, NULL, origin);
#endif
}

//-------------------------------------------------------------------------------------------
inline size_t File::GetCurrentPos() const
{
	ASSERT(IsOpened());

#ifdef __ARCH_64BIT__
	LARGE_INTEGER li;
	li.LowPart = ::SetFilePointer(m_handle, 0, &li.HighPart, FILE_ORIGIN_CURRENT);
	return li.QuadPart;
#else
	return ::SetFilePointer(m_handle, 0, NULL, FILE_ORIGIN_CURRENT);
#endif 
}

//-------------------------------------------------------------------------------------------
inline bool File::EoF() const
{
	return (GetCurrentPos() == GetSize());
}

//-------------------------------------------------------------------------------------------
inline void* File::MapFile(eFileMappingAccessRights accessRights) const
{
	return MapFilePart(accessRights, 0, 0);
}