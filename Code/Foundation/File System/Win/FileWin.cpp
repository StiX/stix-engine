
#include "FileWin.h"
#include "Log.h"
#include "Error.h"
#include "math_lib.h"


static size_t gAllocationGranularity = 65536;

struct AllocGranInit
{
	AllocGranInit()
	{
		SYSTEM_INFO systemInfo;
		GetSystemInfo(&systemInfo);

		gAllocationGranularity = (size_t)systemInfo.dwAllocationGranularity;
	}
};

static AllocGranInit gAllocGranInit;


//-------------------------------------------------------------------------------------------
void UnmapFile(void* mappedAddress, size_t) {
	const size_t kAdjustedAddress = math::round_down_to_multiple_of_pot((size_t)mappedAddress, gAllocationGranularity);

	if (!UnmapViewOfFile((void*)kAdjustedAddress)) {
		log_error("File System", "Cannot unmap view of file, error message: %s", Error::GetLastErrorMessage());
	}
}


//-------------------------------------------------------------------------------------------
File::~File()
{
	if (IsOpened())
	{
		::CloseHandle(m_handle);

		if (m_mappingHandle)
		{
			CloseHandle(m_mappingHandle);
		}
	}
}

//-------------------------------------------------------------------------------------------
File& File::operator=(File&& other)
{
	ASSERT(!m_handle, "Please, use move copy assignment only for non opened files");

	m_handle = other.m_handle;
	m_mappingHandle = other.m_mappingHandle;
#ifdef DEBUG
	m_flags = other.m_flags;
#endif

	other.m_handle = 0;
	other.m_mappingHandle = 0;

	return *this;
}

//-------------------------------------------------------------------------------------------
u32 File::Read(void* pBuf, size_t bytesCount) const
{
	ASSERT(IsOpened());

#ifdef __ARCH_64BIT__
	ASSERT(bytesCount <= UINT_MAX);
#endif

	ASSERT((m_flags & F_NO_BUFFERING) == 0);

	u32 bytesRead = 0;
	::ReadFile(m_handle, pBuf, (u32)bytesCount, (LPDWORD)&bytesRead, NULL);
	return bytesRead;
}

//-------------------------------------------------------------------------------------------
u32 File::Write(const void* pData, size_t bytesCount) const
{
	ASSERT(IsOpened());

#ifdef __ARCH_64BIT__
	ASSERT(bytesCount <= UINT_MAX);
#endif

	ASSERT((m_flags & F_NO_BUFFERING) == 0);	// I guess writing to a file with no buffering will be really slow, so test it before "go live"

	u32 bytesWritten = 0;
	::WriteFile(m_handle, pData, (u32)bytesCount, (LPDWORD)&bytesWritten, NULL);
	return bytesWritten;
}

//-------------------------------------------------------------------------------------------
size_t File::GetSize() const
{
	ASSERT(IsOpened());

	LARGE_INTEGER li;
	BOOL result = GetFileSizeEx(m_handle, &li);
	if (result == 0)
	{
		char path[MAX_PATH];
		GetFinalPathNameByHandle(m_handle, path, MAX_PATH - 1, FILE_NAME_NORMALIZED);
		log_error("File System", "Cannot get file size. File %s, error message: %s", path, Error::GetLastErrorMessage());
	}

#ifdef __ARCH_64BIT__
	return li.QuadPart;
#else
	return li.LowPart;
#endif 
}

//-------------------------------------------------------------------------------------------
void* File::MapFilePart(eFileMappingAccessRights accessRights, size_t offset, size_t length) const
{
	ASSERT(IsOpened());

	static_assert(F_MAPPING_READ == 2, "File mapping Read flag value is wrong!");
	static_assert(F_MAPPING_READ_WRITE == 4, "File mapping Write flag value is wrong!");
	static_assert(FILE_MAP_READ == 4, "File mapping Read flag value is wrong!");
	static_assert(FILE_MAP_WRITE == 2, "File mapping Write flag value is wrong!");
	ASSERT(accessRights == F_MAPPING_READ || accessRights == F_MAPPING_READ_WRITE);

	ASSERT(m_flags & F_SEQUENTIAL_ACCESS_HINT);

	if (!m_mappingHandle)
	{
		m_mappingHandle = CreateFileMapping(m_handle, nullptr, accessRights, 0, 0, nullptr);
	}

	const size_t kAdjustedOffset = math::round_down_to_multiple_of_pot(offset, gAllocationGranularity);
	const size_t kOffsetDiff = offset - kAdjustedOffset;
	length += kOffsetDiff;

	void* ptr = MapViewOfFile(m_mappingHandle, accessRights ^ 0x6, 0, (DWORD)kAdjustedOffset, length);

	if (!ptr)
	{
		log_error("File System", "Cannot create view of file, error message: %s", Error::GetLastErrorMessage());
		return nullptr;
	}

	return (u8*)ptr + kOffsetDiff;
}