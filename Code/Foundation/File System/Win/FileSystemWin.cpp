
#include "FileSystem.h"
#include <windows.h>
#include "Error.h"
#include "Log.h"


static char g_workDir[MAX_PATH] = "";


//-------------------------------------------------------------------------------------------
void SetWorkDir(const char* workDir)
{
	ASSERT(workDir);

	strcpy(g_workDir, workDir);
	
	if (!SetCurrentDirectory(workDir))
	{
		log_error("File System", "Cannot change current directory to %s, error message: %s", workDir, Error::GetLastErrorMessage());
	}
}

//-------------------------------------------------------------------------------------------
const char* GetWorkDir()
{
	return g_workDir;
}

//-------------------------------------------------------------------------------------------
File OpenFile(const char* path, eAccessRights accessRights, u32 flagsAndAttributes, u32 shareMode)
{
	HANDLE handle = ::CreateFile(path, accessRights, shareMode, NULL, OPEN_EXISTING, flagsAndAttributes, NULL);

	if (handle == INVALID_HANDLE_VALUE)
	{
		log_error("File System", "Cannot open file %s, error message: %s", path, Error::GetLastErrorMessage());
		return File();
	}

	return File(handle, flagsAndAttributes);
}

//-------------------------------------------------------------------------------------------
File CreateFile(const char* path, eAccessRights accessRights, u32 flagsAndAttributes, u32 shareMode)
{
	HANDLE handle = ::CreateFile(path, accessRights, shareMode, NULL, CREATE_ALWAYS, flagsAndAttributes, NULL);

	if (handle == INVALID_HANDLE_VALUE)
	{
		log_error("File System", "Cannot create file %s, error message: %s", path, Error::GetLastErrorMessage());
		return File();
	}

	return File(handle, flagsAndAttributes);
}

//-------------------------------------------------------------------------------------------
bool CreateDirectory(const char* path)
{
	return ::CreateDirectory(path, NULL) != 0;
}

//-------------------------------------------------------------------------------------------
File FindAndOpenFile(const char* filePath, eAccessRights accessRights, u32 flagsAndAttributes, u32 shareMode)
{
	WIN32_FIND_DATA findData;
	HANDLE handle = ::FindFirstFile(filePath, &findData);

	if (handle == INVALID_HANDLE_VALUE)
	{
		::FindClose(handle);
		return File();
	}

	const String kFilePath = String(filePath).ExtractDirName() + findData.cFileName;
	return OpenFile(kFilePath.c_str(), accessRights, flagsAndAttributes, shareMode);
}

//-------------------------------------------------------------------------------------------
void FindFiles(const char* pattern, Array<String>& outFiles)
{
	WIN32_FIND_DATA findData;
	HANDLE handle = ::FindFirstFile(pattern, &findData);

	if (handle == INVALID_HANDLE_VALUE)
	{
		return;
	}

	const String fileDir = String(pattern).ExtractDirName();

	outFiles.Append(fileDir + findData.cFileName);

	while(::FindNextFile(handle, &findData))
	{
		outFiles.Append(fileDir + findData.cFileName);
	}

	::FindClose(handle);
}

//-------------------------------------------------------------------------------------------
bool FileExists(const char* filePath)
{
	return GetFileAttributes(filePath) != INVALID_FILE_ATTRIBUTES;
}