
#pragma once

#include "Pair.h"
#include "Array.h"

struct FSObserver;


class FSWatchDog
{
public:
	enum eEventType
	{
		eFileAdded,
		eFileRemoved,
		eFileModified,
		eFileRenamedOldName,
		eFileRenamedNewName,
	};

	typedef void	(*Callback)(const char* filePath, eEventType eventType, void* pUserData);

					FSWatchDog();
					~FSWatchDog();

	void			Init(float updateSecondsDelay);
	void			Shutdown();

	index_t			AddObserver(const char* dirPath, bool recursive, Callback callback, void* pUserData);
	void			RemoveObserver(index_t handle);

	void			Update(float deltaTime);

protected:
private:
	typedef Pair<index_t, FSObserver*> Pair;

	Array<Pair>		m_observers;
	u32				m_pendingObserversCount;
	float			m_updateTimeDelay;
	float			m_updateCounter;

	bool			HasDuplicate(const char* dirPath);
};





//-------------------------------------------------------------------------------------------
inline void FSWatchDog::Init(float updateSecondsDelay)
{
	m_updateTimeDelay = updateSecondsDelay;
}