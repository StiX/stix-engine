
/************************************************************************************/
/* For more details on implementation check:										*/
/*	http://qualapps.blogspot.se/2010/05/understanding-readdirectorychangesw_19.html	*/
/************************************************************************************/

#include "FSWatchDogWin.h"
#include <windows.h>
#include "Debug.h"
#include "Error.h"
#include "Log.h"


static_assert(FSWatchDog::eFileAdded == FILE_ACTION_ADDED - 1, "Constants mismatch");
static_assert(FSWatchDog::eFileRemoved == FILE_ACTION_REMOVED - 1, "Constants mismatch");
static_assert(FSWatchDog::eFileModified == FILE_ACTION_MODIFIED - 1, "Constants mismatch");
static_assert(FSWatchDog::eFileRenamedOldName == FILE_ACTION_RENAMED_OLD_NAME - 1, "Constants mismatch");
static_assert(FSWatchDog::eFileRenamedNewName == FILE_ACTION_RENAMED_NEW_NAME - 1,	"Constants mismatch");


struct FSObserver
{
	OVERLAPPED	m_overlapped;
	HANDLE		m_dirHandle;
	FSWatchDog::Callback m_callback;
	DWORD		m_notificationBuffer[128];
	bool		m_recursive;
	char		m_dirPath[0];

	void		Init(HANDLE dirHandle, const char* dirPath, FSWatchDog::Callback callback, void* pUserData, u32* pPendingCounter, bool recursive);
};

bool WatchDirectory(FSObserver* pObserver);


//-------------------------------------------------------------------------------------------
void CALLBACK FSEventCallback(DWORD errorCode, DWORD numBytesTransfered, LPOVERLAPPED pOverlapped)
{
	FSObserver* pObserver = (FSObserver*)pOverlapped;

	if (errorCode == ERROR_OPERATION_ABORTED)
	{
		u32* pPendingCounter = (u32*)pObserver->m_overlapped.hEvent;
		--(*pPendingCounter);
		free(pObserver);

		return;
	}

	if (!numBytesTransfered)
	{
		return;
	}

	// sizeof(FILE_NOTIFY_INFORMATION) is 16 bytes due to padding, however real data can occupy at least 14 bytes
	ASSERT(numBytesTransfered >= offsetof(FILE_NOTIFY_INFORMATION, FileName) + sizeof(WCHAR));

	char filePath[MAX_PATH];
	FILE_NOTIFY_INFORMATION* pFileInfo;
	FILE_NOTIFY_INFORMATION* pPrevFileInfo = nullptr;
	char* pBuffer = (char*)&pObserver->m_notificationBuffer[0];

	do
	{
		pFileInfo = (FILE_NOTIFY_INFORMATION*)pBuffer;
		pBuffer += pFileInfo->NextEntryOffset;

		if (pPrevFileInfo)
		{
			if (!memcmp(pFileInfo->FileName, pPrevFileInfo->FileName, pFileInfo->FileNameLength)
				&& pFileInfo->Action == pPrevFileInfo->Action)
			{
				continue;
			}
		}

		const int kStrLen = WideCharToMultiByte(CP_ACP, 0, pFileInfo->FileName, pFileInfo->FileNameLength / sizeof(pFileInfo->FileName), filePath, MAX_PATH - 1, NULL, NULL);
		filePath[kStrLen] = 0;

		pObserver->m_callback(filePath, (FSWatchDog::eEventType)(pFileInfo->Action - 1), pOverlapped->Pointer);

		pPrevFileInfo = pFileInfo;

	} while (pFileInfo->NextEntryOffset);

	if (!WatchDirectory(pObserver))
	{
		log_warning("FSWatchDog", "Cannot continue watching on a dir %s. Error message: %s", pObserver->m_dirPath, Error::GetLastErrorMessage());
	}
}

//-------------------------------------------------------------------------------------------
inline void WaitForSystemFSEvents()
{
	MsgWaitForMultipleObjectsEx(0, NULL, 0, QS_ALLEVENTS, MWMO_ALERTABLE);
}

//-------------------------------------------------------------------------------------------
inline bool WatchDirectory(FSObserver* pObserver)
{
	BOOL result = ReadDirectoryChangesW(pObserver->m_dirHandle, pObserver->m_notificationBuffer, sizeof(pObserver->m_notificationBuffer), pObserver->m_recursive
		, FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_CREATION | FILE_NOTIFY_CHANGE_FILE_NAME, NULL, &pObserver->m_overlapped, &FSEventCallback);

	return result != 0;
}

//-------------------------------------------------------------------------------------------
inline void CloseObserver(const FSObserver* pObserver)
{
	CancelIo(pObserver->m_dirHandle);
	CloseHandle(pObserver->m_dirHandle);
}


//-------------------------------------------------------------------------------------------
inline void FSObserver::Init(HANDLE dirHandle, const char* dirPath, FSWatchDog::Callback callback, void* pUserData, u32* pPendingCounter, bool recursive)
{
	m_callback = callback;
	m_dirHandle = dirHandle;
	m_recursive = recursive;

	strcpy(m_dirPath, dirPath);

	// hEvent is not used, when there is a completion function, so it's OK to store private data
	m_overlapped.hEvent = pPendingCounter;
	// Pointer is not used in case of ReadDirectoryChangesW, so it's OK to store private data
	m_overlapped.Pointer = pUserData;
}


//-------------------------------------------------------------------------------------------
FSWatchDog::FSWatchDog()
	: m_pendingObserversCount(0), m_updateTimeDelay(1.0f), m_updateCounter(0.0f)
{
}

//-------------------------------------------------------------------------------------------
FSWatchDog::~FSWatchDog()
{
	ASSERT(m_pendingObserversCount == 0, "You should call Shutdown, before destoying FSWatchDog");
	Shutdown();
}

//-------------------------------------------------------------------------------------------
void FSWatchDog::Shutdown()
{
	for (u32 i = 0; i < m_observers.GetSize(); ++i)
	{
		CloseObserver(m_observers[i].value);
	}

	while (m_pendingObserversCount)
	{
		WaitForSystemFSEvents();
	}
}

//-------------------------------------------------------------------------------------------
index_t FSWatchDog::AddObserver(const char* dirPath, bool recursive, Callback callback, void* pUserData)
{
	ASSERT(callback);
	ASSERT(!HasDuplicate(dirPath));

	HANDLE dirHandle = CreateFile(dirPath, FILE_LIST_DIRECTORY, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE
		, NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, NULL);

	if (INVALID_HANDLE_VALUE == dirHandle)
	{
		log_warning("FSWatchDog", "Cannot open dir %s for a watch. Error message: %s", dirPath, Error::GetLastErrorMessage());

		CloseHandle(dirHandle);

		return kInvalidIndex;
	}

	const size_t kPathLen = strlen(dirPath);
	FSObserver* pObserver = (FSObserver*)malloc(offsetof(FSObserver, m_dirPath) + kPathLen + 1);
	pObserver->Init(dirHandle, dirPath, callback, pUserData, &m_pendingObserversCount, recursive);
	m_observers.EmplaceBack(m_observers.GetSize(), pObserver);

	if (!WatchDirectory(pObserver))
	{
		log_warning("FSWatchDog", "Cannot start watching on a dir %s. Error message: %s", dirPath, Error::GetLastErrorMessage());

		free(pObserver);
		CloseHandle(dirHandle);
		m_observers.RemoveLast();

		return kInvalidIndex;
	}

	++m_pendingObserversCount;

	return m_observers.Last().key;
}

//-------------------------------------------------------------------------------------------
void FSWatchDog::RemoveObserver(index_t handle)
{
	if (handle == kInvalidIndex)
	{
		return;
	}

	for (u32 i = 0; i < m_observers.GetSize(); ++i)
	{
		if (m_observers[i].key == handle)
		{
			// Observer object will be deleted on next update
			CloseObserver(m_observers[i].value);
			m_observers.RemoveFast(i);

			return;
		}
	}
}

//-------------------------------------------------------------------------------------------
void FSWatchDog::Update(float deltaTime)
{
	m_updateCounter += deltaTime;

	if (m_updateTimeDelay >= deltaTime)
	{
		m_updateCounter = 0.0f;

		if (m_pendingObserversCount)
		{
			WaitForSystemFSEvents();
		}
	}
}

//-------------------------------------------------------------------------------------------
bool FSWatchDog::HasDuplicate(const char* dirPath)
{
	(void)dirPath;

#ifdef DEBUG
	for (u32 i = 0; i < m_observers.GetSize(); ++i)
	{
		const int kCmpRes = strcmp(dirPath, m_observers[i].value->m_dirPath);
		if (!kCmpRes)
		{
			return true;
		}
		else if (kCmpRes > 0 && !m_observers[i].value->m_recursive)	// check if dirPath is a subdir of m_observers[i]->m_dirPath, if the latter is non recursive, than it's not a duplicate
		{
			if (strstr(dirPath, m_observers[i].value->m_dirPath))
			{
				return false;
			}
		}
	}
#endif

	return false;
}