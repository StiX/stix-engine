
#pragma once

#include "File.h"


class file_read_stream {
public:
						file_read_stream(const File& file);

	template<class T>
	file_read_stream&	operator&(T& receiver) const { _file.Read(receiver); return *this; }

protected:
private:
	const File&			_file;
};