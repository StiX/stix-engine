
#pragma once

#include "Array.h"

struct FSObserver;


class FSWatchDog
{
public:
	enum eEventType
	{
		eFileAdded,
		eFileRemoved,
		eFileModified,
		eFileRenamedOldName,
		eFileRenamedNewName,
	};
	
	typedef void	(*Callback)(const char* filePath, eEventType eventType, void* pUserData);
	
					FSWatchDog();
					~FSWatchDog();
	
	void			Init(float updateSecondsDelay);
	void			Shutdown();
	
	index_t			AddObserver(const char* dirPath, bool recursive, Callback callback, void* pUserData);
	void			RemoveObserver(index_t handle);
	
	void			Update(float deltaTime);
	
protected:
private:
	struct Pair
	{
		index_t		m_handle;
		FSObserver*	m_pObserver;
		
		inline		Pair(index_t handle, FSObserver* pObserver) : m_handle(handle), m_pObserver(pObserver) {}
	};
	
	Array<Pair>		m_observers;
	float			m_updateTimeDelay;
	
	bool			HasDuplicate(const char* dirPath);
};





//-------------------------------------------------------------------------------------------
inline void FSWatchDog::Init(float updateSecondsDelay)
{
	m_updateTimeDelay = updateSecondsDelay;
}