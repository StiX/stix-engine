
#include "FSWatchDogApple.h"
#include <CoreServices/CoreServices.h>
#include "Debug.h"
#include "Error.h"


struct FSObserver
{
	FSEventStreamRef		m_stream;
	FSWatchDog::Callback	m_callback;
	void*					m_pUserData;
	bool					m_recursive;
	char					m_dirPath[0];
};


//-------------------------------------------------------------------------------------------
const char* GetFileNamePtr(const char* filePath)
{
	const size_t kStrLen = strlen(filePath);
	
	for (ptrdiff_t i = kStrLen - 1; i >= 0; --i)
	{
		if (filePath[i] == '/')
		{
			return &filePath[i + 1];
		}
	}
	
	return filePath;
}

//-------------------------------------------------------------------------------------------
bool DoesFileExists(const char* filePath)
{
	return !access(filePath, F_OK);
}

//-------------------------------------------------------------------------------------------
inline void FilterFileModifiedEvent(const FSObserver* pObserver, const char* filePath)
{
	// file exists
	if (DoesFileExists(filePath))
	{
		pObserver->m_callback(filePath, FSWatchDog::eFileAdded, pObserver->m_pUserData);
	}
	else
	{
		pObserver->m_callback(filePath, FSWatchDog::eFileRemoved, pObserver->m_pUserData);
	}
}

//-------------------------------------------------------------------------------------------
void FSEventCallback(ConstFSEventStreamRef /*stream*/, void* pUserData, size_t eventsCount,
    void* eventPaths, const FSEventStreamEventFlags eventFlags[], const FSEventStreamEventId* /*eventIds*/)
{
	const char* const* paths = (const char* const*)eventPaths;
 
	FSObserver* pObserver = (FSObserver*)pUserData;
	
	bool renamed = false;

	for (u32 i = 0; i < eventsCount; ++i)
	{
		if (eventFlags[i] & kFSEventStreamEventFlagItemIsFile)
		{
			const u32 kReplacedFlag = kFSEventStreamEventFlagItemCreated | kFSEventStreamEventFlagItemRemoved | kFSEventStreamEventFlagItemModified;
			const u32 kReplacedFlag2 = kFSEventStreamEventFlagItemIsFile | kFSEventStreamEventFlagItemRemoved | kFSEventStreamEventFlagItemRenamed;
			
			if ((eventFlags[i] & (kReplacedFlag)) == kReplacedFlag)
			{
				pObserver->m_callback(paths[i], FSWatchDog::eFileModified, pObserver->m_pUserData);
			}
			else if (eventFlags[i] == (kReplacedFlag2))
			{
				pObserver->m_callback(paths[i], FSWatchDog::eFileModified, pObserver->m_pUserData);
			}
			else if (eventFlags[i] & kFSEventStreamEventFlagItemCreated)
			{
				pObserver->m_callback(paths[i], FSWatchDog::eFileAdded, pObserver->m_pUserData);
			}
			else if (eventFlags[i] & kFSEventStreamEventFlagItemRemoved)
			{
				pObserver->m_callback(paths[i], FSWatchDog::eFileRemoved, pObserver->m_pUserData);
			}
			else if (eventFlags[i] & kFSEventStreamEventFlagItemModified)
			{
				const char* fileName = GetFileNamePtr(paths[i]);
				
				// Do not send event, when a .DS_Store file has changed
				if (strcmp(fileName, ".DS_Store"))
				{
					pObserver->m_callback(paths[i], FSWatchDog::eFileModified, pObserver->m_pUserData);
				}
			}
			else if (eventFlags[i] & kFSEventStreamEventFlagItemRenamed)
			{
				// On OS X adding\removing a file raises renamed event

				if ((i + 1) < eventsCount)
				{
					if (eventFlags[i] == eventFlags[i + 1])
					{
						if (renamed)
						{
							pObserver->m_callback(paths[i], FSWatchDog::eFileRenamedNewName, pObserver->m_pUserData);
							renamed = false;
						}
						else
						{
							// this is actual renamed event, not a added\deleted file
							if (!strcmp(paths[i], paths[i + 1]))
							{
								pObserver->m_callback(paths[i], FSWatchDog::eFileRenamedOldName, pObserver->m_pUserData);
								renamed = true;
							}
							else
							{
								FilterFileModifiedEvent(pObserver, paths[i]);
							}
						}
						
						continue;
					}
				}

				// This will be executed either, when there is only one event (file added\removed) or when it checks last event.
				// Renamed flag was either set in previous condition (the file is renamed, and we have a new name) or this is a added\removed event
				if (renamed)
				{
					pObserver->m_callback(paths[i], FSWatchDog::eFileRenamedNewName, pObserver->m_pUserData);
					renamed = false;
				}
				else
				{
					FilterFileModifiedEvent(pObserver, paths[i]);
				}
			}
		}
	}
}


//-------------------------------------------------------------------------------------------
void DestroyObserver(FSObserver* pObserver)
{
	FSEventStreamStop(pObserver->m_stream);
	FSEventStreamInvalidate(pObserver->m_stream);
	FSEventStreamRelease(pObserver->m_stream);
	
	free(pObserver);
}

//-------------------------------------------------------------------------------------------
FSWatchDog::FSWatchDog()
	: m_updateTimeDelay(1.0f)
{
}

//-------------------------------------------------------------------------------------------
FSWatchDog::~FSWatchDog()
{
	assert(!m_observers.GetSize());
	
	Shutdown();
}

//-------------------------------------------------------------------------------------------
void FSWatchDog::Shutdown()
{
	for (u32 i = 0; i < m_observers.GetSize(); ++i)
	{
		DestroyObserver(m_observers[i].m_pObserver);
	}
}

//-------------------------------------------------------------------------------------------
index_t FSWatchDog::AddObserver(const char* dirPath, bool recursive, Callback callback, void* pUserData)
{
	ASSERT(callback);
	ASSERT(!HasDuplicate(dirPath));
	ASSERT(recursive, "Non recursive observers are not supported now");
	
#ifdef DEBUG
	if (!DoesFileExists(dirPath))
	{
		log_warning("FSWatchDog", "Cannot start watching on a dir %s, it doesn't exists", dirPath);
	}
#endif
	
	const size_t kPathLen = strlen(dirPath);
	FSObserver* pObserver = (FSObserver*)malloc(offsetof(FSObserver, m_dirPath) + kPathLen + 1);
	strcpy(pObserver->m_dirPath, dirPath);
	

	CFStringRef cfString = CFStringCreateWithCStringNoCopy(nullptr, pObserver->m_dirPath, kCFStringEncodingUTF8, kCFAllocatorNull);
	CFArrayRef pathToWatch = CFArrayCreate(NULL, (const void **)&cfString, 1, NULL);
	
	FSEventStreamContext context;
	memset(&context, 0, sizeof(FSEventStreamContext));
	context.info = pObserver;
	
	const CFAbsoluteTime kDelayInSec = m_updateTimeDelay;
 
	pObserver->m_stream = FSEventStreamCreate(nullptr, &FSEventCallback, &context, pathToWatch, kFSEventStreamEventIdSinceNow, kDelayInSec, kFSEventStreamCreateFlagFileEvents);
	if (!pObserver->m_stream)
	{
		log_warning("FSWatchDog", "Cannot start watching on a dir %s. Error message: %s", dirPath, Error::GetLastErrorMessage());

		free(pObserver);
		
		return kInvalidIndex;
	}
	
	FSEventStreamScheduleWithRunLoop(pObserver->m_stream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
	FSEventStreamStart(pObserver->m_stream);

	
	pObserver->m_callback = callback;
	pObserver->m_recursive = recursive;
	pObserver->m_pUserData = pUserData;
	
	const index_t kHandle = m_observers.GetSize();
	m_observers.EmplaceBack(kHandle, pObserver);
	
	return kHandle;
}

//-------------------------------------------------------------------------------------------
void FSWatchDog::RemoveObserver(index_t handle)
{
	if (handle == kInvalidIndex)
	{
		return;
	}
	
	for (u32 i = 0; i < m_observers.GetSize(); ++i)
	{
		if (m_observers[i].m_handle == handle)
		{
			DestroyObserver(m_observers[i].m_pObserver);
			m_observers.RemoveFast(i);

			return;
		}
	}
}

//-------------------------------------------------------------------------------------------
void FSWatchDog::Update(float deltaTime)
{
	// Do nothing for OS X
	(void)deltaTime;
}

//-------------------------------------------------------------------------------------------
bool FSWatchDog::HasDuplicate(const char* dirPath)
{
	(void)dirPath;
	
#ifdef DEBUG
	for (u32 i = 0; i < m_observers.GetSize(); ++i)
	{
		const int kCmpRes = strcmp(dirPath, m_observers[i].m_pObserver->m_dirPath);
		if (!kCmpRes)
		{
			return true;
		}
		else if (kCmpRes > 0 && !m_observers[i].m_pObserver->m_recursive)	// check if dirPath is a subdir of m_observers[i]->m_dirPath, if the latter is non recursive, than it's not a duplicate
		{
			if (strstr(dirPath, m_observers[i].m_pObserver->m_dirPath))
			{
				return false;
			}
		}
	}
#endif
	
	return false;
}