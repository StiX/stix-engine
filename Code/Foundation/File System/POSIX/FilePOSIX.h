
#pragma once

#include <unistd.h>
#include "Types.h"
#include "Debug.h"
#include "ClassHelpers.h"
#include "FileSystemFlagsPOSIX.h"


enum eFileOrigin
{
	FILE_ORIGIN_BEGIN	= SEEK_SET,
	FILE_ORIGIN_END		= SEEK_END,
	FILE_ORIGIN_CURRENT	= SEEK_CUR,
};


void UnmapFile(void* mappedAddress, size_t size);


class File
{
public:
				File();
				//TODO: Make flags available only in Debug builds?!
				File(int handle, u32 flags);
				File(File&& rhs);
				~File();

	bool		IsOpened() const;

	File&		operator=(File&& other);

	template	<class T>
	T			Read() const;
	u32			Read(void* pBuf, size_t bytesCount) const;		// Returns count of actual bytes read

	template	<class T>
	u32			Write(const T& data) const;
	u32			Write(const void* pData, size_t bytesCount) const;

	void		Seek(ptrdiff_t distanceToMove, eFileOrigin origin) const;
	size_t		GetCurrentPos() const;
	bool		EoF() const;
	size_t		GetSize() const;
	
	void*		MapFile(eFileMappingAccessRights accessRights) const;
	void*		MapFilePart(eFileMappingAccessRights accessRights, size_t offset, size_t length) const;

protected:
private:
	int			m_handle;
#ifdef DEBUG
	u32			m_flags;
#endif
	
	MAKE_UNCOPYABLE(File);
};





//-------------------------------------------------------------------------------------------
inline File::File()
	: m_handle(-1)
#ifdef DEBUG
	, m_flags(0)
#endif
{
}

//-------------------------------------------------------------------------------------------
inline File::File(int handle, u32 flags)
	: m_handle(handle)
#ifdef DEBUG
	, m_flags(flags)
#endif
{
}

//-------------------------------------------------------------------------------------------
inline File::File(File&& rhs)
	: m_handle(rhs.m_handle)
#ifdef DEBUG
	, m_flags(rhs.m_flags)
#endif
{
}

//-------------------------------------------------------------------------------------------
inline File::~File()
{
	if (IsOpened())
	{
		::close(m_handle);
	}
}

//-------------------------------------------------------------------------------------------
inline bool File::IsOpened() const
{
	return m_handle != -1;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline T File::Read() const
{
	T obj;
	Read(&obj, sizeof(obj));

	return obj;
}

//-------------------------------------------------------------------------------------------
inline u32 File::Read(void* pBuf, size_t bytesCount) const
{
	ASSERT(IsOpened());
	ASSERT(!(m_flags & F_NO_BUFFERING), "No buffering means, that app should do all the caching by itself and all I\\O should be aligned and with granularity");
	
	return (u32)::read(m_handle, pBuf, bytesCount);
}

//-------------------------------------------------------------------------------------------
template <class T>
inline u32 File::Write(const T& data) const
{
	return Write(&data, sizeof(T));
}

//-------------------------------------------------------------------------------------------
inline u32 File::Write(const void* pData, size_t bytesCount) const
{
	ASSERT(IsOpened());
	ASSERT((m_flags & F_NO_BUFFERING) == 0);	// I guess writing to a file with no buffering will be really slow, so test it before "go live"
	
	return (u32)::write(m_handle, pData, bytesCount);
}

//-------------------------------------------------------------------------------------------
inline void File::Seek(ptrdiff_t distanceToMove, eFileOrigin origin) const
{
	ASSERT(IsOpened());

	::lseek(m_handle, distanceToMove, origin);
}

//-------------------------------------------------------------------------------------------
inline size_t File::GetCurrentPos() const
{
	ASSERT(IsOpened());

	return (size_t)::lseek(m_handle, 0, FILE_ORIGIN_CURRENT);
}

//-------------------------------------------------------------------------------------------
inline bool File::EoF() const
{
	const size_t curPos = GetCurrentPos();
	Seek(0, FILE_ORIGIN_END);
	const size_t endPos = GetCurrentPos();
	Seek(curPos, FILE_ORIGIN_BEGIN);

	return curPos == endPos;
}

//-------------------------------------------------------------------------------------------
inline size_t File::GetSize() const
{
	const size_t curPos = GetCurrentPos();
	Seek(0, FILE_ORIGIN_END);
	const size_t endPos = GetCurrentPos();
	Seek(curPos, FILE_ORIGIN_BEGIN);

	return endPos;
}

//-------------------------------------------------------------------------------------------
inline void* File::MapFile(eFileMappingAccessRights accessRights) const
{
	return MapFilePart(accessRights, 0, GetSize());
}