
#pragma once

#include <fcntl.h>
#include <sys/mman.h>


enum eAccessRights
{
	F_ACCESS_READ				= O_RDONLY,
	F_ACCESS_WRITE				= O_WRONLY,
	F_ACCESS_ALL				= O_RDWR,
};

enum eFileMappingAccessRights
{
	F_MAPPING_READ				= PROT_READ,
	F_MAPPING_READ_WRITE		= PROT_WRITE,
};

enum eShareModel
{
	F_SHARE_THIS_PROCESS_ONLY,
	F_SHARE_READ,
	F_SHARE_WRITE,
};


enum eFlagsAndAttributes
{
	F_NO_FLAGS,
	F_NO_BUFFERING,
	F_SEQUENTIAL_ACCESS_HINT,
};


#define MAX_PATH PATH_MAX