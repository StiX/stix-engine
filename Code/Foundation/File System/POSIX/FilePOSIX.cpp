
#include "FilePOSIX.h"
#include "Log.h"
#include "Error.h"
#include "math_lib.h"
#include "VirtualPageAllocator.h"


//-------------------------------------------------------------------------------------------
void UnmapFile(void* mappedAddress, size_t size) {
	const size_t kAdjustedAddress = math::round_down_to_multiple_of_pot((size_t)mappedAddress, (size_t)VirtualPageAllocator::GetVirtualPageSize());
	size += (size_t)mappedAddress - kAdjustedAddress;

	if (munmap((void*)kAdjustedAddress, size)) {
		log_error("File System", "Cannot unmap file, error message: %s", Error::GetLastErrorMessage());
		return;
	}
}


//-------------------------------------------------------------------------------------------
File& File::operator=(File&& other)
{
	ASSERT(!IsOpened(), "Please, use move copy assignment only for non opened files");

	m_handle = other.m_handle;
#ifdef DEBUG
	m_flags = other.m_flags;
#endif

	other.m_handle = -1;

	return *this;
}

//-------------------------------------------------------------------------------------------
void* File::MapFilePart(eFileMappingAccessRights accessRights, size_t offset, size_t length) const
{
	ASSERT(IsOpened());
	ASSERT(m_flags & F_SEQUENTIAL_ACCESS_HINT);

	const size_t kAdjustedOffset = math::round_down_to_multiple_of_pot(offset, (size_t)VirtualPageAllocator::GetVirtualPageSize());
	const size_t kOffsetDiff = offset - kAdjustedOffset;
	length += kOffsetDiff;

	void* ptr = mmap(nullptr, length, accessRights, MAP_SHARED, m_handle, kAdjustedOffset);
	if (ptr == MAP_FAILED)
	{
		log_error("File System", "Cannot map file, error message: %s", Error::GetLastErrorMessage());
		return nullptr;
	}

	return (u8*)ptr + kOffsetDiff;
}