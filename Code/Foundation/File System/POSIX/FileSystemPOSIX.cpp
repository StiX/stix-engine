
#include "FileSystem.h"
#include <fcntl.h>
#include <dirent.h>
#include <cstring>
#include <sys/stat.h>
#include "File.h"
#include "Error.h"
#include "StringUtil.h"
#include "Log.h"


static char g_workDir[MAX_PATH] = "";


//-------------------------------------------------------------------------------------------
void SetWorkDir(const char* workDir)
{
	ASSERT(workDir);

	strcpy(g_workDir, workDir);
	
	if (chdir(workDir))
	{
		log_error("File System", "Cannot change working directory to %s, error message: %s", workDir, Error::GetLastErrorMessage());
	}
}

//-------------------------------------------------------------------------------------------
const char* GetWorkDir()
{
	return g_workDir;
}

//-------------------------------------------------------------------------------------------
File OpenFile(const char* path, eAccessRights accessRights, u32 flagsAndAttributes, u32 /*shareMode*/)
{
	int flags = accessRights;

#ifdef __linux__
	if (flagsAndAttributes != 0)
	{
		if (flagsAndAttributes & F_NO_BUFFERING)
		{
			flags |= O_DIRECT;
		}

		ASSERT((flagsAndAttributes & (~(F_NO_BUFFERING | F_SEQUENTIAL_ACCESS_HINT))) == 0, "There are other unknown flags");
	}
#endif

	const int fileDescr = open(path, flags);

	if (fileDescr == -1)
	{
		log_error("File System", "Cannot open file %s, error message: %s", path, Error::GetLastErrorMessage());
		return File(fileDescr, 0);
	}

#ifdef __linux__
	if (flagsAndAttributes & F_SEQUENTIAL_ACCESS_HINT)
	{
		posix_fadvise(fileDescr, 0, 0, POSIX_FADV_SEQUENTIAL);
	}
#elif defined __APPLE__
	if (flagsAndAttributes != 0)
	{
		if (flagsAndAttributes & F_NO_BUFFERING)
		{
			fcntl(fileDescr, F_NOCACHE, 1);
		}
		
		if (flagsAndAttributes & F_SEQUENTIAL_ACCESS_HINT)
		{
			fcntl(fileDescr, F_RDAHEAD, 1);
		}
		
		ASSERT((flagsAndAttributes & (~(F_NO_BUFFERING | F_SEQUENTIAL_ACCESS_HINT))) == 0, "There are other unknown flags");
	}
#endif

	return File(fileDescr, flagsAndAttributes);
}

//-------------------------------------------------------------------------------------------
File CreateFile(const char* path, eAccessRights accessRights, u32 flagsAndAttributes, u32 /*shareMode*/)
{
	int flags = accessRights;

#ifdef __linux__
	if (flagsAndAttributes != 0)
	{
		if (flagsAndAttributes & F_NO_BUFFERING)
		{
			flags |= O_DIRECT;
		}

		ASSERT((flagsAndAttributes & (~(F_NO_BUFFERING | F_SEQUENTIAL_ACCESS_HINT))) == 0, "There are other unknown flags");
	}
#endif

	const int mode = 00666;	// All have read\wright permissions;
	const int fileDescr = open(path, O_CREAT | flags, mode);

	if (fileDescr == -1)
	{
		log_error("File System", "Cannot create file %s, error message: %s", path, Error::GetLastErrorMessage());
		return File(fileDescr, 0);
	}

#ifdef __linux__
	if (flagsAndAttributes & F_SEQUENTIAL_ACCESS_HINT)
	{
		posix_fadvise(fileDescr, 0, 0, POSIX_FADV_SEQUENTIAL);
	}
#elif defined __APPLE__
	if (flagsAndAttributes != 0)
	{
		if (flagsAndAttributes & F_NO_BUFFERING)
		{
			fcntl(fileDescr, F_NOCACHE, 1);
		}
		
		if (flagsAndAttributes & F_SEQUENTIAL_ACCESS_HINT)
		{
			fcntl(fileDescr, F_RDAHEAD, 1);
		}
		
		ASSERT((flagsAndAttributes & (~(F_NO_BUFFERING | F_SEQUENTIAL_ACCESS_HINT))) == 0, "There are other unknown flags");
	}
#endif

	return File(fileDescr, flagsAndAttributes);
}

//-------------------------------------------------------------------------------------------
bool CreateDirectory(const char* path)
{
	return mkdir(path, 0777) == 0;
}

//-------------------------------------------------------------------------------------------
File FindAndOpenFile(const char* filePath, eAccessRights accessRights, u32 flagsAndAttributes, u32 shareMode)
{
	DIR *d;
	struct dirent *dir;
	
	const String kPattern(filePath);
	const String kDirPath = kPattern.ExtractDirName();
	const String kFile = kPattern.ExtractFileName();
	
	d = opendir(kDirPath.c_str());
	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			if(strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0 )
			{
				continue;
			}
			
			if (StringUtil::MatchWildcardPattern(dir->d_name, kFile.c_str()))
			{
				return OpenFile(dir->d_name, accessRights, flagsAndAttributes, shareMode);
			}
		}
		
		closedir(d);
	}
	
	return File(-1, 0);
}

//-------------------------------------------------------------------------------------------
void FindFiles(const char* pattern, Array<String>& outFiles)
{
	DIR *d;
	struct dirent *dir;

	const String kPattern(pattern);
	const String kDirPath = kPattern.ExtractDirName();
	const String kFile = kPattern.ExtractFileName();

	d = opendir(kDirPath.c_str());
	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			if(strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0 )
			{
				continue;
			}
			
			if (StringUtil::MatchWildcardPattern(dir->d_name, kFile.c_str()))
			{
				const String kFilename = kDirPath + dir->d_name;
				outFiles.Append(kFilename);
			}
		}

		closedir(d);
	}
}

//-------------------------------------------------------------------------------------------
bool FileExists(const char* filePath)
{
	return !access(filePath, F_OK);
}