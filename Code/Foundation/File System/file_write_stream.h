
#pragma once

#include "File.h"


class file_write_stream {
public:
						file_write_stream(File& file) : _file(file) {};

	template<class T>
	file_write_stream&	operator&(const T& value) { _file.Write(value); return *this; }

protected:
private:
	File&				_file;
};