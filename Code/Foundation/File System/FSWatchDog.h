
#pragma once


#ifdef __WIN__
#	include "Win/FSWatchDogWin.h"
#elif defined __OSX__
#	include "OSX/FSWatchDogApple.h"
#elif defined __PLATFORM_MOBILE__ // iOS and Android
#	include "Mobile/FSWatchDogMobile.h"
#else
#	error "Not implemented for your platform"
#endif