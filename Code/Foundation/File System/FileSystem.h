
#pragma once

#include "File.h"
#include "Array.h"
#include "Str.h"
#ifdef __WIN__
#	include "Win/FileSystemFlagsWin.h"
#else
#	include "POSIX/FileSystemFlagsPOSIX.h"
#endif

#define F_DEFAULT_FLAGS		F_SEQUENTIAL_ACCESS_HINT


void SetWorkDir(const char* workDir);
const char* GetWorkDir();

// Use F_SEQUENTIAL_ACCESS_HITN as a flag for a file that is going to be mapped, otherwise use F_NO_BUFFERING for file that is going to be read once to the buffer
File OpenFile(const char* path, eAccessRights accessRights, u32 flagsAndAttributes, u32 shareMode = F_SHARE_READ);
File CreateFile(const char* path, eAccessRights accessRights, u32 flagsAndAttributes = F_DEFAULT_FLAGS, u32 shareMode = F_SHARE_READ);

bool CreateDirectory(const char* path);

File FindAndOpenFile(const char* filePath, eAccessRights accessRights, u32 flagsAndAttributes = F_DEFAULT_FLAGS, u32 shareMode = F_SHARE_READ);
void FindFiles(const char* pattern, Array<String>& outFiles);

bool FileExists(const char* filePath);