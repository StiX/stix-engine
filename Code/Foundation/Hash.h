
#pragma once

#include "StringHash.h"


size_t FNVHash(size_t key);
u32 MurmurHash32(u32 key);
u64 MurmurHash64(u64 key);


template<class T>
struct Hasher
{
	typedef T KeyType;

	static size_t	Hash(KeyType);
	static KeyType	GetKey(const T&);
};





//-------------------------------------------------------------------------------------------
template<>
struct Hasher<u32>
{
	typedef u32 KeyType;

	static inline size_t Hash(KeyType key)
	{
		// FNV performs better than Murmur
		return FNVHash(key);
	}

	static inline KeyType GetKey(u32 object)
	{
		return object;
	}
};

//-------------------------------------------------------------------------------------------
template<>
struct Hasher<void*>
{
	typedef size_t KeyType;
	
	static inline size_t Hash(KeyType key)
	{
		// FNV performs better than Murmur
		return FNVHash(key);
	}
	
	static inline KeyType GetKey(const void* object)
	{
		return (KeyType)object;
	}
};

//-------------------------------------------------------------------------------------------
template<>
struct Hasher<StringHash>
{
	typedef u32 KeyType;

	static inline size_t Hash(KeyType key)
	{
		return (size_t)key;
	}

	static inline KeyType GetKey(const StringHash& object)
	{
		return object.hash();
	}
};





//-------------------------------------------------------------------------------------------
inline size_t FNVHash(size_t key)
{
#ifdef __ARCH_32BIT__
	const size_t kOffset = 2166136261U;
	const size_t kPrime = 16777619U;
#else
	const size_t kOffset = 14695981039346656037U;
	const size_t kPrime = 1099511628211U;
#endif

	const u8* ptr = (u8*)&key;

	size_t hash = kOffset;
	for (u32 i = 0; i < sizeof(key); ++i)
	{
		hash ^= (size_t)ptr[i];
		hash *= kPrime;
	}

	return hash;
}

//-------------------------------------------------------------------------------------------
inline u32 MurmurHash32(u32 key)
{
	key ^= key >> 16;
	key *= 0x85ebca6b;
	key ^= key >> 13;
	key *= 0xc2b2ae35;
	key ^= key >> 16;

	return key;
}

//-------------------------------------------------------------------------------------------
inline u64 MurmurHash64(u64 key)
{
	key ^= key >> 33;
	key *= 0xff51afd7ed558ccd;
	key ^= key >> 33;
	key *= 0xc4ceb9fe1a85ec53;
	key ^= key >> 33;

	return key;
}