
#pragma once

#include "Types.h"


template <typename T>
struct Vector2D;

typedef Vector2D<s32>	vec2i;
typedef Vector2D<u32>	vec2u;
typedef Vector2D<float>	vec2f;

template <typename T>
struct Vector3D;

typedef Vector3D<s32>	vec3i;
typedef Vector3D<u32>	vec3u;
typedef Vector3D<float>	vec3f;

template <typename T>
struct Vector4D;

typedef Vector4D<s32>	vec4i;
typedef Vector4D<u32>	vec4u;
typedef Vector4D<float>	vec4f;
typedef Vector4D<u8>	color4;

struct mat4;
struct mat3x4;