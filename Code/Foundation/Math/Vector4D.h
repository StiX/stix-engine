
#pragma once

#include "math_lib.h"
#include "math_types.h"


template <typename T>
struct __align_type(16) Vector4D			//16 bytes alignment will align array of vectors to cache line boundary
{
	T					x, y, z, w;

						Vector4D();
	explicit			Vector4D(T value);
						Vector4D(T x, T y, T z, T w);
	explicit			Vector4D(const Vector2D<T>& other);
	explicit			Vector4D(const Vector3D<T>& other);
	explicit			Vector4D(const Vector3D<T>& other, T w);

	Vector4D			operator+ (const Vector4D& rhs) const;		// Element by element math operations
	Vector4D&			operator+=(const Vector4D& rhs);

	Vector4D			operator- (const Vector4D& rhs) const;
	Vector4D&			operator-=(const Vector4D& rhs);

	Vector4D			operator* (const T& scalar) const;
	Vector4D			operator* (const Vector4D& rhs) const;
	Vector4D&			operator*=(const Vector4D& rhs);

	Vector4D			operator/ (const Vector4D& rhs) const;
	Vector4D&			operator/=(const Vector4D& rhs);

	bool				operator==(const Vector4D& rhs) const;

	bool				operator< (const Vector4D& rhs) const;
	bool				operator> (const Vector4D& rhs) const;

	float				GetLength() const;
	T					GetLengthSquared() const;

	void				Normalize();

	T					Dot(const Vector4D& rhs) const;

	static Vector4D		Lerp(const Vector4D& vec1, const Vector4D& vec2, float t);

	T*					GetPointer();
	const T*			GetPointer() const;

	const Vector3D<T>&	ToVector3D() const;
						operator const Vector3D<T>&() const;
};





//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>::Vector4D()
	//: x(0), y(0), z(0), w(0)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>::Vector4D(T value)
	: x(value), y(value), z(value), w(value)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>::Vector4D(T x, T y, T z, T w)
	: x(x), y(y), z(z), w(w)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>::Vector4D(const Vector2D<T>& other)
	: x(other.x), y(other.y), z(0), w(0)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>::Vector4D(const Vector3D<T>& other)
	: x(other.x), y(other.y), z(other.z), w(0)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>::Vector4D(const Vector3D<T>& other, T w)
	: x(other.x), y(other.y), z(other.z), w(w)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T> Vector4D<T>::operator+ (const Vector4D& rhs) const
{
	return Vector4D(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>& Vector4D<T>::operator+= (const Vector4D& rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	w += rhs.w;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T> Vector4D<T>::operator- (const Vector4D& rhs) const
{
	return Vector4D(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>& Vector4D<T>::operator-=(const Vector4D& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	w -= rhs.w;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T> Vector4D<T>::operator* (const T& scalar) const
{
	return Vector4D(x * scalar, y * scalar, z * scalar, w * scalar);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T> Vector4D<T>::operator* (const Vector4D& rhs) const
{
	return Vector4D(x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>& Vector4D<T>::operator*=(const Vector4D& rhs)
{
	x *= rhs.x;
	y *= rhs.y;
	z *= rhs.z;
	w *= rhs.w;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T> Vector4D<T>::operator/ (const Vector4D& rhs) const
{
	return Vector4D(x / rhs.x, y / rhs.y, z / rhs.z, w / rhs.w);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>& Vector4D<T>::operator/=(const Vector4D& rhs)
{
	x /= rhs.x;
	y /= rhs.y;
	z /= rhs.z;
	w /= rhs.w;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector4D<T>::operator==(const Vector4D& rhs) const
{
	return (x == rhs.x) && (y == rhs.y) && (z == rhs.z) && (w == rhs.w);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector4D<T>::operator< (const Vector4D& rhs) const
{
	return (x < rhs.x) && (y < rhs.y) && (z < rhs.z) && (w < rhs.w);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector4D<T>::operator> (const Vector4D& rhs) const
{
	return (x > rhs.x) && (y > rhs.y) && (z > rhs.z) && (w > rhs.w);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline float Vector4D<T>::GetLength() const
{
	return math::sqrt(GetLengthSquared());
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T Vector4D<T>::GetLengthSquared() const
{
	return Dot(*this);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline void Vector4D<T>::Normalize()
{
	const float kInvLength = 1.0f / GetLength();

	x *= kInvLength;
	y *= kInvLength;
	z *= kInvLength;
	w *= kInvLength;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T Vector4D<T>::Dot(const Vector4D& rhs) const
{
	return x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T> Vector4D<T>::Lerp(const Vector4D& vec1, const Vector4D& vec2, float t)
{
	return vec1 + (vec2 - vec1) * t;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T* Vector4D<T>::GetPointer()
{
	return &x;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline const T* Vector4D<T>::GetPointer() const
{
	return &x;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline const Vector3D<T>& Vector4D<T>::ToVector3D() const
{
	return *(reinterpret_cast<const Vector3D<T>*>(GetPointer()));
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector4D<T>::operator const Vector3D<T>&() const
{
	return ToVector3D();
}
