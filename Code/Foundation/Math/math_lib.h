
/*
	Trigonometric math functions are in RADIANS
*/

#pragma once

#include <cmath>
#include "Types.h"

#undef min
#undef max


namespace math {
	const float	pi = 3.141592653589f;
	const float epsilon = 0.00000001f;

	template <class T> T	align(T offset, T alignment);

	void*					ptr_offset(void* ptr, u32 offset);

	float					sqrt(float val);
	double					sqrt(double val);

	float					abs(float val);
	double					abs(double val);

	// Trigonometry
	float					deg_to_rad(float x);
	float					rad_to_deg(float x);

	float					sin(float radians);
	float					asin(float sin);

	float					cos(float radians);
	float					acos(float cos);

	void					sin_cos(float radians, float& sin, float& cos);

	float					tan(float radians);
	float					atan(float tangent);

	float					cotan(float radians);

	float					round(float val);
	float					floor(float val);
	float					ceil(float val);

	template <class T> T	round_to_multiple(T val, T multiplier);

	template <class T> T	round_down_to_multiple_of_pot(T value, T multiplier);

	u32						next_pow2(u32 val);				// this function will return next PoT value, even if val is PoT i.e. 512 -> 1024
	u32						nearest_pow2(u32 val);
	u32						is_power_of_2(u32 val);
	u32						count_power_of_pot_val(u32 val);	// power of PoT value, i.e. 2 -> 1, 8 -> 3

	template <class T> T	min(const T& a, const T& b);
	template <class T> T	max(const T& a, const T& b);

	template <class T> T	clamp(const T& val, const T& min, const T& max) { return val < min ? min : val > max ? max : val; }

}





//-------------------------------------------------------------------------------------------
template <class T>
__forceinline T math::align(T offset, T alignment) {
	return (offset + alignment - 1) & ~(alignment - 1);
}

//-------------------------------------------------------------------------------------------
__forceinline void* math::ptr_offset(void* ptr, u32 offset) {
	return (void*)((size_t)ptr + offset);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::sqrt(float val) {
	return ::sqrt(val);
}

//-------------------------------------------------------------------------------------------
__forceinline double math::sqrt(double val) {
	return ::sqrt(val);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::abs(float val) {
	return ::fabs(val);
}

//-------------------------------------------------------------------------------------------
__forceinline double math::abs(double val) {
	return ::fabs(val);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::deg_to_rad(float x) {
	return x * pi / 180.0f;
}

//-------------------------------------------------------------------------------------------
__forceinline float math::rad_to_deg(float x) {
	return x * 180.0f / pi;
}

//-------------------------------------------------------------------------------------------
__forceinline float math::sin(float radians) {
	return ::sin(radians);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::asin(float sin) {
	return ::asin(sin);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::cos(float radians) {
	return ::cos(radians);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::acos(float cos) {
	return ::acos(cos);
}

//-------------------------------------------------------------------------------------------
__forceinline void math::sin_cos(float radians, float& sin, float& cos) {
	sin = ::sin(radians);
	cos = ::cos(radians);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::tan(float radians) {
	return ::tan(radians);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::atan(float tangent) {
	return ::atan(tangent);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::cotan(float radians) {
	return tan(pi / 2.0f - radians);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::round(float val) {
	return floor(val + 0.5f);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::floor(float val) {
	return ::floor(val);
}

//-------------------------------------------------------------------------------------------
__forceinline float math::ceil(float val) {
	return ::ceil(val);
}

//-------------------------------------------------------------------------------------------
template <class T>
__forceinline T math::round_to_multiple(T val, T multiplier) {
	return (val + multiplier - 1) & ~(multiplier - 1);
}

//-------------------------------------------------------------------------------------------
template <class T>
__forceinline T math::round_down_to_multiple_of_pot(T value, T multiplier) {
	return value & ~(multiplier - 1);
}

//-------------------------------------------------------------------------------------------
__forceinline u32 math::next_pow2(u32 val) {
	val |= val >> 1;
	val |= val >> 2;
	val |= val >> 4;
	val |= val >> 8;
	val |= val >> 16;
	return ++val;
}

//-------------------------------------------------------------------------------------------
__forceinline u32 math::nearest_pow2(u32 val) {
	--val;
	val |= val >> 1;
	val |= val >> 2;
	val |= val >> 4;
	val |= val >> 8;
	val |= val >> 16;
	return ++val;
}

//-------------------------------------------------------------------------------------------
__forceinline u32 math::is_power_of_2(u32 val) {
	return ((val != 0) && ((val & (val - 1)) == 0));
}

//-------------------------------------------------------------------------------------------
__forceinline u32 math::count_power_of_pot_val(u32 val) {
	union FP32 {
		float	flt;
		u32		uint;
	};

	FP32 x = {(float)val};
	return (x.uint >> 23) - 127;
}

//-------------------------------------------------------------------------------------------
template <class T>
__forceinline T math::min(const T& a, const T& b) {
	return a < b ? a : b;
}

//-------------------------------------------------------------------------------------------
template <class T>
__forceinline T math::max(const T& a, const T& b) {
	return a < b ? b : a;
}