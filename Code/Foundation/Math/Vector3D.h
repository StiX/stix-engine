
#pragma once

#include "Vector2D.h"
#include "math_lib.h"
#include "math_types.h"


template <typename T>
struct /*__align(16)*/ Vector3D			//16 bytes alignment will align array of vectors to cache line boundary
{
	T				x, y, z;

					Vector3D();
	explicit		Vector3D(T value);
					Vector3D(T x, T y, T z);
	explicit		Vector3D(const Vector2D<T>& other);
					template <typename F>
	explicit		Vector3D(const Vector2D<F>& other);

	Vector3D		operator+ (const Vector3D& rhs) const;		// Element by element math operations
	Vector3D&		operator+=(const Vector3D& rhs);

	Vector3D		operator- () const;
	Vector3D		operator- (const Vector3D& rhs) const;
	Vector3D&		operator-=(const Vector3D& rhs);

	Vector3D		operator* (T scalar) const;
	Vector3D		operator* (const Vector3D& rhs) const;
	Vector3D&		operator*=(const Vector3D& rhs);

	Vector3D		operator/ (T scalar) const;
	Vector3D		operator/ (const Vector3D& rhs) const;
	Vector3D&		operator/=(const Vector3D& rhs);

	bool			operator==(const Vector3D& rhs) const;
	bool			operator!=(const Vector3D& rhs) const;

	bool			operator< (const Vector3D& rhs) const;
	bool			operator<=(const Vector3D& rhs) const;

	bool			operator> (const Vector3D& rhs) const;
	bool			operator>=(const Vector3D& rhs) const;

	float			DistanceTo(const Vector3D& rhs) const;
	T				DistanceToSquared(const Vector3D& rhs) const;

	float			GetLength() const;
	T				GetLengthSquared() const;

	Vector3D&		Normalize();
	Vector3D		Normalized() const;

	T				Dot(const Vector3D& rhs) const;
	Vector3D		Cross(const Vector3D& vec) const;

	static Vector3D	Lerp(const Vector3D& vec1, const Vector3D& vec2, float t);

	static float	GetAngle(const Vector3D& a, const Vector3D& b);

	T*				GetPointer();
	const T*		GetPointer() const;
};


template <typename T>
Vector3D<T>			operator*(float scalar, const Vector3D<T>& vec) { return vec * scalar; }




//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>::Vector3D()
	//: x(0), y(0), z(0)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>::Vector3D(T value)
	:x(value), y(value), z(value)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>::Vector3D(T x, T y, T z)
	: x(x), y(y), z(z)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>::Vector3D(const Vector2D<T>& other)
	: x(other.x), y(other.y), z(0)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
template <typename F>
__forceinline Vector3D<T>::Vector3D(const Vector2D<F>& other)
	: x((T)other.x), y((T)other.y), z(0)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::operator+ (const Vector3D& rhs) const
{
	return Vector3D(x + rhs.x, y + rhs.y, z + rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>& Vector3D<T>::operator+= (const Vector3D& rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::operator- () const
{
	return Vector3D(-x, -y, -z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::operator- (const Vector3D& rhs) const
{
	return Vector3D(x - rhs.x, y - rhs.y, z - rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>& Vector3D<T>::operator-=(const Vector3D& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::operator* (T scalar) const
{
	return Vector3D(x * scalar, y * scalar, z * scalar);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::operator* (const Vector3D& rhs) const
{
	return Vector3D(x * rhs.x, y * rhs.y, z * rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>& Vector3D<T>::operator*=(const Vector3D& rhs)
{
	x *= rhs.x;
	y *= rhs.y;
	z *= rhs.z;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::operator/ (const Vector3D& rhs) const
{
	return Vector3D(x / rhs.x, y / rhs.y, z / rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::operator/ (T scalar) const
{
	return Vector3D(x / scalar, y / scalar, z / scalar);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>& Vector3D<T>::operator/=(const Vector3D& rhs)
{
	x /= rhs.x;
	y /= rhs.y;
	z /= rhs.z;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector3D<T>::operator==(const Vector3D& rhs) const
{
	return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector3D<T>::operator!=(const Vector3D& rhs) const
{
	return !(*this == rhs);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector3D<T>::operator< (const Vector3D& rhs) const
{
	return (x < rhs.x) && (y < rhs.y) && (z < rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector3D<T>::operator<=(const Vector3D& rhs) const
{
	return (x <= rhs.x) && (y <= rhs.y) && (z <= rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector3D<T>::operator> (const Vector3D& rhs) const
{
	return (x > rhs.x) && (y > rhs.y) && (z > rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector3D<T>::operator>=(const Vector3D& rhs) const
{
	return (x >= rhs.x) && (y >= rhs.y) && (z >= rhs.z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline float Vector3D<T>::DistanceTo(const Vector3D& rhs) const
{
	return math::sqrt(DistanceToSquared(rhs));
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T Vector3D<T>::DistanceToSquared(const Vector3D& rhs) const
{
	return (rhs.x - x) * (rhs.x - x) + (rhs.y - y) * (rhs.y - y) + (rhs.z - z) * (rhs.z - z);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline float Vector3D<T>::GetLength() const
{
	return math::sqrt(GetLengthSquared());
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T Vector3D<T>::GetLengthSquared() const
{
	return Dot(*this);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T>& Vector3D<T>::Normalize()
{
	const float kInvLength = 1.0f / GetLength();

	x *= kInvLength;
	y *= kInvLength;
	z *= kInvLength;

	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::Normalized() const
{
	const float kInvLength = 1.0f / GetLength();

	return Vector3D(x * kInvLength, y * kInvLength, z * kInvLength);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T Vector3D<T>::Dot(const Vector3D& rhs) const
{
	return x * rhs.x + y * rhs.y + z * rhs.z;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::Cross(const Vector3D& vec) const
{
	return Vector3D(y * vec.z - z * vec.y,
					z * vec.x - x * vec.z,
					x * vec.y - y * vec.x);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector3D<T> Vector3D<T>::Lerp(const Vector3D& vec1, const Vector3D& vec2, float t)
{
	return vec1 + (vec2 - vec1) * t;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline float	Vector3D<T>::GetAngle(const Vector3D& a, const Vector3D& b)
{
	return math::acos(a.Dot(b) / (a.GetLength() * b.GetLength()));
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T* Vector3D<T>::GetPointer()
{
	return &x;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline const T* Vector3D<T>::GetPointer() const
{
	return &x;
}