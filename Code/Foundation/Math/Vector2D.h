
#pragma once

#include "math_lib.h"
#include "math_types.h"


template <typename T>
struct /*__align(8)*/ Vector2D			//8 bytes alignment will align array of vectors to cache line boundary
{
	T x, y;

				Vector2D();
	explicit	Vector2D(T val);
				Vector2D(T x, T y);
				template <typename F>
				Vector2D(const Vector2D<F>& other);

	Vector2D	operator+ (const Vector2D& rhs) const;		// Element by element math operations
	Vector2D&	operator+=(const Vector2D& rhs);

	Vector2D	operator- (const Vector2D& rhs) const;
	Vector2D&	operator-=(const Vector2D& rhs);

	Vector2D	operator* (const Vector2D& rhs) const;
	Vector2D	operator* (float value) const;
	Vector2D&	operator*=(const Vector2D& rhs);

	Vector2D	operator/ (const Vector2D& rhs) const;
	Vector2D	operator/ (float value) const;
	Vector2D&	operator/=(const Vector2D& rhs);

	bool		operator==(const Vector2D& rhs) const;
	bool		operator!=(const Vector2D& rhs) const;

	float		GetLength() const;
	T			GetLengthSquared() const;

	void		Normalize();

	T			Dot(const Vector2D& rhs) const;

	Vector2D	Lerp(const Vector2D& vec, float t) const;

	T*			GetPointer();
	const T*	GetPointer() const;
};





//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T>::Vector2D()
	//: x(0), y(0)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T>::Vector2D(T val)
	: x(val), y(val)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T>::Vector2D(T x, T y)
	: x(x), y(y)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
template <typename F>
__forceinline Vector2D<T>::Vector2D(const Vector2D<F>& other)
	: x((T)other.x), y((T)other.y)
{
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T> Vector2D<T>::operator+ (const Vector2D& rhs) const
{
	return Vector2D(x + rhs.x, y + rhs.y);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T>& Vector2D<T>::operator+= (const Vector2D& rhs)
{
	x += rhs.x;
	y += rhs.y;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T> Vector2D<T>::operator- (const Vector2D& rhs) const
{
	return Vector2D(x - rhs.x, y - rhs.y);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T>& Vector2D<T>::operator-=(const Vector2D& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T> Vector2D<T>::operator* (const Vector2D& rhs) const
{
	return Vector2D(x * rhs.x, y * rhs.y);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T> Vector2D<T>::operator* (float value) const
{
	return Vector2D(x * value, y * value);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T>& Vector2D<T>::operator*=(const Vector2D& rhs)
{
	x *= rhs.x;
	y *= rhs.y;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T> Vector2D<T>::operator/ (const Vector2D& rhs) const
{
	return Vector2D(x / rhs.x, y / rhs.y);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T> Vector2D<T>::operator/ (float value) const
{
	return Vector2D(x / value, y / value);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T>& Vector2D<T>::operator/=(const Vector2D& rhs)
{
	x /= rhs.x;
	y /= rhs.y;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector2D<T>::operator==(const Vector2D& rhs) const
{
	return (x == rhs.x) && (y == rhs.y);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline bool Vector2D<T>::operator!=(const Vector2D& rhs) const
{
	return !(*this == rhs);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline float Vector2D<T>::GetLength() const
{
	return math::sqrt(GetLengthSquared());
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T Vector2D<T>::GetLengthSquared() const
{
	return x * x + y * y;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline void Vector2D<T>::Normalize()
{
	const T kInvLength = 1.0f / GetLength();

	x *= kInvLength;
	y *= kInvLength;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T Vector2D<T>::Dot(const Vector2D& rhs) const
{
	return x * rhs.x + y * rhs.y;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline Vector2D<T> Vector2D<T>::Lerp(const Vector2D& vec, float t) const
{
	return Vector2D(x * (1.0f - t) + vec.x * t,
					y * (1.0f - t) + vec.y * t);
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline T* Vector2D<T>::GetPointer()
{
	return &x;
}

//-------------------------------------------------------------------------------------------
template <typename T>
__forceinline const T* Vector2D<T>::GetPointer() const
{
	return &x;
}