
#pragma once


struct SIMDlib_Generic
{
	//Matrix4x4
	static void Mat4ByMat4Mult(const float* __restrict a, const float* __restrict b, float* __restrict output);
	static void Mat4ByMat3x4Mult(const float* __restrict a, const float* __restrict b, float* __restrict output);
	static void Mat4ByVec4Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);
	static void Mat4ByVec3Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);
	static void Mat4ByVec2Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);

	//Matrix3x4
	static void Mat34ByMat34Mult(const float* __restrict a, const float* __restrict b, float* __restrict output);
	static void Mat34ByVec4Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);
	static void Mat34ByVec3Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);
};


//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib_Generic::Mat4ByMat4Mult(const float* __restrict a, const float* __restrict b, float* __restrict output)
{
	output[0] = a[0] * b[0] + a[1] * b[4] + a[2] * b[8] + a[3] * b[12];
	output[1] = a[0] * b[1] + a[1] * b[5] + a[2] * b[9] + a[3] * b[13];
	output[2] = a[0] * b[2] + a[1] * b[6] + a[2] * b[10]+ a[3] * b[14];
	output[3] = a[0] * b[3] + a[1] * b[7] + a[2] * b[11]+ a[3] * b[15];
	
	output[4] = a[4] * b[0] + a[5] * b[4] + a[6] * b[8] + a[7] * b[12];
	output[5] = a[4] * b[1] + a[5] * b[5] + a[6] * b[9] + a[7] * b[13];
	output[6] = a[4] * b[2] + a[5] * b[6] + a[6] * b[10]+ a[7] * b[14];
	output[7] = a[4] * b[3] + a[5] * b[7] + a[6] * b[11]+ a[7] * b[15];
	
	output[8] = a[8] * b[0] + a[9] * b[4] + a[10] * b[8] + a[11] * b[12];
	output[9] = a[8] * b[1] + a[9] * b[5] + a[10] * b[9] + a[11] * b[13];
	output[10]= a[8] * b[2] + a[9] * b[6] + a[10] * b[10]+ a[11] * b[14];
	output[11]= a[8] * b[3] + a[9] * b[7] + a[10] * b[11]+ a[11] * b[15];

	output[12]= a[12] * b[0] + a[13] * b[4] + a[14] * b[8] + a[15] * b[12];
	output[13]= a[12] * b[1] + a[13] * b[5] + a[14] * b[9] + a[15] * b[13];
	output[14]= a[12] * b[2] + a[13] * b[6] + a[14] * b[10]+ a[15] * b[14];
	output[15]= a[12] * b[3] + a[13] * b[7] + a[14] * b[11]+ a[15] * b[15];
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib_Generic::Mat4ByMat3x4Mult(const float* __restrict a, const float* __restrict b, float* __restrict output) {
	output[0] = a[0] * b[0] + a[1] * b[4] + a[2] * b[8];
	output[1] = a[0] * b[1] + a[1] * b[5] + a[2] * b[9];
	output[2] = a[0] * b[2] + a[1] * b[6] + a[2] * b[10];
	output[3] = a[0] * b[3] + a[1] * b[7] + a[2] * b[11] + a[3];

	output[4] = a[4] * b[0] + a[5] * b[4] + a[6] * b[8];
	output[5] = a[4] * b[1] + a[5] * b[5] + a[6] * b[9];
	output[6] = a[4] * b[2] + a[5] * b[6] + a[6] * b[10];
	output[7] = a[4] * b[3] + a[5] * b[7] + a[6] * b[11] + a[7];

	output[8] = a[8] * b[0] + a[9] * b[4] + a[10] * b[8];
	output[9] = a[8] * b[1] + a[9] * b[5] + a[10] * b[9];
	output[10] = a[8] * b[2] + a[9] * b[6] + a[10] * b[10];
	output[11] = a[8] * b[3] + a[9] * b[7] + a[10] * b[11] + a[11];

	output[12] = a[12] * b[0] + a[13] * b[4] + a[14] * b[8];
	output[13] = a[12] * b[1] + a[13] * b[5] + a[14] * b[9];
	output[14] = a[12] * b[2] + a[13] * b[6] + a[14] * b[10];
	output[15] = a[12] * b[3] + a[13] * b[7] + a[14] * b[11] + a[15];
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib_Generic::Mat4ByVec4Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	output[0] = mat[0] * vec[0] + mat[1] * vec[1] + mat[2] * vec[2] + mat[3] * vec[3];
	output[1] = mat[4] * vec[0] + mat[5] * vec[1] + mat[6] * vec[2] + mat[7] * vec[3];
	output[2] = mat[8] * vec[0] + mat[9] * vec[1] + mat[10]* vec[2] + mat[11]* vec[3];
	output[3] = mat[12]* vec[0] + mat[13]* vec[1] + mat[14]* vec[2] + mat[15]* vec[3];
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib_Generic::Mat4ByVec3Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	output[0] = mat[0] * vec[0] + mat[1] * vec[1] + mat[2] * vec[2] + mat[3];
	output[1] = mat[4] * vec[0] + mat[5] * vec[1] + mat[6] * vec[2] + mat[7];
	output[2] = mat[8] * vec[0] + mat[9] * vec[1] + mat[10] * vec[2] + mat[11];
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib_Generic::Mat4ByVec2Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	output[0] = mat[0] * vec[0] + mat[1] * vec[1] + mat[2];
	output[1] = mat[4] * vec[0] + mat[5] * vec[1] + mat[6];
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib_Generic::Mat34ByMat34Mult(const float* __restrict a, const float* __restrict b, float* __restrict output)
{
	output[0]	= a[0] * b[0] + a[1] * b[4] + a[2] * b[8];
	output[1]	= a[0] * b[1] + a[1] * b[5] + a[2] * b[9];
	output[2]	= a[0] * b[2] + a[1] * b[6] + a[2] * b[10];
	output[3]	= a[0] * b[3] + a[1] * b[7] + a[2] * b[11] + a[3];
	
	output[4]	= a[4] * b[0] + a[5] * b[4] + a[6] * b[8];
	output[5]	= a[4] * b[1] + a[5] * b[5] + a[6] * b[9];
	output[6]	= a[4] * b[2] + a[5] * b[6] + a[6] * b[10];
	output[7]	= a[4] * b[3] + a[5] * b[7] + a[6] * b[11] + a[7];
	
	output[8]	= a[8] * b[0] + a[9] * b[4] + a[10] * b[8];
	output[9]	= a[8] * b[1] + a[9] * b[5] + a[10] * b[9];
	output[10]	= a[8] * b[2] + a[9] * b[6] + a[10] * b[10];
	output[11]	= a[8] * b[3] + a[9] * b[7] + a[10] * b[11] + a[11];
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib_Generic::Mat34ByVec4Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	output[0] = mat[0] * vec[0] + mat[1] * vec[1] + mat[2] * vec[2] + mat[3] * vec[3];
	output[1] = mat[4] * vec[0] + mat[5] * vec[1] + mat[6] * vec[2] + mat[7] * vec[3];
	output[2] = mat[8] * vec[0] + mat[9] * vec[1] + mat[10] * vec[2] + mat[11] * vec[3];
	output[3] = vec[3];
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib_Generic::Mat34ByVec3Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	output[0] = mat[0] * vec[0] + mat[1] * vec[1] + mat[2] * vec[2];
	output[1] = mat[4] * vec[0] + mat[5] * vec[1] + mat[6] * vec[2];
	output[2] = mat[8] * vec[0] + mat[9] * vec[1] + mat[10] * vec[2];
}