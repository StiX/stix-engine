
#pragma once

#include "SIMDlib_Generic.h"


struct SIMDlib
{
	//Matrix4x4
	static void Mat4ByMat4Mult(const float* __restrict a, const float* __restrict b, float* __restrict output);
	static void Mat4ByMat3x4Mult(const float* __restrict a, const float* __restrict b, float* __restrict output);
	static void Mat4ByVec4Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);
	static void Mat4ByVec3Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);
	static void Mat4ByVec2Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);

	//Matrix3x4
	static void Mat34ByMat34Mult(const float* __restrict a, const float* __restrict b, float* __restrict output);
	static void Mat34ByVec4Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);
	static void Mat34ByVec3Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output);
};


//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib::Mat4ByMat4Mult(const float* __restrict a, const float* __restrict b, float* __restrict output)
{
	SIMDlib_Generic::Mat4ByMat4Mult(a, b, output);
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib::Mat4ByMat3x4Mult(const float* __restrict a, const float* __restrict b, float* __restrict output) {
	SIMDlib_Generic::Mat4ByMat3x4Mult(a, b, output);
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib::Mat4ByVec4Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	SIMDlib_Generic::Mat4ByVec4Mult(mat, vec, output);
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib::Mat4ByVec3Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	SIMDlib_Generic::Mat4ByVec3Mult(mat, vec, output);
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib::Mat4ByVec2Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	SIMDlib_Generic::Mat4ByVec2Mult(mat, vec, output);
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib::Mat34ByMat34Mult(const float* __restrict a, const float* __restrict b, float* __restrict output)
{
	SIMDlib_Generic::Mat34ByMat34Mult(a, b, output);
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib::Mat34ByVec4Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	SIMDlib_Generic::Mat34ByVec4Mult(mat, vec, output);
}

//-------------------------------------------------------------------------------------------
__forceinline void SIMDlib::Mat34ByVec3Mult(const float* __restrict mat, const float* __restrict vec, float* __restrict output)
{
	SIMDlib_Generic::Mat34ByVec3Mult(mat, vec, output);
}