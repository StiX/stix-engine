
#pragma once

#include "mat4.h"


struct Quat
{
	float			x, y, z, w;

					Quat();
					Quat(const Quat& quat);
	explicit		Quat(const vec4f& vec);
					Quat(const vec3f& axis, float rad);
					Quat(float x, float y, float z, float w);
					Quat(const vec3f& vec1, const vec3f& vec2);

	Quat			operator*(const Quat& q2) const;
	Quat			operator*(float scaleFactor) const;

	Quat			operator-(const Quat& q2) const;
	Quat			operator+(const Quat& q2) const;

	static Quat		Slerp(const Quat& q1, const Quat& q2, float t);
	
	vec3f			RotateVec(const vec3f& vec) const;

	float			Dot(const Quat& quat) const;

	Quat			GetConjugate() const;

	void			Normalize();

	static float	GetAngleBetweenQuats(const Quat& q1, const Quat& q2);

	mat4			CreateMatrix() const;
	mat3x4			create_matrix3x4() const;

	void			ToEulerAngles(vec3f& angles);

	void			ComputeW();

	float*			Pointer();
	const float*	Pointer() const;
};



//-------------------------------------------------------------------------------------------
__forceinline Quat::Quat()
	: x(0.0f), y(0.0f), z(0.0f), w(0.0f)
{
}

//-------------------------------------------------------------------------------------------
__forceinline Quat::Quat(const vec4f& vec)
	: x(vec.x), y(vec.y), z(vec.z), w(vec.w)
{
}

//-------------------------------------------------------------------------------------------
__forceinline Quat::Quat(const Quat& quat)
	: x(quat.x), y(quat.y), z(quat.z), w(quat.w)
{

}

//-------------------------------------------------------------------------------------------
__forceinline Quat::Quat(const vec3f& axis, float rad)
{
	float sin, cos;
	math::sin_cos(rad * 0.5f, sin, cos);

	x = axis.x * sin;
	y = axis.y * sin;
	z = axis.z * sin;
	w = cos;
}

//-------------------------------------------------------------------------------------------
__forceinline Quat::Quat(float x, float y, float z, float w)
	: x(x), y(y), z(z), w(w)
{
}

//-------------------------------------------------------------------------------------------
__forceinline Quat::Quat(const vec3f& vec1, const vec3f& vec2)
{
	if (vec1 == vec2)
	{
		*this = Quat(vec3f(1.0f, 0.0f, 0.0f), 180.0f);
	}

	const vec3f c = vec1.Cross(vec2);
	const float d = vec1.Dot(vec2);
	const float s = math::sqrt((1 + d) * 2);
	const float inv = 1.0f / s;

	x = c.x * inv;
	y = c.y * inv;
	z = c.z * inv;
	w = s * 0.5f;
}

//-------------------------------------------------------------------------------------------
__forceinline Quat Quat::operator*(const Quat& q2) const
{
	return Quat(w * q2.x + x * q2.w + y * q2.z - z * q2.y,
				w * q2.y + y * q2.w + z * q2.x - x * q2.z,
				w * q2.z + z * q2.w + x * q2.y - y * q2.x,
				w * q2.w - x * q2.x - y * q2.y - z * q2.z);
}

//-------------------------------------------------------------------------------------------
__forceinline Quat Quat::operator*(float scaleFactor) const
{
	return Quat(x * scaleFactor, y * scaleFactor, z * scaleFactor, w * scaleFactor);
}

//-------------------------------------------------------------------------------------------
__forceinline Quat Quat::operator-(const Quat& q2) const
{
	return Quat(x - q2.x, y - q2.y, z - q2.z, w - q2.w);
}

//-------------------------------------------------------------------------------------------
__forceinline Quat Quat::operator+(const Quat& q2) const
{
	return Quat(x + q2.x, y + q2.y, z + q2.z, w + q2.w);
}

//-------------------------------------------------------------------------------------------
__forceinline Quat Quat::Slerp(const Quat& q1, const Quat& q2, float t)
{
	const float TRESHOLD = 0.9995f;
	
	Quat temp = q1;

	float dot = q1.Dot(q2);
	if (dot < 0)
	{
		dot *= -1.0f;	// flip sign for shortest distance
		temp = temp * -1.0f;
	}

	if (dot > TRESHOLD)
	{
		return Quat(vec4f::Lerp(*((vec4f*)&temp), *((vec4f*)&q2), t));
	}

	float degrees = math::acos(dot) * t;	//angle between q1 and res quaternions

	Quat res = q2 - temp * dot;
	res.Normalize();

	float sin = 0, cos = 0;
	math::sin_cos(degrees, sin, cos);

	res = temp * cos + res * sin;
	//res.Normalize();

	return res;
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f Quat::RotateVec(const vec3f& vec) const
{
	const vec3f v(x, y, z);
	const vec3f res = 2.0f * v.Dot(vec) * v + (w * w - v.Dot(v)) * vec + 2.0f * w * v.Cross(vec);
	return res;
}

//-------------------------------------------------------------------------------------------
__forceinline float Quat::Dot(const Quat& quat) const
{
	return x * quat.x + y * quat.y + z * quat.z + w * quat.w;
}

//-------------------------------------------------------------------------------------------
__forceinline Quat Quat::GetConjugate() const
{
	return Quat(-x, -y, -z, w);
}

//-------------------------------------------------------------------------------------------
__forceinline void Quat::Normalize()
{
	const float kInvLength = 1.0f / math::sqrt(Dot(*this));
	
	x *= kInvLength;
	y *= kInvLength;
	z *= kInvLength;
	w *= kInvLength;
}

//-------------------------------------------------------------------------------------------
__forceinline float Quat::GetAngleBetweenQuats(const Quat& q1, const Quat& q2)
{
	return 2.0f * math::acos(math::abs(q1.Dot(q2)));
}

//-------------------------------------------------------------------------------------------
__forceinline mat4 Quat::CreateMatrix() const
{
	mat4 mat;
	mat.m_1_1 = 1.0f - 2.0f * (y * y + z * z);	mat.m_1_2 = 2.0f * (x * y - z * w);			mat.m_1_3 = 2.0f * (x * z + y * w);			mat.m_1_4 = 0.0f;
	mat.m_2_1 = 2.0f * (x * y + z * w);			mat.m_2_2 = 1.0f - 2.0f * (x * x + z * z);	mat.m_2_3 = 2.0f * (y * z - x * w);			mat.m_2_4 = 0.0f;
	mat.m_3_1 = 2.0f * (x * z - y * w);			mat.m_3_2 = 2.0f * (z * y + x * w);			mat.m_3_3 = 1.0f - 2.0f * (x * x + y * y);	mat.m_3_4 = 0.0f;
	mat.m_4_1 = 0.0f;							mat.m_4_2 = 0.0f;							mat.m_4_3 = 0.0f;							mat.m_4_4 = 1.0f;
	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline mat3x4 Quat::create_matrix3x4() const {
	mat3x4 mat;
	mat.m_1_1 = 1.0f - 2.0f * (y * y + z * z);	mat.m_1_2 = 2.0f * (x * y - z * w);			mat.m_1_3 = 2.0f * (x * z + y * w);			mat.m_1_4 = 0.0f;
	mat.m_2_1 = 2.0f * (x * y + z * w);			mat.m_2_2 = 1.0f - 2.0f * (x * x + z * z);	mat.m_2_3 = 2.0f * (y * z - x * w);			mat.m_2_4 = 0.0f;
	mat.m_3_1 = 2.0f * (x * z - y * w);			mat.m_3_2 = 2.0f * (z * y + x * w);			mat.m_3_3 = 1.0f - 2.0f * (x * x + y * y);	mat.m_3_4 = 0.0f;
	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline void Quat::ToEulerAngles(vec3f& angles)
{
	angles.x = math::atan(2.0f * (w * x + y * z) / (w * w - x * x - y * y + z * z));
	angles.y = -math::asin(2.0f * (x * z - w * y));
	angles.z = math::atan(2.0f * (w * z + x * y) / (w * w + x * x - y * y - z * z));
}

//-------------------------------------------------------------------------------------------
__forceinline void Quat::ComputeW()
{
	w = 1.0f - x * x - y * y - z * z;
	if (w < 0.0f)
	{
		w = 0.0f;
	}
	else
	{
		w = -math::sqrt(w);
	}
}

//-------------------------------------------------------------------------------------------
__forceinline float* Quat::Pointer()
{
	return (float*)this;
}

//-------------------------------------------------------------------------------------------
__forceinline const float* Quat::Pointer() const
{
	return (float*)this;
}