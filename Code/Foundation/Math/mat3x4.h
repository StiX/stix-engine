
#pragma once

#include "Debug.h"
#include "SIMDlib.h"
#include "math_lib.h"
#include "Vector3D.h"


// A row major matrix - i.e. translation is a 3rd, 7th and 11th components of the matrix
// mat3x4 was designed to represent affine transformations
struct __align_type(16) mat3x4 {
	union {
#if defined(DEBUG) && defined(_MSC_VER)		// makes easier to view matrices in VS debugger
		float		m[3][4];
#endif
		float		mat[12];
		
		struct {
			float	m_1_1, m_1_2, m_1_3, m_1_4;
			float	m_2_1, m_2_2, m_2_3, m_2_4;
			float	m_3_1, m_3_2, m_3_3, m_3_4;
		};
	};

	static mat3x4	identity;
	static mat3x4	zero;
	
	static mat3x4	create_rotation(float radians, const vec3f& normalized_axis_vector);
	static mat3x4	create_translation(const vec3f& position);
	static mat3x4	create_scale(const vec3f& scale);

	mat3x4			operator* (const mat3x4& rhs) const;
	mat3x4&			operator*=(const mat3x4& rhs);
	vec4f			operator* (const vec4f& rhs) const;
	vec3f			operator* (const vec3f& rhs) const;

	void			translate_to(const vec3f& vec);
	void			translate_to(float x, float y, float z);
	void			translate_by(const vec3f& vec);
	void			translate_by(float x, float y, float z);
	
	vec3f			side_vec() const;
	vec3f			up_vec() const;
	vec3f			direction_vec() const;
	vec3f			translation() const;

	mat3x4			inverted_orthogonal() const;	// Applicable only for orthogonal transformations only - translation, rotation
	mat3x4			inverted_affine() const;
	mat3x4			inverted() const;				// Inverts any transformations

	float*			pointer();
	const float*	pointer() const;
};





//-------------------------------------------------------------------------------------------
__forceinline mat3x4 mat3x4::create_rotation(float radians, const vec3f& normalized_axis_vector) {
	float s, c;
	math::sin_cos(radians, s, c);

	const float xx = normalized_axis_vector.x * normalized_axis_vector.x;
	const float yy = normalized_axis_vector.y * normalized_axis_vector.y;
	const float zz = normalized_axis_vector.z * normalized_axis_vector.z;

	const float xy = normalized_axis_vector.x * normalized_axis_vector.y;
	const float xz = normalized_axis_vector.x * normalized_axis_vector.z;
	const float yz = normalized_axis_vector.y * normalized_axis_vector.z;

	const float sx = s * normalized_axis_vector.x;
	const float sy = s * normalized_axis_vector.y;
	const float sz = s * normalized_axis_vector.z;

	mat3x4 res;
	res.m_1_1 = (1.0f - c) * xx + c;	res.m_1_2 = (1.0f - c) * xy + sz;	res.m_1_3 = (1.0f - c) * xz - sy;	res.m_1_4 = 0.0f;
	res.m_2_1 = (1.0f - c) * xy - sz;	res.m_2_2 = (1.0f - c) * yy + c;	res.m_2_3 = (1.0f - c) * yz + sx;	res.m_2_4 = 0.0f;
	res.m_3_1 = (1.0f - c) * xz + sy;	res.m_3_2 = (1.0f - c) * yz - sx;	res.m_3_3 = (1.0f - c) * zz + c;	res.m_3_4 = 0.0f;

	return res;
}

//-------------------------------------------------------------------------------------------
__forceinline mat3x4 mat3x4::create_translation(const vec3f& position) {
	mat3x4 mat;
	mat.m_1_1 = 1.0f; mat.m_1_2 = 0.0f; mat.m_1_3 = 0.0f; mat.m_1_4 = position.x;
	mat.m_2_1 = 0.0f; mat.m_2_2 = 1.0f; mat.m_2_3 = 0.0f; mat.m_2_4 = position.y;
	mat.m_3_1 = 0.0f; mat.m_3_2 = 0.0f; mat.m_3_3 = 1.0f; mat.m_3_4 = position.z;

	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline mat3x4 mat3x4::create_scale(const vec3f& scale) {
	mat3x4 mat;
	mat.m_1_1 = scale.x;	mat.m_1_2 = 0.0f;		mat.m_1_3 = 0.0f;		mat.m_1_4 = 0.0f;
	mat.m_2_1 = 0.0f;		mat.m_2_2 = scale.y;	mat.m_2_3 = 0.0f;		mat.m_2_4 = 0.0f;
	mat.m_3_1 = 0.0f;		mat.m_3_2 = 0.0f;		mat.m_3_3 = scale.z;	mat.m_3_4 = 0.0f;

	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline mat3x4 mat3x4::operator* (const mat3x4& rhs) const {
	mat3x4 result;
	SIMDlib::Mat34ByMat34Mult(pointer(), rhs.pointer(), result.pointer());
	return result;
}

//-------------------------------------------------------------------------------------------
__forceinline mat3x4& mat3x4::operator*=(const mat3x4& rhs) {
	*this = *this * rhs;
	return *this;
}

//-------------------------------------------------------------------------------------------
__forceinline vec4f mat3x4::operator* (const vec4f& rhs) const {
	vec4f result;
	SIMDlib::Mat34ByVec4Mult(pointer(), rhs.GetPointer(), result.GetPointer());
	return result;
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat3x4::operator* (const vec3f& rhs) const {
	vec3f result;
	SIMDlib::Mat34ByVec3Mult(pointer(), rhs.GetPointer(), result.GetPointer());
	return result;
}

//-------------------------------------------------------------------------------------------
__forceinline void mat3x4::translate_to(const vec3f& vec) {
	m_1_4 = vec.x;
	m_2_4 = vec.y;
	m_3_4 = vec.z;
}

//-------------------------------------------------------------------------------------------
__forceinline void mat3x4::translate_to(float x, float y, float z) {
	m_1_4 = x;
	m_2_4 = y;
	m_3_4 = z;
}

//-------------------------------------------------------------------------------------------
__forceinline void mat3x4::translate_by(const vec3f& vec) {
	m_1_4 += vec.x;
	m_2_4 += vec.y;
	m_3_4 += vec.z;
}

//-------------------------------------------------------------------------------------------
__forceinline void mat3x4::translate_by(float x, float y, float z) {
	m_1_4 += x;
	m_2_4 += y;
	m_3_4 += z;
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat3x4::side_vec() const {
	return vec3f(m_1_1, m_2_1, m_3_1);
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat3x4::up_vec() const {
	return vec3f(m_1_2, m_2_2, m_3_2);
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat3x4::direction_vec() const {
	return vec3f(m_1_3, m_2_3, m_3_3);
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat3x4::translation() const {
	return vec3f(m_1_4, m_2_4, m_3_4);
}

//-------------------------------------------------------------------------------------------
__forceinline mat3x4 mat3x4::inverted_orthogonal() const {
	//9 muls
	mat3x4 res;
	res.m_1_1 = m_1_1; res.m_1_2 = m_2_1; res.m_1_3 = m_3_1;
	res.m_2_1 = m_1_2; res.m_2_2 = m_2_2; res.m_2_3 = m_3_2;
	res.m_3_1 = m_1_3; res.m_3_2 = m_2_3; res.m_3_3 = m_3_3;

	const float x = -m_1_4;
	const float y = -m_2_4;
	const float z = -m_3_4;

	res.m_1_4 = res.m_1_1 * x + res.m_1_2 * y + res.m_1_3 * z;
	res.m_2_4 = res.m_2_1 * x + res.m_2_2 * y + res.m_2_3 * z;
	res.m_3_4 = res.m_3_1 * x + res.m_3_2 * y + res.m_3_3 * z;

	return res;
}

//-------------------------------------------------------------------------------------------
__forceinline mat3x4 mat3x4::inverted_affine() const {
	//27 muls, 3 div
	const float x_len = 1.0f / side_vec().GetLengthSquared();
	const float y_len = 1.0f / up_vec().GetLengthSquared();
	const float z_len = 1.0f / direction_vec().GetLengthSquared();

	mat3x4 res;
	res.m_1_1 = m_1_1 * x_len; res.m_1_2 = m_2_1 * x_len; res.m_1_3 = m_3_1 * x_len;
	res.m_2_1 = m_1_2 * y_len; res.m_2_2 = m_2_2 * y_len; res.m_2_3 = m_3_2 * y_len;
	res.m_3_1 = m_1_3 * z_len; res.m_3_2 = m_2_3 * z_len; res.m_3_3 = m_3_3 * z_len;

	const float x = -m_1_4;
	const float y = -m_2_4;
	const float z = -m_3_4;

	res.m_1_4 = res.m_1_1 * x + res.m_1_2 * y + res.m_1_3 * z;
	res.m_2_4 = res.m_2_1 * x + res.m_2_2 * y + res.m_2_3 * z;
	res.m_3_4 = res.m_3_1 * x + res.m_3_2 * y + res.m_3_3 * z;

	return res;
}

//-------------------------------------------------------------------------------------------
__forceinline mat3x4 mat3x4::inverted() const {
	// 39 muls, 9 subs, 8 adds, 1 div
	// http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
	const float a = m_2_2 * m_3_3 - m_2_3 * m_3_2;
	const float b = m_1_3 * m_3_2 - m_1_2 * m_3_3;
	const float c = m_1_2 * m_2_3 - m_1_3 * m_2_2;

	const float det = m_1_1 * a + m_2_1 * b + m_3_1 * c;
	ASSERT(math::abs(det) >= math::epsilon);
	const float inv_det = 1.0f / det;
	
	// invert 3x3 matrix
	mat3x4 res;
	res.m_1_1 = inv_det * a;
	res.m_1_2 = inv_det * b;
	res.m_1_3 = inv_det * c;

	res.m_2_1 = inv_det * (m_2_3 * m_3_1 - m_2_1 * m_3_3);
	res.m_2_2 = inv_det * (m_1_1 * m_3_3 - m_1_3 * m_3_1);
	res.m_2_3 = inv_det * (m_1_3 * m_2_1 - m_1_1 * m_2_3);

	res.m_3_1 = inv_det * (m_2_1 * m_3_2 - m_2_2 * m_3_1);
	res.m_3_2 = inv_det * (m_1_2 * m_3_1 - m_1_1 * m_3_2);
	res.m_3_3 = inv_det * (m_1_1 * m_2_2 - m_1_2 * m_2_1);

	// multiply translation by inverse 3x3 mat and negate it
	// http://stackoverflow.com/questions/2624422/efficient-4x4-matrix-inverse-affine-transform/2625420#2625420
	res.m_1_4 = -(m_1_4 * res.m_1_1 + m_2_4 * res.m_1_2 + m_3_4 * res.m_1_3);
	res.m_2_4 = -(m_1_4 * res.m_2_1 + m_2_4 * res.m_2_2 + m_3_4 * res.m_2_3);
	res.m_3_4 = -(m_1_4 * res.m_3_1 + m_2_4 * res.m_3_2 + m_3_4 * res.m_3_3);

	return res;
}

//-------------------------------------------------------------------------------------------
__forceinline float* mat3x4::pointer() {
	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline const float* mat3x4::pointer() const {
	return mat;
}