
#pragma once

#include "Vector4D.h"
#include "SIMDlib.h"
#include "math_lib.h"
#include "mat3x4.h"


// A row major matrix - i.e. translation is a 3rd, 7th and 11th components of the matrix
// It is designed to be used as a projection matrix, for affine transformation you have to used mat3x4
struct __align_type(64) mat4 {
	union {
#if defined(DEBUG) && defined(_MSC_VER)		// makes easier to view matrices in VS debugger
		float		m[4][4];
#endif
		float		mat[16];

		struct {
			float	m_1_1, m_1_2, m_1_3, m_1_4;
			float	m_2_1, m_2_2, m_2_3, m_2_4;
			float	m_3_1, m_3_2, m_3_3, m_3_4;
			float	m_4_1, m_4_2, m_4_3, m_4_4;
		};
	};

	static mat4		identity;
	static mat4		zero;

	mat4			operator* (const mat4& rhs) const;
	mat4			operator* (const mat3x4& rhs) const;
	mat4&			operator*=(const mat4& rhs);
	vec4f			operator* (const vec4f& rhs) const;
	vec3f			operator* (const vec3f& rhs) const;

	vec3f			side_vec() const;
	vec3f			up_vec() const;
	vec3f			direction_vec() const;
	vec3f			translation() const;

	mat4			transposed() const;

	static mat4		ortho(float left, float right, float bottom, float top, float z_near, float z_far);
	static mat4		frustum(float left, float right, float bottom, float top, float z_near, float z_far);

	static mat4		look_at(const vec3f& camera_pos, const vec3f& target_pos, const vec3f& camera_up_vector);

	float*			pointer();
	const float*	pointer() const;
};





//-------------------------------------------------------------------------------------------
__forceinline mat4 mat4::operator* (const mat4& rhs) const {
	mat4 result;
	SIMDlib::Mat4ByMat4Mult(this->pointer(), rhs.pointer(), result.pointer());
	return result;
}

//-------------------------------------------------------------------------------------------
__forceinline mat4 mat4::operator* (const mat3x4& rhs) const {
	mat4 result;
	SIMDlib::Mat4ByMat3x4Mult(pointer(), rhs.pointer(), result.pointer());
	return result;
}

//-------------------------------------------------------------------------------------------
__forceinline mat4& mat4::operator*=(const mat4& rhs) {
	*this = *this * rhs;
	return *this;
}

//-------------------------------------------------------------------------------------------
__forceinline vec4f mat4::operator* (const vec4f& rhs) const {
	vec4f result;
	SIMDlib::Mat4ByVec4Mult(this->pointer(), rhs.GetPointer(), result.GetPointer());
	return result;
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat4::operator* (const vec3f& rhs) const {
	vec3f result;
	SIMDlib::Mat4ByVec3Mult(this->pointer(), rhs.GetPointer(), result.GetPointer());
	return result;
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat4::side_vec() const {
	return vec3f(m_1_1, m_2_1, m_3_1);
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat4::up_vec() const {
	return vec3f(m_1_2, m_2_2, m_3_2);
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat4::direction_vec() const {
	return vec3f(m_1_3, m_2_3, m_3_3);
}

//-------------------------------------------------------------------------------------------
__forceinline vec3f mat4::translation() const {
	return vec3f(m_1_4, m_2_4, m_3_4);
}

//-------------------------------------------------------------------------------------------
__forceinline mat4 mat4::transposed() const {
	mat4 result;

	result.m_1_1 = m_1_1;	result.m_1_2 = m_2_1;	result.m_1_3 = m_3_1;	result.m_1_4 = m_4_1;
	result.m_2_1 = m_1_2;	result.m_2_2 = m_2_2;	result.m_2_3 = m_3_2;	result.m_2_4 = m_4_2;
	result.m_3_1 = m_1_3;	result.m_3_2 = m_2_3;	result.m_3_3 = m_3_3;	result.m_3_4 = m_4_3;
	result.m_4_1 = m_1_4;	result.m_4_2 = m_2_4;	result.m_4_3 = m_3_4;	result.m_4_4 = m_4_4;

	return result;
}

//-------------------------------------------------------------------------------------------
__forceinline mat4 mat4::ortho(float left, float right, float bottom, float top, float z_near, float z_far) {
	const float a = 2.0f / (right - left);
	const float b = 2.0f / (top - bottom);
	const float c = 2.0f / (z_near - z_far);

	const float tx = (right + left) / (left - right);
	const float ty = (top + bottom) / (bottom - top);
	const float tz = (z_far + z_near) / (z_near - z_far);

	mat4 mat;
	mat.m_1_1 = a;    mat.m_1_2 = 0.0f; mat.m_1_3 = 0.0f; mat.m_1_4 = tx;
	mat.m_2_1 = 0.0f; mat.m_2_2 = b;    mat.m_2_3 = 0.0f; mat.m_2_4 = ty;
	mat.m_3_1 = 0.0f; mat.m_3_2 = 0.0f; mat.m_3_3 = c;    mat.m_3_4 = tz;
	mat.m_4_1 = 0.0f; mat.m_4_2 = 0.0f; mat.m_4_3 = 0.0f; mat.m_4_4 = 1.0f;

	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline mat4 mat4::frustum(float left, float right, float bottom, float top, float z_near, float z_far) {
	const float a = 2 * z_near / (right - left);
	const float b = 2 * z_near / (top - bottom);
	const float c = (right + left) / (right - left);
	const float d = (top + bottom) / (top - bottom);
	const float e = (z_far + z_near) / (z_near - z_far);
	const float f = 2 * z_far * z_near / (z_near - z_far);

	mat4 mat;
	mat.m_1_1 = a;    mat.m_1_2 = 0.0f; mat.m_1_3 = c;		mat.m_1_4 = 0.0f;
	mat.m_2_1 = 0.0f; mat.m_2_2 = b;    mat.m_2_3 = d;		mat.m_2_4 = 0.0f;
	mat.m_3_1 = 0.0f; mat.m_3_2 = 0.0f; mat.m_3_3 = e;		mat.m_3_4 = f;
	mat.m_4_1 = 0.0f; mat.m_4_2 = 0.0f; mat.m_4_3 = -1.0f;	mat.m_4_4 = 0.0f;

	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline mat4 mat4::look_at(const vec3f& camera_pos, const vec3f& target_pos, const vec3f& camera_up_vector) {
	vec3f zAxis = camera_pos - target_pos;
	zAxis.Normalize();

	vec3f xAxis = camera_up_vector.Cross(zAxis);
	xAxis.Normalize();

	vec3f yAxis = zAxis.Cross(xAxis);

	const float x = xAxis.Dot(camera_pos);
	const float y = yAxis.Dot(camera_pos);
	const float z = zAxis.Dot(camera_pos);
	
	mat4 mat;
	mat.m_1_1 = xAxis.x;	mat.m_1_2 = xAxis.y;	mat.m_1_3 = xAxis.z;	mat.m_1_4 = -x;
	mat.m_2_1 = yAxis.x;	mat.m_2_2 = yAxis.y;	mat.m_2_3 = yAxis.z;	mat.m_2_4 = -y;
	mat.m_3_1 = zAxis.x;	mat.m_3_2 = zAxis.y;	mat.m_3_3 = zAxis.z;	mat.m_3_4 = -z;
	mat.m_4_1 = 0.0f;		mat.m_4_2 = 0.0f;		mat.m_4_3 = 0.0f;		mat.m_4_4 = 1.0f;

	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline float* mat4::pointer() {
	return mat;
}

//-------------------------------------------------------------------------------------------
__forceinline const float* mat4::pointer() const {
	return mat;
}