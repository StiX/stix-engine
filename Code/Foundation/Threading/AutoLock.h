
#pragma once

#include "ClassHelpers.h"


template <class T>
class AutoLock
{
public:
	AutoLock(T& lock);
	~AutoLock();

protected:
private:
	T*	m_lock;

	MAKE_UNCOPYABLE(AutoLock);
};





//-------------------------------------------------------------------------------------------
template <class T>
inline AutoLock<T>::AutoLock(T& lock)
	: m_lock(&lock)
{
	m_lock->Lock();
}

//-------------------------------------------------------------------------------------------
template <class T>
inline AutoLock<T>::~AutoLock()
{
	m_lock->Unlock();
}