
#pragma once

#include <atomic>
#include "Types.h"
#include "Debug.h"
#include "SystemUtil.h"
#include "ClassHelpers.h"


class SpinLock
{
public:
					SpinLock();
					~SpinLock();

	void			Lock();
	bool			TryLock();
	void			Unlock();

protected:
private:
	std::atomic<u32> m_flag;
	//TODO:	add padding to avoid false sharing?!

	MAKE_UNCOPYABLE(SpinLock)
};





//-------------------------------------------------------------------------------------------
inline SpinLock::SpinLock()
	: m_flag(0)
{
}

//-------------------------------------------------------------------------------------------
inline SpinLock::~SpinLock()
{
	ASSERT(m_flag.load(std::memory_order_relaxed) == 0);
}

//-------------------------------------------------------------------------------------------
inline void SpinLock::Lock()
{
	while (true)											// Do the volatile spin read, to get maximum performance
	{
		if (m_flag.load(std::memory_order_relaxed) == 0)	// The lock was released, try to acquire it
		{
			if (TryLock())
			{
				return;
			}
		}

		BusyWaitYield();
	}
}

//-------------------------------------------------------------------------------------------
inline bool SpinLock::TryLock()
{
	return m_flag.exchange(1, std::memory_order_acquire) == 0;
}

//-------------------------------------------------------------------------------------------
inline void SpinLock::Unlock()
{
	m_flag.store(0, std::memory_order_release);
}