
#include "ThreadsSyncPoint.h"
#include "Debug.h"


//-------------------------------------------------------------------------------------------
ThreadsSyncPoint::ThreadsSyncPoint(u32 syncThreadsCount) {
	ASSERT(syncThreadsCount > 1, "Threads sync fence was designed to work with two or more threads");

	m_waitersCount = 0;
	m_targetWaitersCount = syncThreadsCount;
	m_signalled = false;
}

//-------------------------------------------------------------------------------------------
ThreadsSyncPoint::~ThreadsSyncPoint() {
	ReleaseWaiters();
}

//-------------------------------------------------------------------------------------------
void ThreadsSyncPoint::WaitForCondition() {
	std::unique_lock<std::mutex> lock(m_mutex);
	
	++m_waitersCount;
	if (m_waitersCount == m_targetWaitersCount) {
		m_signalled = true;
		
		--m_waitersCount;
		lock.unlock();

		m_condition.notify_all();
		return;
	}

	while (!m_signalled) {
		m_condition.wait(lock);
	}

	--m_waitersCount;
	if (m_waitersCount == 0) {
		m_signalled = false;
	}
}

//-------------------------------------------------------------------------------------------
void ThreadsSyncPoint::ReleaseWaiters() {
	// protect the condition modification with mutex to avoid the case, when another thread is suspended before m_condition.wait(), but after he checked the condition
	m_mutex.lock();
	m_signalled = true;
	m_mutex.unlock();

	m_condition.notify_all();
}