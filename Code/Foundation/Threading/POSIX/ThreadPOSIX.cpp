
#include "ThreadPOSIX.h"
#ifdef __OSX__
#	include <mach/thread_act.h>
#	include <mach/thread_policy.h>
#else
#	include <sched.h>
#endif

#include "Log.h"
#include "Debug.h"


//-------------------------------------------------------------------------------------------
void Thread::set_name(const char* name)
{
#ifdef __APPLE__
	pthread_setname_np(name);
#else
	pthread_setname_np((pthread_t)GetCurrentThreadId(), name);
#endif
}

//-------------------------------------------------------------------------------------------
void Thread::Spawn(ThreadEntry threadEntry, void* pData, const char* threadName, u32 stackSize)
{
	ASSERT(threadEntry);
	ASSERT(!m_thread);
	
	m_entry = threadEntry;
	m_pEntryData = pData;
	m_name = threadName;
	
	pthread_attr_t threadAttributes;
	pthread_attr_init(&threadAttributes);
	
	if (stackSize)
	{
		pthread_attr_setstacksize(&threadAttributes, stackSize);
	}
	
	if (pthread_create(&m_thread, &threadAttributes, ThreadProcProxy, this))
	{
		ASSERT(!"Cannot create thread, error message: %s", Error::GetLastErrorMessage());
	}
}

//-------------------------------------------------------------------------------------------
void Thread::SetAffinityMask(u32 mask) const
{
	(void)mask;
	ASSERT(m_thread);

#ifdef __APPLE__
#	if 0
	// This functionality is disabled because it doesn't bind a thread to a core but trys to give a hint to a scheduler,
	// so it can run threads with the same tag on a cores, that share L2 cache.
	// More info at https://developer.apple.com/library/mac/releasenotes/Performance/RN-AffinityAPI/
	
	thread_affinity_policy affinity;
	affinity.affinity_tag = mask;
	
	if (thread_policy_set(pthread_mach_thread_np(m_thread), THREAD_AFFINITY_POLICY, (thread_policy_t)&affinity, THREAD_AFFINITY_POLICY_COUNT) != KERN_SUCCESS)
	{
		log_error("Thread", "Cannot set thread affinity mask, error message: %s", Error::GetLastErrorMessage());
	}
#	endif
#elif defined __ANDROID__
	// not supported on Android :(
	// check this http://stackoverflow.com/questions/16319725/android-set-thread-affinity and http://miss-cache.blogspot.se/2013/01/android-running-native-code-on-multiple.html
#	warning "Not implemented"
#else
#	error "There is no implementation for this platform yet"
#endif
}

//-------------------------------------------------------------------------------------------
void Thread::SetPriority(ePriority priority)
{
	sched_param schedParams;
    schedParams.sched_priority = 0;
    
	int policy = SCHED_OTHER;
	
	static const int kMinCriticalPriority = sched_get_priority_min(SCHED_FIFO);
	static const int kMaxCriticalPriority = sched_get_priority_max(SCHED_FIFO);
	static const int kMiddleCriticalPriority = kMinCriticalPriority + (kMaxCriticalPriority - kMinCriticalPriority) / 2;

#ifdef __APPLE__
	static const int kMinNormalPriority = kMinCriticalPriority;
	static const int kMaxNormalPriority = kMiddleCriticalPriority;
	static const int kMiddleNormalPriority = kMinNormalPriority + (kMaxNormalPriority - kMinNormalPriority) / 2;
	
	switch (priority) {
		case Lowest:
			schedParams.sched_priority = kMinNormalPriority;
			break;
			
		case Low:
			schedParams.sched_priority = kMiddleNormalPriority;
			break;
			
		case Normal:
			schedParams.sched_priority = kMaxNormalPriority;
			break;
#else
	switch (priority) {
		case Lowest:
#ifndef __ANDROID__
			policy = SCHED_IDLE;
			break;
#endif
			
		case Low:
			policy = SCHED_BATCH;
			break;
			
		case Normal:
			break;
#endif
			
		case High:
			policy = SCHED_FIFO;
			schedParams.sched_priority = kMinCriticalPriority;
			break;
			
		case Highest:
			policy = SCHED_FIFO;
			schedParams.sched_priority = kMiddleCriticalPriority;
			break;
			
		case Critical:
			policy = SCHED_FIFO;
			schedParams.sched_priority = kMaxCriticalPriority;
			break;
			
		default:
			ASSERT(0);
			break;
	}

	
	if (pthread_setschedparam(m_thread, policy, &schedParams)) {
		log_error("Thread", "Cannot set thread priority, error message: %s", Error::GetLastErrorMessage());
        return;
	}
            
    m_priority = priority;
}

//-------------------------------------------------------------------------------------------
inline u32 Thread::Run()
{
	if (m_name.GetLength())
	{
		set_name(m_name.c_str());
	}
	
	return m_entry(m_pEntryData);
}

//-------------------------------------------------------------------------------------------
void* Thread::ThreadProcProxy(void* pThreadPtr)
{
	Thread* pThisThread = (Thread*)pThreadPtr;
	return reinterpret_cast<void*>(pThisThread->Run());
}
