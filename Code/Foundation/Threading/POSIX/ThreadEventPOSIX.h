
#pragma once

#include <pthread.h>
#include "Types.h"
#include "ClassHelpers.h"


class ThreadEvent
{
public:
	explicit		ThreadEvent(bool signalled = false);
					~ThreadEvent();
	
	void			Wait();
	bool			WaitTimeout(u32 milliseconds);		//Returns false if time run out, true - if event was signalled during time-out period
	void			Signal();
	void			Reset();
	
protected:
private:
	pthread_cond_t	m_cond;
	pthread_mutex_t m_mutex;
	u32				m_signalled;
	u32				m_waitersCount;
	
	MAKE_UNCOPYABLE(ThreadEvent);
};





//-------------------------------------------------------------------------------------------
inline void	ThreadEvent::Signal()
{
	pthread_mutex_lock(&m_mutex);
	m_signalled = true;
	pthread_mutex_unlock(&m_mutex);

	pthread_cond_broadcast(&m_cond);
}

//-------------------------------------------------------------------------------------------
inline void	ThreadEvent::Reset()
{
	pthread_mutex_lock(&m_mutex);
	m_signalled = false;
	pthread_mutex_unlock(&m_mutex);
}