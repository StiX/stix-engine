
#pragma once

#include <atomic>
#include <pthread.h>
#include "Types.h"
#include "ClassHelpers.h"
#include "Error.h"
#include "Debug.h"
#include "Str.h"


class Thread
{
public:
	typedef u32		(*ThreadEntry)(void*);
	
	enum ePriority
	{
		Lowest,
		Low,
		Normal,
		High,
		Highest,
		Critical
	};
	
					Thread();
					~Thread();
	
	size_t			GetId() const;
	
	void			Spawn(ThreadEntry threadEntry, void* pData, const char* threadName, u32 stackSize = 0);
	
	void			WaitToTerminate();
	void			RequestTermination();		// Allows user to "request termination" - set a flag, which you can check in your ThreadEntry and terminate a worker loop
	bool			IsTerminationRequested() const;
	
	void			SetAffinityMask(u32 mask) const;
	
	ePriority		GetPriority() const;
	void			SetPriority(ePriority priority);

	const char*		GetName() const;
	
	static bool		YieldThread();
	static size_t	GetCurrentThreadId();
	static void		set_name(const char* name);
	
protected:
private:
	pthread_t		m_thread;
	ThreadEntry		m_entry;
	void*			m_pEntryData;
	String			m_name;
	ePriority		m_priority;
	std::atomic<u32> m_terminationRequested;
	
	u32				Run();
	static void*	ThreadProcProxy(void* pThreadPtr);
	
	MAKE_UNCOPYABLE(Thread);
};





//-------------------------------------------------------------------------------------------
inline Thread::Thread()
	: m_thread(0), m_entry(nullptr), m_pEntryData(nullptr), m_priority(Normal), m_terminationRequested(0)
{
}

//-------------------------------------------------------------------------------------------
inline Thread::~Thread()
{
	if (m_thread)
	{
		RequestTermination();
		WaitToTerminate();
	}
}

//-------------------------------------------------------------------------------------------
inline size_t Thread::GetId() const
{
	return (uintptr_t)m_thread;
}

//-------------------------------------------------------------------------------------------
inline void Thread::WaitToTerminate()
{
	ASSERT(m_thread);
	ASSERT(GetCurrentThreadId() != GetId())
	
	pthread_join(m_thread, nullptr);
}

//-------------------------------------------------------------------------------------------
inline void Thread::RequestTermination()
{
	m_terminationRequested.store(1, std::memory_order_release);
}

//-------------------------------------------------------------------------------------------
inline bool Thread::IsTerminationRequested() const
{
	return m_terminationRequested.load(std::memory_order_acquire) == 1;
}

//-------------------------------------------------------------------------------------------
inline Thread::ePriority Thread::GetPriority() const
{
	return m_priority;
}

//-------------------------------------------------------------------------------------------
inline const char* Thread::GetName() const
{
	return m_name.c_str();
}

//-------------------------------------------------------------------------------------------
inline bool Thread::YieldThread()
{
	return sched_yield() == 0;
}

//-------------------------------------------------------------------------------------------
inline size_t Thread::GetCurrentThreadId()
{
	return (size_t)pthread_self();
}