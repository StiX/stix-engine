
#pragma once

#include <pthread.h>
#include "ClassHelpers.h"
#include "Error.h"
#include "Log.h"


class Mutex
{
public:
						Mutex();
						~Mutex();
	
	void				Lock();
	bool				TryLock();
	void				Unlock();
	
protected:
private:
	pthread_mutex_t		m_mutex;
	
	MAKE_UNCOPYABLE		(Mutex);
};





//-------------------------------------------------------------------------------------------
inline Mutex::Mutex()
{
	pthread_mutexattr_t mutexAttr;
	pthread_mutexattr_init(&mutexAttr);
	pthread_mutexattr_settype(&mutexAttr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutexattr_setpshared(&mutexAttr, PTHREAD_PROCESS_PRIVATE);
	
	if (pthread_mutex_init(&m_mutex, &mutexAttr))
	{
		ASSERT(!"Cannot init mutex, error message: %s", Error::GetLastErrorMessage());
	}
	pthread_mutexattr_destroy(&mutexAttr);
}

//-------------------------------------------------------------------------------------------
inline Mutex::~Mutex()
{
	pthread_mutex_destroy(&m_mutex);
}

//-------------------------------------------------------------------------------------------
inline void Mutex::Lock()
{
	pthread_mutex_lock(&m_mutex);
}

//-------------------------------------------------------------------------------------------
inline bool Mutex::TryLock()
{
	return pthread_mutex_trylock(&m_mutex) == 0;
}

//-------------------------------------------------------------------------------------------
inline void Mutex::Unlock()
{
	pthread_mutex_unlock(&m_mutex);
}