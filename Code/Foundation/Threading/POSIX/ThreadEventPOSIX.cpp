
#include "ThreadEventPOSIX.h"
#include <errno.h>
#include "SystemUtil.h"
#include "Log.h"
#include "Error.h"
#include "Debug.h"

#ifdef __APPLE__
#	include "Timer.h"

	void CalculateWakeTime(u32 waitTimeMs, struct timespec* time)
	{
		const double nano_seconds = timer::nano_seconds() + (double)waitTimeMs * 1E+6;
		const long billion = 1000000000;
		
		time->tv_sec = (u64)(nano_seconds * 1E-9);
		time->tv_nsec = (long)nano_seconds % billion;
	}
#else
#	include <time.h>

	void CalculateWakeTime(u32 waitTimeMs, struct timespec* time)
	{
		clock_gettime(CLOCK_MONOTONIC, time);
		
		const long nanoSec = time->tv_nsec + waitTimeMs * 1000000;
		const long billion = 1000000000;
		
		time->tv_sec = time->tv_sec + nanoSec / billion;
		time->tv_nsec = (nanoSec % billion);
	}
#endif


//-------------------------------------------------------------------------------------------
ThreadEvent::ThreadEvent(bool signalled)
	: m_signalled(signalled), m_waitersCount(0)
{
	pthread_mutexattr_t mutexAttr;
	pthread_mutexattr_init(&mutexAttr);
	pthread_mutexattr_settype(&mutexAttr, PTHREAD_MUTEX_NORMAL);
	pthread_mutexattr_setpshared(&mutexAttr, PTHREAD_PROCESS_PRIVATE);
	
	if (pthread_mutex_init(&m_mutex, &mutexAttr))
	{
		ASSERT(!"Cannot create event mutex, error message: %s", Error::GetLastErrorMessage());
	}
	
	pthread_mutexattr_destroy(&mutexAttr);
	
	pthread_condattr_t condAttr;
	pthread_condattr_init(&condAttr);
	pthread_condattr_setpshared(&condAttr, PTHREAD_PROCESS_PRIVATE);
	
	if (pthread_cond_init(&m_cond, &condAttr))
	{
		ASSERT(!"Cannot create event condition, error message: %s", Error::GetLastErrorMessage());
	}
	
	pthread_condattr_destroy(&condAttr);
}

//-------------------------------------------------------------------------------------------
ThreadEvent::~ThreadEvent()
{
	if (m_waitersCount)
	{
		Signal();	// release threads that are still waiting for the event

		while (*(volatile u32*)&m_waitersCount)	// do buys wait until all threads release mutex
		{
			BusyWaitYield();
		}
	}

	pthread_cond_destroy(&m_cond);
	pthread_mutex_destroy(&m_mutex);
}

//-------------------------------------------------------------------------------------------
void ThreadEvent::Wait()
{
	pthread_mutex_lock(&m_mutex);
	++m_waitersCount;
	
	while (!*(volatile u32*)&m_signalled)	// prevent compiler from optimizing this variable, because it will be changed from another thread and pthread conditional variable can wakeup randomly before signal occurred
	{
		pthread_cond_wait(&m_cond, &m_mutex);
	}
	
	--m_waitersCount;
	pthread_mutex_unlock(&m_mutex);
}

//-------------------------------------------------------------------------------------------
bool ThreadEvent::WaitTimeout(u32 milliseconds)
{
	int result = 0;
	
	timespec time;
	CalculateWakeTime(milliseconds, &time);
	
	pthread_mutex_lock(&m_mutex);
	++m_waitersCount;
	
	while (!*(volatile u32*)&m_signalled && result == 0)
	{
		result = pthread_cond_timedwait(&m_cond, &m_mutex, &time);
	}
	
	--m_waitersCount;
	pthread_mutex_unlock(&m_mutex);
	
	return result == 0;
}