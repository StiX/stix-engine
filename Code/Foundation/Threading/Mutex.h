
#pragma once

// Defines a recursive mutex

#ifdef __WIN__
#	include "Win\MutexWin.h"
#else
#	include "POSIX/MutexPOSIX.h"
#endif