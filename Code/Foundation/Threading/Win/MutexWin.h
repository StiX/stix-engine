
#pragma once

#include <windows.h>
#include "ClassHelpers.h"


class Mutex
{
public:
						Mutex();
						~Mutex();

	void				Lock();
	bool				TryLock();
	void				Unlock();

protected:
private:
	CRITICAL_SECTION	m_criticalSection;

	MAKE_UNCOPYABLE		(Mutex);
};





//-------------------------------------------------------------------------------------------
inline Mutex::Mutex()
{
	InitializeCriticalSection(&m_criticalSection);
}

//-------------------------------------------------------------------------------------------
inline Mutex::~Mutex()
{
	DeleteCriticalSection(&m_criticalSection);
}

//-------------------------------------------------------------------------------------------
inline void Mutex::Lock()
{
	EnterCriticalSection(&m_criticalSection);
}

//-------------------------------------------------------------------------------------------
inline bool Mutex::TryLock()
{
	return TryEnterCriticalSection(&m_criticalSection) != 0;
}

//-------------------------------------------------------------------------------------------
inline void Mutex::Unlock()
{
	LeaveCriticalSection(&m_criticalSection);
}