
#include "ThreadWin.h"
#include <process.h>
#include <cstdlib>
#include "ExternalDebugger.h"


//-------------------------------------------------------------------------------------------
void Thread::set_name(const char* name)
{
#pragma pack(push,8)
	typedef struct tagTHREADNAME_INFO
	{
		DWORD dwType; // Must be 0x1000.
		LPCSTR szName; // Pointer to name (in user addr space).
		DWORD dwThreadID; // Thread ID (-1 = caller thread).
		DWORD dwFlags; // Reserved for future use, must be zero.
	} THREADNAME_INFO;
#pragma pack(pop)

	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = name;
	info.dwThreadID = (u32)GetCurrentThreadId();
	info.dwFlags = 0;

	__try
	{
		const DWORD MS_VC_EXCEPTION = 0x406D1388;
		RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	}
}

//-------------------------------------------------------------------------------------------
void Thread::Spawn(ThreadEntry threadEntry, void* pData, const char* threadName, u32 stackSize)
{
	ASSERT(threadEntry);
	ASSERT(!m_handle);
	
	m_entry = threadEntry;
	m_pEntryData = pData;
	m_name = threadName;

	u32 threadId = 0;
	m_handle = (HANDLE)_beginthreadex(nullptr, stackSize, &ThreadProcProxy, this, 0, &threadId);
	ASSERT(m_handle, "Cannot create thread, error message: %s", Error::GetLastErrorMessage());
	
	m_threadId = (size_t)threadId;
}

//-------------------------------------------------------------------------------------------
inline u32 Thread::Run()
{
	if (m_name.GetLength())
	{
		set_name(m_name.c_str());
	}

	return m_entry(m_pEntryData);
}

//-------------------------------------------------------------------------------------------
u32 Thread::ThreadProcProxy(void* pThreadPtr)
{
	Thread* pThisThread = (Thread*)pThreadPtr;
	return pThisThread->Run();
}