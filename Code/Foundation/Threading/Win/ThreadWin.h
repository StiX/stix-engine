
#pragma once

#include <atomic>
#include <windows.h>
#include "Types.h"
#include "ClassHelpers.h"
#include "Error.h"
#include "Debug.h"
#include "Str.h"
#include "Log.h"


class Thread
{
public:
	typedef u32		(*ThreadEntry)(void*);

	enum ePriority
	{
		Lowest	=	THREAD_PRIORITY_LOWEST,
		Low		=	THREAD_PRIORITY_BELOW_NORMAL,
		Normal	=	THREAD_PRIORITY_NORMAL,
		High	=	THREAD_PRIORITY_ABOVE_NORMAL,
		Highest =	THREAD_PRIORITY_HIGHEST,
		Critical =	THREAD_PRIORITY_TIME_CRITICAL
	};

					Thread();
					~Thread();

	size_t			GetId() const;

	void			Spawn(ThreadEntry threadEntry, void* pData, const char* threadName, u32 stackSize = 0);
	
	void			WaitToTerminate();
	void			RequestTermination();		// Allows user to "request termination" - set a flag, which you can check in your ThreadEntry and terminate a worker loop
	bool			IsTerminationRequested() const;

	void			SetAffinityMask(u32 mask) const;

	ePriority		GetPriority() const;
	void			SetPriority(ePriority priority);

	const char*		GetName() const;

	static bool		YieldThread();
	static size_t	GetCurrentThreadId();
	static void		set_name(const char* name);

protected:
private:
	HANDLE			m_handle;
	size_t			m_threadId;
	ThreadEntry		m_entry;
	void*			m_pEntryData;
	String			m_name;
	ePriority		m_priority;
	std::atomic<u32> m_terminationRequested;

	u32				Run();
	static u32 __stdcall ThreadProcProxy(void* pThreadPtr);

	MAKE_UNCOPYABLE(Thread);
};





//-------------------------------------------------------------------------------------------
inline Thread::Thread()
	: m_handle(0), m_threadId(0), m_entry(nullptr), m_pEntryData(nullptr), m_priority(Normal), m_terminationRequested(0)
{
}

//-------------------------------------------------------------------------------------------
inline Thread::~Thread()
{
	if (m_handle)
	{
		RequestTermination();
		WaitToTerminate();
		CloseHandle(m_handle);
	}
}

//-------------------------------------------------------------------------------------------
inline size_t Thread::GetId() const
{
	return m_threadId;
}

//-------------------------------------------------------------------------------------------
inline void Thread::WaitToTerminate()
{
	ASSERT(m_handle);
	ASSERT(GetCurrentThreadId() != GetId())

	WaitForSingleObject(m_handle, INFINITE);
}

//-------------------------------------------------------------------------------------------
inline void Thread::RequestTermination()
{
	m_terminationRequested.store(1, std::memory_order_release);
}

//-------------------------------------------------------------------------------------------
inline bool Thread::IsTerminationRequested() const
{
	return m_terminationRequested.load(std::memory_order_acquire) == 1;
}

//-------------------------------------------------------------------------------------------
inline void Thread::SetAffinityMask(u32 mask) const
{
	ASSERT(m_handle);

	if (SetThreadAffinityMask(m_handle, mask))
	{
		log_error("Thread", "Cannot set thread affinity mask, error message: %s", Error::GetLastErrorMessage());
	}
}

//-------------------------------------------------------------------------------------------
inline Thread::ePriority Thread::GetPriority() const
{
	return m_priority;
}

//-------------------------------------------------------------------------------------------
inline void Thread::SetPriority(Thread::ePriority priority)
{
	ASSERT(m_handle);

	m_priority = priority;
	if (!SetThreadPriority(m_handle, m_priority))
	{
		log_error("Thread", "Cannot set thread priority, error message: %s", Error::GetLastErrorMessage());
	}
}

//-------------------------------------------------------------------------------------------
inline const char* Thread::GetName() const
{
	return m_name.c_str();
}

//-------------------------------------------------------------------------------------------
inline bool Thread::YieldThread()
{
	// see http://joeduffyblog.com/2006/08/22/priorityinduced-starvation-why-sleep1-is-better-than-sleep0-and-the-windows-balance-set-manager/
	return SwitchToThread() != 0;
}

//-------------------------------------------------------------------------------------------
inline size_t Thread::GetCurrentThreadId()
{
	return (size_t)::GetCurrentThreadId();
}