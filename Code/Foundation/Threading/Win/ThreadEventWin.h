
#pragma once

#include <windows.h>
#include "ClassHelpers.h"
#include "Error.h"
#include "ExternalDebugger.h"
#include "Log.h"


class ThreadEvent {
public:
	explicit		ThreadEvent(bool signalled = false);
					~ThreadEvent();

	void			Wait();								//TODO: make them const?!
	bool			WaitTimeout(u32 milliseconds);		//Returns false if time run out, true - if event was signalled during time-out period
	void			Signal();
	void			Reset();

protected:
private:
	HANDLE			m_event;

	MAKE_UNCOPYABLE(ThreadEvent);
};





//-------------------------------------------------------------------------------------------
inline ThreadEvent::ThreadEvent(bool signalled) {
	m_event = CreateEvent(nullptr, true, signalled, nullptr);
	ASSERT(m_event, "Cannot create event, error message: %s", Error::GetLastErrorMessage());
}

//-------------------------------------------------------------------------------------------
inline ThreadEvent::~ThreadEvent() {
	Signal();	// release threads that are still waiting for the event, if any
	CloseHandle(m_event);
}

//-------------------------------------------------------------------------------------------
inline void ThreadEvent::Wait() {
	WaitForSingleObject(m_event, INFINITE);
}

//-------------------------------------------------------------------------------------------
inline bool	ThreadEvent::WaitTimeout(u32 milliseconds) {
	const u32 result = WaitForSingleObject(m_event, milliseconds);

	switch (result) {
	case WAIT_FAILED:
		ASSERT(!"Cannot wait on event, error message: %s", Error::GetLastErrorMessage());

	case WAIT_TIMEOUT:
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------
inline void	ThreadEvent::Signal() {
	if (!SetEvent(m_event)) {
		ASSERT(!"Cannot set event, error message: %s", Error::GetLastErrorMessage());
	}
}

//-------------------------------------------------------------------------------------------
inline void	ThreadEvent::Reset() {
	if (!ResetEvent(m_event)) {
		ASSERT(!"Cannot reset event, error message: %s", Error::GetLastErrorMessage());
	}
}