
#pragma once

#include <mutex>
#include <condition_variable>
#include "Types.h"


class ThreadsSyncPoint {
public:
							ThreadsSyncPoint(u32 syncThreadsCount);
							~ThreadsSyncPoint();
	
	void					WaitForCondition();
	void					ReleaseWaiters();

protected:
private:
	std::mutex				m_mutex;
	std::condition_variable	m_condition;
	u32						m_waitersCount;
	u32						m_targetWaitersCount;
	volatile bool			m_signalled;
};