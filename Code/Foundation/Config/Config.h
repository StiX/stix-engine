
//#pragma once	// No pragma here due to clang complaining about it, since this file is automatically prepended to every compilable unit


#ifdef __WIN__
#	include "WindowsPlatform.h"
#else
#	include "NixPlatform.h"
#endif

//#define inline __forceinline


#ifdef DEBUG
#	define DEBUG_STRING_HASH
#	define SHOW_CONSOLE
#	define NO_ASSERT (0)
#else
#	define NO_ASSERT (1)
#endif


#ifdef DLL_EXPORTS
#	define DLL_API __declspec(dllexport)
#else
#	define DLL_API /*__declspec(dllimport)*/
#endif