
#pragma once


#ifdef _MSC_VER

#	if _MSC_VER < 1800
#		error "Visual Studio version prior 2013 are not supported"
#	endif

//disable stupid warnings
#	pragma warning(disable: 4127)
#	pragma warning(disable: 4200)
#	pragma warning(disable: 4201)	// nonstandard extension used : nameless struct/union

#	define __align_type(x) __declspec(align(x))

#	ifdef _M_AMD64
#		define __ARCH_INTEL__
#		define __ARCH_64BIT__
#	elif defined (_M_IX86)
#		define __ARCH_INTEL__
#		define __ARCH_32BIT__
#	elif defined (_M_ARM)
#		define __ARCH_ARM__
#		define __ARCH_32BIT__
#	else
#		error "Unknown architecture"
#	endif

#	define __PLATFORM_DESKTOP__

#endif