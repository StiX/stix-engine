
#pragma once

#if defined __GNUC__ || defined __clang__

#	define __forceinline inline __attribute__((always_inline))

#	define __align_type(x) __attribute__((aligned(x)))

#	ifdef __amd64__
#		define __ARCH_INTEL__
#		define __ARCH_64BIT__
#	elif defined (__i386__)
#		define __ARCH_INTEL__
#		define __ARCH_32BIT__
#	elif defined (__aarch64__)
#		define __ARCH_ARM__
#		define __ARCH_64BIT__
#	elif defined (__ARM_ARCH_7A__)
#		define __ARCH_ARM__
#		define __ARCH_32BIT__
#	else
#		error "Unknown architecture"
#	endif

#	if defined __iOS__ || defined __ANDROID__
#		define __PLATFORM_MOBILE__
#	elif defined __OSX__
#		define __PLATFORM_DESKTOP__
#	else
#		error "Unknown platform"
#	endif


#endif