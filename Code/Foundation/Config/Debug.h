
#pragma once


#if NO_ASSERT
#	define ASSERT(exp, ...)
#	define XERROR(error, ...)
#else
#	include <cassert>
#	include <utility>
#	include <cstdio>
#	include "Log.h"


	template <bool V>
	struct HasParameters
	{
		enum { value = V };
	};

	template <bool V>
	struct MoreThanOneParameter
	{
		enum { value = V };
	};


	template<class ... Types>
	inline void PrintAssertion(MoreThanOneParameter<false>, const char* assertExpr, Types&& ... args)
	{
		log_error("Assertion", "%s. Message: %s", assertExpr, std::forward<Types>(args)...);
	}

	template<class ... Types>
	inline void PrintAssertion(MoreThanOneParameter<true>, const char* assertExpr, Types&& ... args)
	{
		char stringBuffer[512];
		sprintf(stringBuffer, args...);
	
		log_error("Assertion", "%s. Message: %s", assertExpr, stringBuffer);
	}

	template<class ... Types>
	inline void PrintAssertion(HasParameters<true>, const char* assertExpr, Types&& ... args)
	{
		PrintAssertion(MoreThanOneParameter<(sizeof...(args) > 1)>(), assertExpr, std::forward<Types>(args)...);
	}

	template<class ... Types>
	inline void PrintAssertion(HasParameters<false>, const char* assertExpr, Types&& ... /*args*/)
	{
		log_error("Assertion", "%s.", assertExpr);
	}

	template<class ... Types>
	inline void PrintAssertion(const char* assertExpr, Types&& ... args)
	{
		PrintAssertion(HasParameters<sizeof...(args) != 0>(), assertExpr, std::forward<Types>(args)...);
	}


#	define ASSERT(exp, ...)							\
	{												\
		if(!(exp))									\
		{											\
			PrintAssertion(#exp, ##__VA_ARGS__);	\
			assert(exp);							\
		}											\
	}

#	define XERROR(error, ...) {						\
		PrintAssertion(error, ##__VA_ARGS__);		\
		assert(!error);								\
	}
#endif
