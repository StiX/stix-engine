
#include "StackBackTrace.h"
#include <cstdio>
#include <cstdlib>
#include "Log.h"
#include "Debug.h"
#include "SystemUtil.h"

#ifndef __ANDROID__
#	include <execinfo.h>
#else
#	include <unwind.h>
#	include <dlfcn.h>
#	include <cxxabi.h>

	struct UnwindData
	{
		void**	capturedFrames;
		void**	endCapturedFrames;
		u32		skipFramesCount;
	};

	struct DemangleBuffer
	{
		char* str;
		static const size_t kLength = 256;

		DemangleBuffer()
		{
			str = (char*)malloc(kLength);
		}

		~DemangleBuffer()
		{
			free(str);
		}
	};
#endif


//-------------------------------------------------------------------------------------------
void LogCallStack(u32 framesCount, void* const* stackFramePointers)	// Resolve symbol name and source location given the path to the executable and an address
{
    (void)framesCount;
    (void)stackFramePointers;
    
#ifdef __ANDROID__
	static DemangleBuffer demangledName;

	for (size_t i = 0; i < framesCount; ++i)	// TODO: fix for release build
	{
		const char* symbol = "";

		Dl_info info;
		if (dladdr(stackFramePointers[i], &info) && info.dli_sname)
		{
			symbol = info.dli_sname;
		}

		int status;
		size_t length = DemangleBuffer::kLength;
		demangledName.str = abi::__cxa_demangle(symbol, demangledName.str, &length, &status);
		if (!status)
		{
			symbol = demangledName.str;
		}

		log_message(nullptr, "  #%d: %X   %s", i, stackFramePointers[i], symbol);	//TODO: print frames into string buffer and than output buffer to the log
	}
#elif !defined __iOS__
	char cmd[1024] = {0};
 
	int strLen =
#   ifdef __OSX__
        sprintf(cmd, "atos -p \"%s\" -d", GetExecutableName());
#   else
        sprintf(cmd, "addr2line -f -i -p -e \"%s\"", GetExecutableName());
#   endif
	
        for (u32 i = 0; i < framesCount; ++i)
        {
            if ((size_t)stackFramePointers[i] <= 1)        // sometimes OS X backtraces have addresses equal to 0x1
            {
                break;
            }
            
            strLen += sprintf(&cmd[strLen], " %p", stackFramePointers[i]);
        }

        system(cmd);
        //TODO: log leak addresses into file log
#endif
}

#ifdef __ANDROID__
//-------------------------------------------------------------------------------------------
static _Unwind_Reason_Code UnwindCallback(_Unwind_Context* pContext, void* pData)
{
	UnwindData* pUnwindData = (UnwindData*)pData;

	int ipBeforeNotFullyExecutedInst = 1;
	//uintptr_t instructionPointer = _Unwind_GetIPInfo(pContext, &ipBeforeNotFullyExecutedInst);
	uintptr_t instructionPointer = _Unwind_GetIP(pContext);

	if (pUnwindData->skipFramesCount > 0)
	{
		--pUnwindData->skipFramesCount;
		return _URC_NO_REASON;
	}

	if (!ipBeforeNotFullyExecutedInst)
	{
		--instructionPointer;
	}

	if (instructionPointer)
	{
		if (pUnwindData->capturedFrames == pUnwindData->endCapturedFrames)
		{
			return _URC_END_OF_STACK;
		}
		else
		{
			*pUnwindData->capturedFrames++ = (void*)instructionPointer;
			return _URC_NO_REASON;
		}
	}
	
	return _URC_END_OF_STACK;
}
#endif

//-------------------------------------------------------------------------------------------
u32 GetStackBackTrace(u32 skipFrames, u32 captureFramesCount, void** capturedFramesPtrs)
{
#ifdef __ANDROID__
	UnwindData data;
	data.skipFramesCount = skipFrames;
	data.capturedFrames = capturedFramesPtrs;
	data.endCapturedFrames = capturedFramesPtrs + captureFramesCount;

	_Unwind_Backtrace(UnwindCallback, &data);

	return captureFramesCount - (data.endCapturedFrames - data.capturedFrames);
#else
	const u32 kMaxCaptureFrames = 128;
	static void* pointers[kMaxCaptureFrames];
	
	ASSERT(captureFramesCount + skipFrames <= kMaxCaptureFrames);
	
	const int kCaptured = backtrace(pointers, captureFramesCount + skipFrames);

	for (u32 i = 0; i < kCaptured - skipFrames; ++i)
	{
		capturedFramesPtrs[i] = pointers[i + skipFrames];
	}
	
	return kCaptured;
#endif
}

//-------------------------------------------------------------------------------------------
void LogMemoryLeaks(u32 framesCount, void* const* framesPtrs)
{
	log_message("Memory", "Memory leak call stack:");
	
	LogCallStack(framesCount, framesPtrs);
}