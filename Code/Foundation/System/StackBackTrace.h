
#pragma once

/****************************************************************************/
/* Please note, that GetStackBackTrace will not work on Windows without VLD.*/
/* To use it without VLD manually call SymSetOptions and SymInitialize.		*/
/****************************************************************************/

#include "Types.h"


u32		GetStackBackTrace(u32 skipFrames, u32 captureFramesCount, void** capturedFramesPtrs);	// returns the number of captured frames

void	LogMemoryLeaks(u32 framesCount, void* const* framesPtrs);