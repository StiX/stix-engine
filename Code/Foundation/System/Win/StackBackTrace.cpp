
#include "StackBackTrace.h"
#include <cstdio>
#include <windows.h>
#include <Dbghelp.h>
#include "Log.h"

#ifdef DEBUG
#	include <crtdbg.h>
#	include "ExternalDebugger.h"
#endif


//-------------------------------------------------------------------------------------------
u32 GetStackBackTrace(u32 skipFrames, u32 captureFramesCount, void** capturedFramesPtrs) {
	return CaptureStackBackTrace(skipFrames, captureFramesCount, capturedFramesPtrs, nullptr);
}

//-------------------------------------------------------------------------------------------
void LogMemoryLeaks(u32 framesCount, void* const* framesPtrs) {
	const size_t buffer_size = 2048;
	char buffer[buffer_size];
	int len = 0;

#define  LOG_LEAK(__str, ...) len += snprintf(&buffer[len], buffer_size - len, __str"\n", __VA_ARGS__);

	LOG_LEAK("Memory leak call stack:");

	DWORD displacement;
	IMAGEHLP_LINE64	source_line_info;
	source_line_info.SizeOfStruct = sizeof(IMAGEHLP_LINE64);

	u8 symbol_info_buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(((SYMBOL_INFO*)0)->Name)];
	PSYMBOL_INFO symbol_info = (PSYMBOL_INFO)symbol_info_buffer;
	symbol_info->SizeOfStruct = sizeof(SYMBOL_INFO);
	symbol_info->MaxNameLen = MAX_SYM_NAME;
	
	HANDLE process = GetCurrentProcess();

	u32 failed_to_symbolicate_count = 0;
	for (u32 i = 0; i < framesCount; ++i) {
		if (SymFromAddr(process, (DWORD64)framesPtrs[i], 0, symbol_info)) {
			if (!SymGetLineFromAddr64(process, (DWORD64)framesPtrs[i], &displacement, &source_line_info)) {
				source_line_info.FileName = "Unknown";
				source_line_info.LineNumber = (DWORD)-1;
			}

			LOG_LEAK("    %s (%d): %s", source_line_info.FileName, source_line_info.LineNumber, symbol_info->Name);
		} else {
			LOG_LEAK("    Failed to symbolicate address 0x%zX", (size_t)framesPtrs[i]);
			++failed_to_symbolicate_count;
		}
	}

	if (failed_to_symbolicate_count * 3 >= framesCount) {
		LOG_LEAK("  Failed to symbolicate some addresses. Make sure app was build with /PROFILE linker option (Linker -> Advanced -> Profile)");
	}

	log_warning("Memory", buffer);

#ifdef DEBUG
	if (IsDebuggerAttached()) {
		_CrtDbgReport(_CRT_WARN, nullptr, 0, nullptr, "%s", buffer);
	}
#endif

#undef LOG_LEAK
}