
#include "Log.h"
#include <atomic>
#include <cstdio>
#include <cstdarg>
#include "Types.h"
#include "Debug.h"

#ifdef DEBUG

#include <ctime>
#include <chrono>
#include "File.h"
#include "StringHash.h"
#include "FileSystem.h"

#ifdef __ANDROID__
#	include <android/log.h>
#	define puts(__str) __android_log_print(android_log_type, "[StiXEngine]", "%s", __str);

	static int android_log_type = ANDROID_LOG_INFO;
#endif

#if defined(__APPLE__) && defined(DEBUG) && defined(SHOW_CONSOLE)
#	include "ExternalDebugger.h"


//-------------------------------------------------------------------------------------------
static bool xcode_colors_available()
{
	const char *xcode_colors = getenv("XcodeColors");
	return xcode_colors && (strcmp(xcode_colors, "YES") == 0);
}

static const bool debugger_attached = IsDebuggerAttached();
static const bool has_xcode_colors = xcode_colors_available();

#endif	// ~DEBUG

namespace {
File					log_file;
u32						log_level;
thread_local StringHash	error_context;

#ifdef __WIN__
	HANDLE	console;
	bool	free_console = false;
#endif

enum class text_color {
	red = 12,
	yellow = 14,
	white = 7,
};


#ifdef SHOW_CONSOLE
#	if !defined(__WIN__) && !defined(__ANDROID__)
//-------------------------------------------------------------------------------------------
void print_color_code(const char* ansi_code, const char* xcode_colors_code) {
#ifdef __APPLE__
	if (debugger_attached) {
		if (has_xcode_colors) {
			printf("%s", xcode_colors_code);
		}

		return;
	}
#endif

	printf("%s", ansi_code);
}
#	endif

//-------------------------------------------------------------------------------------------
void set_console_text_color(text_color color) {
#ifdef __WIN__
	SetConsoleTextAttribute(console, (WORD)color);
#elif defined __ANDROID__
	switch (color) {
	case text_color::red:
		android_log_type = ANDROID_LOG_ERROR;
		break;

	case text_color::yellow:
		android_log_type = ANDROID_LOG_WARN;
		break;

	default:
	case text_color::white:
		android_log_type = ANDROID_LOG_INFO;
		break;
	}
#else
	switch (color) {
	case text_color::yellow:
		print_color_code("\033[0;33m", "\033[fg200,200,0;");
		break;

	case text_color::red:
		print_color_code("\033[0;31m", "\033[fg255,0,0;");
		break;

	default:
	case text_color::white:
		print_color_code("\033[0;30m", "\033[;");
		break;
	}
#endif
}
#endif	//SHOW_CONSOLE

//-------------------------------------------------------------------------------------------
void log_to_file(const char* str, const char* html_tags) {
	if (!log_file.IsOpened()) {
		return;
	}

	const size_t buffer_size = 2048;
	char string_buffer[buffer_size];
	const int strlen = snprintf(string_buffer, buffer_size, html_tags, str);

	log_file.Write(string_buffer, strlen);
}

//-------------------------------------------------------------------------------------------
void log_to_console(text_color color, const char* str) {
	(void)color;
	(void)str;

#ifdef SHOW_CONSOLE
	if (color != text_color::white) {
		set_console_text_color(color);
		puts(str);
		set_console_text_color(text_color::white);
	} else {
		puts(str);
	}
#endif
}

//-------------------------------------------------------------------------------------------
void log_text(text_color color, const char* system, const char* str, const char* html_tags, va_list args, const StringHash* context) {
	const size_t buffer_size = 2048;
	char string_buffer[buffer_size];

	ASSERT(system);

	const auto time				= std::chrono::system_clock::now();
	const std::time_t seconds	= std::chrono::system_clock::to_time_t(time);
	const auto time_truncated	= std::chrono::system_clock::from_time_t(seconds);
	const u32 milliseconds		= (u32)std::chrono::duration_cast<std::chrono::milliseconds>(time - time_truncated).count();

	const std::tm t = *std::localtime(&seconds);
	int len = snprintf(string_buffer, buffer_size, "%02u:%02u:%02u.%03u  [%s] ", t.tm_hour, t.tm_min, t.tm_sec, milliseconds, system);
	len += vsnprintf(&string_buffer[len], buffer_size - len, str, args);

	if (context && *context != StringHash()) {
		snprintf(&string_buffer[len], buffer_size - len, "\n    Error context: %s\n", context->c_str());	// TODO: get a callstack on error context
	}

	log_to_console(color, string_buffer);
	log_to_file(string_buffer, html_tags);
}

}	// nameless namespace



//-------------------------------------------------------------------------------------------
void log_init(const char* log_path) {
	ASSERT(!log_file.IsOpened());

	log_file = CreateFile(log_path, F_ACCESS_WRITE);

	const char file_header[] = R"(<!DOCTYPE html>
<html>
	<head>
		<title>StiX Engine Log</title>
		<style>
			span {
				white-space: pre;
				font-family: monospace;
			}
		</style>
	</head>
	<body>
)";

	log_file.Write(file_header, sizeof(file_header) - 1);

#if defined(__WIN__) && defined(SHOW_CONSOLE)
	console = GetStdHandle(STD_OUTPUT_HANDLE);
	
	if (!console) {
		if (AllocConsole()) {
			console = GetStdHandle(STD_OUTPUT_HANDLE);
			free_console = true;
			*stdout = *freopen("CONOUT$", "wb", stdout);
			//*stdin = *_fdopen(_open_osfhandle((intptr_t)GetStdHandle(STD_INPUT_HANDLE), _O_TEXT), "r");
		}
	}
#endif
}

//-------------------------------------------------------------------------------------------
void log_shutdown() {
	if (log_file.IsOpened()) {
		const char file_footer[] = "\t</body>\n</html>";
		log_file.Write(file_footer, sizeof(file_footer) - 1);
	}

	log_level = UINT_MAX;
	//TODO: call log_file.Close();

#if defined(__WIN__) && defined(SHOW_CONSOLE)
	if (free_console) {
		fclose(stdout);
		//fclose(stdin);

		FreeConsole();
	}
#endif
}

//-------------------------------------------------------------------------------------------
void log_message(const char* system, const char* str, ...) {
	if (log_level > log_type::message) {
		return;
	}

	const char* html_tag = R"(		<span>%s</span><br>)""\n";

	va_list args;
	va_start(args, str);
	log_text(text_color::white, system, str, html_tag, args, nullptr);
	va_end(args);
}

//-------------------------------------------------------------------------------------------
void log_warning(const char* system, const char* str, ...) {
	if (log_level > log_type::warning) {
		return;
	}

	const char* html_tag = R"(		<span style="background-color: #FFFF00">%s</span><br>)""\n";

	va_list args;
	va_start(args, str);
	log_text(text_color::yellow, system, str, html_tag, args, &error_context);
	va_end(args);
}

//-------------------------------------------------------------------------------------------
void log_error(const char* system, const char* str, ...) {
	if (log_level > log_type::error) {
		return;
	}

	const char* html_tag = R"(		<span style="background-color: #FF4D4D">%s</span><br>)""\n";

	va_list args;
	va_start(args, str);
	log_text(text_color::red, system, str, html_tag, args, &error_context);
	va_end(args);
}

//-------------------------------------------------------------------------------------------
void log_set_error_context(const StringHash* context) {
	error_context = context ? *context : StringHash();
}

//-------------------------------------------------------------------------------------------
void log_enable() {
	if (log_level > log_type::error) {
		log_level -= log_type::error;
	}
}

//-------------------------------------------------------------------------------------------
void log_disable() {
	if (log_level <= log_type::error) {
		log_level += log_type::error;
	}
}

//-------------------------------------------------------------------------------------------
void log_set_level(log_type level) {
	log_level = level;
}
#endif //DEBUG

//-------------------------------------------------------------------------------------------
const char*	stringify(const char* format, ...) {
	const int block_size = 256;
	const u32 blocks_count = 8;
	static char string_buffer[block_size * blocks_count];
	static std::atomic<u32> current_block(0);

	const u32 block = current_block.fetch_add(1, std::memory_order_relaxed) % blocks_count;
	
	va_list args;
	va_start(args, format);
	const int strlen = vsnprintf(&string_buffer[block * block_size], block_size, format, args);
	va_end(args);

	ASSERT(strlen < block_size);

	return string_buffer;
}