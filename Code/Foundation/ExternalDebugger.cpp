
#include "ExternalDebugger.h"
#ifdef __APPLE__
#	include <stdbool.h>
#	include <unistd.h>
#	include <sys/sysctl.h>
#	include <cassert>
#elif defined(__WIN__)
#	include <windows.h>
#elif defined (__ANDROID__)
#	include <sys/ptrace.h>
#endif


//-------------------------------------------------------------------------------------------
bool IsDebuggerAttached()
{
#ifdef __APPLE__
	int                 junk;
	int                 mib[4];
	struct kinfo_proc   info;
	size_t              size;
	
	// Initialize the flags so that, if sysctl fails for some bizarre
	// reason, we get a predictable result.
	
	info.kp_proc.p_flag = 0;
	
	// Initialize mib, which tells sysctl the info we want, in this case
	// we're looking for information about a specific process ID.
	
	mib[0] = CTL_KERN;
	mib[1] = KERN_PROC;
	mib[2] = KERN_PROC_PID;
	mib[3] = getpid();
	
	size = sizeof(info);
	junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
	assert(junk == 0);
	
	// We're being debugged if the P_TRACED flag is set.
	
	return ((info.kp_proc.p_flag & P_TRACED) != 0);
#elif defined (__WIN__)
	return IsDebuggerPresent() != 0;
#elif defined (__ANDROID__)
	return ptrace(PTRACE_TRACEME, 0, NULL, 0) == -1;	// http://stackoverflow.com/questions/3596781/how-to-detect-if-the-current-process-is-being-run-by-gdb
#else
#	error "Not implemented"
#endif
}