
// Float16 <-> Float32 implementation.
// Some useful information: https://fgiesen.wordpress.com/2012/03/28/half-to-float-done-quic/

#pragma once

#include "Types.h"

const float kFloat16Max = 65504.0f;
const float kFloat16Min = 6.10351563e-5f;


class float16
{
public:
				float16() {}
	explicit	float16(float value);			// Denormals are zero, 65505.0+ (including NANs) is clamped to Infinity. Approx 5 CPU cycles on Ivy Bridge

	explicit	operator float() const;			// Denormals are zero, NANs are clamped to Infinity. Approx 4 CPU cycles on Ivy Bridge

protected:
private:
	union FP16
	{
		u16		uint;

		struct
		{
			u16	mantissa : 10;
			u16	exponent : 5;
			u16	sign : 1;
		};
	};

	FP16		m_value;
};