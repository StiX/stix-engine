
#include "SystemUtil.h"
#include <cstring>
#include "Debug.h"

#ifdef __APPLE__
#	include <mach-o/dyld.h>
#	include <sys/syslimits.h>
#	define MAX_PATH PATH_MAX
#elif defined __ANDROID__
#	define MAX_PATH 512
#endif


static char g_executablePath[MAX_PATH] = "";
static char g_executableDirPath[MAX_PATH] = "";
static char g_executableName[256] = "Stix Engine";

u32 InternalInitExecutableFilePath();		// returns path length

//-------------------------------------------------------------------------------------------
static void ProcessPath(u32 pathLength)
{
	for (int i = pathLength; i >= 0; --i)
	{
		if (g_executablePath[i] == '/' || g_executablePath[i] == '\\')
		{
			memcpy(g_executableDirPath, g_executablePath, i + 1);
			g_executableDirPath[i + 2] = 0;

			const u32 kNameLength = pathLength - i;
			memcpy(g_executableName, &g_executablePath[i + 1], kNameLength);
			break;
		}
	}
}


struct InitPath
{
	InitPath()
	{
		const u32 kLength = InternalInitExecutableFilePath();

		ProcessPath(kLength);
	}
};

static InitPath g_initPath;

//-------------------------------------------------------------------------------------------
const char* GetExecutableName()
{
	return g_executableName;
}

//-------------------------------------------------------------------------------------------
const char* GetExecutableFilePath()
{
	return g_executablePath;
}

//-------------------------------------------------------------------------------------------
const char* GetExecutableDirPath()
{
	return g_executableDirPath;
}

//-------------------------------------------------------------------------------------------
#ifdef __ANDROID__
void SetExecutableFilePath(const char* filePath)
{
	const u32 strLen = strlen(filePath);
	ASSERT(strLen < MAX_PATH)

	memcpy(g_executablePath, filePath, strLen + 1);

	ProcessPath(strLen);
	memcpy(g_executableDirPath, filePath, strLen + 1);
}
#endif

//-------------------------------------------------------------------------------------------
inline u32 InternalInitExecutableFilePath()		// returns path length
{
#ifdef __WIN__
	const HMODULE kAppModuleHandle = GetModuleHandle(NULL);
	return GetModuleFileName(kAppModuleHandle, g_executablePath, MAX_PATH);
#elif defined __APPLE__
	u32 pathLength = PATH_MAX;
	
	const int result = _NSGetExecutablePath(g_executablePath, &pathLength);
	ASSERT(result == 0, "Looks like PATH_MAX is not enough");
	(void)result;
	
	return (u32)strlen(g_executablePath);
#elif defined __ANDROID__
	return 0;		// Android sucks, so I have a special function for this
#else
#	error "Not implemented on your platform"
#endif
}