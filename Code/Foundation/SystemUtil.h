
#pragma once

#ifdef __WIN__
#	include <windows.h>
#else
#	include <unistd.h>
#endif

#ifdef __ARCH_INTEL__
#	include <xmmintrin.h>
#endif

#include "Types.h"


void		SystemSleep(u32 milliseconds);
void		BusyWaitYield();

const char* GetExecutableName();
const char*	GetExecutableDirPath();
const char*	GetExecutableFilePath();

#ifdef __ANDROID__
	void	SetExecutableFilePath(const char* filePath);
#endif





//-------------------------------------------------------------------------------------------
__forceinline void SystemSleep(u32 milliseconds)
{
#ifdef __WIN__
	Sleep(milliseconds);
#else
	usleep(milliseconds * 1000);
#endif
}

//-------------------------------------------------------------------------------------------
__forceinline void BusyWaitYield()
{
#ifdef __ARCH_ARM__
#	ifdef 	__clang__
		__builtin_arm_yield();
#	else
		__asm ("YIELD");
#	endif
#elif defined(__WIN__)
	YieldProcessor();
#else
	_mm_pause();
#endif
}