
#pragma once


#define MAKE_UNCOPYABLE(__class)			\
		__class(const __class&) = delete;	\
		__class& operator=(const __class&) = delete;