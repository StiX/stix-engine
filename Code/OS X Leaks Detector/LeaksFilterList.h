
#pragma once

#include "Array.h"


struct FilterRange
{
	const void*	m_begin;
	const void*	m_end;
	
				FilterRange(const void* begin, const void* end)
					: m_begin(begin), m_end(end)
				{}
};


void PopulateFilterList(Array<FilterRange>& filterList);