
/************************************************************************************************************************/
/* For additional information take a look at:																			*/
/* https://developer.apple.com/library/prerelease/mac/documentation/DeveloperTools/Conceptual/MachORuntime/index.html	*/
/* http://ddeville.me/2014/04/dynamic-linking/																			*/
/* http://www.cocoawithlove.com/2008/02/imp-of-current-method.html														*/
/************************************************************************************************************************/

#include "LeaksFilterList.h"
#include <mach-o/dyld.h>
#include <dlfcn.h>


//-------------------------------------------------------------------------------------------
size_t GetImageCodeSize(const mach_header* pImageHeader)
{
	static const char* textSegmentName = "__TEXT";	// image code is stored in __TEXT segment
	
	const bool k64BitImage = pImageHeader->magic == MH_MAGIC_64 || pImageHeader->magic == MH_CIGAM_64;
	const size_t kImageHeaderSize = k64BitImage ? sizeof(mach_header_64) : sizeof(mach_header);
	
	uintptr_t loadCommandPointer = (uintptr_t)pImageHeader + kImageHeaderSize;
	
	for (u32 i = 0; i < pImageHeader->ncmds; ++i)
	{
		const load_command* pLoadCommand = (load_command*)loadCommandPointer;
		
		if (!pLoadCommand->cmdsize)
		{
			continue;
		}
		else if (pLoadCommand->cmd == LC_SEGMENT_64)
		{
			const segment_command_64* pSegmentCommand = (segment_command_64*)pLoadCommand;
			if (!strcmp(pSegmentCommand->segname, textSegmentName))
			{
				// segment size is always multiple of page size, however real code size in __TEXT segment can occupy less
				return pSegmentCommand->vmsize;
			}
		}
		else if (pLoadCommand->cmd == LC_SEGMENT)
		{
			const segment_command* pSegmentCommand = (segment_command*)pLoadCommand;
			if (!strcmp(pSegmentCommand->segname, textSegmentName))
			{
				// segment size is always multiple of page size, however real code size in __TEXT segment can occupy less
				return pSegmentCommand->vmsize;
			}
		}
		
		loadCommandPointer += pLoadCommand->cmdsize;
	}
	
	return 0;
}

//-------------------------------------------------------------------------------------------
void AddAppToFilterList(Array<FilterRange>& filterList)
{
	const struct mach_header* pHeader = _dyld_get_image_header(0);	// app image is always going to have 0 index
	Dl_info imageInfo;
	if (!dladdr(pHeader, &imageInfo))
	{
		// Cannot get image info
		return;
	}
	
	const void* pImageCodeEndAddr = (void*)((size_t)imageInfo.dli_fbase + GetImageCodeSize(pHeader));
	filterList.EmplaceBack(imageInfo.dli_fbase, pImageCodeEndAddr);
}

//-------------------------------------------------------------------------------------------
void PopulateFilterList(Array<FilterRange>& filterList)
{
	AddAppToFilterList(filterList);
}