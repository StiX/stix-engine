
#include "MemoryLeakDetector.h"
#include "Types.h"
#include "SpinLock.h"
#include "HashTable.h"
#include "ObjectsPool.h"
#include "StackBackTrace.h"
#include "LeaksFilterList.h"


static const u32 kSkipFrames = 4;
static const u32 kTracesCount = 60;
static const u32 kInitialAllocationsReserve = 65536;

struct StackTrace
{
	void*	m_traces[kTracesCount];
	
			StackTrace()
			{
				memset(m_traces, 0, sizeof(StackTrace));
			}
};


class LeakDetector
{
public:
					LeakDetector();
					~LeakDetector();
	
	void			ReportAllocation(void* ptr);
	void			ReportRealloc(void* oldPtr, void* newPtr);
	void			ReportFree(void* ptr);
	
protected:
private:
	typedef HashTable<void*, StackTrace*> StackTraceMap;
	
	StackTraceMap	m_map;
	ObjectsPool<StackTrace>	m_pool;
	SpinLock		m_lock;
	Array<FilterRange> m_whitelist;
	
	bool			Filter(const StackTrace& stackTrace) const;
};

static LeakDetector* g_leakDetector = nullptr;





//-------------------------------------------------------------------------------------------
inline LeakDetector::LeakDetector()
	: m_map(kInitialAllocationsReserve), m_pool(kInitialAllocationsReserve, 8)	//20MB pool
{
	PopulateFilterList(m_whitelist);
}

//-------------------------------------------------------------------------------------------
inline LeakDetector::~LeakDetector()
{
	AtomicStorePtr((void**)&g_leakDetector, nullptr);	//TODO: this is still unsafe and can cause a situation when another thread already read the value of g_leakDetector, and we are destroying it
	
	if (m_map.GetStoredElementsCount())
	{
		StackTraceMap::Iterator it = m_map.GetFirstElement();
		
		while (it)
		{
			StackTrace* trace = it.GetValue();
			++it;
			
			if (Filter(*trace))
			{
				LogMemoryLeaks(kTracesCount, trace->m_traces);
			}
			
			m_pool.DeleteObject(trace);
		}
		
		g_leakDetector = nullptr;
	}
}

//-------------------------------------------------------------------------------------------
inline void LeakDetector::ReportAllocation(void* ptr)
{
	m_lock.Lock();
	
	StackTrace* pStackTrace = m_pool.CreateObject();
	GetStackBackTrace(kSkipFrames, kTracesCount, pStackTrace->m_traces);
	m_map.Insert(ptr, pStackTrace);
	
	m_lock.Unlock();
}

//-------------------------------------------------------------------------------------------
void LeakDetector::ReportRealloc(void* oldPtr, void* newPtr)
{
	if(oldPtr == newPtr)
	{
		return;
	}
	
	m_lock.Lock();
	
	StackTrace* pTrace = m_map.Find(oldPtr);
	if (pTrace)
	{
		m_map.Remove(oldPtr);
	}
	else
	{
		pTrace = m_pool.CreateObject();
	}
	
	GetStackBackTrace(kSkipFrames, kTracesCount, pTrace->m_traces);
	m_map.Insert(newPtr, pTrace);
	
	m_lock.Unlock();
}

//-------------------------------------------------------------------------------------------
inline void LeakDetector::ReportFree(void* ptr)
{
	if (!ptr)
	{
		return;
	}
	
	m_lock.Lock();
	
	StackTrace* pTrace = m_map.Find(ptr);	// some allocations are made by system libs (they are not tracked), but they are freed by client code
//	ASSERT(pTrace, "Attempt to free non registered allocation");
	
	if (pTrace)
	{
		m_map.Remove(ptr);
		m_pool.DeleteObject(pTrace);
	}
	
	m_lock.Unlock();
}

//-------------------------------------------------------------------------------------------
bool LeakDetector::Filter(const StackTrace& stackTrace) const
{
	if (!stackTrace.m_traces[0])
	{
		return false;
	}
	
	for (int i = 0; i < m_whitelist.GetSize(); ++i)
	{
		if ((size_t)stackTrace.m_traces[0] >= (size_t)m_whitelist[i].m_begin && (size_t)stackTrace.m_traces[0] < (size_t)m_whitelist[i].m_end)
		{
			return true;
		}
	}

	return false;
}



struct LeakDetectorFinalizer
{
	~LeakDetectorFinalizer()
	{
		delete g_leakDetector;
	}
};

LeakDetectorFinalizer g_leakDetectorFinalizer __attribute__ ((init_priority (101)));


//-------------------------------------------------------------------------------------------
__attribute__ ((constructor))
void InitLeakDetector()
{
	g_leakDetector = new LeakDetector;
}

//-------------------------------------------------------------------------------------------
void ReportAllocation(void* ptr)
{
	if (g_leakDetector)
	{
		g_leakDetector->ReportAllocation(ptr);
	}
}

//-------------------------------------------------------------------------------------------
void ReportReallocation(void* oldPtr, void* newPtr)
{
	if (g_leakDetector)
	{
		g_leakDetector->ReportRealloc(oldPtr, newPtr);
	}
}

//-------------------------------------------------------------------------------------------
void ReportDeallocation(void* ptr)
{
	if (g_leakDetector)
	{
		g_leakDetector->ReportFree(ptr);
	}
}