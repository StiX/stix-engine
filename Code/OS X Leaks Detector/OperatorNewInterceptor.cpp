
#include <new>
#include <dlfcn.h>
#include "MemoryLeakDetector.h"


typedef void*	(*SystemNew)(std::size_t size);
typedef void	(*SystemDelete)(void* ptr);


static SystemNew	systemNew = 0;
static SystemDelete	systemDelete = 0;


//-------------------------------------------------------------------------------------------
__attribute__ ((constructor))
void InitCppInterceptors()
{
	systemNew		= (SystemNew)dlsym(RTLD_NEXT, "_Znwm");
	systemDelete	= (SystemDelete)dlsym(RTLD_NEXT, "_ZdlPv");
}


//-------------------------------------------------------------------------------------------
void* operator new(std::size_t size)
{
	void* ptr = systemNew(size);
	ReportAllocation(ptr);
	
	return ptr;
}

//-------------------------------------------------------------------------------------------
void operator delete(void* ptr) noexcept
{
	ReportDeallocation(ptr);
	systemDelete(ptr);
}