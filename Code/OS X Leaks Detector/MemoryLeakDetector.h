
#pragma once


#ifdef __cplusplus
extern "C"
{
#endif

	void ReportAllocation(void* ptr);
	void ReportReallocation(void* oldPtr, void* newPtr);
	void ReportDeallocation(void* ptr);

#ifdef __cplusplus
}
#endif