
#include <cstdio>
#include <cstdlib>
#include "SystemUtil.h"


struct LeakDetector
{
	LeakDetector();
	~LeakDetector();
};

static LeakDetector g_leakDetector;


//-------------------------------------------------------------------------------------------
inline LeakDetector::LeakDetector()
{
	system("set env MallocStackLogging 1");
}

//-------------------------------------------------------------------------------------------
inline LeakDetector::~LeakDetector()
{
	char cmd[128];
	sprintf(cmd, "leaks \"%s\" -exclude=glfwInit -q", GetExecutableName());
	
	system(cmd);
}