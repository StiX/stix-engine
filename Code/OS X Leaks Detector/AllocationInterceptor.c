
#include <dlfcn.h>
#include "MemoryLeakDetector.h"


typedef void*	(*SystemMalloc)(size_t size);
typedef void*	(*SystemCalloc)(size_t num_items, size_t size);
typedef void*	(*SystemValloc)(size_t size);
typedef void*	(*SystemRealloc)(void* ptr, size_t size);
typedef int		(*SystemMemalign)(void **memptr, size_t alignment, size_t size);
typedef void	(*SystemFree)(void* ptr);

typedef char*	(*SystemStrdup)(const char *s);
typedef char*	(*SystemStrndup)(const char *s, size_t n);


static SystemMalloc		systemMalloc = 0;
static SystemCalloc		systemCalloc = 0;
static SystemValloc		systemValloc = 0;
static SystemRealloc	systemRealloc = 0;
static SystemMemalign	systemMemalign = 0;
static SystemFree		systemFree = 0;
static SystemStrdup		systemStrdup = 0;
static SystemStrndup	systemStrndup = 0;


//-------------------------------------------------------------------------------------------
__attribute__ ((constructor))
void InitCInterceptors()
{
	systemMalloc	= (SystemMalloc)dlsym(RTLD_NEXT, "malloc");
	systemCalloc	= (SystemCalloc)dlsym(RTLD_NEXT, "calloc");
	systemValloc	= (SystemValloc)dlsym(RTLD_NEXT, "valloc");
	systemRealloc	= (SystemRealloc)dlsym(RTLD_NEXT, "realloc");
	systemMemalign	= (SystemMemalign)dlsym(RTLD_NEXT, "posix_memalign");
	systemFree		= (SystemFree)dlsym(RTLD_NEXT, "free");
	systemStrdup	= (SystemStrdup)dlsym(RTLD_NEXT, "strdup");
	systemStrndup	= (SystemStrndup)dlsym(RTLD_NEXT, "strndup");
}


//-------------------------------------------------------------------------------------------
void* malloc(size_t size)
{
	void* ptr = systemMalloc(size);
	ReportAllocation(ptr);
	
	return ptr;
}

//-------------------------------------------------------------------------------------------
void* calloc(size_t num_items, size_t size)
{
	void* ptr = systemCalloc(num_items, size);
	ReportAllocation(ptr);

	return ptr;
}

//-------------------------------------------------------------------------------------------
void* valloc(size_t size)
{
	void* ptr = systemValloc(size);
	ReportAllocation(ptr);
	
	return ptr;
}

//-------------------------------------------------------------------------------------------
void* realloc(void* ptr, size_t size)
{
	void* newPtr = systemRealloc(ptr, size);
	ReportReallocation(ptr, newPtr);
	
	return newPtr;
}

//-------------------------------------------------------------------------------------------
int posix_memalign(void **memptr, size_t alignment, size_t size)
{
	int result = systemMemalign(memptr, alignment, size);
	
	if (result == 0)
	{
		ReportAllocation(*memptr);
	}
	
	return result;
}

//-------------------------------------------------------------------------------------------
void free(void* ptr)
{
	ReportDeallocation(ptr);
	systemFree(ptr);
}

//-------------------------------------------------------------------------------------------
char* strdup(const char* s)
{
	char* str = systemStrdup(s);
	ReportAllocation(str);
	
	return str;
}

//-------------------------------------------------------------------------------------------
char* strndup(const char* s, size_t n)
{
	char* str = systemStrndup(s, n);
	ReportAllocation(str);
	
	return str;
}