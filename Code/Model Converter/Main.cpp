
#ifdef __WIN__
#	include <vld.h>
#endif
#include "Log.h"
#include "Str.h"
#include "SystemUtil.h"
#include "FileSystem.h"
#include "FBXConverter.h"


int main(int argc, const char * argv[])
{
	String exePath(GetExecutableDirPath());
	SetWorkDir(exePath.c_str());

	log_init("../Model Converter Log.html");

	for (int i = 1; i < argc; ++i)
	{
		FBXConverter converter(argv[i]);

		String path(argv[i]);
		path.StripFileExtension();
		path += ".smf";

		converter.SaveToFile(path.c_str());
	}

	log_shutdown();

	return 0;
}