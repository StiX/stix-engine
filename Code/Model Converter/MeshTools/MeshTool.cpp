
#include "MeshTool.h"
#include <cstdlib>
#include <cstring>
#include "Debug.h"


//-------------------------------------------------------------------------------------------
u32 IndexMesh(u8* pVertexData, u16* pIndices, u32 verticesCount, u32 vertexSize)
{
	ASSERT(pIndices);
	ASSERT(pVertexData);
	ASSERT(vertexSize > 0);
	ASSERT(verticesCount > 0);

	//const u32 kIndexSize = (verticesCount > 255) ? 2 : 1;

	u32 uniqueCount = verticesCount;
	u16 vertexId = 0;

	u8* pReferencedVerts = (u8*)malloc(verticesCount * sizeof(*pReferencedVerts));
	memset(pReferencedVerts, 0, verticesCount * sizeof(*pReferencedVerts));

	for (u32 i = 0; i < verticesCount; ++i)
	{
		if (pReferencedVerts[i] == 0)
		{
			pIndices[i] = vertexId;

			for (u32 j = i + 1; j < verticesCount; ++j)
			{
				if (memcmp(&pVertexData[i * vertexSize], &pVertexData[j * vertexSize], vertexSize) == 0)
				{
					pIndices[j] = vertexId;

					--uniqueCount;

					pReferencedVerts[j] = true;
				}
			}

			++vertexId;
		}
	}

	u32 copyPos = 0;
	for (u32 i = 0; i < verticesCount; ++i)
	{
		if (pReferencedVerts[i] == 0)
		{
			memcpy(&pVertexData[copyPos * vertexSize], &pVertexData[i * vertexSize], vertexSize);
			++copyPos;
		}
	}

	free(pReferencedVerts);

	return uniqueCount;
}