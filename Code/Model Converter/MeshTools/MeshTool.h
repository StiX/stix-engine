
#pragma once

#include "Types.h"


u32	IndexMesh(u8* pVertexData, u16* pIndices, u32 verticesCount, u32 vertexSize);