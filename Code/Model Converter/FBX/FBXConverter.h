
#pragma once

#include <fbxsdk.h>
#include "Array.h"

class Mesh;


class FBXConverter
{
public:
	explicit	FBXConverter(const char* fbxFilePath);
				~FBXConverter();

	void		SaveToFile(const char* filePath) const;

protected:
private:
	double		m_conversionScale;
	FbxNode*	m_pRootNode;
	const char*	m_pFilePath;
	Array<Mesh>	m_meshes;

	bool		Convert(FbxNode* pRootNode);
	bool		ConvertNodesRecursively(FbxNode* pRootNode);
	bool		ConvertNode(FbxNode* pNode);
};