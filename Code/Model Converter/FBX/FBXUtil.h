
#pragma once

#include <fbxsdk.h>
#include "Types.h"
#include "Vector3D.h"
#include "float16.h"

typedef Vector2D<float16> vec2h;


void	PrintHierarchy(FbxNode* pRootNode, FbxString& string, int depth = 0);
u32		GetSkeletonBonesCount(const FbxNode* pRoot);
u32		GetConvertibleNodesCount(const FbxNode* pRoot);

vec3f	GetVertexPos(const FbxMesh* pMesh, const FbxMatrix& globalTransform, u32 controlPointIndex);
vec2h	GetUV(FbxMesh* pMesh, u32 triangleIndex, u32 triangleVertIndex, u32 controlPointIndex);

vec3f	FbxVec4ToVec3(const FbxVector4& vec);
vec2h	FbxVec2ToVec2h(const FbxVector2& vec);