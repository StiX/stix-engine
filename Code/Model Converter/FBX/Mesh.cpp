
#include "Mesh.h"
#include "Log.h"
#include "File.h"
#include "FBXUtil.h"
#include "MeshTool.h"
#include "WriteStream.h"
#include "file_write_stream.h"
#include "VertexDescription.h"


//-------------------------------------------------------------------------------------------
void CheckUnsupportedFeatures(const FbxMesh* pMesh)
{
	if (pMesh->GetElementUVCount() > 1)
	{
		log_warning("FBX", "Node %s has %d UV elements. Only fist set will be used.", pMesh->GetNode()->GetName(), pMesh->GetElementUVCount());
	}

	if (pMesh->GetElementNormalCount() > 1)
	{
		log_warning("FBX", "Node %s has %d normal elements. Only fist set will be used.", pMesh->GetNode()->GetName(), pMesh->GetElementNormalCount());
	}

	if (pMesh->GetNode()->GetMaterialCount() > 1)
	{
		log_warning("FBX", "Node %s has %d materials. Only first one will be used.", pMesh->GetNode()->GetName(), pMesh->GetNode()->GetMaterialCount());
	}

	//if (pMesh->GetNode()->GetSrcObjectCount<FbxSurfaceMaterial>() > 1)
	//{
	//	log_warning("FBX", "Unsupported materials count %d!", pMesh->GetNode()->GetSrcObjectCount<FbxSurfaceMaterial>());
	//	hasUnsupported = true;
	//}

	if (pMesh->GetElementPolygonGroupCount())
	{
		log_warning("FBX", "Node %s has polygon groups, they are partially supported");

		const FbxGeometryElementPolygonGroup* pPolyGrp = pMesh->GetElementPolygonGroup(0);
		switch (pPolyGrp->GetMappingMode())
		{
		case FbxGeometryElement::eByPolygon:
			if (pPolyGrp->GetReferenceMode() == FbxGeometryElement::eIndex)
			{
				break;
			}
		default:
			log_warning("FBX", "This node's group mapping (id %d) is not supported.", pPolyGrp->GetMappingMode());
			break;
		}
	}
}

//-------------------------------------------------------------------------------------------
Mesh::Mesh(FbxMesh* pFbxMesh, double conversionScale)
{
	CheckUnsupportedFeatures(pFbxMesh);

	FbxAMatrix scaleMat;
	scaleMat.SetS(FbxVector4(conversionScale, conversionScale, conversionScale, 1.0));
	m_globalTransform = scaleMat * pFbxMesh->GetNode()->EvaluateGlobalTransform();

	CreateVertexDescription(pFbxMesh);
	ExtractVertexData(pFbxMesh);
	IndexVertexData(pFbxMesh);
}

//-------------------------------------------------------------------------------------------
Mesh::~Mesh()
{
	free(m_pIndexData);
	free(m_pVertexData);
}

//-------------------------------------------------------------------------------------------
void Mesh::WriteToFile(File& file) const
{
	file.Write(m_verticesCount);
	file.Write(m_indicesCount);

	file_write_stream file_stream(file);
	m_vertexDescription.serialize(file_stream);

	file.Write(m_pVertexData, m_verticesCount * m_vertexDescription.vertexSize);

	const u32 kIndexSize = sizeof(u16);		//TODO: fix it
	file.Write(m_pIndexData, m_indicesCount * kIndexSize);
}

//-------------------------------------------------------------------------------------------
void Mesh::CreateVertexDescription(FbxMesh* pFbxMesh)
{
	pFbxMesh->ComputeBBox();
	const FbxDouble3 kBBoxMin = m_globalTransform.MultNormalize(pFbxMesh->BBoxMin.Get());
	const FbxDouble3 kBBoxMax = m_globalTransform.MultNormalize(pFbxMesh->BBoxMax.Get());

	const bool kBBoxMinOutOfRange = kBBoxMin[0] < -kFloat16Max || kBBoxMin[1] < -kFloat16Max || kBBoxMin[2] < -kFloat16Max;
	const bool kBBoxMaxOutOfRange = kBBoxMax[0] > kFloat16Max || kBBoxMax[1] > kFloat16Max || kBBoxMax[2] > kFloat16Max;
	const bool kVertexPosIsFloat16 = (kBBoxMinOutOfRange || kBBoxMaxOutOfRange) ? false : true;

	m_vertexDescription.AddAttribute(VertexAttribute::Position, kVertexPosIsFloat16 ? VertexAttribute::HalfFloat : VertexAttribute::Float, 3);

	if (pFbxMesh->GetElementUVCount())
	{
		m_vertexDescription.AddAttribute(VertexAttribute::UV, VertexAttribute::HalfFloat, 2);
	}
}

//-------------------------------------------------------------------------------------------
void Mesh::ExtractVertexData(FbxMesh* pFbxMesh)
{
	const u32 kVerticesInPolygonCount = 3;
	const u32 kPolygonsCount = pFbxMesh->GetPolygonCount();
	const u32 kVertexDataSize = kPolygonsCount * kVerticesInPolygonCount * m_vertexDescription.vertexSize;

	m_pVertexData = malloc(kVertexDataSize);
	WriteStream meshData(m_pVertexData, kVertexDataSize);

	for (u32 i = 0; i < kPolygonsCount; ++i)
	{
		for (u32 j = 0; j < kVerticesInPolygonCount; ++j)
		{
			const int kControlPointIndex = pFbxMesh->GetPolygonVertex(i, j);

			// Write position
			const vec3f kPos = GetVertexPos(pFbxMesh, m_globalTransform, kControlPointIndex);

			if (m_vertexDescription.attributes[0].format == VertexAttribute::HalfFloat)
			{
				meshData.Write(float16(kPos.x));
				meshData.Write(float16(kPos.y));
				meshData.Write(float16(kPos.z));
			}
			else
			{
				meshData.Write(kPos);
			}

			// Write UV
			if (pFbxMesh->GetElementUVCount())
			{
				vec2h uv = GetUV(pFbxMesh, i, j, kControlPointIndex);
				meshData.Write(uv);
			}

			// TODO: other vertex attributes
		}
	}
}

//-------------------------------------------------------------------------------------------
void Mesh::IndexVertexData(FbxMesh* pFbxMesh)
{
	const u32 kIndexSize = sizeof(u16);
	const u32 kVerticesInPolygonCount = 3;
	m_indicesCount = pFbxMesh->GetPolygonCount() * kVerticesInPolygonCount;
	m_pIndexData = malloc(kIndexSize * m_indicesCount);

	m_verticesCount = IndexMesh((u8*)m_pVertexData, (u16*)m_pIndexData, m_indicesCount, m_vertexDescription.vertexSize);

	bool kIndexed = (m_verticesCount * m_vertexDescription.vertexSize + m_indicesCount * kIndexSize) < m_indicesCount * m_vertexDescription.vertexSize;
}