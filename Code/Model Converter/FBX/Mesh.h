
#pragma once

#include <fbxsdk.h>
#include "Types.h"
#include "VertexDescription.h"

class File;
struct VertexDescription;


class Mesh
{
public:
						Mesh(FbxMesh* pFbxMesh, double conversionScale);
						~Mesh();

	void				WriteToFile(File& file) const;

protected:
private:
	void*				m_pIndexData;
	u32					m_indicesCount;
	void*				m_pVertexData;
	u32					m_verticesCount;
	FbxMatrix			m_globalTransform;
	VertexDescription	m_vertexDescription;

	void				CreateVertexDescription(FbxMesh* pFbxMesh);
	void				ExtractVertexData(FbxMesh* pFbxMesh);
	void				IndexVertexData(FbxMesh* pFbxMesh);
};