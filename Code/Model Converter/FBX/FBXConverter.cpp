
#include "FBXConverter.h"
#include "Log.h"
#include "Mesh.h"
#include "FBXUtil.h"
#include "FileSystem.h"
#include "StringHash.h"


//-------------------------------------------------------------------------------------------
FBXConverter::FBXConverter(const char* fbxFilePath)
	: m_pFilePath(fbxFilePath)
{
	FbxManager* pFbxManager = FbxManager::Create();

	FbxIOSettings* pIOSettings = FbxIOSettings::Create(pFbxManager, IOSROOT);
	pFbxManager->SetIOSettings(pIOSettings);

	FbxImporter* pImporter = FbxImporter::Create(pFbxManager, "");
	if (!pImporter->Initialize(fbxFilePath, -1, pIOSettings))
	{
		pFbxManager->Destroy();
		return;
	}

	FbxScene* pScene = FbxScene::Create(pFbxManager, "Scene");
	pImporter->Import(pScene);
	pImporter->Destroy();

	FbxAxisSystem::OpenGL.ConvertScene(pScene);

	m_pRootNode = pScene->GetRootNode();
	m_conversionScale = FbxSystemUnit::m.GetConversionFactorFrom(pScene->GetGlobalSettings().GetSystemUnit());

	Convert(m_pRootNode);
}

//-------------------------------------------------------------------------------------------
FBXConverter::~FBXConverter()
{
	if (m_pRootNode)
	{
		m_pRootNode->GetFbxManager()->Destroy();
	}
}

//-------------------------------------------------------------------------------------------
void FBXConverter::SaveToFile(const char* filePath) const
{
	ASSERT(filePath);

	File outFile = CreateFile(filePath, F_ACCESS_WRITE);

	for (u32 i = 0; i < m_meshes.GetSize(); ++i)
	{
		m_meshes[i].WriteToFile(outFile);
	}
}

//-------------------------------------------------------------------------------------------
bool FBXConverter::Convert(FbxNode* pRootNode)
{
	if (!pRootNode)
	{
		return false;
	}
	
	FbxString string;
	PrintHierarchy(pRootNode, string);
	log_message("FBX", "Hierarchy:\n%s", string.Buffer());

	if (GetConvertibleNodesCount(pRootNode) == 0)
	{
		log_warning("FBX", "There are no nodes to convert in %s", m_pFilePath);
		return false;
	}

	ConvertNodesRecursively(pRootNode);

	return true;
}

//-------------------------------------------------------------------------------------------
bool FBXConverter::ConvertNodesRecursively(FbxNode* pRootNode)
{
	bool result = true;

	for (int i = 0; i < pRootNode->GetChildCount(); ++i)
	{
		FbxNode* pChildNode = pRootNode->GetChild(i);

		StringHash error_context(pChildNode->GetName());
		log_set_error_context(&error_context);
		
		result &= ConvertNode(pChildNode);
		log_set_error_context(nullptr);

		result &= ConvertNodesRecursively(pChildNode);
	}

	return result;
}

//-------------------------------------------------------------------------------------------
bool FBXConverter::ConvertNode(FbxNode* pNode)
{
	if (const FbxNodeAttribute* pNodeAttrib = pNode->GetNodeAttribute())
	{
		FbxGeometryConverter geometryConverter(pNode->GetFbxManager());

		switch (FbxNodeAttribute::EType type = pNodeAttrib->GetAttributeType())
		{
		case FbxNodeAttribute::eMesh:
		case FbxNodeAttribute::eNurbs:
		case FbxNodeAttribute::eNurbsSurface:
		case FbxNodeAttribute::ePatch:
			if (geometryConverter.Triangulate(pNode->GetNodeAttribute(), true))
			{
				m_meshes.EmplaceBack(pNode->GetMesh(), m_conversionScale);
			}
			else
			{
				log_error("FBX", "Failed to triangulate mesh at %s node! Skipping!", pNode->GetName());
				return false;
			}

		case FbxNodeAttribute::eSkeleton:
			return true;

		case FbxNodeAttribute::eNull:
			return false;

		default:
			log_warning("FBX", "Non supported node type id %d!\n", type);	// TODO: map node type to string
			return false;
		}
	}

	return false;
}