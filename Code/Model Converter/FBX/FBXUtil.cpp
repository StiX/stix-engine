
#include "FBXUtil.h"
#include "Log.h"


//-------------------------------------------------------------------------------------------
void PrintHierarchy(FbxNode* pNode, FbxString& string, int depth)
{
	for (int i = 0; i < depth; i++)
	{
		string += "  ";
	}

	if (const FbxNodeAttribute* pNodeAttrib = pNode->GetNodeAttribute())
	{
		switch (pNodeAttrib->GetAttributeType())
		{
		case FbxNodeAttribute::eMesh:
			string += "MESH:  ";
			break;

		case FbxNodeAttribute::eSkeleton:
			string += "SKELETON:  ";
			break;

		case FbxNodeAttribute::eNurbs:
			string += "NURB:  ";
			break;

		case FbxNodeAttribute::eNurbsSurface:
			string += "NURB SURFACE:  ";
			break;

		case FbxNodeAttribute::ePatch:
			string += "PATCH:  ";
			break;

		case FbxNodeAttribute::eMarker:
			string += "MARKER:  ";
			break;

		case FbxNodeAttribute::eNull:
			string += "NULL:  ";
			break;

		default:
			string += pNodeAttrib->GetAttributeType();
			break;
		}
	}

	string += pNode->GetName();
	string += "\n";

	for (int i = 0; i < pNode->GetChildCount(); i++)
	{
		PrintHierarchy(pNode->GetChild(i), string, depth + 1);
	}
}

//-------------------------------------------------------------------------------------------
u32 GetSkeletonBonesCount(const FbxNode* pRoot)
{
	u32 bonesCount = 0;

	if (const FbxNodeAttribute* pNodeAttrib = pRoot->GetNodeAttribute())
	{
		if (pNodeAttrib->GetAttributeType() == FbxNodeAttribute::eSkeleton)
		{
			++bonesCount;
		}
	}

	for (int i = 0; i < pRoot->GetChildCount(); ++i)
	{
		bonesCount += GetSkeletonBonesCount(pRoot->GetChild(i));
	}

	return bonesCount;
}

//-------------------------------------------------------------------------------------------
u32 GetConvertibleNodesCount(const FbxNode* pRoot)
{
	u32 convertibleNodesCount = 0;

	for (int i = 0; i < pRoot->GetChildCount(); ++i)
	{
		const FbxNode* pChildNode = pRoot->GetChild(i);
		if (const FbxNodeAttribute* pNodeAttrib = pChildNode->GetNodeAttribute())
		{
			switch (FbxNodeAttribute::EType type = pNodeAttrib->GetAttributeType())
			{
			case FbxNodeAttribute::eMesh:
			case FbxNodeAttribute::eNurbs:
			case FbxNodeAttribute::eNurbsSurface:
			case FbxNodeAttribute::ePatch:
			case FbxNodeAttribute::eSkeleton:
				++convertibleNodesCount;
				break;

			default:
				break;
			}
		}

		convertibleNodesCount += GetConvertibleNodesCount(pChildNode);
	}

	return convertibleNodesCount;
}

//-------------------------------------------------------------------------------------------
vec3f GetVertexPos(const FbxMesh* pMesh, const FbxMatrix& globalTransform, u32 controlPointIndex)
{
	const FbxVector4* pVertices = pMesh->GetControlPoints();

	FbxVector4 pos = pVertices[controlPointIndex];
	pos = globalTransform.MultNormalize(pos);

	return FbxVec4ToVec3(pos);
}

//-------------------------------------------------------------------------------------------
vec2h GetUV(FbxMesh* pMesh, u32 triangleIndex, u32 triangleVertIndex, u32 controlPointIndex)
{
	for (int uv = 0; uv < pMesh->GetElementUVCount(); ++uv)
	{
		const FbxGeometryElementUV* pUV = pMesh->GetElementUV(uv);

		switch (pUV->GetMappingMode())
		{
		case FbxGeometryElement::eByControlPoint:
			switch (pUV->GetReferenceMode())
			{
			case FbxGeometryElement::eDirect:
				return FbxVec2ToVec2h(pUV->GetDirectArray().GetAt(controlPointIndex));

			case FbxGeometryElement::eIndexToDirect:
			{
				int id = pUV->GetIndexArray().GetAt(controlPointIndex);
				return FbxVec2ToVec2h(pUV->GetDirectArray().GetAt(id));
			}

			default:
				log_error("FBX", "Unsupported eByControlPoint Reference Mode %d for UVs!", pUV->GetReferenceMode());
				return vec2h(float16(1.0f));
			}
			break;

		case FbxGeometryElement::eByPolygonVertex:
			switch (pUV->GetReferenceMode())
			{
			case FbxGeometryElement::eDirect:
			case FbxGeometryElement::eIndexToDirect:
			{
				const int textureUVIndex = pMesh->GetTextureUVIndex(triangleIndex, triangleVertIndex);
				return FbxVec2ToVec2h(pUV->GetDirectArray().GetAt(textureUVIndex));
			}

			default:
				log_error("FBX", "Unsupported eByPolygonVertex Reference Mode %d for UVs!", pUV->GetReferenceMode());
				return vec2h(float16(1.0f));
			}

			break;

		default:
			log_error("FBX", "Unsupported Mapping Mode %d for UVs!", pUV->GetMappingMode());
			return vec2h(float16(1.0f));
		}
	}

	log_error("FBX", "Error getting uv!");
	return vec2h(float16(1.0f));
}

//-------------------------------------------------------------------------------------------
vec3f FbxVec4ToVec3(const FbxVector4& vec)
{
	return vec3f((float)vec[0], (float)vec[1], (float)vec[2]);
}

//-------------------------------------------------------------------------------------------
vec2h FbxVec2ToVec2h(const FbxVector2& vec)
{
	return vec2h(float16((float)vec[0]), float16((float)vec[1]));
}