# README #

C++ Game Engine targeting PC and high spec mobile devices (capable of OpenGL ES 3.0+).

### License ###
This project is licensed under [MIT](http://opensource.org/licenses/MIT) license.

### Contribution guidelines ###
* [List of allowed C++11 features](https://bitbucket.org/StiX/stix-engine/wiki/List of allowed C++11 features)


### Windows build status: [![Build status](https://ci.appveyor.com/api/projects/status/j5yuv525v6vbr5m1/branch/master?svg=true)](https://ci.appveyor.com/project/StiX/stix-engine/branch/master)