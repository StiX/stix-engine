
local function split(str, pat)
	local t = {}
	local fpat = "(.-)" .. pat
	local last_end = 1
	local s, e, cap = str:find(fpat, 1)
	while s do
		if s ~= 1 or cap ~= "" then
			table.insert(t,cap)
		end
		last_end = e+1
		s, e, cap = str:find(fpat, last_end)
	end
	if last_end <= #str then
		cap = str:sub(last_end)
		table.insert(t, cap)
	end
	return t
end

local platformPrefixes = {"pc", "ios", "android"}

local function StripPlatformPrefix(filepath)
    local str = string.gsub(filepath, ".+/", "")
	local subStrings = split(str, '%.')
	local extension = subStrings[#subStrings]
	
	for i = 1, #subStrings do
		for j = 1, #platformPrefixes do
			if subStrings[i] == platformPrefixes[j] then
				for k = #subStrings, i, -1 do
					table.remove(subStrings, k)
				end
				break
			end
		end
	end
	
	return table.concat(subStrings, "."), extension
end

function table.fastRemove(t, index)
	t[index] = t[#t]
	table.remove(t)
end

function CopyPlatformResources(sourcePath, destPath, platform)
	os.rmdir(destPath)

	files = os.matchfiles(sourcePath.."/**.*")
	local renameList = {}
	
	-- clear other platform files
	local i = 1
	while i <= #files do
		local pattern, extension = StripPlatformPrefix(files[i])
		local index = string.find(files[i], pattern, 1, true)
		pattern = string.sub(files[i], 1, index - 1)..pattern
		
		local matchingIndexes = {i}
		
		for j = i + 1, #files do
			if string.find(files[j], pattern, 1, true) ~= nil then
				table.insert(matchingIndexes, j) 
			end
		end
		
		if #matchingIndexes > 1 then
		    local oldRenameListSize = #renameList
		    
		    for j = 1, #matchingIndexes do
		        local index = matchingIndexes[j]
		        if string.find(files[index], "."..platform.."."..extension, 1, true) ~= nil then
		            table.insert(renameList, files[index])
		            break
		        end
		    end
	    
	        --insert original file without platform index, if there were found no overload
            if oldRenameListSize == #renameList then
                table.insert(files, files[i])
            end
	    
	    	for j = #matchingIndexes, 1, -1 do
    			table.fastRemove(files, matchingIndexes[j])
		    end
		else
		    i = i + 1    
		end
	end
	
	for i = 1, #files do
	    local filename = string.gsub(files[i], ".+/", "")
	    local index = string.find(files[i], filename, 1, true)
	    local deltaPath = string.sub(files[i], sourcePath:len() + 1, index - 1)

		os.execute("{COPY} \""..files[i].."\" \""..destPath..deltaPath.."\"")
	end
	
	for i = 1, #renameList do
	    local filename = string.gsub(renameList[i], ".+/", "")
	    local index = string.find(renameList[i], filename, 1, true)
	    local deltaPath = string.sub(renameList[i], sourcePath:len() + 1, index - 1)
	    local strippedFilename = string.gsub(filename, "%."..platform, "", 1)

		os.execute("{COPY} \""..renameList[i].."\" \""..destPath..deltaPath.."\"")
		
		local fileDir = destPath..deltaPath
		local renameCmd = path.translate(fileDir..filename.."\" \""..fileDir..strippedFilename.."\"")
		os.execute("{MOVE} \""..renameCmd)
	end
end