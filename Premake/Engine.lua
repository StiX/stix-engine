
dofile "PremakeUtil.lua"
dofile "ResourcesUpdater.lua"


-- *****	FOUNDATION PROJECT	***** --
project "Foundation"
	language	"C++"
	kind		"StaticLib"
	
	targetdir ("%{sln.location}/obj/%{cfg.platform}/%{cfg.buildcfg}/%{prj.name}")
	
	files {
		"../Code/Foundation/**.h",
		"../Code/Foundation/**.c",
		"../Code/Foundation/**.cc",
		"../Code/Foundation/**.cpp",
	}
	
	includedirs {
		"../Code/Foundation",
		"../Code/Foundation/**",
	}
	
	DeclareCompilationFlags()
	EnableFoundationFlags()
	

-- *****	ENGINE PROJECT	***** --
project "Engine"
	language	"C++"
	kind "StaticLib"
	targetdir ("%{sln.location}/obj/%{cfg.platform}/%{cfg.buildcfg}/%{prj.name}")
	
	files {
		"../Code/Engine/**.h",
		"../Code/Engine/**.c",
		"../Code/Engine/**.cc",
		"../Code/Engine/**.cpp",
	}
	
	includedirs {
		"../Code/Foundation",
		"../Code/Foundation/**",
		
		"../Code/Engine",
		"../Code/Engine/**",
		
		"../Code/Application",
	}
	
	filter { "action:vs*", "action:not android" }
		includedirs { "../Code/Dependencies/VLD/include" }
		
	filter { "Debug", "action:vs*", "action:not android" }
		defines { "RENDERER_DEBUG" }
		
	filter {"options:osx"}
		files {
			"../Code/Dependencies/glfw/**.h",
			"../Code/Dependencies/glfw/**.c",
			"../Code/Dependencies/glfw/**.m",
		}
	
	DeclareCompilationFlags()

	

-- *****	APPLICATION PROJECT	***** --
project "Application"
	language "C++"
	
	for dir = 1, #g_gameDirsToInclude do
		files {
			g_workDir..g_gameDirsToInclude[dir].."/**.h",
			g_workDir..g_gameDirsToInclude[dir].."/**.c",
			g_workDir..g_gameDirsToInclude[dir].."/**.cc",
			g_workDir..g_gameDirsToInclude[dir].."/**.cpp"
		}
		
		includedirs {
			g_workDir..g_gameDirsToInclude[dir],
			g_workDir..g_gameDirsToInclude[dir].."/**/"
		}
	end
	
	includedirs {
		"../Code/Foundation",
		"../Code/Foundation/**",
		
		"../Code/Engine",
		"../Code/Engine/**",
	}
	
	dependson {
		"Engine",
		"Foundation",
	}
	
	links {
		"Engine",
		"Foundation",
	}
	
	filter "action:vs*"
		kind "WindowedApp"
		
		links {
			"OpenGL32",
			"Dbghelp"
		}
		
	filter {"action:xcode*"}
		kind "ConsoleApp"
	
		links {
			"IOKit.framework",
			"CoreVideo.framework"
		}
		
	filter {"options:osx"}
		links {
			"Cocoa.framework",
			"OpenGL.framework",
		}
		
	filter {"options:ios"}
		kind "WindowedApp"
		
		files {
			"../Code/Engine/**.m",
			"../Code/Engine/**.mm",
			"../Code/Engine/**.plist",
			"../Code/Engine/**.storyboard",
		}
		
		links {
			"UIKit.framework",
			"GLKit.framework",
			"OpenGLES.framework",
			"CoreFoundation.framework",
		}
		
		xcodebuildsettings
		{			
			["INFOPLIST_FILE"] = "$(SRCROOT)/../../Code/Engine/Entry/iOS/Info.plist";
			["PRODUCT_BUNDLE_IDENTIFIER"] = "com.StiX.Engine";
			["CODE_SIGN_IDENTITY"] = "iPhone Developer";
			["TARGETED_DEVICE_FAMILY"] = "1,2";
		}
		
	filter "action:android"
		kind "SharedLib"
		
		links {
			"GLESv3",
			"EGL",
			"m",
		}
		
		targetdir ("%{sln.location}/obj/%{cfg.platform}/%{cfg.buildcfg}/%{prj.name}")
	
	DeclareCompilationFlags()
	EnableExecutableFlags()
	
	
if _ACTION == "android" then
	project "Packaging"
		kind "Packaging"
		
		links {
			"Application",
		}
		
		files {
			"%{sln.location}/res/values/strings.xml",
            "%{sln.location}/assets/**.*",
		}
		
		CopyPlatformResources(g_workDir.."/Game Data", g_workDir.."Projects/android/assets/Game Data/", "android")
		
		os.execute("{COPY} ../Code/Engine/Entry/Android/res \""..g_workDir.."/Projects/android/res\"")

		antbuild (g_engineDir.."Code/Engine/Entry/Android/build.xml")
		androidmanifest (g_engineDir.."Code/Engine/Entry/Android/AndroidManifest.xml")
		antproperties (g_engineDir.."Code/Engine/Entry/Android/project.properties")
		
		symbolspath "%{sln.location}/obj/%{cfg.buildcfg}/Application"
end