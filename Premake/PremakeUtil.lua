
require "vsandroid"

g_workDir = nil or g_workDir
g_engineDir = nil or g_engineDir
g_gameDirsToInclude = nil or g_gameDirsToInclude
g_platformType = nil or g_platformType

newoption {
	trigger     = "ios",
	description = "Xcode project type",
}

newoption {
	trigger     = "osx",
	description = "Xcode project type",
}

local function GenerateSolution(projects)
	workspace "Engine"
		configurations { "Debug", "Release" }
		
		if _ACTION == "android" then
			startproject "Packaging"
		else
			startproject "Application"
		end
		
		filter {"options:ios"}
			location ("Projects/iOS")
		
		filter {"options:osx"}
			location ("Projects/OSX")
			
		filter "action:android"
			system "vsandroid"
			architecture "arm64"
			
		filter {"options:not ios", "options:not osx"}
			location ("Projects/".._ACTION)
			
		filter "action:vs*"
			platforms { "x64" }

			configuration "x64"
				architecture "x64"
				
		for i = 1, #projects do
			dofile (projects[i])
		end
end

local function ExcludeEverythingBut(currentPlatform)
	platforms = { "Win", "OSX", "iOS", "Android", "Mobile" }
	
	platformParents = {}
	platformParents["Win"] = nil
	platformParents["OSX"] = "Apple"
	platformParents["iOS"] = "Apple"
	platformParents["Apple"] = "POSIX"
	platformParents["POSIX"] = nil
	platformParents["Linux"] = "POSIX"
	platformParents["Android"] = "Linux"
	
	platformAlias = {}
	platformAlias["iOS"] = "Mobile"
	platformAlias["Android"] = "Mobile"
	
	
	for i = 1, table.getn(platforms) do
		platform = platforms[i]
		if platform ~= currentPlatform and platformAlias[currentPlatform] ~= platform then
			repeat
				excludes { "../Code/**/"..platform.."/**" }
				removeincludedirs { "../Code/**/"..platform }
				
				platform = platformParents[platform]
				
				local parent = platformParents[currentPlatform]
				while parent ~= nil do
					if platform == parent then
						platform = nil
						break
					end
					
					parent = platformParents[parent]
				end
			until platform == nil			
		end
	end
end

function GenerateProjectFiles(gameDirsToInclude)
	g_workDir = debug.getinfo(2).source:match("@?(.*/)")
	g_engineDir = debug.getinfo(1).source:match("@?(.*/)")
	g_engineDir = g_engineDir.."../"
	g_engineDir = path.normalize(g_engineDir).."/"
	
	g_gameDirsToInclude = gameDirsToInclude
	
	if _OPTIONS["ios"] ~= nil or _OPTIONS["osx"] ~= nil then
		_ACTION = "xcode4"
	end
	
	local projects = {"Premake/Engine.lua"}
	
	if _OPTIONS["ios"] == nil and _ACTION ~= "android" then
		table.insert(projects, "Premake/Model Converter.lua")
		table.insert(projects, "Premake/Testbed.lua")
	end
	
	if _OPTIONS["ios"] ~= nil or _ACTION == "android" then
		g_platformType = "Mobile"
	else
		g_platformType = "Desktop"
	end
	
	GenerateSolution(projects)
end

function DeclareCompilationFlags()
	filter {}	-- reset configuration

	flags {
		"MultiProcessorCompile",
		"NoMinimalRebuild",
	}
	
	floatingpoint "Fast"
	
	warnings "Extra"
	
	symbols "On"
	
	characterset "MBCS"
		
	configuration "Debug*"
		defines { "DEBUG" }
		
		optimize "Off"
		
		editandcontinue "On"
		
		rtti "On"
		
		
	configuration "Release*"
		defines { "RELEASE" }
		
		flags {
			"NoIncrementalLink",
			--"FatalWarnings"
		}
		
		exceptionhandling "Off"
		
		optimize "Full"
		
		rtti "Off"
		
		
			-- **** COMMON VS CONFIG **** --
	filter { "action:vs*", "action:not android" }
		system "windows"
		
		ExcludeEverythingBut("Win")
		
		flags { "NoPCH" }
		
		defines {
			"__WIN__",
			--"_CRT_SECURE_NO_DEPRECATE",
			"_CRT_SECURE_NO_WARNINGS",
			--"_CRT_NONSTDC_NO_DEPRECATE",
			"WIN32_LEAN_AND_MEAN"
		}
		
		buildoptions {
			--[["/Gr",]]
			"/Oi",
			"/fp:except-",
			"/sdl"
		}
		
		forceincludes { "Config.h" }
		
		
			-- **** VS RELEASE **** --
	filter { "action:vs*", "action:not android", "Release*" }
		buildoptions {
			"/Ob2",
			"/GL",
		}

		linkoptions { "/LTCG" }



			-- **** COMMON XCODE CONFIG **** --
	filter { "action:xcode4" }
		system "macosx"
		
		pchheader (g_engineDir.."Code/Foundation/Config/Config.h")
		
		xcodebuildsettings
		{
			["CLANG_CXX_LANGUAGE_STANDARD"] = "c++11";
			["CLANG_CXX_LIBRARY"]  = "libc++";
		}
	
		
			-- **** OS X CONFIG **** --
	filter {"options:osx"}
		ExcludeEverythingBut("OSX")
		
		defines { "__OSX__" }
		
		xcodebuildsettings
		{
			["MACOSX_DEPLOYMENT_TARGET"] = "10.7";
			["COMBINE_HIDPI_IMAGES"] = "YES";
		}
		
		
			-- **** iOS CONFIG **** --
	filter {"options:ios"}
		ExcludeEverythingBut("iOS")
		
		defines { "__iOS__" }
		
		xcodebuildsettings
		{
			["ARCHS"] = "$(ARCHS_STANDARD)";
			["IPHONEOS_DEPLOYMENT_TARGET"] = "9.0";
			["SDKROOT"] = "iphoneos";
		}
		
		
			-- **** Android CONFIG **** --
	filter {"action:android"}
		system "vsandroid"
		
		androidapilevel (21)
	
		ExcludeEverythingBut("Android")
		
		flags { "StaticRuntime" }
		
		forceincludes { "Config.h" }	-- should be PCH instead, probably
		
		toolchainversion "Clang 3.8"
		
		cppstandard "c++11"
		stl "gnu"	-- libc++ has bugs :(
		
		configuration "Debug"
			exceptionhandling "UnwindTables"	-- for stack backtrace
			
		filter {"architecture:arm"}
			defines { "__ARM_ARCH_7A__" }
			vectorextensions "NEON"
			instructionmode "arm"
end

function EnableFoundationFlags()
	filter { "action:vs*", "files:**/farmhash.cc" }
		buildoptions { "/wd\"4244\"", "/wd\"4307\"" }
		
	filter { "action:vs*", "files:**/StackBackTrace.cpp" }
		buildoptions { "/wd\"4091\"" }
		
	filter { "action:vs*", "files:**/farmhash.cc" }
		buildoptions { "/wd\"4267\"" }

	filter { "Release*", "files:**/JsonParser.cpp" }
		optimize "Size"
end

function EnableExecutableFlags()
	filter {}	-- reset all configurations\filters

	configuration { "Debug*" }
		targetsuffix ("_d")
	
	configuration { "x64" }
		targetdir ("%{wks.location}/../../Bin64/")
		
	filter {"options:osx"}
		targetdir ("%{wks.location}/../../Bin64/")
		
	configuration { "vs*", "x64" }
		libdirs { "../Code/Dependencies/VLD/lib/Win64" }
		
	filter { "action:vs2015 or vs2017" }
		files { "../Code/StiX Engine.natvis" }
		
	if string.match(_ACTION, "vs*") then
		os.execute("{COPY} ../Code/Dependencies/VLD/bin/Win64/*.* \""..g_workDir.."Bin64\"")
	end
end