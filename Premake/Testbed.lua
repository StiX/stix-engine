
require "PremakeUtil"


-- *****	Testbed PROJECT	***** --
project "Testbed"
	language	"C++"
	kind		"ConsoleApp"
	
	files {
		"../Code/Testbed/**.h",
		"../Code/Testbed/**.cpp",
	}
	
	includedirs {
		"../Code/Foundation",
		"../Code/Foundation/**",
		"../Code/Testbed/**",
	}
	
	dependson { "Foundation" }
	
	links { "Foundation" }
	
	configuration { "vs*" }
		includedirs { "../Code/Dependencies/VLD/include" }
	
	DeclareCompilationFlags()
	EnableFoundationFlags()
	
	EnableExecutableFlags()