
require "PremakeUtil"


-- *****	MODEL CONVERTER PROJECT	***** --
project "ModelConverter"
	language	"C++"
	kind		"ConsoleApp"
	
	files {
		"../Code/Model Converter/**.h",
		"../Code/Model Converter/**.cpp",
		"../Code/Engine/Renderer/VertexDescription.h",
		"../Code/Engine/Renderer/VertexDescription.cpp",
	}
	
	includedirs {
		"../Code/Foundation",
		"../Code/Foundation/**",
		"../Code/Model Converter/",
		"../Code/Model Converter/**",
		"../Code/Engine/Renderer/",
	}
	
	dependson { "Foundation" }
	
	links { "Foundation" }
	
	defines { "FBXSDK_SHARED" }
	
	configuration { "vs*" }
		includedirs {
			"../Code/Dependencies/VLD/include",
			"../Code/Dependencies/FBX/include",
		}
		
		links { "libfbxsdk" }
		
	filter { "action:xcode*" }
		links { "fbxsdk" }
	
		xcodebuildsettings
		{			
			["HEADER_SEARCH_PATHS"] = "../Code/Dependencies/FBX/include";
			["OTHER_CPLUSPLUSFLAGS"] = "-Xclang -isystem-prefix -Xclang fbxsdk/"; -- suppress warnings in FBX SDK headers
		}
	
	DeclareCompilationFlags()
	EnableFoundationFlags()
	EnableExecutableFlags()

	
	configuration { "vs*", "x64", "Debug" }
		libdirs { "../Code/Dependencies/FBX/lib/".."vs2015".."/x64/debug" }
		
	configuration { "vs*", "x64", "Release" }	
		libdirs { "../Code/Dependencies/FBX/lib/".."vs2015".."/x64/release" }
		
	filter { "action:xcode*", "Debug" }
		libdirs { "../Code/Dependencies/FBX/lib/clang/debug" }
		
	filter { "action:xcode*", "Release" }
		libdirs { "../Code/Dependencies/FBX/lib/clang/release" }
	
	if string.match(_ACTION, "vs*") then
		-- TODO: replace by post build step
		os.execute("{COPY} ../Code/Dependencies/FBX/lib/".._ACTION.."/x64/debug/libfbxsdk.dll \""..g_workDir.."Bin64\"")
		--os.execute("{COPY} ../Code/Dependencies/FBX/lib/".._ACTION.."/x64/release/libfbxsdk.dll "..g_workDir.."Bin64\"")
	elseif string.match(_ACTION, "xcode*") then
		 os.execute("{COPY} ../Code/Dependencies/FBX/lib/clang/debug/libfbxsdk.dylib \""..g_workDir.."Bin64\"")
	end