#! /bin/bash

cd "`dirname "$BASH_SOURCE"`"

./premake5 --ios

# close terminal window
osascript -e 'tell application "Terminal" to quit' &
exit