#! /bin/bash

cd "`dirname "$BASH_SOURCE"`"

./premake5 --osx

# close terminal window
osascript -e 'tell application "Terminal" to quit' &
exit